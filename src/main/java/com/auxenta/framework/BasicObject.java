/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.auxenta.framework.core.FWObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.core.IUIElements;
import com.auxenta.framework.elements.DefaultUIElement;
import com.auxenta.framework.elements.DefaultUIElements;
import com.auxenta.framework.locators.IElementLocator;
import com.auxenta.framework.util.UIType;

/**
 * The Class BasicObject.
 *
 * @author Niroshen Landerz
 * @date Aug 30th 2014
 *
 *       An abstract class for dealing with elements and pieces.
 */

public abstract class BasicObject extends FWObject {

	/** The logger. */
	private static Logger logger = Logger.getLogger(BasicObject.class);

	/** The element list. */
	protected List<IUIElement> elementList = new ArrayList<IUIElement>();

	/** The piece list. */
	protected List<BasicPieceObject> pieceList = new ArrayList<BasicPieceObject>();

	/** The current page. */
	private String currentPage;

	/**
	 * Instantiates a new basic object.
	 *
	 * @param driver the driver
	 */
	protected BasicObject(final IUIDriver driver) {
		super(driver);
		currentPage = this.getClass().getSimpleName();
	}

	/**
	 * Returning locator by driver.
	 *
	 * @param uiType the ui type
	 * @param value the value
	 * @return IElementLocator capable of finding desired element.
	 */
	public abstract IElementLocator getLocator(UIType uiType, String value);

	/**
	 * Implementing elements verification for page and piece Printing page/piece
	 * name and go through elements and pieces recursively.
	 */
	public void verifyAllElements() {
		logger.debug("Verifying " + getClass().getName() + " ...");
		verificationElements.add("<<" + getClass().getName() + ">> (" + elementList.size() + ") elements");
		for (IUIElement element : elementList) {
			element.waitToBePresent();
			verificationCount++;
			verificationElements.add(element.getLocator().toString());
		}
		for (BasicPieceObject piece : pieceList) {
			piece.verifyAllElements();
		}
	}

	/**
	 * A method for element definition with element type and value This method
	 * will not instantiate the WebElement or find the HTML element on the page.
	 * It will only create the object that defines the expected location of the
	 * HTML element on the page. It employs lazy instantiation, so the {@code}
	 * WebDriver.findElement(By){@code} is not called until it is needed by a
	 * method such as {@link}UIElement.isPresent(){@link} or {@link}
	 * UIElement.isVisible(){@link};
	 *
	 * @param uiType
	 *            what By method should we use? (For example xpath or CSS)
	 * @param value
	 *            what is the specification to find the element. For example:
	 *            {@code} //div[contains(text(),'MyCourse')]{@code} or {@code}
	 *            div#row1{@code}
	 * @return a UIElement object that contains the information needed to find
	 *         the element later.
	 */
	public IUIElement createElement(final UIType uiType, final String value) {
		return createElement(uiType, value, null, true);
	}

	/**
	 * A method for element definition with element type, value and 'verify'
	 * 'verify' is a flag for verifying the element in method
	 * 'verifyAllElements()'. if it's true, the element will be automatically
	 * verified in the method. if it's false, the element will be skipped.
	 *
	 * @param uiType the ui type
	 * @param value the value
	 * @param verify the verify
	 * @return the IUI element
	 */
	public IUIElement createElement(final UIType uiType, final String value, final boolean verify) {
		return createElement(uiType, value, null, verify);
	}

	/**
	 * Creates the element.
	 *
	 * @param uiType the ui type
	 * @param value the value
	 * @param description the description
	 * @return the IUI element
	 */
	public IUIElement createElement(final UIType uiType, final String value, final String description) {
		return createElement(uiType, value, description, true);
	}

	/**
	 * Creates the element.
	 *
	 * @param uiType the ui type
	 * @param value the value
	 * @param description the description
	 * @param verify the verify
	 * @return the IUI element
	 */
	public IUIElement createElement(final UIType uiType, final String value, final String description, final boolean verify) {
		IElementLocator locator = getLocator(uiType, value);
		IUIElement ele = new DefaultUIElement(uiDriver, locator, description);
		if (verify) {
			elementList.add(ele);
		}
		return ele;
	}

	/**
	 * Creates the elements.
	 *
	 * @param uiType the ui type
	 * @param value the value
	 * @return the IUI elements
	 */
	public IUIElements createElements(final UIType uiType, final String value) {
		IElementLocator locator = getLocator(uiType, value);
		return new DefaultUIElements(uiDriver, locator);
	}

	/**
	 * Adds the piece.
	 *
	 * @param piece the piece
	 * @return the basic object
	 */
	public BasicObject addPiece(final BasicPieceObject piece) {
		if (!piece.isEmpty()) {
			logger.info("Adding piece:" + piece.root.getLocator().toString() + " successfully!");
			pieceList.add(piece);
		}
		return this;
	}

	/**
	 * Sets the current page.
	 *
	 * @param page the new current page
	 */
	public void setCurrentPage(final String page) {
		currentPage = page;
	}

	/**
	 * Gets the current page.
	 *
	 * @return the current page
	 */
	public String getCurrentPage() {
		return currentPage;
	}

}
