/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.listeners;

import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

import com.auxenta.framework.Assert.AuxAssert;
import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.core.FWNoUIDriverObject;

/**
 * This class helps to re execute failed test programmatically .<br>
 *
 * To use it, you need to set {@code retryAnalyzer} element of {@link Test} annotation.<br>
 * Value of {@code retryAnalyzer} element should point to this class <br>
 * Also you must add {@code maxfailedtestretry} property to {@code config/config.cfg} file. This
 * value specifies maximum number of times you want to retry failed test if it fails during retry <br>
 * Here is an example: <br>
 * <br>
 *
 * <code>
 * &#64;Test(retryAnalyzer = RetryFailedTest.class) <br>
 * private void foo()&#123; <br>
 * &nbsp;&nbsp;&nbsp; Assert.fail();<br>
 * &#125;
 * </code> <br>
 * If {@code maxfailedtestretry = 5}, {@code foo()} will be executed 6 times. Ones as its called and
 * then 5 times more as specified by retry logic<br>
 * <br>
 * You can exclude FAILED tests from TestNG report (so that you only see most recent result from the
 * retried test). To do so, use {@link RetryFailedTestListener} to mark FAILED tests as SKIPPED
 *
 * @author Niroshen Landerz
 * @date Aug 30th 2014
 */
public class RetryFailedTest extends FWNoUIDriverObject implements IRetryAnalyzer {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(RetryFailedTest.class);

    /** The count. */
    private int count = 0;

    /** The max count. */
    private int maxCount;

    /**
     * Instantiates a new retry failed test.
     */
    public RetryFailedTest() {
        try {
            this.maxCount = Integer.parseInt(config.getValue(ConfigKeys.KEY_MAX_FAILED_TEST_RETRY
                    .getKey()));
        } catch (NumberFormatException e) {
           logger.warn(
                    "Failed to get key 'maxfailedtestretry' from config.cfg file",
                    e);
           this.maxCount = 0;
        }
    }

    /* (non-Javadoc)
     * @see org.testng.IRetryAnalyzer#retry(org.testng.ITestResult)
     */
    @Override
    public boolean retry(final ITestResult result) {
        if (count < maxCount) {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            for (StackTraceElement elem : stackTraceElements) {
                if (elem.getClassName().contains("RetryFailedTest")) {
                	count++;
                    logger.trace("This method was called by RetryTestListener.class.   Skipped incrementing counter");
                    config.setValue("testFailed", "true");
                    AuxAssert.failierRetry();
                    return true;
                }
            }
            count++;
            logger.info("RETRYING TO RUN " + result.getInstanceName() + "."
            + result.getName() + "(). Retry number " + count);
            config.setValue("testFailed", "true");
            AuxAssert.failierRetry();
            return true;
        }
        return false;
    }
}
