/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.listeners;

/**
 * @author Niroshen Landerz
 * @date Aug 26th 2014
 */
import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import com.auxenta.framework.Assert.AuxAssert;
import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.core.FWObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.drivers.DebugUIDriver;
import com.auxenta.framework.exceptions.FrameworkException;

/**
 * Takes several screenshots if a test fails 1. Uses Java Robot to take a
 * screenshot of the desktop 2. Uses getInstance to get the failing test and
 * uses FWObject to get the UIDriver to capture an image of the failed test
 * specifically 2.a. Captures an image of the browser that failed 2.b. Captures
 * the HTML of the failed page using the test's UIDriver 2.c. Captures HTML from
 * all nested iframes.
 *
 * Links will be included in the TestNG Report based on either a default value
 * designed to work with Jenkins or the
 * <code>"screenShotDirPrefix" ITestContext</code> attribute the
 * <code>BasicTestObject.setDefaultScreenshotLink()<code> value
 * the <code>@Parameter() that has a value of "screenShotDirPrefix:path"</code>
 *
 * @see CapturePageOnFailureEvent
 */
public class CapturePageOnFailureListener extends TestListenerAdapter {

	/**
	 * Gets the default screen shot path.
	 *
	 * @return the default screen shot path
	 */
	private static String getDefaultScreenShotPath() {
		return config.getValue(ConfigKeys.KEY_SCREENSHOT_PATH.getKey());
	}

	/** The config. */
	protected static IConfig config = DefaultConfig.getConfig();

	/** The Constant SCREENSHOT_ATTRIBUTE. */
	public static final String SCREENSHOT_ATTRIBUTE = "screenShotDirPrefix";

	/** The Constant DIR_PREFIX_PARAM. */
	private static final String DIR_PREFIX_PARAM = SCREENSHOT_ATTRIBUTE + ":";

	/** The Constant DRIVER_URL_ATTRIBUTE. */
	public static final String DRIVER_URL_ATTRIBUTE = "driverUrl";

	/** The logger. */
	private Logger logger = Logger.getLogger(this.getClass());

	/** The quit_driver_on_failure. */
	boolean quit_driver_on_failure;

	/** The Constant DEFAULT_SCREENSHOT_DIR_PREFIX. */
	private static final String DEFAULT_SCREENSHOT_DIR_PREFIX = getDefaultScreenShotPath();

	/**
	 * Adds the link to report.
	 *
	 * @param dirPrefix the dir prefix
	 * @param file the file
	 */
	private void addLinkToReport(final String dirPrefix, final File file) {
		if (file != null) {
			Reporter.log("<a class='eSeScreenshot' href=\""
					+ getRelativeHref(dirPrefix, file)
					+ "\" target=\"_blank\">" + file.getName() + "</a>");
			Reporter.log("<br>");

		}
	}

	/**
	 * Adds the sauce labs link to report.
	 *
	 * @param remoteDriver the remote driver
	 */
	private void addSauceLabsLinkToReport(final RemoteWebDriver remoteDriver) {
		Reporter.log("<script type=\"text/javascript\" src=\"https://saucelabs.com/job-embed/"
				+ remoteDriver.getSessionId().toString() + "></script>");
	}

	/**
	 * Adds the thumbnails to report.
	 *
	 * @param dirPrefix the dir prefix
	 * @param files the files
	 */
	private void addThumbnailsToReport(final String dirPrefix, final File... files) {
		for (File file : files) {
			if (file != null) {
				if (file.getName().endsWith("html")) {
					addLinkToReport(dirPrefix, file);
				} else {
					addThumbnailToReport(dirPrefix, file);
				}

			}
		}
	}

	/**
	 * Adds the thumbnail to report.
	 *
	 * @param dirPrefix the dir prefix
	 * @param file the file
	 */
	private void addThumbnailToReport(final String dirPrefix, final File file) {
		if (file != null) {
			Reporter.log("<a class='eSeScreenshot' href=\""
					+ getRelativeHref(dirPrefix, file)
					+ "\" target=\"_blank\">" + "<img width='200' src='"
					+ getRelativeHref(dirPrefix, file) + "' /></a>");

		}
	}

	/**
	 * Sometimes your results report is included in a web page that is not in
	 * the same directory as your screenshots. When that happens you can
	 * configure a prefix for the report links that will direct the browser to
	 * the correct directory by setting a prefix on the default link to be used
	 * when including screenshots in the TestNG report.
	 *
	 * For example, to have a link to
	 * "../artifacts/screenShots/20131015_firstTest.png" call this method with
	 * the parameter "../artifacts".
	 *
	 * This method will first look at 1. A default value designed to work with
	 * Jenkins or the <code>"screenShotDirPrefix" ITestContext</code> attribute
	 * the <code>BasicTestObject.setDefaultScreenshotLink()<code> value
	 * the <code>@Parameter() that has a value of "screenShotDirPrefix:path"</code>
	 *
	 * @param testResult            ITestResult object to extract attributes and parameters from.
	 * @return the default link path
	 */
	private String getDefaultLinkPath(final ITestResult testResult) {
		String dirPrefix = DEFAULT_SCREENSHOT_DIR_PREFIX;
		try {
			String dirAttrib = (String) testResult
					.getAttribute(SCREENSHOT_ATTRIBUTE);
			if (dirAttrib != null) {
				dirPrefix = dirAttrib;
			}
		} catch (ClassCastException cce) {
			logger.error("Unable to get attribute " + SCREENSHOT_ATTRIBUTE
					+ ", using default" + dirPrefix + ". "
					+ cce.getLocalizedMessage(), cce);
			throw new FrameworkException("Unable to get attribute " + SCREENSHOT_ATTRIBUTE, cce);
		}
		Object[] parameters = testResult.getParameters();
		logger.log(Level.DEBUG, "CapturePageOnFailure found "
				+ parameters.length + " parameters ");
		for (Object param : parameters) {
			if (param instanceof String) {
				String paramStr = (String) param;
				logger.log(Level.DEBUG,
						"CapturePageOnFailure found string parameter " + param);
				if (paramStr.startsWith(DIR_PREFIX_PARAM)) {
					logger.log(Level.INFO, "CapturePageOnFailure found "
							+ DIR_PREFIX_PARAM + " parameter " + param);
					dirPrefix = paramStr.substring(paramStr
							.indexOf(DIR_PREFIX_PARAM)
							+ DIR_PREFIX_PARAM.length());
				} else {
					logger.log(Level.INFO, "CapturePageOnFailure found "
							+ DIR_PREFIX_PARAM + " parameter " + param);

				}
				logger.log(Level.INFO, "CapturePageOnFailure will prepend "
						+ dirPrefix + " to screenShot directory links");
			}

		}
		return dirPrefix;
	}

	/**
	 * Gets the driver url.
	 *
	 * @param testResult the test result
	 * @return the driver url
	 */
	private String getDriverUrl(final ITestResult testResult) {
		String driverUrl;
		try {
			driverUrl = (String) testResult.getAttribute(DRIVER_URL_ATTRIBUTE);
			if (driverUrl != null) {
				return driverUrl;
			}
		} catch (ClassCastException cce) {
			logger.warn("Unable to get attribute " + DRIVER_URL_ATTRIBUTE
					+ ", using default 'localhost'. "
					+ cce.getLocalizedMessage());
			throw new FrameworkException("Unable to get attribute " + SCREENSHOT_ATTRIBUTE, cce);
		}
		return "localhost";
	}

	/**
	 * Gets the relative href.
	 *
	 * @param dirPrefix the dir prefix
	 * @param file the file
	 * @return the relative href
	 */
	private String getRelativeHref(final String dirPrefix, final File file) {
		if (file != null) {
			return dirPrefix + "screenShots/" + file.getName();
		} else {
			return "";
		}

	}

	/**
	 * User will not be able to see links to screenshots taken from
	 * configuration methods.
	 *
	 * @param itr the itr
	 * @see {@link https
	 *      ://groups.google.com/forum/?fromgroups=#!topic/testng-users
	 *      /8ncZt1ga7Bs}
	 */
	@Override
	public void onConfigurationFailure(final ITestResult itr) {
		this.onTestFailure(itr);
	}

	/* (non-Javadoc)
	 * @see org.testng.TestListenerAdapter#onTestFailure(org.testng.ITestResult)
	 */
	@Override
	public void onTestFailure(final ITestResult testResult) {
		logger.trace("CapturePageOnFailureListener.onTestFailure()");

		AuxAssert.clearAllAsserResults();

		if (config.getValue("quitdriveronfailure") == null) {
			logger.info("quitdriveronfailure is not in config, setting to TRUE");
			quit_driver_on_failure = true;
		} else if (config.getValue("quitdriveronfailure").toUpperCase()
				.equals("FALSE")) {
			logger.info("quitdriveronfailure is set to FALSE");
			quit_driver_on_failure = false;
		} else {
			logger.info("quitdriveronfailure is set to TRUE");
			quit_driver_on_failure = true;
		}

		String dirPrefix = getDefaultLinkPath(testResult);
		if (testResult.getInstance() instanceof FWObject) {
			logger.log(Level.TRACE, "CapturePageOnFailure got framework object");
			FWObject ob = (FWObject) testResult.getInstance();
			IUIDriver uiDriver = ob.getUIDriver();
			logger.info("Capturing Javascript Console greater than warnings.");
			if (!(uiDriver.getWebDriver() instanceof InternetExplorerDriver)) {
				for (LogEntry entry : uiDriver
						.getConsoleLog(java.util.logging.Level.SEVERE)) {
					String consoleLogEntry = "Console Log: " + entry.getLevel()
							+ ":" + entry.getMessage();
					logger.warn(consoleLogEntry);
					Reporter.log(consoleLogEntry);
				}
			}
			DebugUIDriver debugDriver = new DebugUIDriver(
					testResult.getTestContext(), uiDriver);
			File browserScreenshotDest = null;
			File hTMLDest = null;
			String suffix = testResult.getName() + testResult.getStatus();
			try {
				try {
					
					hTMLDest = debugDriver.captureHTML(suffix);
				} catch (Exception e1) {
					logger.error("There was some error taking the browser HTML but not thrown the error as it should not stop the execution" + e1.getMessage());
				}
				if (uiDriver != null) {
					logger.debug("uiDriver is not null.");
					WebDriver driver = uiDriver.getWebDriver();
					String driverUrl = getDriverUrl(testResult);
					if (driver instanceof RemoteWebDriver
							&& driverUrl.contains("http")) {
						logger.debug("debugDriver is Remote. driverUrl="
								+ driverUrl);
						if (driverUrl.contains("sauce")) {
							logger.debug("debugDriver is remote on SauceLabs");
							addSauceLabsLinkToReport((RemoteWebDriver) driver);
						} else {
							logger.debug("debugDriver is remote NOT on SauceLabs");
							browserScreenshotDest = debugDriver
									.captureRemoteBrowser(suffix + "_remote");
							addThumbnailsToReport(dirPrefix,
									browserScreenshotDest,
									browserScreenshotDest, hTMLDest);
						}
					} else {
						logger.debug("debugDriver is local");
						File desctopImageDest = debugDriver
								.captureDesktop(suffix + "_desktop");
						browserScreenshotDest = debugDriver
								.captureBrowser(suffix + "_browser");
						addThumbnailsToReport(dirPrefix, desctopImageDest,
								browserScreenshotDest, hTMLDest);
					}
				}
			} catch (Exception e) {
				logger.error("Exception trying to take screenshot", e);
				throw new FrameworkException("Exception trying to take screenshot ", e);
			}
			try {
				if (quit_driver_on_failure) {
					debugDriver.quit();
				}
			} catch (Exception e) {
				logger.error(
						"Exception trying to quit browser after taking screenshot: ",
						e);
				throw new FrameworkException("Exception trying to quit browser after "
						+ "taking screenshot ", e);
			}
		} else {
			logger.log(Level.DEBUG, "CapturePageOnFailure with no FWOBject");
			try {
				DebugUIDriver debugDriver = new DebugUIDriver(
						testResult.getTestContext(), new HtmlUnitDriver());
				File desctopImageDest = debugDriver.captureDesktop("Unknown_"
						+ "_desktop");
				addThumbnailsToReport(dirPrefix, desctopImageDest);

				if (quit_driver_on_failure) {
					debugDriver.quit();
				}
			} catch (Exception wde) {
				logger.error("Unable to capture HTML from page due to exception: "
						+ wde.getLocalizedMessage(), wde);
				throw new FrameworkException("Unable to capture HTML from page due to exception ", wde);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.testng.TestListenerAdapter#onTestSkipped(org.testng.ITestResult)
	 
	@Override
	public void onTestSkipped(final ITestResult tr) {
		logger.trace("CapturePageOnFailureListener.onTestSkipped()");
		IRetryAnalyzer retryAnalyzer = tr.getMethod().getRetryAnalyzer();
		if (retryAnalyzer == null
				|| !(retryAnalyzer instanceof RetryFailedTest)) {
			super.onTestSkipped(tr);
			return;
		} else {
			
			 // If this method was marked as SKIPPED by RetryFailedTestListener, take a screenshot of a failure
			 
			this.onTestFailure(tr);
		}
		
		*/
		
		@Override
		public void onTestSkipped(final ITestResult tr) {
			super.onTestSkipped(tr);
			return;
		}
}
