/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.locators;

import java.util.List;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

/**
 * A type of element locator which locates element by itself.
 *
 * @author Niroshen Landerz
 * @date Sept 1st 2014
 */

public class ElementLocatorBySelf extends AbstractElementLocator {

	/** The self. */
	private WebElement self;

	/** The selves. */
	private List<WebElement> selves;

	/**
	 * Instantiates a new element locator by self.
	 *
	 * @param self the selfVal
	 */
	public ElementLocatorBySelf(final WebElement selfVal) {
		super(null);
		this.self = selfVal;
	}

	/**
	 * Instantiates a new element locator by self.
	 *
	 * @param selves the selvesVal
	 */
	public ElementLocatorBySelf(final List<WebElement> selvesVal) {
		super(null);
		this.selves = selvesVal;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.locators.IElementLocator#locateElement()
	 */
	@Override
	public WebElement locateElement() {
		try {
			self.isDisplayed();
			return self;
		} catch (StaleElementReferenceException ex) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.locators.IElementLocator#locateElements()
	 */
	@Override
	public List<WebElement> locateElements() {
		return selves;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (selves != null ? "s" : "");
	}
}
