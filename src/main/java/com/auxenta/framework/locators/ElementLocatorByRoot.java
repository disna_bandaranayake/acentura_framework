/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.locators;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.util.ByUtil;

/**
 * A type of element locator which locates element by root.
 *
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */

public class ElementLocatorByRoot extends AbstractElementLocator {

	/** The logger. */
	private static Logger logger = Logger.getLogger(ElementLocatorByRoot.class);

	/** The root. */
	private IUIElement root;

	/**
	 * Instantiates a new element locator by root.
	 *
	 * @param root the root
	 * @param by the by
	 */
	public ElementLocatorByRoot(final IUIElement rootVal, final By by) {
		super(by);
		this.root = rootVal;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.locators.IElementLocator#locateElement()
	 */
	@Override
	public WebElement locateElement() {
		if (root != null && by != null) {
			try {
				return root.findElement(by);
			} catch (NoSuchElementException ex) {
				logger.error("As designed, if element is not found, it is safe to ignore this NoSuchElementException" + ex.getMessage());
			} catch (StaleElementReferenceException ex) {
				logger.error("As designed, if element is not found, it is safe to ignore this StaleElementReferenceException" + ex.getMessage());
			} catch (WebDriverException ex) {
				logger.error("As designed, if element is not found, it is safe to ignore this WebDriverException" + ex.getMessage());
			}
		}
		return null;
	}

	/**
	 * Locate elements.
	 *
	 * @return the list
	 * @see com.auxent.test.auxtest.framework.locators#locateElements().
	 */
	@Override
	public List<WebElement> locateElements() {
		if (root != null && by != null) {
			List<WebElement> elements = root.findElements(by);
			if (elements != null && elements.size() > 0) {
				return elements;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ByUtil.getByStr(by);
	}
}
