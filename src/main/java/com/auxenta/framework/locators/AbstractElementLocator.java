/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.locators;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import com.auxenta.framework.util.ByUtil;
import com.auxenta.framework.util.UIType;

/**
 * An abstract class for elementlocator.
 *
 * @author Niroshen Landerz
 * @date - Aug 29th 2014
 */

public abstract class AbstractElementLocator implements IElementLocator {

	/** The by. */
	protected By by;

	/** The original value. */
	protected String originalValue;

	/** The replace values. */
	protected List<String> replaceValues = new ArrayList<String>();

	/**
	 * Instantiates a new abstract element locator.
	 *
	 * @param by the by
	 */
	public AbstractElementLocator(final By by) {
		this.by = by;
		originalValue = ByUtil.getByValue(by);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.locators.IElementLocator#replaceValues(java.lang.String[])
	 */
	@Override
	public void replaceValues(final String... values) {
		replaceValues.clear();
		UIType uiType = ByUtil.getUITypeBy(by);
		String eleValue = originalValue;
		for (int i = 0; i < values.length; i++) {
			String value = values[i];
			eleValue = eleValue.replace("{" + (i + 1) + "}", value);
			replaceValues.add(value);
		}
		by = ByUtil.getBy(uiType, eleValue);
	}
}
