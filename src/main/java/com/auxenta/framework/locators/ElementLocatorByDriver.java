/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.locators;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.util.ByUtil;

/**
 * A type of element locator which locates element by driver.
 *
 * @author Niroshen Landerz
 * @date Aug 27th 2014
 */

public class ElementLocatorByDriver extends AbstractElementLocator {

	/** The logger. */
	private static Logger logger = Logger.getLogger(ElementLocatorByDriver.class);

	/** The ui driver. */
	private IUIDriver uiDriver;

	/**
	 * Instantiates a new element locator by driver.
	 *
	 * @param driver the driver
	 * @param by the by
	 */
	public ElementLocatorByDriver(final IUIDriver driver, final By by) {
		super(by);
		this.uiDriver = driver;
	}
	
	/**
	 * Locate element.
	 *
	 * @return the web element
	 * @see com.auxenta.test.auxtest.framework.locators#locateElement().
	 */
	@Override
	public WebElement locateElement() {
		if (uiDriver != null && by != null) {
			try {
				return uiDriver.findElement(by);
			} catch (NoSuchElementException ex) {
				logger.info("As designed, if element is not found, it is safe to ignore this NoSuchElementException" + ex.getMessage());
			} catch (StaleElementReferenceException ex) {
				logger.info("As designed, if element is not found, it is safe to ignore this StaleElementReferenceException" + ex.getMessage());
			}
		}
		return null;
	}

	/**
	 * Locate elements.
	 *
	 * @return the list
	 * @see com.auxenta.test.auxtest.framework.locators#locateElements().
	 */
	@Override
	public List<WebElement> locateElements() {
		if (uiDriver != null && by != null) {
			List<WebElement> elements = uiDriver.findElements(by);
			if (elements != null && elements.size() > 0) {
				return elements;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ByUtil.getByStr(by);
	}
}
