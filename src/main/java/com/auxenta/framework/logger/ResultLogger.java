package com.auxenta.framework.logger;

import org.apache.log4j.Logger;

public class ResultLogger {

	/** The log info. */
		
	private static final Logger logger = Logger
			.getLogger(ResultLogger.class);

	/**
	 * Log to passed.
	 * 
	 * @param message
	 *            the message
	 */
	public void logResult(String message) {

		//logInfo.println("\n" + message);
		logger.info(message);
	}

}
