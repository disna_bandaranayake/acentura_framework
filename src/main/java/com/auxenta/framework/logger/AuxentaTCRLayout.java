/*
 *================================================================
 * Copyright  (c)     : 2015 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.logger;

import java.util.Date;

import org.apache.log4j.Layout;
import org.apache.log4j.helpers.Transform;
import org.apache.log4j.spi.LoggingEvent;

/**
 * The Class AuxentaTCRLayout.
 */
public class AuxentaTCRLayout extends Layout {

	/** The buf size. */
	protected final int BUF_SIZE = 256;

	/** The max capacity. */
	protected final int MAX_CAPACITY = 1024;

	/** The trace prefix. */
	static String TRACE_PREFIX = "<br>&nbsp;&nbsp;&nbsp;&nbsp;";

	/** The sbuf. */
	private StringBuffer sbuf = new StringBuffer(256);

	/**
	 * The Constant LOCATION_INFO_OPTION.
	 * 
	 * @deprecated
	 */
	public static final String LOCATION_INFO_OPTION = "LocationInfo";

	/** The Constant TITLE_OPTION. */
	public static final String TITLE_OPTION = "Title";

	/** The location info. */
	boolean locationInfo = false;

	/** The title. */
	String title = "Log4J Log Messages";

	/**
	 * Instantiates a new auxenta tcr layout.
	 */
	public AuxentaTCRLayout() {
	}

	/**
	 * Sets the location info.
	 * 
	 * @param flag
	 *            the new location info
	 */
	public void setLocationInfo(boolean flag) {
		this.locationInfo = flag;
	}

	/**
	 * Gets the location info.
	 * 
	 * @return the location info
	 */
	public boolean getLocationInfo() {
		return this.locationInfo;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Layout#getContentType()
	 */
	public String getContentType() {
		return "text/html";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.spi.OptionHandler#activateOptions()
	 */
	public void activateOptions() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Layout#format(org.apache.log4j.spi.LoggingEvent)
	 */
	public String format(LoggingEvent event) {
		if (this.sbuf.capacity() > 1024) {
			this.sbuf = new StringBuffer(256);
		} else {
			this.sbuf.setLength(0);
		}

		this.sbuf.append(Layout.LINE_SEP + "<tr>" + Layout.LINE_SEP);

		this.sbuf.append("<td width=\"15%\" style=\"vertical-align:top\">");
		this.sbuf.append(new Date());
		this.sbuf.append("</td>" + Layout.LINE_SEP);

		String result = event.getRenderedMessage();
		String[] resultArray = result.split("\n ");
		String[] arrayForTitle = resultArray[0].split(" : ");

		result = result.replaceAll("\n PASSED", "</span><br/><span style=\"color:green\" >");
		result = result.replaceAll("\n FAILED", "</span><br/><span style=\"color:red;font-weight: bold\" >");

		if (result.contains("FAILED")) {

			this.sbuf.append("<td width=\"25%\" title=\"Test Case Name\" style=\"color:red;vertical-align:top;font-weight: bold\">");
			this.sbuf.append(arrayForTitle[1]);
			this.sbuf.append("</td>" + Layout.LINE_SEP);

		} else {

			this.sbuf.append("<td width=\"25%\" title=\"Test Case Name\" style=\"color:green;vertical-align:top\">");
			this.sbuf.append(arrayForTitle[1]);
			this.sbuf.append("</td>" + Layout.LINE_SEP);

		}

		this.sbuf.append("<td title=\"Result\" style=\"color:red;font-weight: bold\">");
		this.sbuf.append("<span style=\"color:blue;\">" + result + "</span>");
		this.sbuf.append("</td>" + Layout.LINE_SEP);

		this.sbuf.append("</tr>" + Layout.LINE_SEP);

		if (event.getNDC() != null) {
			this.sbuf.append("<tr><td bgcolor=\"#EEEEEE\" style=\"font-size : xx-small;\" colspan=\"6\" title=\"Nested Diagnostic Context\">");
			this.sbuf.append("NDC: " + Transform.escapeTags(event.getNDC()));
			this.sbuf.append("</td></tr>" + Layout.LINE_SEP);
		}

		String[] s = event.getThrowableStrRep();
		if (s != null) {
			this.sbuf.append("<tr><td bgcolor=\"#993300\" style=\"color:White; font-size : xx-small;\" colspan=\"6\">");
			appendThrowableAsHTML(s, this.sbuf);
			this.sbuf.append("</td></tr>" + Layout.LINE_SEP);
		}

		return this.sbuf.toString();
	}

	/**
	 * Append throwable as html.
	 * 
	 * @param s
	 *            the s
	 * @param sbuf
	 *            the sbuf
	 */
	void appendThrowableAsHTML(String[] s, StringBuffer sbuf) {
		if (s != null) {
			int len = s.length;
			if (len == 0)
				return;
			sbuf.append(Transform.escapeTags(s[0]));
			sbuf.append(Layout.LINE_SEP);
			for (int i = 1; i < len; i++) {
				sbuf.append(TRACE_PREFIX);
				sbuf.append(Transform.escapeTags(s[i]));
				sbuf.append(Layout.LINE_SEP);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Layout#getHeader()
	 */
	public String getHeader() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
				+ Layout.LINE_SEP);
		sbuf.append("<html>" + Layout.LINE_SEP);
		sbuf.append("<head>" + Layout.LINE_SEP);
		sbuf.append("<title>" + this.title + "</title>" + Layout.LINE_SEP);
		sbuf.append("<style type=\"text/css\">" + Layout.LINE_SEP);
		sbuf.append("<!--" + Layout.LINE_SEP);
		sbuf.append("body, table {font-family: arial,sans-serif; font-size: x-small;}" + Layout.LINE_SEP);
		sbuf.append("th {background: #336699; color: #FFFFFF; text-align: left;}" + Layout.LINE_SEP);
		sbuf.append("-->" + Layout.LINE_SEP);
		sbuf.append("</style>" + Layout.LINE_SEP);
		sbuf.append("</head>" + Layout.LINE_SEP);
		sbuf.append("<body bgcolor=\"#FFFFFF\" topmargin=\"6\" leftmargin=\"6\">" + Layout.LINE_SEP);
		sbuf.append("<hr size=\"1\" noshade>" + Layout.LINE_SEP);
		sbuf.append("Log session start time " + new Date() + "<br>" + Layout.LINE_SEP);
		sbuf.append("<br>" + Layout.LINE_SEP);
		sbuf.append("<table cellspacing=\"0\" cellpadding=\"4\" border=\"1\" bordercolor=\"#224466\" width=\"100%\">"
				+ Layout.LINE_SEP);
		sbuf.append("<tr>" + Layout.LINE_SEP);
		sbuf.append("<th>Time</th>" + Layout.LINE_SEP);
		sbuf.append("<th>Test Case</th>" + Layout.LINE_SEP);
		sbuf.append("<th>Test Case Result</th>" + Layout.LINE_SEP);
		sbuf.append("</tr>" + Layout.LINE_SEP);
		return sbuf.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Layout#getFooter()
	 */
	public String getFooter() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append("</table>" + Layout.LINE_SEP);
		sbuf.append("<br>" + Layout.LINE_SEP);
		sbuf.append("<p> � <a href='www.auxenta.com'>Auxenta Inc</a> 2015. <p> </body></html>");
		return sbuf.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.log4j.Layout#ignoresThrowable()
	 */
	public boolean ignoresThrowable() {
		return false;
	}
}