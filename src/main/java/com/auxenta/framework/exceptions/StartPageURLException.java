/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for start page url.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public class StartPageURLException extends FrameworkException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6257321054975424313L;
    
    /** The ex message. */
    private static String exMessage = "Could not find startpage: ";

    /**
     * Instantiates a new start page url exception.
     *
     * @param page the page
     */
    public StartPageURLException(final String page) {
        super(exMessage + page);
    }

    /**
     * Instantiates a new start page url exception.
     *
     * @param page the page
     * @param cause the cause
     */
    public StartPageURLException(final String page, final Throwable cause) {
        super(exMessage + page, cause);
    }
}