/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for start page object class.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */

public class StartPageClassException extends FrameworkException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1467632135456464355L;
    
    /** The ex message. */
    private static String exMessage = "Could not find startpage: ";

    /**
     * Instantiates a new start page class exception.
     *
     * @param page the page
     */
    public StartPageClassException(final String page) {
        super(exMessage + page);
    }

    /**
     * Instantiates a new start page class exception.
     *
     * @param page the page
     * @param cause the cause
     */
    public StartPageClassException(final String page, final Throwable cause) {
        super(exMessage + page, cause);
    }
}
