/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for verifying element.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public class UIElementVerificationException extends FrameworkException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1325584287135124845L;
    
    /** The ex message. */
    private static String exMessage = "Verification exception for element: ";

    /**
     * Instantiates a new UI element verification exception.
     *
     * @param elementBy the element by
     */
    public UIElementVerificationException(String elementBy) {
        super(exMessage + elementBy);
    }

    /**
     * Instantiates a new UI element verification exception.
     *
     * @param elementBy the element by
     * @param ms the ms
     */
    public UIElementVerificationException(String elementBy, long ms) {
        super(exMessage + elementBy + " in " + ms + " milliseconds");
    }
}
