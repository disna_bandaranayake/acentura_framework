/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for invalid ui driver.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */

public class InvalidUIDriverException extends FrameworkException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4983213218412168798L;
    
    /** The ex message. */
    private static String exMessage = "Invalid UIDriver";

    /**
     * Instantiates a new invalid ui driver exception.
     */
    public InvalidUIDriverException() {
        super(exMessage + "!");
    }

    /**
     * Instantiates a new invalid ui driver exception.
     *
     * @param path the path
     */
    public InvalidUIDriverException(final String path) {
        super(exMessage + " under path: " + path);
    }
}