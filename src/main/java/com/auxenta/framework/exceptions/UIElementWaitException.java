/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for waiting element timeout.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public class UIElementWaitException extends FrameworkException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5679146487498461464L;
    
    /** The ex message. */
    private static String exMessage = "Wait exception for element: ";

    /**
     * Instantiates a new UI element wait exception.
     *
     * @param elementBy the element by
     */
    public UIElementWaitException(final String elementBy) {
        super(exMessage + elementBy);
    }

    /**
     * Instantiates a new UI element wait exception.
     *
     * @param elementBy the element by
     * @param ms the ms
     */
    public UIElementWaitException(final String elementBy, final long ms) {
        super(exMessage + elementBy + " in " + ms + " milliseconds");
    }
}
