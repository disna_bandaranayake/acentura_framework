/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * TestlinkVerificationException - Generated when we get an exception when verifying the testlink data in
 * the configuration file.
 *
 * @author Niroshen Landerz
 * @date Sept 5th 2014
 */
public class TestlinkVerificationException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5807944209082201065L;
    
    /** The ex message. */
    private static String exMessage = "TestLink Verification exception: ";

    /**
     * Constructor for when we generate the exception not as part of another exception.
     *
     * @param message the message
     */
    public TestlinkVerificationException(final String message) {
        super(exMessage+ " "+message);
    }

    /**
     * COnstructor for both message and exception.
     *
     * @param message the message
     * @param exception the exception
     */
    public TestlinkVerificationException(final String message, final Throwable exception) {
        super(exMessage + " "+message, exception);
    }


}
