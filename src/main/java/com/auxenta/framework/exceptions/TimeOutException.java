/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * The Class TimeOutException.
 *
 * @author Niroshen Landerz
 * @date Aug 24th 2014
 */
public class TimeOutException extends UIElementVerificationException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5635982300627046789L;
    
    /** The ex message. */
    private static String exMessage = "Timed out: ";

    /**
     * Instantiates a new time out exception.
     *
     * @param message the message
     */
    public TimeOutException(final String message) {
        super(exMessage + message);
    }

    /**
     * Instantiates a new time out exception.
     *
     * @param message the message
     * @param ms the ms
     */
    public TimeOutException(final String message, final long ms) {
        super(exMessage + message + " in " + ms + " milliseconds");
    }
}
