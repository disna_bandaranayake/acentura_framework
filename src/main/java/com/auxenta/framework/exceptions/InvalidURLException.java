/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for invalid url.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */
public class InvalidURLException extends FrameworkException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6798465413541526565L;
    
    /** The ex message. */
    private static String exMessage = "exception for invalid url:";

    /**
     * Instantiates a new invalid url exception.
     *
     * @param url the url
     */
    public InvalidURLException(final String url) {
        super(exMessage + url);
    }

    /**
     * Instantiates a new invalid url exception.
     *
     * @param url the url
     * @param cause the cause
     */
    public InvalidURLException(final String url, final Throwable cause) {
        super(exMessage + url, cause);
    }
}
