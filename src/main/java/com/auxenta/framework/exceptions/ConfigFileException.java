/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

/**
 * An Exception for invalid config file.
 *
 * @author Niroshen Landerz
 * @date Aug 24th 2014
 */

public class ConfigFileException extends FrameworkException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6287456143256425934L;

	/** The ex message. */
	private static String exMessage = "Config file exception: ";

	/**
	 * Instantiates a new config file exception.
	 *
	 * @param path
	 *            the path
	 */
	public ConfigFileException(final String path) {
		super(exMessage + path);
	}

	/**
	 * Instantiates a new config file exception.
	 *
	 * @param path
	 *            the path
	 * @param cause
	 *            the cause
	 */
	public ConfigFileException(final String path, final Throwable cause) {
		super(exMessage + path, cause);
	}
}
