/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exceptions;

import org.apache.log4j.Logger;

/**
 * The Class FrameworkException.
 *
 * @author Niroshen Landerz
 * @date Sept 1st 2014
 */

public class FrameworkException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7837726868693732934L;
    
    /** The logger. */
    private static Logger logger = Logger.getLogger(FrameworkException.class);

    /**
     * Instantiates a new framework exception.
     *
     * @param message the message
     */
    public FrameworkException(final String message) {
        super(message);
        logger.error(message);
    }

    /**
     * Instantiates a new framework exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public FrameworkException(final String message, final Throwable cause) {
        super(message, cause);
        logger.error(message, cause);
    }
}
