/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.util;

/**
 * An enum for holding all default variables for auxtest framework.
 *
 * @author Niroshen Landerz
 * @date - August 31st 2014
 */

public enum Default {

	/** The config path. */
	CONFIG_PATH("/config/config.cfg"),
	/** The ui type. */
	UI_TYPE("xpath"),
	/** The driver url. */
	DRIVER_URL("localhost"),
	/** The driver host. */
	DRIVER_HOST("localhost"),
	/** The driver port. */
	DRIVER_PORT("4567"),
	/** The start page. */
	START_PAGE("http://172.26.30.46:8888/CRMOrder/faces/megatron/MegatronDashBoard.jspx?"),
	/** The driver browser. */
	DRIVER_BROWSER("firefox"),
	/** The os. */
	OS("Windows 7");
	/** The config var depth. */
	public static int CONFIG_VAR_DEPTH = 3;

	/** The value. */
	private String value;

	/**
	 * Instantiates a new default.
	 *
	 * @param value
	 *            the value
	 */
	private Default(final String val) {
		this.value = val;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}
}
