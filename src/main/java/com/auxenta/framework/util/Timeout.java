/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.util;

/**
 * The Enum Timeout.
 *
 * @author Niroshen Landerz
 * @date Sept 1st 2014
 */
public enum Timeout {

	/** The wait timeout. */
	WAIT_TIMEOUT(60000),

	/** The verify timeout. */
	VERIFY_TIMEOUT(60000),

	/** The verify interval. */
	VERIFY_INTERVAL(0),

	/** The element timeout. */
	ELEMENT_TIMEOUT(3000),

	/** The elements timeout. */
	ELEMENTS_TIMEOUT(3000),

	/** The pageload timeout. */
	PAGELOAD_TIMEOUT(0),

	/** The sleep half sec. */
	SLEEP_HALF_SEC(500),

	/** The SLEE p_1_ sec. */
	SLEEP_1_SEC(1000),

	/** The SLEE p_2_ sec. */
	SLEEP_2_SEC(2000),

	/** The SLEE p_5_ sec. */
	SLEEP_5_SEC(5000),

	/** The SLEE p_10_ sec. */
	SLEEP_10_SEC(10000),

	/** The SLEE p_20_ sec. */
	SLEEP_20_SEC(20000),

	/** The SLEE p_30_ sec. */
	SLEEP_30_SEC(30000),

	/** The SLEE p_1_ min. */
	SLEEP_1_MIN(60000),

	/** The SLEE p_2_ min. */
	SLEEP_2_MIN(120000),

	/** The SLEE p_5_ min. */
	SLEEP_5_MIN(300000);

	/** The value. */
	long value;

	/**
	 * Instantiates a new timeout.
	 * 
	 * @param timeout
	 *            the timeout
	 */
	private Timeout(final long timeout) {
		this.value = timeout;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public long getValue() {
		return this.value;
	}
}
