/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.util;

/**
 * The Enum SearchScope.
 *
 * @author Niroshen Landerz
 * @date - August 31st 2014
 */
public enum SearchScope {

	/** The equals. */
	EQUALS,
	/** The contains. */
	CONTAINS;
}
