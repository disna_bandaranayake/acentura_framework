/*
 *================================================================
 * Copyright  (c)    : 2015 Dialog Axiata PLC, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.util;

import java.util.Hashtable;

import org.testng.ITestNGMethod;
import org.testng.annotations.DataProvider;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.tools.exlfile.ExcelFileReader;

/**
 * Data Provider class for TestNG test cases.
 *
 * @author Niroshen Landerz
 * @version 1.0
 */
public class GenericDataProvider {

	/** Data Provider for Excel based data. */

	protected static IConfig config = DefaultConfig.getConfig();
	
	/** The excelfilepath. */
	public static String EXCELFILEPATH = config.getValue(ConfigKeys.PROJECT_BASE_DIR.getKey().trim()) + config.getValue(ConfigKeys.KEY_DATA_WORKBOOK_PATH.getKey().trim());

	/**
	 * DTV post paid discount.
	 *
	 * @param context the context
	 * @return the object[][]
	 * @throws Exception the exception
	 */
	@DataProvider
	public static Object[][] DTVPostPaidDiscount(ITestNGMethod context) throws Exception {
		Object[][] data = null;

		String SheetName = "Sheet1";
		String filepath = EXCELFILEPATH + "StandaradDTVPostPaidFullPackDiscount.xls";

		data = callMethod(filepath, SheetName);
		return data;
	}
	
	/**
	 * DTV post paid discount tin.
	 *
	 * @param context the context
	 * @return the object[][]
	 * @throws Exception the exception
	 */
	@DataProvider
	public static Object[][] DTVPostPaidDiscountTIN(ITestNGMethod context) throws Exception {
		Object[][] data = null;

		String SheetName = "Sheet1";
		String filepath = EXCELFILEPATH + "StandaradDTVPostPaidFullPackDiscount_Auto-TIN.xls";

		data = callMethod(filepath, SheetName);
		return data;
	}

	/**
	 * DTV post paid half pack.
	 *
	 * @param context the context
	 * @return the object[][]
	 * @throws Exception the exception
	 */
	@DataProvider
	public static Object[][] DTVPostPaidHalfPack(ITestNGMethod context) throws Exception {
		Object[][] data = null;

		String SheetName = "Sheet1";
		String filepath = EXCELFILEPATH + "StandaradDTVPostPaidHalfPackDiscount.xls";

		data = callMethod(filepath, SheetName);
		return data;
	}
	
	/**
	 * CDMA tax calculation.
	 *
	 * @param context the context
	 * @return the object[][]
	 * @throws Exception the exception
	 */
	@DataProvider
	public static Object[][] CDMAtaxCalculation(ITestNGMethod context) throws Exception {
		Object[][] data = null;

		String SheetName = "Sheet1";
		String filepath = EXCELFILEPATH + "CDMAtaxCalculation.xls";

		data = callMethod(filepath, SheetName);
		return data;
	}

	/**
	 * Call method.
	 *
	 * @param filepath the filepath
	 * @param SheetName the sheet name
	 * @return the object[][]
	 * @throws Exception the exception
	 */
	private static Object[][] callMethod(String filepath, String SheetName) throws Exception {
		Object[][] data = null;
		ExcelFileReader xls = new ExcelFileReader(filepath, SheetName);

		Hashtable<String, String> table = null;
		int numofcols = 0;
		while (!xls.getCellData(SheetName, numofcols, 0).equals("")) {

			numofcols++;
		}

		int numofrows = 0;
		while (!xls.getCellData(SheetName, 0, numofrows + 1).equals("")) {
			numofrows++;
		}
		data = new Object[numofrows][1]; // The column will be 1 only as we will
		for (int row = 1; row < numofrows + 1; row++) {
			table = new Hashtable<String, String>();
			for (int col = 0; col < numofcols; col++) {

				table.put(xls.getCellData(SheetName, col, 0), xls.getCellData(SheetName, col, row));
			}
			data[row - 1][0] = table;
		}
		return data;
	}

}
