/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.util;

/**
 * The Class OSType.
 *
 * @author Niroshen Landerz
 * @date Sept 9th 2014
 */
public final class OSType {

	/** The os. */
	private static String OS = null;

	/**
	 * Gets the os name.
	 *
	 * @return the os name
	 */
	public static String getOsName() {
		if (OS == null) {
			OS = System.getProperty("os.name");
		}
		return OS;
	}

	/**
	 * Gets the OS family from os name.
	 *
	 * @return the OS family from os name
	 */
	public static String getOSFamilyFromOsName() {
		if (OS == null) {
			OS = System.getProperty("os.family");
		}
		return OS;
	}

}
