/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.util;

/**
 * Browser Types.
 *
 * @author Niroshen Landerz
 * @since - Sept 1st 2014
 */

public enum Browser {

    /** The firefox. */
    FIREFOX(1),

    /** The chrome. */
    CHROME(2),

    /** The internetexplorer. */
    INTERNETEXPLORER(3),

    /** The safari. */
    SAFARI(2),

    /** The opera. */
    OPERA(1),

    /** The android. */
    ANDROID(2),

    /** The ipad. */
    IPAD(3),

    /** The iphone. */
    IPHONE(3),

    /** The htmlunit. */
    HTMLUNIT(1),

    /** The htmlunitwithjs. */
    HTMLUNITWITHJS(1);

    /** The timeout value. */
    private int timeoutValue;

    /**
     * Instantiates a new browser.
     *
     * @param value the value
     */
    Browser(final int value) {
        timeoutValue = value;
    }

    /**
     * Instantiates a new browser.
     */
    Browser() {
        timeoutValue = 1;
    }

    /**
     * Gets the timeout.
     *
     * @return the timeout
     */
    public int getTimeout() {
        return timeoutValue;
    }


}
