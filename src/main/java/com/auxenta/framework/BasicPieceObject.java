/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.locators.ElementLocatorByRoot;
import com.auxenta.framework.locators.IElementLocator;
import com.auxenta.framework.util.ByUtil;
import com.auxenta.framework.util.UIType;

/**
 * This is the base class for all page pieces in auxtest framework. It is constructed by root
 * element of the piece.
 *
 * @author Niroshen Landerz
 * @date Aug 30th 2014
 *
 */

public class BasicPieceObject extends BasicObject {

    /** The root. */
    protected IUIElement root;

    /**
     * Instantiates a new basic piece object.
     *
     * @param element the element
     */
    public BasicPieceObject(final IUIElement element) {
        super(element.getUIDriver());
        this.root = element;
    }

    /**
     * Instantiates a new basic piece object.
     *
     * @param element the element
     * @param currentPage the current page
     */
    public BasicPieceObject(final IUIElement element, final String currentPage) {
        super(element.getUIDriver());
        this.root = element;
        setCurrentPage(currentPage);
    }

    /**
     * Returning locator by root element.
     *
     * @param uiType the ui type
     * @param value the value
     * @return the locator
     */
    public IElementLocator getLocator(final UIType uiType, final String value) {
        return new ElementLocatorByRoot(root, ByUtil.getBy(uiType, value));
    }

    /**
     * Checks if is empty.
     *
     * @return true, if is empty
     */
    public boolean isEmpty() {
        List<WebElement> children = root.findElements(ByUtil.getBy(
                UIType.Xpath,
                "./*[position() = 1]"));
        return (children == null || children.size() == 0);
    }

	/**
	 * Checks if is present.
	 *
	 * @return true, if is present
	 */
	public boolean isPresent() {
		return root != null && root.isPresent();
	}

	/**
	 * Wait for the root element of the piece object to be present.
	 */
	public void waitToBePresent() {
		if (this.root != null) {
			this.root.waitToBePresent();
		}
	}

	/**
	 * Wait for the root element of the piece object to be absent.
	 */
	public void waitToBeAbsent() {
		if (this.root != null) {
			this.root.waitToBeAbsent();
		}
	}

    /**
     * Checks if is displayed.
     *
     * @return true, if is displayed
     */
    public boolean isDisplayed() {
        return root != null && root.isDisplayed();
    }

	/**
	 * Wait for the root element of this object to be displayed.
	 */
	public void waitToBeDisplayed()	{
		if (this.root != null) {
			this.root.waitToBeDisplayed();
		}
	}

	/**
	 * Wait for the root of the element to be hidden.
	 */
	public void waitToBeHidden() {
		if (this.root != null) {
			this.root.waitToBeHidden();
		}
	}

}
