/**
 * 
 */
package com.auxenta.framework.external.pageobjects.iedriver;

import java.util.List;

import com.auxenta.framework.BasicPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.core.IUIElements;
import com.auxenta.framework.util.UIType;

/**
 * @author Niroshen Landerz
 * @date Sept 5th 2014
 */

public class IEDriverIndexPage extends BasicPageObject {

    private static final String IE_DRIVER_STORAGE_URL = "http://selenium-release.storage.googleapis.com/index.html";

    private IUIElements versions = createElements(UIType.Xpath, "//td/a[contains(@href,'?path=')]");
    private IUIElement index = createElement(UIType.Link, "/index.html");

    /**
     * @param driver
     */
    public IEDriverIndexPage(IUIDriver driver) {
        super(driver);
        driver.get(IE_DRIVER_STORAGE_URL);
        verifyPageLoad();
    }

    private void verifyPageLoad() {
        index.waitToBeDisplayed(5000);
    }

    public IUIElement getLatestVersion() {
        List<IUIElement> versionList = versions.getUIElementsList();
        int major = 0, minor = 0;
        IUIElement latestElement = null;
        for (IUIElement e : versionList) {
            if (!e.getText().contains(".")) {
                continue;
            }
            int currentMajor = Integer.parseInt(e.getText().split("\\.")[0]);
            int currentMinor = Integer.parseInt(e.getText().split("\\.")[1]);
            if (currentMajor > major) {
                latestElement = e;
                major = currentMajor;
                minor = currentMinor;
            } else
                if (currentMajor == major) {
                    if (currentMinor > minor) {
                        latestElement = e;
                        minor = currentMinor;
                    }
                } else {
                    // version is less, so doesn't matter
                }
        }
        return latestElement;
    }

    /**
     * currently hardcoding to win32 since it seems to work better than 64
     * 
     * @return
     */
    public IEDriverDownloadPage goToReleasePage() {
        getLatestVersion().click();
        IEDriverDownloadPage ieDriverDownloadPage = new IEDriverDownloadPage(uiDriver);
        ieDriverDownloadPage.verifyPageLoad();
        return ieDriverDownloadPage;
    }
}
