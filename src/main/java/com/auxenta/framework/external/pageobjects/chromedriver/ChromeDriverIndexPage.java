/**
 * 
 */
package com.auxenta.framework.external.pageobjects.chromedriver;

import com.auxenta.framework.BasicPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.util.UIType;

/**
 * @author Niroshen Landerz
 * @date Aug 28th 2014
 */

public class ChromeDriverIndexPage extends BasicPageObject {

    private IUIElement latestRelease = createElement(UIType.Link, "/LATEST_RELEASE");
    public static final String CHROME_DRIVER_STORAGE_URL = "http://chromedriver.storage.googleapis.com/index.html";

    /**
     * @param driver
     */
    public ChromeDriverIndexPage(IUIDriver driver) {
        super(driver);
        driver.get(CHROME_DRIVER_STORAGE_URL);
        driver.maximizeWindow();
        verifyPageLoad();
    }

    public void verifyPageLoad() {
        latestRelease.waitToBeEnabled(5000);
    }

    /**
     * click latest release button and get number. used to select which one to download
     * 
     * @return
     */
    public String getLatestRelease() {
        latestRelease.click();
        ChromeDriverLatestReleasePage latestReleasePage = new ChromeDriverLatestReleasePage(
                uiDriver);
        String version = latestReleasePage.getVersionNumber();
        uiDriver.back();
        return version;
    }

    public ChromeDriverVersionPage goToReleasePage(String latestVersion) {
        IUIElement releaseLink = createElement(UIType.Link, "?path=" + latestVersion + "/");
        releaseLink.click();
        ChromeDriverVersionPage chromeDriverVersionPage = new ChromeDriverVersionPage(uiDriver);
        chromeDriverVersionPage.verifyPageLoad();
        return chromeDriverVersionPage;
    }
}