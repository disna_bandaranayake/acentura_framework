/**
 * 
 */
package com.auxenta.framework.external.pageobjects.chromedriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.auxenta.framework.BasicPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.util.UIType;

/**
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */

public class ChromeDriverVersionPage extends BasicPageObject {
    /**
     * @param driver
     */
    protected ChromeDriverVersionPage(IUIDriver driver) {
        super(driver);
    }

    private static final Logger logger = Logger.getLogger(ChromeDriverVersionPage.class);
    private IUIElement chromeDriverLink = createElement(
            UIType.Text,
            "chromedriver_" + getOS() + ".zip");

    /**
     * 
     */
    public void verifyPageLoad() {
        chromeDriverLink.waitToBeDisplayed();
    }

    public String downloadChrome() throws IOException {
        URL website;
        try {

            website = new URL(chromeDriverLink.getAttribute("href"));
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos;
            File chromeDriverZipFile = new File("chromedriver_" + getOS() + ".zip");
            fos = new FileOutputStream(chromeDriverZipFile);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            logger.debug("location of chrome driver: " + chromeDriverZipFile.getAbsolutePath());
            return chromeDriverZipFile.getAbsolutePath();
        } catch (MalformedURLException e) {
            logger.error("Error", e);
            throw new TestAutomationException("MalformedURLException ", e);
        } catch (FileNotFoundException e) {
        	logger.error("Error", e);
        	throw new TestAutomationException("FileNotFoundException ", e);
        } catch (IOException e) {
        	logger.error("Error", e);
        	throw new TestAutomationException("IOException ", e);
        }
    }

    /**
     * win/mac/linux
     * 
     * @return
     */
    private static String getOS() {
        String operatingSystem = System.getProperty("os.name").toLowerCase();
        if (operatingSystem.startsWith("win"))
            return "win32";
        else
            if (operatingSystem.startsWith("mac"))
                return "mac32";
            else
                if (getArchitecture() == 32)
                    return "linux32";
                else
                    return "linux64";
    }

    private Date getDateModified() throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(chromeDriverLink.findUIElement(
                By.xpath("./../../td[3]")).getText());
    }

    /**
     * checking whether the file on disk was last modified after the date on the webpage
     * 
     * @param path
     * @return
     */
    public boolean isUpToDate(String path) {

        File file = new File(path);
        Date fileAge = new Date(file.lastModified());

        try {
            return fileAge.after(getDateModified());
        } catch (ParseException e) {
        	logger.error("Invalid date format ", e);
        	throw new TestAutomationException("Invalid date format ", e);
        }
    }

    /**
     * chrome provides an etag, not a hash. public boolean verifyCorrect(File file){ return false; }
     * public String hash(File file){ return "blah"; }
     */

    /**
     * may potentially return incorrect data. technically returns java architecture, not device
     * architecture
     * 
     * @return
     */
    private static int getArchitecture() {
        int arch = 32;
        try {
            arch = Integer.valueOf(System.getProperty("os.arch"));
        } catch (Exception e) {
        	logger.error("Error ", e);
            if (System.getProperty("os.arch").equalsIgnoreCase("i386")) {
                arch = 32;
            } else {
                arch = 64;
            }
        }
        return arch;
    }
}
