/**
 * 
 */
package com.auxenta.framework.external.pageobjects.iedriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import com.auxenta.framework.BasicPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.util.UIType;

/**
 * @author Niroshen Landerz
 * @date Sept 6th 2014
 */

public class IEDriverDownloadPage extends BasicPageObject {

    private static final Logger logger = Logger.getLogger(IEDriverDownloadPage.class);
    private IUIElement IEDriverLink = createElement(UIType.PartialLinkText, "IEDriverServer_Win32");

    /**
     * @param driver
     */
    public IEDriverDownloadPage(IUIDriver driver) {
        super(driver);
    }

    public String downloadIE() {
        URL website;
        try {

            website = new URL(IEDriverLink.getAttribute("href"));
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos;
            File ieDriverZipFile = new File("iedriver_win32.zip");
            fos = new FileOutputStream(ieDriverZipFile);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            logger.debug("location de ie driver: " + ieDriverZipFile.getAbsolutePath());
            return ieDriverZipFile.getAbsolutePath();
        } catch (MalformedURLException e) {
        	logger.error("MalformedURLException ", e);
        	throw new TestAutomationException("MalformedURLException ", e);
        } catch (FileNotFoundException e) {
        	logger.error("File not found", e);
        	throw new TestAutomationException("File not found ", e);
        } catch (IOException e) {
        	logger.error("IOException ", e);
        	throw new TestAutomationException("IOException ", e);
        }
    }

    /**
     * 
     */
    public void verifyPageLoad() {
        uiDriver.waitForTextToDisplay("ETag");
    }

    private Date getDateModified() throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(IEDriverLink.findUIElement(
                By.xpath("./../../td[3]")).getText());
    }

    /**
     * checking whether the file on disk was last modified after the date on the webpage
     * 
     * @param path
     * @return
     */
    public boolean isUpToDate(String path) {

        File file = new File(path);
        Date fileAge = new Date(file.lastModified());

        try {
            return fileAge.after(getDateModified());
        } catch (ParseException e) {
        	logger.error("Invalid date format ", e);
        	throw new TestAutomationException("Invalid date format ", e);
        }
    }
}
