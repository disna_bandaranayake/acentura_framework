/**
 * 
 */
package com.auxenta.framework.external.pageobjects.chromedriver;

import com.auxenta.framework.BasicPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.util.UIType;

/**
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public class ChromeDriverLatestReleasePage extends BasicPageObject {

    private IUIElement versionNumber = createElement(UIType.Css, "pre");

    /**
     * @param driver
     */
    public ChromeDriverLatestReleasePage(IUIDriver driver) {
        super(driver);

    }

    public String getVersionNumber() {
        versionNumber.waitToBeDisplayed();
        return versionNumber.getText();
    }
}
