package com.auxenta.framework.external.download;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.drivers.DefaultUIDriver;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.external.pageobjects.iedriver.IEDriverDownloadPage;
import com.auxenta.framework.external.pageobjects.iedriver.IEDriverIndexPage;
import com.auxenta.framework.util.Browser;

/**
 * @author Niroshen Landerz Download IEDriver if needed
 */
public class InstallIEDriver extends InstallWebDriverProxy {

	private static final Logger logger = Logger
			.getLogger(InstallIEDriver.class);

	/**
	 * for testing purposes, or to force download of driver
	 * 
	 * @param forceNewDownload
	 */
	@Test
	@Parameters({ "forceNewDownload" })
	public void testUpdateIEDriverWhenNecessary(
			@Optional("false") boolean forceNewDownload) {
		updateDriverIfNecessary(forceNewDownload, DefaultConfig.getConfig());
	}

	@Override
	public String updateDriverIfNecessary(IConfig config) {
		return updateDriverIfNecessary(false, config);
	}

	public String updateDriverIfNecessary(boolean forceNewDownload,
			IConfig config) {
		return updateDriverIfNecessary(forceNewDownload, false, config);
	}

	public String updateDriverIfNecessary(boolean forceNewDownload,
			boolean skipDownload, IConfig config) {
		String configPath = config.getValue(ConfigKeys.KEY_IE_DRIVER_PATH
				.getKey());
		if (configPath != null && !configPath.isEmpty()) {
			logger.info("Read " + ConfigKeys.KEY_IE_DRIVER_PATH.getKey()
					+ " from config. Using " + configPath);
			return configPath;
		}

		String expectedPath = getDriverPath(config);
		if (skipDownload)
			return expectedPath;

		File textFile = new File(
				new File(WEBDRIVER_DIRECTORY).getAbsolutePath()
						+ "/lastUpdatedAt.txt");

		// a 2-tuple; see checkTextFileForUpdate()'s docstring
		Map<Boolean, Integer> checkUpdate = checkTextFileForUpdate(textFile,
				Browser.INTERNETEXPLORER);
		boolean needsUpdate = checkUpdate.containsKey(true);
		int lineNumber = checkUpdate.get(needsUpdate);

		if (!needsUpdate) {
			logger.debug("Existing ie driver newer than one month");
			return expectedPath;
		} else {
			logger.debug("Existing ie driver older than one month");

			IEDriverIndexPage ieDriverIndexPage = null;
			IUIDriver uiDriver = null;
			try {
				FirefoxDriver driver = new FirefoxDriver();
				uiDriver = new DefaultUIDriver(driver);
				ieDriverIndexPage = new IEDriverIndexPage(uiDriver);
			} catch (Exception e) {
				logger.error("Unable to detect latest iedriver version from internet explorer driver website", e);
				throw new TestAutomationException("Unable to detect latest iedriver version from internet explorer driver website", e);
			}
			String ieDriverPath = getDriverPath(config);
			try {
				
				IEDriverDownloadPage ieDriverDownloadPage = ieDriverIndexPage
						.goToReleasePage();
				if (forceNewDownload
						|| !ieDriverDownloadPage.isUpToDate(ieDriverPath)) {
					String zipFilePath = ieDriverDownloadPage.downloadIE();
					ieDriverPath = installZipFile(zipFilePath, ieDriverPath);
				} else {
					logger.debug("iedriver is up to date");
				}
			} catch (Exception e) {
				logger.error("Unable to install new iedriver", e);
				throw new TestAutomationException("Unable to install new iedriver", e);
			} finally {
				// update text file, using the number kept track of earlier
				replaceLineInTextFile(textFile,
						"INTERNETEXPLORERLastUpdatedAt=" + new Date(),
						lineNumber);

				if (uiDriver != null) {
					uiDriver.quit();
				}
			}
			return ieDriverPath;
		}
	}

	public String getDriverPath(IConfig config) {
		String ieDriverPath = config.getValue(ConfigKeys.KEY_IE_DRIVER_PATH
				.getKey());
		if (ieDriverPath != null && !ieDriverPath.isEmpty()) {
			logger.info("IE Driver path set in config file to:  "
					+ ieDriverPath);
			return ieDriverPath;
		} else {
			File iEDriverFolder = new File(WEBDRIVER_DIRECTORY);
			if (!iEDriverFolder.exists()) {
				iEDriverFolder.mkdir();
			}
			File ieDriver = new File(iEDriverFolder.getAbsolutePath()
					+ "/IEDriverServer.exe");
			logger.info("IE Driver path set in InstallIEDriver to:  "
					+ ieDriver.getAbsolutePath());
			return ieDriver.getAbsolutePath();
		}

	}

	@Test
	public void testPath() {
		logger.info("Path=" + getDriverPath(DefaultConfig.getConfig()));
	}

}
