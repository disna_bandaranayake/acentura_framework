package com.auxenta.framework.external.download;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import org.apache.log4j.Logger;

import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.util.Browser;

/**
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */
public abstract class InstallWebDriverProxy {

	private static final Logger logger = Logger.getLogger(InstallWebDriverProxy.class);

	public static final String WEBDRIVER_DIRECTORY;

	static {
		WEBDRIVER_DIRECTORY = System.getProperty("user.dir") + "\\drivers";
	}

	public static String getDefaultFolder() {
		return WEBDRIVER_DIRECTORY;
	}

	/** Retrieve text from a file as a List of lines */
	public List<String> readFile(File file) throws IOException {
		List<String> fileLines = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		try {
			String line = br.readLine();

			while (line != null) {
				StringBuilder sb = new StringBuilder();
				sb.append(line);
				fileLines.add(sb.toString());
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		return fileLines;
	}

	/**
	 * Replaces the line in the given file at position lnum with newText. The
	 * lnum parameter can also be -1; if it is, the newText is appended to the
	 * end of the file.
	 */
	public void replaceLineInTextFile(File file, String newText, int lnum) {
		try {
			List<String> contents = readFile(file);
			if (lnum == -1) {
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,
						true), "UTF-8"));

				out.write(newText);
				out.newLine();
				out.close();
			} else {
				File fileBak = new File(file.getAbsolutePath() + ".bak");
				file.renameTo(fileBak);

				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,
						false), "UTF-8"));

				for (int i = 0; i < contents.size(); i++) {
					if (i == lnum) {
						out.write(newText);
					} else {
						out.write(contents.get(i));
					}
					out.newLine();
				}
				fileBak.delete();
				out.close();
			}
		} catch (IOException e) {
			logger.error("file wasn't found", e);
			throw new TestAutomationException("file wasn't found", e);
		}
	}

	/**
	 * @param browser
	 *            either "chrome" or "ie" (lowercase)
	 * @return a 2-tuple; for example <true, 2> indicates that an update is
	 *         required and the text file line to be changed is line 2. The
	 *         second value may be -1 indicating that the text file is to be
	 *         appended to rather than changed. If the first value is false then
	 *         the second value is ignored.
	 */
	protected Map<Boolean, Integer> checkTextFileForUpdate(File textFile, Browser browser) {

		List<String> textFileLines = new ArrayList<String>();
		try {
			textFile.createNewFile(); // will do nothing if file already exists
			logger.info("Attempt to read from " + textFile.getAbsolutePath());
			textFileLines = readFile(textFile);
			logger.info("Read successful");
		} catch (IOException e) {
			logger.error("Read failed", e);
			throw new TestAutomationException("Read failed", e);
		}

		// get the update date if it's there
		Date parsedDate = new Date(0); // sentinel
		// keep track of which line in the text file to update later
		int lineNumber = -1;
		for (int i = 0; i < textFileLines.size(); i++) {
			String line = textFileLines.get(i);
			logger.info(browser + "LastUpdatedAt=");
			if (line.startsWith(browser + "LastUpdatedAt=")) {
				lineNumber = i;
				String unparsedDate = line.substring(line.indexOf('=') + 1);
				logger.debug("Attempting to parse " + unparsedDate + " as a Date");
				try {
					DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss zzz yyyy");
					parsedDate = df.parse(unparsedDate);
				} catch (ParseException e) {
					logger.error("Error", e);
					throw new TestAutomationException("Error", e);
				}
			}
		}
		if (lineNumber == -1) {
			logger.debug("No date found to parse");
		}

		Calendar then = Calendar.getInstance();
		then.setTime(parsedDate);
		then.add(Calendar.MONTH, 1);
		Calendar now = Calendar.getInstance();

		Map<Boolean, Integer> result = new HashMap<Boolean, Integer>(1);
		result.put(then.before(now), lineNumber);
		return result;
	}

	/**
	 * unzip file to correct location for webdriver, then delete zip file
	 * 
	 * @param driverZipFile
	 */
	public static void unzip(String driverZipFile) {

		try {
			ZipFile zipFile = new ZipFile(driverZipFile);
			File driverFolder = new File(getDefaultFolder());
			if (!driverFolder.exists()) {
				driverFolder.mkdir();
			}
			zipFile.extractAll(driverFolder.getAbsolutePath());
			zipFile.getFile().delete();
		} catch (ZipException e) {
			logger.error("Unable to unzip from " + driverZipFile + " to " + getDefaultFolder() + " due to: "
					+ e.getLocalizedMessage(), e);
			throw new TestAutomationException("Unable to unzip from " + driverZipFile + " to "
					+ getDefaultFolder() + " due to: " + e.getLocalizedMessage(), e);
		}
	}

	public InstallWebDriverProxy() {
		super();
	}

	/**
	 * checks if zipfile and destination file exist. if destination exists,
	 * deletes it, if zip doesn't exist, fails.
	 * 
	 * @param zipFilePath
	 * @return
	 * @throws FileNotFoundException
	 */
	public String installZipFile(String zipFilePath, String driverPath) throws FileNotFoundException {
		File zipFile = new File(zipFilePath);
		if (!zipFile.exists()) {
			throw new FileNotFoundException("Could not find downloaded WebDriver Zip file at "
					+ zipFile.getAbsolutePath());
		}
		logger.info("Path=" + driverPath);
		File driverFile = new File(driverPath);
		if (!driverFile.exists()) {
			logger.debug(driverFile.getAbsolutePath() + " does not exist ");
		} else {
			logger.debug(driverFile.getAbsolutePath() + " exists, deleting before unzipping ");
			driverFile.delete();
		}
		logger.debug(" unzipping webdriver from " + zipFile.getAbsolutePath());
		unzip(zipFilePath);

		Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
		perms.add(PosixFilePermission.OWNER_EXECUTE);
		try {
			Files.setPosixFilePermissions(driverFile.toPath(), perms);
		} catch (IOException e) {
			logger.error("Unable to set driver to executeable", e);
			throw new TestAutomationException("Unable to set driver to executeable", e);
		}
		if (driverFile.exists()) {
			return driverPath;
		} else {
			throw new TestAutomationException("Unable to download " + driverFile.getAbsolutePath());
		}
	}

	/**
	 * check if driver is up to date, if not, download new driver. if
	 * forceNewDownload, download new driver regardless of wheter up to date or
	 * not
	 * 
	 * @param forceNewDownload
	 * @return
	 */
	public abstract String updateDriverIfNecessary(IConfig config);

	/**
	 * get location on harddisk of webdriver
	 * 
	 * @return
	 */
	public abstract String getDriverPath(IConfig config);
}
