package com.auxenta.framework.external.download;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.drivers.DefaultUIDriver;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.external.pageobjects.chromedriver.ChromeDriverIndexPage;
import com.auxenta.framework.external.pageobjects.chromedriver.ChromeDriverVersionPage;
import com.auxenta.framework.util.Browser;

/**
 * Class to download the ChromeDriver locally.
 */
public class InstallChromeDriver extends InstallWebDriverProxy {

	private static final Logger logger = Logger
			.getLogger(InstallChromeDriver.class);

	/**
	 * Tries to determine if the driver is up to date and download it otherwise.
	 * 
	 * @param config 
	 */
	@Override
	public String updateDriverIfNecessary(IConfig config) {
		return updateDriverIfNecessary(false, config);
	}

	/**
	 * Tries to determine if the driver is up to date and download it otherwise.
	 *  
	 * @param config 
	 */
	private String updateDriverIfNecessary(boolean forceNewDownload,IConfig config) {
		return updateDriverIfNecessary(forceNewDownload, false, config);
	}

	public String updateDriverIfNecessary(boolean forceNewDownload,	boolean skipDownload, IConfig config) {

		String configPath = config.getValue(ConfigKeys.KEY_CHROME_DRIVER_PATH
				.getKey());
		if (configPath != null && !configPath.isEmpty()) {
			logger.info("Read " + ConfigKeys.KEY_CHROME_DRIVER_PATH.getKey()
					+ " from config. Using " + configPath);
			String absolutePath = this.getClass().getResource(configPath).getPath();
			return absolutePath;
		}

		String expectedPath = getDriverPath(config);
		if (skipDownload)
			return expectedPath;

		File textFile = new File(
				new File(WEBDRIVER_DIRECTORY).getAbsolutePath()
						+ "/lastUpdatedAt.txt");

		// a 2-tuple; see checkTextFileForUpdate()'s docstring
		Map<Boolean, Integer> checkUpdate = checkTextFileForUpdate(textFile,
				Browser.CHROME);
		boolean needsUpdate = checkUpdate.containsKey(true);
		int lineNumber = checkUpdate.get(needsUpdate);

		if (!needsUpdate) {
			logger.debug("Existing chrome driver newer than one month");
			return expectedPath;
		} else {
			logger.debug("Existing chrome driver older than one month");
			String LatestVersion = "0.0";
			ChromeDriverIndexPage chromeDriverIndexPage = null;
			IUIDriver uiDriver = null;
			try {
				FirefoxDriver driver = new FirefoxDriver();
				uiDriver = new DefaultUIDriver(driver);
				chromeDriverIndexPage = new ChromeDriverIndexPage(uiDriver);
				LatestVersion = chromeDriverIndexPage.getLatestRelease();
				logger.debug("latest Version of chromeDriver: " + LatestVersion);
			} catch (Exception e) {
				logger.error("Unable to detect latest chromedriver version from chromedriver website", e);
				throw new TestAutomationException("Unable to detect latest chromedriver version from chromedriver website", e);
			}
			String chromeDriverPath = getDriverPath();
			try {
				
				ChromeDriverVersionPage chromeDriverVersionPage = chromeDriverIndexPage
						.goToReleasePage(LatestVersion);
				if (forceNewDownload
						|| !chromeDriverVersionPage
								.isUpToDate(chromeDriverPath)) {

					String zipFilePath = chromeDriverVersionPage
							.downloadChrome();
					chromeDriverPath = installZipFile(zipFilePath,
							getDriverPath(config));
				} else {
					logger.debug("chromedriver is up to date");
				}
			} catch (Exception e) {
				logger.error("Unable to install new chromedriver", e);
				throw new TestAutomationException("Unable to install new chromedriver", e);
			} finally {
				logger.info("Rewriting line " + lineNumber + " in "
						+ textFile.getAbsolutePath());

				// update text file, using the number kept track of earlier
				replaceLineInTextFile(textFile, "CHROMELastUpdatedAt="
						+ new Date(), lineNumber);

				if (uiDriver != null) {
					uiDriver.quit();
				}
			}

			return chromeDriverPath;
		}

	}

	public String getDriverPath() {
		return getDriverPath(DefaultConfig.getConfig());
	}

	public String getDriverPath(IConfig config) {
		String chromeDriverPath = config
				.getValue(ConfigKeys.KEY_CHROME_DRIVER_PATH.getKey());
		if (chromeDriverPath != null && !chromeDriverPath.isEmpty()) {
			logger.info("Chrome Driver path set in config file to:  "
					+ chromeDriverPath);
			return chromeDriverPath;
		} else {
			File chromeDriverFolder = new File(WEBDRIVER_DIRECTORY);
			if (!chromeDriverFolder.exists()) {
				chromeDriverFolder.mkdir();
			}
			File chromeDriver = null;
			chromeDriver = new File(chromeDriverFolder.getAbsolutePath()
					+ "/chromedriver.exe");
			logger.info("Chrome Driver path set in InstallChromeDriver to:  "
					+ chromeDriver.getAbsolutePath());

			return chromeDriver.getAbsolutePath();
		}
	}

}
