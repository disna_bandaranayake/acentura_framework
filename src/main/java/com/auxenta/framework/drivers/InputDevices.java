/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;

import com.auxenta.framework.core.IUIDriver;

/**
 * The Class InputDevices.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */

public class InputDevices implements HasInputDevices {

    /** The ui driver. */
    private IUIDriver uiDriver;

    /**
     * Instantiates a new input devices.
     *
     * @param uidriver the uidriver
     */
    public InputDevices(final IUIDriver uidriver) {
        this.uiDriver = uidriver;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.interactions.HasInputDevices#getKeyboard()
     */
    @Override
    public Keyboard getKeyboard() {
        HasInputDevices devices = (HasInputDevices) uiDriver.getWebDriver();
        return devices.getKeyboard();
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.interactions.HasInputDevices#getMouse()
     */
    @Override
    public Mouse getMouse() {
        HasInputDevices devices = (HasInputDevices) uiDriver.getWebDriver();
        return devices.getMouse();
    }
}
