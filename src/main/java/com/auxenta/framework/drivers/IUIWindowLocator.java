/**
 * 
 */
package com.auxenta.framework.drivers;

/**
 * @author Niroshen Landerz
 * @date Sept 4th 2014
 */
public interface IUIWindowLocator {

    /**
     * Will switch to default window if it was set using @see {@link #rememberMainWindow()} or will
     * switch to default content
     */
    void switchToMainWindow();

    /**
     * Switch to latest window
     */
    void switchToNewWindow();

    /**
     * Switch to new popup by name
     *
     * @param popupName
     */
    void switchToPopupWindowByName(String popupName);

    /**
     * @param popupName
     * @param timeout
     */
    void switchToPopupWindowByName(String popupName, long timeout);

    /**
     * Switched to new window by name
     *
     * @param windowName
     */
    void switchToWindowByName(String windowName);

    /**
     * Preserves main window handle
     */
    void rememberMainWindow();

    /**
     * Switch to first window
     */
    void switchToFirstWindow();

    /**
     * Switched to new window using its partial title
     *
     * @param text
     * @return handle or name of the window you switched to
     */
    String switchToWindowTitleContaining(String text);
}
