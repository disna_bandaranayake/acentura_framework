/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.core.IUIElements;
import com.auxenta.framework.elements.DefaultUIElements;
import com.auxenta.framework.exceptions.FrameworkException;
import com.auxenta.framework.exceptions.TimeOutException;
import com.auxenta.framework.locators.ElementLocatorByDriver;
import com.auxenta.framework.util.ByUtil;
import com.auxenta.framework.util.SearchScope;
import com.auxenta.framework.util.Timeout;
import com.auxenta.framework.util.UIType;

/**
 * The Class FrameLocator.
 *
 * @author Niroshen Landerz
 * @date Aug 24th 2014
 */

public class FrameLocator implements IUIFrameLocator {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(FrameLocator.class);

    /** The ui driver. */
    private IUIDriver uiDriver;

    /** The config. */
    protected static IConfig config = DefaultConfig.getConfig();

    /** The config_wait_timeout. */
    private long config_wait_timeout;

    /** The config_verify_interval. */
    private long config_verify_interval;

    /**
     * Instantiates a new frame locator.
     *
     * @param uiDriver the ui driver
     */
    public FrameLocator(final IUIDriver uiDriver) {
        this.uiDriver = uiDriver;
        this.setupConfigValue();
    }

    /**
     * Setup config value.
     */
    private void setupConfigValue() {
        config_wait_timeout = config.getLongValue(
                ConfigKeys.KEY_WAIT_TIMEOUT.getKey(),
                Timeout.WAIT_TIMEOUT.getValue());
        config_verify_interval = config.getLongValue(
                ConfigKeys.KEY_VERIFY_INTERVAL.getKey(),
                Timeout.VERIFY_INTERVAL.getValue());
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#selectFrame(java.lang.String)
     */
    @Override
    public boolean selectFrame(final String frameName) {
        return selectFrame(frameName, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#selectFrame(java.lang.String, long)
     */
    @Override
    public boolean selectFrame(final String frameName, final long timeout) {
        logger.debug("Selecting frame: " + frameName + " in " + timeout + " ms");
        boolean foundFrame = false;
        long stop = System.currentTimeMillis() + timeout;
        do {
            try {
                if (frameName != null && !"".equals(frameName)) {
                    uiDriver.switchTo().frame(frameName);
                    foundFrame = true;
                }
            } catch (NoSuchFrameException ex) {
            	logger.error("NoSuchFrameException ", ex);
                foundFrame = false;
            }
        } while (System.currentTimeMillis() < stop && !foundFrame);
        logger.debug("Selected frame: " + frameName + " in " + timeout + " ms");
        return foundFrame;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#getFramesInPage()
     */
    @Override
    public List<IUIElement> getFramesInPage() {
        IUIElements frames = new DefaultUIElements(uiDriver, new ElementLocatorByDriver(
                uiDriver,
                ByUtil.getBy(UIType.TagName, "frame")));
        IUIElements iframes = new DefaultUIElements(uiDriver, new ElementLocatorByDriver(
                uiDriver,
                ByUtil.getBy(UIType.TagName, "iframe")));
        List<IUIElement> framesAndIframes = new ArrayList<IUIElement>();
        if (frames.areAllPresent()) {
            framesAndIframes.addAll(frames.getUIElementsList());
        }
        if (iframes.areAllPresent()) {
            framesAndIframes.addAll(iframes.getUIElementsList());
        }
        return framesAndIframes;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#
     * switchToFrameByTitle(java.lang.String, com.auxenta.test.auxtest.framework.util.SearchScope)
     */
    @Override
    public void switchToFrameByTitle(String titleName, SearchScope searchScope) {
        List<IUIElement> frames = this.getFramesInPage();
        for (IUIElement frame : frames) {
            uiDriver.switchTo().frame(frame.getWebElement());
            String frameTitle = uiDriver.executeScript("return document.title");
            switch (searchScope) {
                case CONTAINS:
                    if (frameTitle.contains(titleName)) {
                        logger.debug("Switched to frame with title containing text '" 
                    + titleName + "', frame XPath = " + frame.getDescription());
                        return;
                    }
                    break;
                case EQUALS:
                    if (frameTitle.equals(titleName)) {
                        logger.debug("Switched to frame with title '" + titleName + "', frame XPath = " + frame
                                .getDescription());
                        return;
                    }
                    break;
                default:
                	break;
            }
            uiDriver.switchTo().defaultContent();
        }
        logger.debug("Failed to switch to frame by title where title " + searchScope.toString() 
        		+ " '" + titleName + "'");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#getTitle()
     */
    @Override
    public String getTitle() {
        IUIElement myTitle = uiDriver.findUIElement(UIType.TagName, "TITLE", "Frame Title Element");
        myTitle.waitToBePresent();
        return ((String) uiDriver.executeScript("return arguments[0].innerHTML", myTitle
                .getWebElement())).trim();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#
     * waitForTextInFrameToBePresentAndSwitch(java.lang.String, java.lang.String)
     */
    @Override
    public void waitForTextInFrameToBePresentAndSwitch(
            final String frameNameOrID,
            final String textInFrame) {
        waitForTextInFrameToBePresentAndSwitch(frameNameOrID, textInFrame, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#
     * waitForTextInFrameToBePresentAndSwitch(java.lang.String, java.lang.String, long)
     */
    @Override
    public void waitForTextInFrameToBePresentAndSwitch(
            final String frameNameOrID,
            final String textInFrame,
            final long timeout) {

        try {
            WebDriverWait wdw = new WebDriverWait(uiDriver.getWebDriver(), TimeUnit.SECONDS
                    .convert(timeout, TimeUnit.MILLISECONDS));
            uiDriver.getWebDriver().switchTo().defaultContent();
            wdw.until(new ExpectedCondition<Boolean>() {

                public Boolean apply(final WebDriver webDriver) {
                    webDriver.switchTo().defaultContent();
                    return webDriver.switchTo().frame(frameNameOrID).getPageSource().contains(
                            textInFrame);
                }
            });
        } catch (Exception e) {
            throw new TimeOutException(
                    "Text '" + textInFrame + "' is not present in frame with id/name = '" + frameNameOrID + "' \n" + e
                            .getMessage());
        }
        logger.debug("Switched to frame with ID/Name = '" + frameNameOrID + "' text '" + textInFrame + "' is present");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#waitForElementToBePresentAndSwitch(java.lang.String, com.auxenta.test.auxtest.framework.core.IUIElement)
     */
    @Override
    public void waitForElementToBePresentAndSwitch(
            final String frameNameOrID,
            final IUIElement uiElementInFrame) {
        waitForElementToBePresentAndSwitch(frameNameOrID, uiElementInFrame, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#waitForElementToBePresentAndSwitch(java.lang.String, com.auxenta.test.auxtest.framework.core.IUIElement, long)
     */
    @Override
    public void waitForElementToBePresentAndSwitch(
            final String frameNameOrID,
            final IUIElement uiElementInFrame,
            final long timeout) {
        try {
            WebDriverWait wdw = new WebDriverWait(uiDriver.getWebDriver(), TimeUnit.SECONDS
                    .convert(timeout, TimeUnit.MILLISECONDS));
            uiDriver.getWebDriver().switchTo().defaultContent();
            wdw.until(new ExpectedCondition<Boolean>() {

                public Boolean apply(final WebDriver webDriver) {
                    webDriver.switchTo().defaultContent();
                    webDriver.switchTo().frame(frameNameOrID);
                    return uiElementInFrame.isPresent();
                }
            });
        } catch (Exception e) {
            throw new TimeOutException(
                    "Element '" + uiElementInFrame + "' did not appear in frame with id/name = '" 
            + frameNameOrID + "' \n" + e.getMessage());
        }
        logger.debug("Switched to frame with ID/Name = '" + frameNameOrID + "'. Element '" 
        + uiElementInFrame + "' is present in frame");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#waitForElementToBeDisplayedAndSwitch(java.lang.String, com.auxenta.test.auxtest.framework.core.IUIElement)
     */
    @Override
    public void waitForElementToBeDisplayedAndSwitch(
            final String frameNameOrID,
            final IUIElement uiElementInFrame) {
        waitForElementToBeDisplayedAndSwitch(frameNameOrID, uiElementInFrame, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#waitForElementToBeDisplayedAndSwitch(java.lang.String, com.auxenta.test.auxtest.framework.core.IUIElement, long)
     */
    @Override
    public void waitForElementToBeDisplayedAndSwitch(
            final String frameNameOrID,
            final IUIElement uiElementInFrame,
            final long timeout) {
        try {
            WebDriverWait wdw = new WebDriverWait(uiDriver.getWebDriver(), TimeUnit.SECONDS
                    .convert(timeout, TimeUnit.MILLISECONDS));
            uiDriver.getWebDriver().switchTo().defaultContent();
            wdw.until(new ExpectedCondition<Boolean>() {

                public Boolean apply(final WebDriver webDriver) {
                    webDriver.switchTo().defaultContent();
                    webDriver.switchTo().frame(frameNameOrID);
                    return uiElementInFrame.isDisplayed();
                }
            });
        } catch (Exception e) {
            throw new TimeOutException(
                    "Element '" + uiElementInFrame + "' is not displayed in frame with id/name = '" 
            + frameNameOrID + "' \n" + e.getMessage());
        }
        logger.debug("Switched to frame with ID/Name = '" + frameNameOrID + "'. Element '" 
        + uiElementInFrame + "' is displayed in frame");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#
     * waitForElementToBeDisplayedAndSwitchToNestedFrame(java.lang.String[], 
     * com.auxenta.test.auxtest.framework.core.IUIElement)
     */
    @Override
    public void waitForElementToBeDisplayedAndSwitchToNestedFrame(
            final String[] frameNamesOrIDs,
            final IUIElement uiElementInFrame) {
        waitForElementToBeDisplayedAndSwitchToNestedFrame(
                frameNamesOrIDs,
                uiElementInFrame,
                config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#
     * waitForElementToBeDisplayedAndSwitchToNestedFrame(java.lang.String[], 
     * com.auxenta.test.auxtest.framework.core.IUIElement, long)
     */
    @Override
    public void waitForElementToBeDisplayedAndSwitchToNestedFrame(
            final String[] frameNamesOrIDs,
            final IUIElement uiElementInFrame,
            final long timeout) {
        try {
            WebDriverWait wdw = new WebDriverWait(uiDriver.getWebDriver(), TimeUnit.SECONDS
                    .convert(timeout, TimeUnit.MILLISECONDS));
            uiDriver.getWebDriver().switchTo().defaultContent();
            wdw.until(new ExpectedCondition<Boolean>() {

                public Boolean apply(final WebDriver webDriver) {
                    webDriver.switchTo().defaultContent();
                    for (String frameNameOrID : frameNamesOrIDs) {
                        webDriver.switchTo().frame(frameNameOrID);
                    }
                    return uiElementInFrame.isDisplayed();
                }
            });
        } catch (Exception e) {
            throw new TimeOutException(
                    "Element '" + uiElementInFrame + "' is not displayed in frame with id/name = '" + Arrays
                            .toString(frameNamesOrIDs) + "' \n" + e.getMessage());
        }

        logger.debug("Switched to frames with ID/Name = '" + Arrays.toString(frameNamesOrIDs) + "'. Element '" + uiElementInFrame + "' is present in last frame");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#waitForAndSwitchToNestedFrame(java.lang.String, com.auxenta.test.auxtest.framework.drivers.FrameLocator.FrameLocatorType)
     */
    @Override
    public void waitForAndSwitchToNestedFrame(final String frameNameOrID, final FrameLocatorType nameOrID) {
        waitForAndSwitchToNestedFrame(frameNameOrID, nameOrID, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIFrameLocator#waitForAndSwitchToNestedFrame(java.lang.String, com.auxenta.test.auxtest.framework.drivers.FrameLocator.FrameLocatorType, long)
     */
    @Override
    public void waitForAndSwitchToNestedFrame(
    		final String frameNameOrID,
    		final FrameLocatorType nameOrID,
    		final long timeout) {

        Frame neededFrame;
        switch (nameOrID) {
            case NAME:
                neededFrame = new Frame(null, frameNameOrID);
                break;
            case ID:
                neededFrame = new Frame(frameNameOrID, null);
                break;
            default:
                throw new FrameworkException(
                        "Invalid parameteres, frameNameOfId = '" + frameNameOrID + "', FrameLocatorType = " + nameOrID);
        }

        uiDriver.switchTo().defaultContent();
        long stopTime = System.currentTimeMillis() + timeout;
        while (!findAndSelectFrame(neededFrame)) {
            uiDriver.sleep(config_verify_interval);
            if (System.currentTimeMillis() > stopTime) {
                throw new TimeOutException("Timed out waiting to select frame " + frameNameOrID);
            }
        }
        logger.debug("Switched to frame with " + nameOrID + " = '" + frameNameOrID + "'");
    }

    /**
     * Tries to find and select frame anywhere on the page that matches the name returns true if
     * successful.
     *
     * @param frameName the frame name
     * @return true, if successful
     */
    private boolean findAndSelectFrame(final Frame frameName) {
        List<Frame> framePath = new LinkedList<Frame>();
        if (switchToFrameGivenPath(frameName, framePath)) {
            return followFramePath(framePath);
        }
        return false;
    }

    /**
     * Follows frame path then checks if desired frame exists at the end of path, if it does return
     * true also if true returned frame path will have sought after frame added to the end.
     *
     * @param frameName the frame name
     * @param framePath the frame path
     * @return true, if successful
     */
    private boolean switchToFrameGivenPath(final Frame frameName, final List<Frame> framePath) {
        if (!followFramePath(framePath)) {
            // Could not switch to the frame
            return false;
        }
        List<Frame> frameIds = getFrameIdsAndNames();
        if (frameIds.contains(frameName)) {
            // we found the path, add it to the dictionary and return
            framePath.add(frameName);
            return true;
        }
        for (Frame s : frameIds) {
            // add the path and recurse
            framePath.add(s);
            if (switchToFrameGivenPath(frameName, framePath)) {
                return true;
            }
            // was not fruitful remove the path
            framePath.remove(s);
        }
        // Could not find it this round
        return false;
    }

    /**
     * Gets the frame ids and names.
     *
     * @return the frame ids and names
     */
    private List<Frame> getFrameIdsAndNames() {
        List<Frame> frameIds = new LinkedList<Frame>();
        String[] Tags = {"frame", "iframe"};
        for (String tag : Tags) {
            List<WebElement> returnElements = uiDriver.findElements(By.tagName(tag));
            for (WebElement e : returnElements) {
                // Get the ID or name of the frame or iframe
            	frameIds.add(new Frame(e.getAttribute("id"), e.getAttribute("name")));
            }
        }
        return frameIds;
    }

    /**
     * Switch to the last frame given frame path.
     *
     * @param framePath the frame path
     * @return true if followed path to the end and selected last frame
     */
    private boolean followFramePath(final List<Frame> framePath) {
        uiDriver.switchTo().defaultContent();
        try {
            for (Frame s : framePath) {
                if (s.getId() != null) {
                    try
                    {
                        uiDriver.switchTo().frame(s.getId());
                    } catch(NoSuchFrameException e) {
                        //2nd chance as chrome has trouble selecting frames via the first way
                        uiDriver.switchTo().frame(uiDriver.findElement(By.name(s.getId())));
                    }
                } else
                    if (s.getName() != null) {
                        try
                        {
                            uiDriver.switchTo().frame(s.getName());
                        } catch(NoSuchFrameException e) {
                          //2nd chance as chrome has trouble selecting frames via the first way
                            uiDriver.switchTo().frame(uiDriver.findElement(By.name(s.getName())));
                        }
                    }
            }
            // if were here the path was followed
            return true;
        } catch (Exception e) {
        	logger.error("Exception ", e);
            return false;
        }
    }

    /**
     * The Enum FrameLocatorType.
     */
    public enum FrameLocatorType {

        /** The name. */
        NAME,
 /** The id. */
 ID;
    }

    /**
     * The Class Frame.
     */
    class Frame {

        /** The name. */
        private String name;

        /** The id. */
        private String id;

        /**
         * Instantiates a new frame.
         *
         * @param id the id
         * @param name the name
         */
        Frame(final String idVal, final String name) {
            if (idVal == null || idVal.equals("")) {
                this.id = null;
            } else {
                this.id = idVal;
            }

            if (name == null || name.equals("")) {
                this.name = null;
            } else {
                this.name = name;
            }
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "Name = '" + name + "', ID = '" + id + "'";
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(final Object other) {
            if (this.getName() != null && ((Frame) other).getName() != null) {
                if (this.getName().equals(((Frame) other).getName())) {
                    return true;
                }
            } else
                if (this.getId() != null && ((Frame) other).getId() != null) {
                    if (this.getId().equals(((Frame) other).getId())) {
                        return true;
                    }
                }
            return false;
        }
    }

}
