/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.auxenta.framework.BasicTestObject;
import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.exceptions.InvalidUIDriverException;
import com.auxenta.framework.external.download.InstallChromeDriver;
import com.auxenta.framework.tools.ProcessTool;
import com.auxenta.framework.tools.os.WindowsProcessTool;
import com.auxenta.framework.util.Browser;

/**
 * A class for launching proper driver based on definition in config file. It
 * supports to launch firefox, chrome and ie driver with multiple versions
 * locally. And it also supports to launch remote webdriver.
 * 
 * @author Niroshen Landerz
 * 
 * @date Aug 24th 2014
 * 
 */

public class IDriverLauncher {

	/** The Constant CHROME_DRIVER_PROCESS_WIN. */
	private static final String CHROME_DRIVER_PROCESS_WIN = "chromedriver.exe *32";

	/** The logger. */
	private static Logger logger = Logger.getLogger(IDriverLauncher.class);

	/** The config. */
	private IConfig config = null;

	/** The firefox profile. */
	private FirefoxProfile firefoxProfile = null;

	/** The desired capabilities. */
	private DesiredCapabilities desiredCapabilities = null;

	/**
	 * Instantiates a new i driver launcher.
	 * 
	 * @param config
	 *            the config
	 */
	public IDriverLauncher(IConfig config) {
		this.config = config;
	}

	/**
	 * Launch driver.
	 * 
	 * @return the web driver
	 */
	public WebDriver launchDriver() {
		String browserStr = config.getValue(ConfigKeys.KEY_BROWSER.getKey()).toUpperCase();

		Browser browser;
		try {
			browser = Browser.valueOf(browserStr);
		} catch (IllegalArgumentException illegalArgument) {
			logger.warn("Failed to parse browser type '" + browserStr + "', setting browser to firefox");
			browser = Browser.FIREFOX;
		}

		String driverURL = config.getValue(ConfigKeys.KEY_DRIVER_URL.getKey());
		if (driverURL == null || "".equals(driverURL))
			driverURL = "localhost";
		if ("localhost".equals(driverURL))
			return getLocalWebDriver(browser);
		else {
			if (!driverURL.contains("http"))
				driverURL = "http://" + driverURL;
			DesiredCapabilities capabilities = getCapabilities(browser);
			capabilities.setCapability("platform", config.getValue(ConfigKeys.KEY_OS.getKey()).toString());
			capabilities.setCapability("version", config.getValue(ConfigKeys.KEY_BROWSER_VERSION.getKey())
					.toString());
			// capabilities.setCapability("device-type", type);
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.BROWSER, Level.SEVERE);
			capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			String orientation = config.getValue(ConfigKeys.KEY_ORIENTATION.getKey());
			if (orientation != null) {
				capabilities.setCapability("device-orientation", orientation);
			}
			String buildName = "auxtest";

			capabilities.setCapability("name", buildName);
			logger.info("Set Build Name to '" + buildName + "' while launching browser");
			String methodName = "Unknown method";
			if (BasicTestObject.testMethodName != null) {
				methodName = BasicTestObject.testMethodName;
			}
			capabilities.setCapability("methodName", methodName);
			logger.info("Set Method Name to '" + methodName + "' while launching browser");
			try {
				return new RemoteWebDriver(new URL(driverURL), capabilities);
			} catch (MalformedURLException mfue) {
				logger.debug("Malformed driver url=" + driverURL, mfue);
			} catch (RuntimeException rte) {
				logger.error("Runtime Exception trying to launch RemoteWebDriver at " + driverURL + " with "
						+ capabilities.toString() + rte.getLocalizedMessage() + rte.getCause(), rte);
				throw rte;
			}
		}
		return null;
	}

	/**
	 * Launch driver.
	 * 
	 * @param browser
	 *            the browser
	 * @return the web driver
	 */
	public WebDriver launchDriver(Browser browser) {
		return getLocalWebDriver(browser);
	}

	/**
	 * Gets the local web driver.
	 * 
	 * @param browser
	 *            the browser
	 * @return the local web driver
	 */
	private WebDriver getLocalWebDriver(Browser browser) {
		ProcessTool pt = new ProcessTool();
		WindowsProcessTool wpt = new WindowsProcessTool();
		switch (browser) {
		case FIREFOX:
			/*
			 while (pt.filterProcesses("firefox.exe").size() > 1) {
				pt.killProcess("firefox.exe");
			}
			*/ 

			if (firefoxProfile == null && desiredCapabilities == null) {
				FirefoxProfile prof = new FirefoxProfile();
				prof.setAssumeUntrustedCertificateIssuer(true);
				prof.setPreference("Browser.link.open_newwindow.restriction", 1);
				prof.setPreference("privacy.popups.policy", 0);
				prof.setEnableNativeEvents(false);
				return new FirefoxDriver(prof);
			} else if (firefoxProfile != null && desiredCapabilities == null) {
				return new FirefoxDriver(firefoxProfile);
			} else if (firefoxProfile == null && desiredCapabilities != null) {
				return new FirefoxDriver(desiredCapabilities);
			}

		case CHROME:
			try {
				while (wpt.filterProcessesDirect("chrome.exe").size() > 1) {
					wpt.forceKillProcessDirect("chrome.exe");
				}

				while (wpt.filterProcessesDirect("chromedriver.exe").size() > 1) {
					wpt.forceKillProcessDirect("chromedriver.exe");
				}

				String chromeDriverPath = new InstallChromeDriver().updateDriverIfNecessary(
						Boolean.parseBoolean(config.getValue(ConfigKeys.KEY_FORCE_DRIVER_DOWNLOAD.getKey())),
						Boolean.parseBoolean(config.getValue(ConfigKeys.KEY_SKIP_DRIVER_DOWNLOAD.getKey())),
						config);
				System.setProperty(ConfigKeys.KEY_CHROME_DRIVER.getKey(), chromeDriverPath);
				/*
				 * following line is a hack to keep chromedriver logging from
				 * filling our logs
				 * System.setProperty("webdriver.chrome.logfile",
				 * checkDriverPath( "/drivers/chromedriver.log",
				 * "/drivers/chromedriver.log")); /*
				 */
				logger.debug("Chrome driver path = " + new File(chromeDriverPath).getAbsolutePath());
				desiredCapabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
				// desiredCapabilities.setCapability("chrome.binary","<<your chrome path>>");
				desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
				if (desiredCapabilities == null) {
					return new ChromeDriver();
					
				} else {
					return  new ChromeDriver(desiredCapabilities);
					
				}
			} catch (WebDriverException wex) {
				logger.error("Exception while creating ChromeDriver: ", wex);
				// this exception might cause a zombie chromedriver.exe on jenkins, kill it
				logger.error("Attempting to kill Chromedriver");
				try {
					WindowsUtils.killByName(CHROME_DRIVER_PROCESS_WIN);
				} catch (Exception e) {
					logger.warn("Unable to kill " + CHROME_DRIVER_PROCESS_WIN);
				}
				throw wex;
			}
		case INTERNETEXPLORER:
			while (pt.filterProcesses("iexplore.exe").size() > 1) {
				pt.killProcess("iexplore.exe");
			}

			while (pt.filterProcesses("IEDriverServer.exe").size() > 1) {
				pt.forceKillProcess("IEDriverServer.exe");
			}

			// TODO : -This is wrong, for the moment it is working so leaving it like that NRL 
			// if (ieDriverPath == null) {
			String relativePath = config.getValue(ConfigKeys.KEY_IE_DRIVER_PATH.getKey());
			String ieDriverPath = this.getClass().getResource(relativePath).getPath();
			//absolutePath = checkDriverPath(absolutePath, "/drivers/iedriver.exe");
			// }
			
			System.setProperty(ConfigKeys.KEY_IE_DRIVER.getKey(), ieDriverPath);
			logger.debug("IE driver path = " + new File(ieDriverPath).getAbsolutePath());
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(
					InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, false);
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability("ignoreZoomSetting", true);
			capabilities.setCapability("ie.ensureCleanSession", true);
			capabilities.setCapability("nativeEvents",false);
			capabilities.setCapability("requireWindowFocus",true);
			capabilities.setCapability("IntroduceInstabilityByIgnoringProtectedModeSettings",true);
			
			try {
				return new InternetExplorerDriver(capabilities);
			} catch (WebDriverException wex) {
				logger.error("Exception while creating InternetExplorerDriver: ", wex);
				// this exception might cause a zombie iedriver on jenkins, kill
				// it
				logger.error("Attempting to kill InternetExplorerDriver");
				throw wex;
			}
		default:
			return new FirefoxDriver();
		}
	}

	/**
	 * Gets the capabilities.
	 * 
	 * @param browser
	 *            the browser
	 * @return the capabilities
	 */
	private static DesiredCapabilities getCapabilities(Browser browser) {
		switch (browser) {
		case FIREFOX:
			return DesiredCapabilities.firefox();
		case CHROME:
			return DesiredCapabilities.chrome();
		case INTERNETEXPLORER:
			return DesiredCapabilities.internetExplorer();
		default: // Chrome is the most used browser as of Jan 2014
			return DesiredCapabilities.chrome();
		}
	}

	/**
	 * Check driver path.
	 * 
	 * @param path
	 *            the path
	 * @param defaultPath
	 *            the default path
	 * @return the string
	 */
	private String checkDriverPath(String path, String defaultPath) {
		if ("".equals(path) || path.startsWith("/")) {
			if ("".equals(path))
				path = defaultPath;
			URL url = getClass().getResource(path);
			if (url != null)
				path = url.getFile();
			else
				throw new InvalidUIDriverException(path);
		} else {
			File file = new File(path);
			if (!file.exists())
				throw new InvalidUIDriverException(path);
		}
		return path;
	}

	/**
	 * Gets the firefox profile.
	 * 
	 * @return the firefox profile
	 */
	public FirefoxProfile getFirefoxProfile() {
		return firefoxProfile;
	}

	/**
	 * Sets the firefox profile.
	 * 
	 * @param firefoxProfile
	 *            the new firefox profile
	 */
	public void setFirefoxProfile(FirefoxProfile firefoxProfile) {
		this.firefoxProfile = firefoxProfile;
	}

	/**
	 * Gets the desired capabilities.
	 * 
	 * @return the desired capabilities
	 */
	public DesiredCapabilities getDesiredCapabilities() {
		return desiredCapabilities;
	}

	/**
	 * Sets the desired capabilities.
	 * 
	 * @param desiredCapabilities
	 *            the new desired capabilities
	 */
	public void setDesiredCapabilities(DesiredCapabilities desiredCapabilities) {
		this.desiredCapabilities = desiredCapabilities;
	}

}
