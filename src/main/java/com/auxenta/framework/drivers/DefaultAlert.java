/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.UIElementVerificationException;
import com.auxenta.framework.tools.ParameterChecker;
import com.auxenta.framework.util.SearchScope;
import com.auxenta.framework.util.Timeout;

/**
 * The Class DefaultAlert.
 *
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */
public class DefaultAlert implements IUIAlert {

    /** The logger. */
    private static Logger logger = Logger.getLogger(DefaultAlert.class);

    /** The driver. */
    private WebDriver driver;

    /** The config. */
    protected static IConfig config = DefaultConfig.getConfig();

    /** The config_wait_timeout. */
    long config_wait_timeout;

    /**
     * Instantiates a new default alert.
     *
     * @param webDriver the web driver
     */
    public DefaultAlert(final WebDriver webDriver) {
        driver = webDriver;
        this.setupConfigValue();
    }

    /**
     * Setup config value.
     */
    private void setupConfigValue() {
        config_wait_timeout = config.getLongValue(
                ConfigKeys.KEY_WAIT_TIMEOUT.getKey(),
                Timeout.WAIT_TIMEOUT.getValue());
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#acceptAlertIfPresentAndGetMessage()
     */
    @Override
    public String acceptAlertIfPresentAndGetMessage() {
        return acceptAlertIfPresentAndGetMessage(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#acceptAlertIfPresentAndGetMessage(long)
     */
    @Override
    public String acceptAlertIfPresentAndGetMessage(final long timeout) {
        String text = "";
        long stop = System.currentTimeMillis() + timeout;
        do {
            try {
                Alert alert = driver.switchTo().alert();
                if (alert != null) {
                    text = alert.getText();
                    alert.accept();
                }
            } catch (NoAlertPresentException ex) {
            	logger.error("NoAlertPresentException (swallow this since we don't care whether alert is present)", ex);
                // swallow this since we don't care whether alert is present
            }
        } while (System.currentTimeMillis() < stop && ParameterChecker.isNullOrEmpty(text));
        if (ParameterChecker.isNullOrEmpty(text)) {
            logger.error("Alert was not found after waiting for " + timeout + " milliseconds");
            return text;
        }
        logger.debug("Accepted alert with message '" + text + "' in less then " + timeout + " milliseconds");
        return text;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#acceptAlertIfPresent()
     */
    @Override
    public void acceptAlertIfPresent() {
        acceptAlertIfPresent(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#acceptAlertIfPresent(long)
     */
    @Override
    public void acceptAlertIfPresent(final long timeout) {
        long stop = System.currentTimeMillis() + timeout;
        boolean found = false;
        do {
            try {
                Alert alert = driver.switchTo().alert();
                if (alert != null) {
                    alert.accept();
                    found = true;
                }
            } catch (NoAlertPresentException ex) {
            	logger.info("NoAlertPresentException : - swallowed this since we don't care whether alert is present");
                // swallow this since we don't care whether alert is present
            } catch (Exception e) {
                logger.warn("Unexpected error while trying to accept alert: " + e
                        .getLocalizedMessage(), e);
                throw new TestAutomationException("Unexpected error while trying to accept alert", e);
            }
        } while (System.currentTimeMillis() < stop && !found);
        if (!found) {
            logger.info("Alert was not found after waiting for " + timeout + " milliseconds");
            return;
        }
        logger.debug("Accepted alert in less then " + timeout + " milliseconds");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#dismissAlertIfPresent()
     */
    @Override
    public void dismissAlertIfPresent() {
        dismissAlertIfPresent(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#dismissAlertIfPresent(long)
     */
    @Override
    public void dismissAlertIfPresent(final long timeout) {
        long stop = System.currentTimeMillis() + timeout;
        boolean found = false;
        do {
            try {
                Alert alert = driver.switchTo().alert();
                if (alert != null) {
                    alert.dismiss();
                    found = true;
                }
            } catch (NoAlertPresentException ex) {
            	logger.error("NoAlertPresentException (swallow this since we don't care whether alert is present)", ex);
                // swallow this since we are relying on 'found' flag
            }
        } while (System.currentTimeMillis() < stop && !found);
        if (!found) {
            logger.error("Alert was not found after waiting for " + timeout + " milliseconds");
            return;
        }
        logger.debug("Dismissed alert in less then " + timeout + " milliseconds");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#
     * acceptAlertMatchingMessage(java.lang.String, com.auxenta.test.auxtest.framework.util.SearchScope)
     */
    @Override
    public void acceptAlertMatchingMessage(final String message, final SearchScope scope)
            throws NoAlertPresentException, UIElementVerificationException {
        Alert alert = driver.switchTo().alert();
        try {
            if ((scope.equals(SearchScope.EQUALS) && !alert.getText().equals(message)) || scope
                    .equals(SearchScope.CONTAINS) && !alert.getText().contains(message)) {
                String errorMsg = "Did not get expected alert. Got '" + alert.getText() +
                		"' but was expecting '" + message + "'";
                logger.error(errorMsg);
                throw new UIElementVerificationException(errorMsg);
            }
            logger.debug("Accept alert: " + alert.getText());
            alert.accept();
        } catch (NoAlertPresentException e) {
            logger.error("Alert not present. Expected alert with message '" + message + "'.");
            throw new TestAutomationException("Alert not present", e);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIAlert#getAlertMessage()
     */
    @Override
    public String getAlertMessage() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertMessage = alert.getText();
            logger.debug("Received alert: " + alertMessage);
            return alertMessage;
        } catch (NoAlertPresentException e) {
            logger.error("Checked for alert dialog, but found none. Continuing", e);
            return NO_ALERT_FOUND;
        }
    }
}
