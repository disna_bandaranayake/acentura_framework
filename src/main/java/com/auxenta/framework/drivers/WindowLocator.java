/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.FrameworkException;
import com.auxenta.framework.exceptions.InvalidUIDriverException;
import com.auxenta.framework.util.Timeout;

/**
 * <li><b> POPUP </b> - New window in front of other windows
 *
 * <li><b> New Window </b> - New window created by user action.
 *
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */
public class WindowLocator implements IUIWindowLocator {

    /** The main window name. */
    private String mainWindowName = "";

    /** The logger. */
    private static Logger logger = Logger.getLogger(WindowLocator.class);

    /** The driver. */
    private WebDriver driver;

    /** The config. */
    private static IConfig config = DefaultConfig.getConfig();

    /** The config_wait_timeout. */
    long config_wait_timeout;

    /**
     * Check if driver is not null.
     *
     * @return true if driver is not {@code null}
     */
    private boolean checkDriver() {
        if (driver == null) {
            throw new InvalidUIDriverException();
        }
        return true;
    }

    /**
     * Instantiates a new window locator.
     *
     * @param webDriver the web driver
     */
    public WindowLocator(final WebDriver webDriver) {
        driver = webDriver;
        this.setupConfigValue();
    }

    /**
     * Setup config value.
     */
    private void setupConfigValue() {
        config_wait_timeout = config.getLongValue(
                ConfigKeys.KEY_WAIT_TIMEOUT.getKey(),
                Timeout.WAIT_TIMEOUT.getValue());
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#rememberMainWindow()
     */
    @Override
    public void rememberMainWindow() {
        if (checkDriver()) {
            mainWindowName = driver.getWindowHandle();
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToMainWindow()
     */
    @Override
    public void switchToMainWindow() {
        if (checkDriver()) {
            logger.debug("Trying to switch to mainWindowName = " + mainWindowName + "");
            if (mainWindowName == null || "".equals(mainWindowName)) {
                logger.debug("MainWindow was not set, switching to default");
                driver.switchTo().defaultContent();
            } else {
                try {
                    driver.getWindowHandle();
                } catch (NoSuchWindowException nswe) {
                    logger.debug("Got NoSuchWindowException, might have been called after closing a window, ex:" + nswe
                            .getMessage());
                    driver.switchTo().window(mainWindowName);
                    return;
                }
                if (!mainWindowName.equals(driver.getWindowHandle())) {
                    driver.switchTo().window(mainWindowName);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToNewWindow()
     */
    @Override
    public void switchToNewWindow() {
        if (checkDriver()) {
            String currentHandle = driver.getWindowHandle();
            Set<String> handles = driver.getWindowHandles();
            for (String handle : handles) {
                if (!currentHandle.equals(handle)) {
                    driver.switchTo().window(handle);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToPopupWindowByName(java.lang.String)
     */
    @Override
    public void switchToPopupWindowByName(final String windowName) {
        switchToPopupWindowByName(windowName, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToPopupWindowByName(java.lang.String, long)
     */
    @Override
    public void switchToPopupWindowByName(final String windowName, final long timeout) {
        logger.debug("Selecting window: " + windowName + " in " + timeout + " ms");
        if (checkDriver()) {
            switchToMainWindow();
            long stop = System.currentTimeMillis() + timeout;
            boolean foundPopup = true;
            do {
                try {
                    if (windowName == null || "".equals(windowName)) {
                        String lastestWin = getLastestWindowHandle();
                        if (!driver.getWindowHandle().equals(lastestWin)) {
                            driver.switchTo().window(lastestWin);
                        }
                    } else {
                        driver.switchTo().window(windowName);
                    }
                    foundPopup = true;
                } catch (NoAlertPresentException ex) {
                	logger.error("NoAlertPresentException ", ex);
                    foundPopup = false;
                }
            } while (System.currentTimeMillis() < stop && !foundPopup);
        }
        logger.debug("Selected popup: " + windowName + " in " + timeout + " ms");
    }

    /**
     * Gets the lastest window handle.
     *
     * @return the lastest window handle
     */
    private String getLastestWindowHandle() {
        if (checkDriver()) {
            Object[] handles = driver.getWindowHandles().toArray();
            if (handles.length > 0) {
                return handles[handles.length - 1].toString();
            }
        }
        return "";
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToWindowByName(java.lang.String)
     */
    @Override
    public void switchToWindowByName(final String windowName) {
        logger.debug("Selecting window: " + windowName);
        if (checkDriver()) {
            if (windowName == null || "".equals(windowName)) {
                switchToMainWindow();
            } else {
                driver.switchTo().window(windowName);
            }
        }
        logger.debug("Selected window: " + windowName);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToFirstWindow()
     */
    @Override
    public void switchToFirstWindow() {
        logger.debug("Switching to first window handle in list");
        Set<String> handles = driver.getWindowHandles();
        logger.debug("Found " + handles.size() + " window handles");
        java.util.Iterator<String> windowIterator = handles.iterator();
        String firstHandle = windowIterator.next();
        logger.debug("switching to " + firstHandle);
        try {
            driver.switchTo().window(firstHandle);
        } catch (NoSuchWindowException nsw) {
            logger.error("Unable to switch to first window ", nsw);
            throw nsw;
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IUIWindowLocator#switchToWindowTitleContaining(java.lang.String)
     */
    @Override
    public String switchToWindowTitleContaining(final String text) {
        logger.debug("switch to window titled " + text);
        Set<String> windowHandles;
        long stopTime = config_wait_timeout + System.currentTimeMillis();
        while (System.currentTimeMillis() < stopTime) {
            try {
                driver.switchTo().defaultContent();
                if (driver.getTitle().contains(text)) {
                    return driver.getWindowHandle();
                }
            } catch (NoSuchFrameException nsfe) {
                logger.error(
                        "Unable to switch to frame defaultContext. Did you just close a window?",
                        nsfe);
                throw new TestAutomationException("Unable to switch to frame defaultContext", nsfe);
            } catch (IllegalStateException ise) {
                logger.error("Illegal State trying to switch to frame defaultContext", ise);
                throw new TestAutomationException("Illegal State trying to switch to frame defaultContext", ise);
            } catch (WebDriverException wde) {
                logger.error("WebDriverException trying to switch to frame defaultContext", wde);
                throw new TestAutomationException("WebDriverException trying to switch to frame defaultContext", wde);
            }
            windowHandles = driver.getWindowHandles();
            logger.debug("checking " + windowHandles.size() + " windows.");
            for (String handle : windowHandles) {
                try {
                    driver.switchTo().window(handle);
                } catch (NoSuchWindowException e) {
                    logger.error("Failed to find window with handle " + handle + ", moving on", e);
                    continue;
                }
                String title = driver.getTitle();
                logger.debug("checking window(" + handle + ")with title:" + title);
                if (title.contains(text)) {
                    logger.debug("Matched on window title: " + title);
                    return handle;
                } else {
                    logger.debug("window title '" + title + "' did not contain '" + text + "'");
                }
            }
        }

        windowHandles = driver.getWindowHandles();
        logger.debug("capturing " + windowHandles.size() + " windows.");
        for (String handle : windowHandles) {
            driver.switchTo().window(handle);
        }
        throw new FrameworkException(
                "Failed to switch to window which title containing text '" + text + "' Page = '" + driver
                        .getCurrentUrl() + "'");
    }
}
