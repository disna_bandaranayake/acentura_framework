/**
 * 
 */
package com.auxenta.framework.drivers;

import org.openqa.selenium.NoAlertPresentException;

import com.auxenta.framework.exceptions.UIElementVerificationException;
import com.auxenta.framework.util.SearchScope;

/**
 * We need to think of better name for this class since it has both Alerts and popups
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */
public interface IUIAlert {

    static String NO_ALERT_FOUND = "No Alert Found";

    /**
     * @param message
     *            - Expected message from Alert body
     * @param scope
     *            - Exact match or substring of Alert body
     * @throws NoAlertPresentException
     *             - if Alert not found
     * @throws UIElementVerificationException
     *             - if Alert message does not match expected {@code message}
     */
    void acceptAlertMatchingMessage(String message, SearchScope scope)
            throws NoAlertPresentException, UIElementVerificationException;

    /**
     * Accept Alert <br>
     * <b>Note:</b> This method will pass even if Alert is not present
     */
    void acceptAlertIfPresent();

    /**
     * Accept Alert <br>
     * <br>
     * <b>Note:</b> This method will pass even if Alert is not present
     *
     * @param timeout
     *            - max timeout time to wait for alert to appear
     *
     *
     */
    void acceptAlertIfPresent(long timeout);

    /**
     * Dismiss Alert <br>
     * <b>Note:</b> This method will pass even if Alert is not present
     */
    void dismissAlertIfPresent();

    /**
     * Dismiss Alert <br>
     * <b>Note:</b> This method will pass even if Alert is not present
     *
     * @param timeout
     *            - max timeout time to wait for alert to appear
     */
    void dismissAlertIfPresent(long timeout);

    /**
     * Accept Alert <br>
     * <b>Note:</b> This method will pass even if Alert is not present
     *
     * @return Alert message, or empty String if Alert is not found
     */
    String acceptAlertIfPresentAndGetMessage();

    /**
     * Accept Alert <br>
     * <b>Note:</b> This method will pass even if Alert is not present
     *
     * @param timeout
     *            - max timeout time to wait for alert to appear
     * @return - Alert message, or empty String if Alert is not found
     */
    String acceptAlertIfPresentAndGetMessage(long timeout);

    /**
     * @return Message from Alert body, or empty String if Alert was not found
     */
    String getAlertMessage();

}
