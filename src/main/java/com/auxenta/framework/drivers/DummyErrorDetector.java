/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import com.auxenta.framework.exceptions.UIElementVerificationException;

/**
 * The Class DummyErrorDetector.
 *
 * @author Niroshen
 * @date Sept 6th 2014
 */
public class DummyErrorDetector implements IErrorDetector {

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IErrorDetector#assertNoError()
     */
    @Override
    public void assertNoError() {}

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IErrorDetector#assertError(java.lang.String)
     */
    @Override
    public void assertError(String expectedError) {
        throw new UIElementVerificationException("Assert dummy error: " + expectedError);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.drivers.IErrorDetector#getError()
     */
    @Override
    public String getError() {
        return IErrorDetector.NO_ERROR;
    }

}
