/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import java.awt.Toolkit;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;



import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.core.FWNoUIDriverObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.elements.DefaultUIElement;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.TimeOutException;
import com.auxenta.framework.locators.ElementLocatorByDriver;
import com.auxenta.framework.locators.IElementLocator;
import com.auxenta.framework.util.Browser;
import com.auxenta.framework.util.ByUtil;
import com.auxenta.framework.util.SearchScope;
import com.auxenta.framework.util.UIType;

/**
 * The Class DefaultUIDriver.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

/**
 * A generic uidriver based on WebDriver, which implements all functions for
 * UIDriver.<br>
 *
 * <li><b>Present</b> - Element in DOM
 *
 * <li><b>Absent</b> - Element is NOT in DOM
 *
 * <li><b>Displayed</b> - Element is in DOM and is visible
 *
 * <li><b>Hidden</b> - Element is in DOM and is NOT visible
 *
 * <li><b>Wait</b> - Waits for something to happen and THROWS TimeOutException
 * on failure
 *
 * <li><b>is/has</b> - Returns boolean, true or false, does not throw exception
 *
 */
public class DefaultUIDriver extends FWNoUIDriverObject implements IUIDriver {

	/** The logger. */
	private static Logger logger = Logger.getLogger(DefaultUIDriver.class);

	/** The driver. */
	protected WebDriver driver;

	/** The window locator. */
	private WindowLocator windowLocator;

	/** The log entries. */
	private List<LogEntry> logEntries = new ArrayList<LogEntry>();

	/**
	 * If you are using this constructor you are responsible to initialize
	 * {@link FWObject#uiDriver} instance. <br>
	 * <br>
	 * <b>Consider using following methods instead: </b> <li>
	 * {@link BasicTestObject#getStartPage()}, <li>
	 * {@link BasicTestObject#launchBrowser(Browser, String)}, <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile)},
	 * <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile, String)}
	 * <li>
	 * {@link BasicTestObject#launchBrowserWithDesiredCapabilities(DesiredCapabilities)}
	 *
	 *
	 */
	public DefaultUIDriver() {
		IDriverLauncher launcher = new IDriverLauncher(config);
		driver = launcher.launchDriver();
		Capabilities currentCapabilities = getCapabilities(driver);
		logger.info("Browser and operating system information: browser=["
				+ currentCapabilities.getBrowserName() + "] browser_version=["
				+ currentCapabilities.getVersion() + "] platform=[" + currentCapabilities.getPlatform() + "]");
	}

	/**
	 * If you are using this constructor you are responsible to initialize
	 * {@link FWObject#uiDriver} instance. <br>
	 * <br>
	 * <b>Consider using following methods instead: </b> <li>
	 * {@link BasicTestObject#getStartPage()}, <li>
	 * {@link BasicTestObject#launchBrowser(Browser, String)}, <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile)},
	 * <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile, String)}
	 * <li>
	 * {@link BasicTestObject#launchBrowserWithDesiredCapabilities(DesiredCapabilities)}
	 *
	 * @param webDriver the web driver
	 */
	public DefaultUIDriver(final WebDriver webDriver) {
		driver = webDriver;
		Capabilities currentCapabilities = getCapabilities(driver);
		logger.info("Browser and operating system information: browser=["
				+ currentCapabilities.getBrowserName() + "] browser_version=["
				+ currentCapabilities.getVersion() + "] platform=[" + currentCapabilities.getPlatform() + "]");
	}

	/**
	 * Set {@link FirefoxProfile} before lunching {@link FirefoxDriver} <br>
	 * If you are using this constructor you are responsible to initialize
	 * {@link FWObject#uiDriver} instance. <br>
	 * <br>
	 * <b>Consider using following methods instead: </b> <li>
	 * {@link BasicTestObject#getStartPage()}, <li>
	 * {@link BasicTestObject#launchBrowser(Browser, String)}, <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile)},
	 * <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile, String)}
	 * <li>
	 * {@link BasicTestObject#launchBrowserWithDesiredCapabilities(DesiredCapabilities)}
	 *
	 * @param profile the profile
	 */
	public DefaultUIDriver(final FirefoxProfile profile) {
		IDriverLauncher launcher = new IDriverLauncher(config);
		launcher.setFirefoxProfile(profile);
		driver = launcher.launchDriver(Browser.FIREFOX);
		Capabilities currentCapabilities = getCapabilities(driver);
		logger.info("Browser and operating system information: browser=["
				+ currentCapabilities.getBrowserName() + "] browser_version=["
				+ currentCapabilities.getVersion() + "] platform=[" + currentCapabilities.getPlatform() + "]");
	}

	/**
	 * Set {@link DesiredCapabilities} before lunching {@link IUIDriver} <br>
	 * If you are using this constructor you are responsible to initialize
	 * {@link FWObject#uiDriver} instance. <br>
	 * <br>
	 * <b>Consider using following methods instead: </b> <li>
	 * {@link BasicTestObject#getStartPage()}, <li>
	 * {@link BasicTestObject#launchBrowser(Browser, String)}, <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile)},
	 * <li>
	 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile, String)},<li>
	 * {@link BasicTestObject#launchBrowserWithDesiredCapabilities(DesiredCapabilities)}
	 *
	 * @param cap the cap
	 */
	public DefaultUIDriver(final DesiredCapabilities cap) {
		IDriverLauncher launcher = new IDriverLauncher(config);
		launcher.setDesiredCapabilities(cap);
		driver = launcher.launchDriver(Browser.valueOf(config.getValue(ConfigKeys.KEY_BROWSER.getKey())
				.toUpperCase()));
		Capabilities currentCapabilities = getCapabilities(driver);
		logger.info("Browser and operating system information: browser=["
				+ currentCapabilities.getBrowserName() + "] browser_version=["
				+ currentCapabilities.getVersion() + "] platform=[" + currentCapabilities.getPlatform() + "]");
	}

/**
 * If you are using this constructor you are responsible to initialize {@link FWObject#uiDriver}
 * instance. <br>
 * <br>
 * <b>Consider using following methods instead: </b> <li> {@link BasicTestObject#getStartPage()},
 * <li> {@link BasicTestObject#launchBrowser(Browser, String)}, <li>
 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile)}, <li>
 * {@link BasicTestObject#launchFirefoxBrowserWithProfile(FirefoxProfile, String)
 *
 * @param browser the browser
 * @link BasicTestObject#launchBrowserWithDesiredCapabilities(DesiredCapabilities)}
 */
	public DefaultUIDriver(final Browser browser) {
		IDriverLauncher launcher = new IDriverLauncher(config);
		driver = launcher.launchDriver(browser);
		Capabilities currentCapabilities = getCapabilities(driver);
		logger.info("Browser and operating system information: browser=["
				+ currentCapabilities.getBrowserName() + "] browser_version=["
				+ currentCapabilities.getVersion() + "] platform=[" + currentCapabilities.getPlatform() + "]");
	}

	/**
	 * Gets the capabilities.
	 *
	 * @param newDriver the new driver
	 * @return see {@link Capabilities}
	 */
	private static Capabilities getCapabilities(final WebDriver newDriver) {
		if (newDriver instanceof HasCapabilities) {
			HasCapabilities version = (HasCapabilities) newDriver;
			Capabilities cap = version.getCapabilities();
			return cap;
		} else {
			return null;
		}
	}

	/**
	 * Sets the web driver.
	 *
	 * @param webDriver the new web driver
	 */
	public void setWebDriver(final WebDriver webDriver) {
		driver = webDriver;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getUIAlert()
	 */
	@Override
	public IUIAlert getUIAlert() {
		return new DefaultAlert(driver);
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#close()
	 */
	@Override
	public void close() {

		int numOfWindows = driver.getWindowHandles().size();
		String title = driver.getTitle();
		if (isAvailable()) {
			if (numOfWindows == 0) {
				logger.warn("Driver does not have any windows, quitting WebDriver");
				quit();
				return;
			} else if (numOfWindows == 1) {
				logger.debug("'" + title
						+ "' is the only window present. Closing this window and quit webDriver");
				driver.close();
				sleep(1000);// it takes some time to close webdriver
				logger.debug("Quit WebDriver");
				return;
			} else if (numOfWindows > 1) {
				logger.debug("WebDriver found " + numOfWindows
						+ " windows that are open, going to close currently selected window");
				driver.close();
				while (!(driver.getWindowHandles().size() < numOfWindows)) {
					logger.debug("Waiting for '" + title + "' window to close");
					this.sleep(100);
				}
				logger.debug("Closed window titled '" + title + "'");
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#deleteAllCookies()
	 */
	@Override
	public void deleteAllCookies() {
		if (isAvailable()) {
			driver.manage().deleteAllCookies();
			logger.debug("Deleted all the cookies for the current domain");
		}
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.JavascriptExecutor#executeAsyncScript(java.lang.String, java.lang.Object[])
	 */
	@Override
	public Object executeAsyncScript(final String arg0, final Object... arg1) {
		Object result = null;
		if (isAvailable()) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			result = js.executeAsyncScript(arg0, arg1);
			logger.debug("Running Async JavaScript '" + arg0 + "' with parameters '" + Arrays.toString(arg1)
					+ "', result: " + result.toString());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#executeScript(java.lang.String)
	 */
	@Override
	public String executeScript(final String script) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object executeResult = js.executeScript(script);
		return executeResult == null ? "" : executeResult.toString();
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.JavascriptExecutor#executeScript(java.lang.String, java.lang.Object[])
	 */
	@Override
	public Object executeScript(final String script, final Object... args) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		logger.debug("Running JavaScript '" + script + "' with parameters '" + Arrays.toString(args) + "'");
		return js.executeScript(script, args);
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#findElement(org.openqa.selenium.By)
	 */
	@Override
	public WebElement findElement(final By by) {
		WebElement ele = null;
		if (isAvailable()) {
			ele = driver.findElement(by);
		}
		return ele;
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#findElements(org.openqa.selenium.By)
	 */
	@Override
	public List<WebElement> findElements(final By by) {

		List<WebElement> list = null;
		if (isAvailable()) {
			list = driver.findElements(by);
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#findUIElement(com.auxenta.test.auxtest.framework.util.UIType, java.lang.String)
	 */
	@Override
	public IUIElement findUIElement(final UIType type, final String value) {
		return findUIElement(type, value, null);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#findUIElement(com.auxenta.test.auxtest.framework.util.UIType, java.lang.String, java.lang.String)
	 */
	@Override
	public IUIElement findUIElement(final UIType type, final String value, final String description) {
		IElementLocator locator = new ElementLocatorByDriver(this, ByUtil.getBy(type, value));
		return new DefaultUIElement(this, locator, description);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#get(java.lang.String)
	 */
	@Override
	public void get(String url) {
		if (isAvailable()) {
			File localPage = new File(url.replace("file://", ""));
			if (localPage.exists()) {
				url = "file://" + localPage.getAbsolutePath();
			}
			driver.get(url);
			logger.debug("Opened url: " + url);
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getAllLinksOnPage()
	 */
	@Override
	public ArrayList<String> getAllLinksOnPage() {
		List<WebElement> anchors = this.findElements(By.xpath("//a"));
		ArrayList<String> urls = new ArrayList<String>();
		for (WebElement a : anchors) {
			try {
				String href = a.getAttribute("href");
				urls.add(href);
				logger.debug("Found link on page:" + href);
			} catch (Exception e) {
				logger.error("Unable to get href from anchor: " + e.getLocalizedMessage(), e);
				throw new TestAutomationException("Unable to get href from anchor", e);
			}
		}
		return urls;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getAttributeFromDisabledFieldByID(java.lang.String, java.lang.String)
	 */
	@Override
	public String getAttributeFromDisabledFieldByID(final String id, final String attribute) {
		String result = null;
		result = String.valueOf(((JavascriptExecutor) this).executeScript("return document.getElementById('"
				+ id + "').getAttribute('" + attribute + "');", ""));
		logger.debug("Got attribute from disabled field: id=\"" + id + "\", " + attribute + "=\"" + result
				+ "\"");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.HasCapabilities#getCapabilities()
	 */
	@Override
	public Capabilities getCapabilities() {
		return getCapabilities(driver);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#hasCapability(java.lang.String)
	 */
	@Override
	public boolean hasCapability(final String capability) {
		Capabilities cap = this.getCapabilities();
		if (cap != null) {
			return cap.getCapability(capability) != null;
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#getCurrentUrl()
	 */
	@Override
	public String getCurrentUrl() {
		String url = defaultStr;
		logger.trace("DefaultUIDriver.getCurrentUrl starts as " + defaultStr);
		if (isAvailable()) {
			logger.trace("DefaultUIDriver.getCurrentUrl setting...");
		}
		url = driver.getCurrentUrl();
		logger.trace("DefaultUIDriver.getCurrentUrl set to '" + url + "'");
		return url;
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#getPageSource()
	 */
	@Override
	public String getPageSource() {
		String pageSource = defaultStr;
		if (isAvailable()) {
			pageSource = driver.getPageSource();
		}
		return pageSource;
	}

	/**
	 * JavaScript call {@code return [element].innerHTML; }
	 *
	 * <p>
	 * Returns the innerHTML of the provided UIElement as returned by the
	 * javascript method [element].innerHTML, if and only if the browser's
	 * javascript engine supports innerHTML.
	 * </p>
	 *
	 * @param element
	 *            as the UIElement
	 * @return the innerHTML of the UIElement
	 */
	@Override
	public String getInnerHtmlOf(final IUIElement element) {
		return (String) executeScript("return arguments[0].innerHTML;", element.getWebElement());
	}

	/**
	 * JavaScript call {@code return [element].outerHTML; }
	 *
	 * <p>
	 * Returns the outerHTML of the provided UIElement as returned by the
	 * javascript method [element].outerHTML, if and only if the browser's
	 * javascript engine supports outerHTML.
	 * </p>
	 *
	 * @param element
	 *            as the UIElement
	 * @return the outerHTML of the UIElement
	 */
	@Override
	public String getOuterHtmlOf(final IUIElement element) {
		return (String) executeScript("return arguments[0].outerHTML;", element.getWebElement());
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#getTitle()
	 */
	@Override
	public String getTitle() {
		String title = defaultStr;
		if (isAvailable()) {
			title = driver.getTitle();
		}
		return title;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getValueFromDisabledFieldByID(java.lang.String)
	 */
	@Override
	public String getValueFromDisabledFieldByID(final String id) {
		String result = null;
		result = (String) ((JavascriptExecutor) this).executeScript("return document.getElementById('" + id
				+ "').value;", "");
		logger.debug("Got attribute from disabled field: id=\"" + id + "\", value=\"" + result + "\"");
		return result;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getWebDriver()
	 */
	@Override
	public WebDriver getWebDriver() {
		return driver;
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#getWindowHandle()
	 */
	@Override
	public String getWindowHandle() {
		String handle = defaultStr;
		if (isAvailable()) {
			handle = driver.getWindowHandle();
		}
		return handle;
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#getWindowHandles()
	 */
	@Override
	public Set<String> getWindowHandles() {
		Set<String> handles = null;
		if (isAvailable()) {
			handles = driver.getWindowHandles();
		}
		return handles;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#isWindowTitleContains(java.lang.String)
	 */
	@Override
	public boolean isWindowTitleContains(final String substring) {
		String currentTitle = driver.getTitle();
		logger.debug("Checking if page title '" + currentTitle + "' contains substring '" + substring + "'");
		return currentTitle.contains(substring);
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#manage()
	 */
	@Override
	public Options manage() {
		Options manage = null;
		if (isAvailable()) {
			manage = driver.manage();
		}
		return manage;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getWindowSize()
	 */
	@Override
	public Dimension getWindowSize() {
		Dimension windowDim = null;
		try {
			int height = this.manage().window().getSize().getHeight();
			int width = this.manage().window().getSize().getWidth();
			windowDim = new Dimension(width, height);
		} catch (Exception e) {
			logger.debug("Failed obtain window size.", e);
		}
		return windowDim;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getClientScreenSize()
	 */
	@Override
	public Dimension getClientScreenSize() {
		Dimension clientDim = null;
		try {
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			clientDim = new Dimension(toolkit.getScreenSize().width, toolkit.getScreenSize().height);
		} catch (Exception e) {
			logger.debug("Failed to obtain the client screen size.", e);
		}
		return clientDim;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getConsoleLog(java.util.logging.Level)
	 */
	public LogEntries getConsoleLog(final Level levelFilter) {
		this.logEntries.addAll(this.manage().logs().get(LogType.BROWSER).getAll());
		return new LogEntries(new LogEntries(logEntries).filter(levelFilter));
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#maximizeWindow()
	 */
	@Override
	public void maximizeWindow() {
		if (isAvailable()) {
			/*
			 * driver.manage().window().setPosition(new Point(0, 0));
			 * java.awt.Dimension screenSize =
			 * java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			 * logger.debug("Maximizing window  new width = " +
			 * screenSize.getWidth() + " new height = " + screenSize
			 * .getHeight()); Dimension dim = new Dimension((int)
			 * screenSize.getWidth(), (int) screenSize.getHeight());
			 * driver.manage().window().setSize(dim);
			 */
			driver.manage().window().maximize();
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#moveBrowserToXYHeightWidth(int, int, int, int)
	 */
	@Override
	public void moveBrowserToXYHeightWidth(final int x, final int y, final int height, final int width) {
		String jscode = "window.resizeTo(" + height + "," + width + "); window.moveTo(" + x + "," + y
				+ "); window.focus();";
		try {
			logger.debug("Moving browser to X = " + x + ", Y = " + y + ", HEIGHT =  " + height + ", WIDTH = "
					+ width);
			this.executeScript(jscode);
		} catch (Exception e) {
			logger.error("Unable to move browser using javascript:\n" + jscode, e);
			throw new TestAutomationException("Unable to move browser using javascript", e);
		}
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#navigate()
	 */
	@Override
	public Navigation navigate() {
		Navigation navigate = null;
		if (isAvailable()) {
			navigate = driver.navigate();
		}
		return navigate;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#quit()
	 */
	@Override
	public void quit() {
		if (isAvailable()) {
			logger.debug("Quitting WebDriver");
			try {
				logger.trace("Quit WebDriver");
			} catch (Exception e) {
				logger.warn("Exception quiting driver: " + e.getLocalizedMessage());
			}

		}

	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver.Navigation#refresh()
	 */
	@Override
	public void refresh() {
		if (isAvailable()) {
			driver.navigate().refresh();
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#scrollToPageBottom()
	 */
	@Override
	public void scrollToPageBottom() {
		if (isAvailable()) {
			executeScript("window.scrollTo(0, document.body.scrollHeight)");
		}
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver#switchTo()
	 */
	@Override
	public TargetLocator switchTo() {
		TargetLocator locator = null;
		if (isAvailable()) {
			locator = driver.switchTo();
		}
		return locator;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#hasQuit()
	 */
	@Override
	public boolean hasQuit() {
		try {
			assertBrowserExists();
			return false;
		} catch (SessionNotFoundException snfe) {
			logger.error("SessionNotFoundException", snfe);
			return true;
		}
	}

	/**
	 * Assert browser exists.
	 */
	private void assertBrowserExists() {
		Set<String> handles = driver.getWindowHandles();
		logger.debug("Found " + handles.size() + " windows while checking if driver quit.");
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver.Navigation#back()
	 */
	@Override
	public void back() {
		if (isAvailable()) {
			driver.navigate().back();
		}

	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver.Navigation#forward()
	 */
	@Override
	public void forward() {
		if (isAvailable()) {
			driver.navigate().forward();
		}
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver.Navigation#to(java.lang.String)
	 */
	@Override
	public void to(final String url) {
		if (isAvailable()) {
			driver.navigate().to(url);
		}
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.WebDriver.Navigation#to(java.net.URL)
	 */
	@Override
	public void to(final URL url) {
		if (isAvailable()) {
			driver.navigate().to(url);
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTextToDisplay(java.lang.String)
	 */
	@Override
	public void waitForTextToDisplay(final String text) {
		waitForTextToDisplay(text, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTextToDisplay(java.lang.String, long)
	 */
	@Override
	public void waitForTextToDisplay(final String text, final long timeout) {
		logger.debug("Waiting for text: '" + text + "' to appear");
		if (isAvailable()) {
			IElementLocator locator = new ElementLocatorByDriver(this, ByUtil.getBy(UIType.Xpath,
					"//*[text()[contains(.,'" + text + "')]]"));
			IUIElement tempEle = new DefaultUIElement(this, locator, text);
			waitToBeDisplayed(tempEle, timeout);
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTextToHide(java.lang.String)
	 */
	@Override
	public void waitForTextToHide(final String text) {
		waitForTextToHide(text, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTextToHide(java.lang.String, long)
	 */
	@Override
	public void waitForTextToHide(final String text, final long timeout) {
		logger.debug("Waiting for text: '" + text + "' to appear");
		if (isAvailable()) {
			IElementLocator locator = new ElementLocatorByDriver(this, ByUtil.getBy(UIType.Xpath,
					"//*[text()[contains(.,'" + text + "')]]"));
			IUIElement tempEle = new DefaultUIElement(this, locator, text);
			waitToBeHidden(tempEle, timeout);
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBeAbsent(com.auxenta.test.auxtest.framework.core.IUIElement)
	 */
	@Override
	public void waitToBeAbsent(final IUIElement expected) {
		waitToBeAbsent(expected, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBeAbsent(com.auxenta.test.auxtest.framework.core.IUIElement, long)
	 */
	@Override
	public void waitToBeAbsent(final IUIElement expected, final long timeout) {
		if (expected != null) {
			if (isAvailable()) {
				expected.waitToBeAbsent(timeout);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBeDisplayed(com.auxenta.test.auxtest.framework.core.IUIElement)
	 */
	@Override
	public void waitToBeDisplayed(final IUIElement expected) {
		waitToBeDisplayed(expected, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBeDisplayed(com.auxenta.test.auxtest.framework.core.IUIElement, long)
	 */
	@Override
	public void waitToBeDisplayed(final IUIElement expected, final long timeout) {
		if (expected != null) {
			if (isAvailable()) {
				expected.waitToBeDisplayed(timeout);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBeHidden(com.auxenta.test.auxtest.framework.core.IUIElement)
	 */
	@Override
	public void waitToBeHidden(final IUIElement expected) {
		waitToBeHidden(expected, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBeHidden(com.auxenta.test.auxtest.framework.core.IUIElement, long)
	 */
	@Override
	public void waitToBeHidden(final IUIElement expected, final long timeout) {
		if (expected != null) {
			if (isAvailable()) {
				expected.waitToBeHidden(timeout);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBePresent(com.auxenta.test.auxtest.framework.core.IUIElement)
	 */
	@Override
	public void waitToBePresent(final IUIElement expected) {
		waitToBePresent(expected, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitToBePresent(com.auxenta.test.auxtest.framework.core.IUIElement, long)
	 */
	@Override
	public void waitToBePresent(final IUIElement expected, final long timeout) {
		if (expected != null) {
			if (isAvailable()) {
				expected.waitToBePresent(timeout);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#isAvailable()
	 */
	@Override
	public boolean isAvailable() {
		logger.trace("Checking if driver is available for driver == " + driver);
		if (driver == null) {
			return false;
		}
		boolean isAvailable = false;

		try {
			Set<String> handles = driver.getWindowHandles();
			logger.trace("Number of window handles: " + handles.size());
			isAvailable = true;
		} catch (UnreachableBrowserException ube) {
			logger.error("Unable to get list of windows. Browser is probably closed. "
					+ ube.getLocalizedMessage());//, ube);
			//throw new TestAutomationException("Unable to get list of windows. " + "Browser is probably closed", ube);
		} catch (SessionNotFoundException snf) {
			logger.error("Browser can't be used after quit() was called"); //, snf);
			//throw new TestAutomationException("Browser can't be used after quit() was called", snf);
		} catch (Exception e) {
			logger.error("Browser not available due to " + e.getLocalizedMessage(), e);
			throw new TestAutomationException("Browser not available", e);
		}
		return isAvailable;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTextOnPage(java.lang.String[], long)
	 */
	@Override
	public String[] waitForTextOnPage(final String[] expectedArray, final long timeout) {
		ExpectedCondition<Boolean> matchFound = new ExpectedCondition<Boolean>() {

			public Boolean apply(final WebDriver d) {
				return (whichTextIsOnPage(expectedArray).length > 0);
			}
		};
		WebDriverWait w = new WebDriverWait(driver, TimeUnit.SECONDS.convert(timeout, TimeUnit.MILLISECONDS));
		w.until(matchFound);
		return whichTextIsOnPage(expectedArray);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTextOnPage(java.lang.String[])
	 */
	@Override
	public String[] waitForTextOnPage(final String[] expectedArray) {
		return waitForTextOnPage(expectedArray, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#whichTextIsOnPage(java.lang.String[])
	 */
	@Override
	public String[] whichTextIsOnPage(final String[] expectedArray) {
		List<String> presentOnPage = new LinkedList<String>();
		for (String expected : expectedArray) {
			if (driver.getPageSource().contains(expected)) {
				presentOnPage.add(expected);

			}
		}
		String[] stringsOnPage = presentOnPage.toArray(new String[0]);
		logger.debug("Found '" + Arrays.toString(stringsOnPage) + "' text on page");
		return stringsOnPage;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getUIWindowLocator()
	 */
	@Override
	public IUIWindowLocator getUIWindowLocator() {
		if (windowLocator == null) {
			windowLocator = new WindowLocator(driver);
		}
		return windowLocator;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getUIObjectType()
	 */
	@Override
	public String getUIObjectType() {
		return IUIDriver.class.getSimpleName();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#isTextVisible(java.lang.String)
	 */
	@Override
	public boolean isTextVisible(final String text) {
		boolean result = false;
		if (isAvailable()) {
			IElementLocator locator = new ElementLocatorByDriver(this, ByUtil.getBy(UIType.Xpath,
					"//*[contains(text(),'" + text + "')]"));
			IUIElement tempEle = new DefaultUIElement(this, locator, text);
			result = tempEle.isDisplayed();
		}
		logger.debug(result ? "This text '" + text + "' is visible" : "This text '" + text
				+ "' is NOT visible");
		return result;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getUIFrameLocator()
	 */
	@Override
	public IUIFrameLocator getUIFrameLocator() {
		return new FrameLocator(this);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#
	 * waitForStallingElementToBeHidden(com.auxenta.test.auxtest.framework.core.IUIElement, long, long)
	 */
	@Override
	public void waitForStallingElementToBeHidden(final IUIElement stallingElement, final long timeToAppear,
			final long timeToBeHidden) {
		try {
			stallingElement.waitToBeDisplayed(timeToAppear);
			logger.debug("stalling element appeared.  will wait for it to disappear.  element: "
					+ stallingElement);
		} catch (TimeOutException toe) {
			logger.error("stalling element did not appear.  continuing with test.", toe);
			return;
		}
		stallingElement.waitToBeHidden(timeToBeHidden);
		logger.debug("stalling element disappear within timeout.  continuing with test.");

	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#
	 * waitForTitleToMatch(java.lang.String, com.auxenta.test.auxtest.framework.util.SearchScope)
	 */
	@Override
	public void waitForTitleToMatch(final String title, final SearchScope scope) {
		waitForTitleToMatch(title, scope, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTitleToMatch(java.lang.String, com.auxenta.test.auxtest.framework.util.SearchScope, long)
	 */
	@Override
	public void waitForTitleToMatch(final String title, final SearchScope scope, final long timeout) {
		long limit = System.currentTimeMillis() + timeout;
		String currentTitle = "";
		while (System.currentTimeMillis() < limit) {
			currentTitle = driver.getTitle();
			if (currentTitle != null) {
				switch (scope) {
				case CONTAINS:
					if (currentTitle.contains(title)) {
						return;
					}
					break;
				case EQUALS:
					if (currentTitle.equals(title)) {
						return;
					}
					break;
				}
			}
			if (timeout < config_verify_interval) {
				sleep(timeout);
			} else {
				sleep(config_verify_interval);
			}
		}
		currentTitle = driver.getTitle();
		if (currentTitle != null) {
			switch (scope) {
			case CONTAINS:
				if (currentTitle.contains(title)) {
					return;
				}
			case EQUALS:
				if (currentTitle.equals(title)) {
					return;
				}
			}
		}
		throw new TimeOutException("Expected title to " + scope.toString() + " '" + title + "' waited for "
				+ timeout + " ms. Instead found '" + currentTitle + "' as current title.");
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTitleToChange(java.lang.String)
	 */
	@Override
	public void waitForTitleToChange(final String previousTitle) {
		waitForTitleToChange(previousTitle, config_wait_timeout);
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#waitForTitleToChange(java.lang.String, long)
	 */
	@Override
	public void waitForTitleToChange(final String previousTitle, final long timeout) {

		ExpectedCondition<Boolean> nextPage = new ExpectedCondition<Boolean>() {

			public Boolean apply(final WebDriver d) {
				String newTitle = d.getTitle();
				logger.debug("Waiting for page title to change, current page title = " + newTitle);
				boolean titleChanged = !newTitle.equals(previousTitle);
				return Boolean.valueOf(titleChanged);
			}
		};
		try {
			WebDriverWait wait = new WebDriverWait(getWebDriver(), TimeUnit.SECONDS.convert(timeout,
					TimeUnit.MILLISECONDS));
			wait.until(nextPage);
			logger.info("Page title has changed from '" + previousTitle + "' to '" + this.getTitle() + "'");
		} catch (RuntimeException rte) {
			throw new TimeOutException("Timed out waiting for Page Title to change from '" + previousTitle
					+ "'.Caught Exception:" + rte.getLocalizedMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#isTextPresentInTitle(java.lang.String)
	 */
	@Override
	public boolean isTextPresentInTitle(final String text) {
		String currentTitle = this.getTitle();
		if (currentTitle != null) {
			return currentTitle.contains(text);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#getXPath(org.openqa.selenium.WebElement)
	 */
	@Override
	public String getXPath(final WebElement element) {
		logger.trace("Calculating XPATH for element " + element);
		String jscript = "function getPathTo(node) {" + "  var stack = [];"
				+ "  while(node.parentNode !== null) {" + "    stack.unshift(node.tagName);"
				+ "    node = node.parentNode;" + "  }" + "  return stack.join('/');" + "}"
				+ "return getPathTo(arguments[0]);";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Object executeResult = null;
		try {
			executeResult = js.executeScript(jscript, element);
		} catch (Exception e) {
			logger.error("Failed to get XPATH for element " + element, e);
			throw new TestAutomationException("Failed to get XPATH for element", e);
		}
		return executeResult == null ? "" : executeResult.toString();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.core.IUIDriver#sendKeysToBrowser(java.lang.CharSequence[])
	 */
	@Override
	public void sendKeysToBrowser(final CharSequence... keysToSend) {
		Actions actions = new Actions(this.getWebDriver());
		actions.sendKeys(keysToSend).build().perform();
	}

	/**
	 * Handle all the error conditions including unsupported and null pointer.
	 *
	 * @param timeout the timeout
	 * @param units the units
	 */
	public void setPageLoadTimeout(final long timeout, final TimeUnit units) {
		logger.debug("attempting driver.manage().timeouts().pageLoadTimeout(timeout, units);");
		try {
			driver.manage().timeouts().pageLoadTimeout(timeout, units);
		} catch (Exception e) {
			logger.warn("Unable to set pageLoadTimeout. ", e);
			throw new TestAutomationException("Unable to set pageLoadTimeout", e);
		}
	}
	
	public void waitForAjax(){
		
	}
	
	public void waitTillAdfPageLoadComplete(){
		
	}
	
	public void waitTillPageLoadComplete(){
		
	}
	
	@Deprecated
	public IUIElement FindByTextJQuery(IUIDriver uiDriver, String string1, String string2){
		IUIElement ele = uiDriver.findUIElement(UIType.Xpath, "div");
		return ele;
	}
	}
