/**
 * 
 */
package com.auxenta.framework.drivers;

import java.util.List;

import org.openqa.selenium.WebDriver.TargetLocator;

import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.drivers.FrameLocator.FrameLocatorType;
import com.auxenta.framework.util.SearchScope;

/**
 *
 * Frame related actions
 *
 * <li><b>Present</b> - Element in DOM of the frame
 * 
 * <li><b>Displayed</b> - Element is in DOM of the frame and is visible
 *
 * <li><b>Wait</b> - Waits for something to happen and THROWS TimeOutException on failure
 *
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */

public interface IUIFrameLocator {

    /**
     * Switch to frame by name or id
     *
     * @param frameName
     * @return true if switched to frame, false otherwise
     */
    boolean selectFrame(String frameName);

    /**
     * Switch to frame by name or id
     *
     * @param frameName
     * @param timeout
     * @return true if switched to frame, false otherwise
     */
    boolean selectFrame(String frameName, long timeout);

    /**
     * List of frames on current page
     *
     * @return
     */
    public List<IUIElement> getFramesInPage();

    /**
     * Switch to frame by frames title text
     *
     * @param titleName
     *            - title inside the frame
     * @param searchScope
     * @return driver will switch to frame
     */
    void switchToFrameByTitle(String titleName, SearchScope searchScope);

    /**
     * Try to switch to the frame with matching text if it exists, otherwise keep trying until
     * timeout. When this method completes, the UIDriver will be pointing to the frame with the
     * matching text.<br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()} and therefore will not work
     * with nested frames</i>
     *
     * @param frameNameOrID
     * @param textInFrame
     */
    void waitForTextInFrameToBePresentAndSwitch(String frameNameOrID, String textInFrame);

    /**
     * Wait for text to be present in frames DOM and switch to that frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()} and therefore will not work
     * with nested frames</i>
     *
     * @param frameNameOrID
     * @param textInFrame
     * @param timeout
     */
    void waitForTextInFrameToBePresentAndSwitch(
            String frameNameOrID,
            String textInFrame,
            long timeout);

    /**
     * Get title of the frame
     * 
     * @return
     */
    String getTitle();

    /**
     * Wait for element to be present in frames DOM and switch to that frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()} and therefore will not work
     * with nested frames</i>
     *
     * @param frameNameOrID
     * @param uiElementInFrame
     * @param timeout
     */
    void waitForElementToBePresentAndSwitch(
            String frameNameOrID,
            IUIElement uiElementInFrame,
            long timeout);

    /**
     * Wait for element to be present in frames DOM and switch to that frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()} and therefore will not work
     * with nested frames</i>
     *
     * @param frameNameOrID
     * @param uiElementInFrame
     */
    void waitForElementToBePresentAndSwitch(String frameNameOrID, IUIElement uiElementInFrame);

    /**
     * Wait for element to be displayed in frames DOM and switch to that frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()} and therefore will not work
     * with nested frames</i>
     *
     * @param frameNameOrID
     * @param uiElementInFrame
     * @param timeout
     */
    void waitForElementToBeDisplayedAndSwitch(
            String frameNameOrID,
            IUIElement uiElementInFrame,
            long timeout);

    /**
     * Wait for element to be displayed in frames DOM and switch to that frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()} and therefore will not work
     * with nested frames</i>
     *
     * @param frameNameOrID
     * @param uiElementInFrame
     */
    void waitForElementToBeDisplayedAndSwitch(String frameNameOrID, IUIElement uiElementInFrame);

    /**
     * Starts with the first frame in array then continues going deeper until no frames remains in
     * array and wait for element to be displayed in the last frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()}, therefore you must specify
     * all frames in array starting with outer (first frame) in the page</i>
     *
     * @param frameNamesOrIDs
     * @param uiElementInFrame
     * @param timeout
     */
    void waitForElementToBeDisplayedAndSwitchToNestedFrame(
            String[] frameNamesOrIDs,
            IUIElement uiElementInFrame,
            long timeout);

    /**
     * Starts with the first frame in array then continues going deeper until no frames remains in
     * array and wait for element to be displayed in the last frame <br>
     * <br>
     * <i>Note: This method calls {@link TargetLocator#defaultContent()}, therefore you must specify
     * all frames in array starting with outer (first frame) in the page</i>
     *
     * @param frameNamesOrIDs
     * @param uiElementInFrame
     */
    void waitForElementToBeDisplayedAndSwitchToNestedFrame(
            String[] frameNamesOrIDs,
            IUIElement uiElementInFrame);

    /**
     * Will recursively search through all frames and switch to the specified frame
     *
     * @param frameNameOrID
     * @param nameOrID
     */
    void waitForAndSwitchToNestedFrame(String frameNameOrID, FrameLocatorType nameOrID);

    /**
     * Will recursively search through all frames and switch to the specified frame
     *
     * @param frameNameOrID
     * @param nameOrID
     * @param timeout
     */
    void waitForAndSwitchToNestedFrame(String frameNameOrID, FrameLocatorType nameOrID, long timeout);
    
    

}
