/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.drivers;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.ITestContext;

import com.auxenta.framework.core.FWObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.tools.DateTools;
import com.auxenta.framework.tools.FileHelper;

/**
 * The Class DebugUIDriver.
 *
 * @author Niroshen Landerz
 * @date Sept 6th 2014
 */
public class DebugUIDriver extends FWObject {

    /** The logger. */
    private static Logger logger = Logger.getLogger(DebugUIDriver.class);

    /** The output dir. */
    private File outputDir;

    /** The screen shots dir name. */
    private String screenShotsDirName = "/screenShots";

    /** The driver. */
    private WebDriver driver;

    /**
     * If you are running your tests using TestNG, use this constructor. ITestContext is used to get
     * reports directory
     *
     * @param context the context
     * @param webDriver the web driver
     */
    public DebugUIDriver(final ITestContext context, final WebDriver webDriver) {
        File dir = new File(context.getOutputDirectory());
        outputDir = new File(dir.getParent() + screenShotsDirName);
        outputDir.mkdirs();
        if (webDriver instanceof IUIDriver) {
            setUIDriver((IUIDriver) webDriver);
            driver = ((IUIDriver) webDriver).getWebDriver();
        } else {
            this.driver = webDriver;
        }
    }

    /**
     * Instantiates a new debug ui driver.
     *
     * @param screenShotsDir            - directory where you want to store generated screenshots and .html files
     * @param webDriver the web driver
     */
    public DebugUIDriver(final File screenShotsDir, final WebDriver webDriver) {
        outputDir = screenShotsDir;
        outputDir.mkdirs();
        if (webDriver instanceof IUIDriver) {
            setUIDriver((IUIDriver) webDriver);
            driver = ((IUIDriver) webDriver).getWebDriver();
        } else {
            this.driver = webDriver;
        }
    }

    /**
     * Capture browser using {@link TakesScreenshot} to a file.
     *
     * @param fileName            - Base file name, file extension and time stamp will be added automatically
     * @return generated .png file
     */
    public File captureBrowser(final String fileName) {
        File dest = null;
        if (driver instanceof TakesScreenshot) {
            dest = new File(generateFilePath(fileName + ".png"));
            File scrFile = null;
            try {
                scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            } catch (WebDriverException e1) {
                logger.error("Failed to capture browser but not throwing the exceptoin as it should not fail the continuation" + e1.getMessage());
                //throw new TestAutomationException("Failed to capture browser", e1);
            }
            try {
                FileHelper.copyFile(scrFile, dest);
            } catch (IOException e) {
                logger.error("Failed to copy file", e);
                throw new TestAutomationException("Failed to copy file", e);
            }
        } else {
            logger.debug("Failed to capture screenshot, screenshot is not supported for current web driver");
        }
        return dest;
    }

    /**
     * Capture browser using Augmenter to add screenshot capability to a RemoteWebDriver and then
     * use {@link TakesScreenshot} to a file.
     *
     * @param fileName            - Base file name, file extension and time stamp will be added automatically
     * @return generated .png file
     */
    public File captureRemoteBrowser(final String fileName) {
        File dest = null;
        if (driver instanceof RemoteWebDriver) {
            dest = new File(generateFilePath(fileName + ".png"));
            File scrFile;
            try {
                driver = new Augmenter().augment(driver);
                scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            } catch (WebDriverException e1) {
                logger.error("Failed to capture remote browser", e1);
                throw new TestAutomationException("Failed to capture remote browser", e1);
            }
            try {
                FileHelper.copyFile(scrFile, dest);
            } catch (IOException e) {
                logger.error("Failed to copy file", e);
                throw new TestAutomationException("Failed to copy file", e);
            }
        } else {
            logger.debug("Failed to capture screenshot, screenshot is not supported for current web driver");
        }
        return dest;
    }

    /**
     * Capture HTML source to a file.
     *
     * @param fileName            - Base file name, file extension and time stamp will be added automatically
     * @return generated .html file
     */
    public File captureHTML(final String fileName) {
        if (driver != null) {
            String htmlLogFilename = generateFilePath(fileName + ".html");
            String html = "Source not found";

            try {
                html = driver.getPageSource();
            } catch (WebDriverException e) {
                logger.debug(e);
                try {
                    driver.switchTo().defaultContent();
                } catch (WebDriverException e1 ) 
                {
                    logger.error("Failed to capture HTML", e1);
                    logger.error("WebDriver exception is swallowed as it should not failed writing the remainder of the test results" + e1.getMessage());
                    //throw new TestAutomationException("Failed to capture HTML", e1);
                } 
                
                html = driver.getPageSource();
            }
            try {
                logger.debug("DEBUG: writing HTML of problem page to: " + htmlLogFilename);
                FileWriter htmlLog = new FileWriter(htmlLogFilename);
                htmlLog.write(html);
                htmlLog.close();
                logger.debug("DEBUG: wrote HTML of problem page to: " + htmlLogFilename);
            } catch (IOException e1) {
                logger.error("IOException", e1);
                throw new TestAutomationException("IOException", e1);
            }
            return new File(htmlLogFilename);
        } else {
            return null;
        }
    }

    /**
     * Capture desktop using {@link Robot} to a file.
     *
     * @param fileName            - Base file name, file extension and time stamp will be added automatically
     * @return generated .png file
     */
    public File captureDesktop(final String fileName) {
        String deskTopScreenshotFileName = generateFilePath(fileName + ".png");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);
        Robot robot;
        File imageFile = null;
        try {
            robot = new Robot();
            BufferedImage image = robot.createScreenCapture(screenRectangle);
            imageFile = new File(deskTopScreenshotFileName);
            logger.debug("Writing image to " + imageFile.getAbsolutePath());
            ImageIO.write(image, "png", imageFile);
            logger.debug("Captured screenshot of the desktop to " + imageFile.getAbsolutePath());
        } catch (AWTException e) {
            logger.error("AWTException", e);
            throw new TestAutomationException("AWTException", e);
        } catch (IOException ioe) {
        	 logger.error("IOException", ioe);
             throw new TestAutomationException("IOException", ioe);
        }
        return imageFile;
    }

    /**
     * Gets the output dir.
     *
     * @return the output dir
     */
    public String getOutputDir() {
        return outputDir.getAbsolutePath();
    }

    /**
     * Prints information about all open windows to the logger.debug
     */
    public void debugWindows() {
        logger.debug("Listing window handles");
        Set<String> handles = null;
        try {
            handles = driver.getWindowHandles();
        } catch (UnreachableBrowserException ube) {
        	logger.error("UnreachableBrowserException", ube);
            throw new TestAutomationException("UnreachableBrowserException", ube);
        }
        logger.debug("Found " + handles.size() + " window handles");
        for (String handle : handles) {
            logger.debug("switching to " + handle);
            try {
                driver.switchTo().window(handle);
                logger.debug("title: " + driver.getTitle());
                logger.debug("url: " + driver.getCurrentUrl());
            } catch (NoSuchWindowException nsw) {
                logger.error(nsw.getLocalizedMessage(), nsw);
            }
        }
        logger.debug("switching to active element");
        try {
            driver.switchTo().activeElement();
        } catch (NoSuchWindowException nsw) {
            logger.error(nsw.getLocalizedMessage(), nsw);
        }
        logger.debug("switching to defaultContent");
        try {
            driver.switchTo().defaultContent();
        } catch (NoSuchWindowException nsw) {
            logger.error(nsw.getLocalizedMessage(), nsw);
        }
    }

    /**
     * Gets the page source.
     *
     * @return the page source
     */
    public String getPageSource() {
        try {
            return driver.getPageSource();
        } catch (Exception e) {
            try {
                logger.warn("Unable to get page source" + e.getLocalizedMessage());
                logger.debug("Trying main window after failed get source");
                String baseFileName = generateFilePath("noSource");
                captureBrowser(baseFileName);
                driver.switchTo().defaultContent();
                return driver.getPageSource();
            } catch (Exception e2) {
                logger.error("Unable to get page source from default content either " + e2
                        .getLocalizedMessage(), e2);
                return "Unable to get page source from default content either " + e2
                        .getLocalizedMessage();
            }
        }
    }

    /**
     * Generate file path.
     *
     * @param fileName the file name
     * @return Absolute path for a new file
     */
    private String generateFilePath(final String fileName) {
        return outputDir.getAbsolutePath() + "/" + DateTools
                .getTimeStamp("yyyy.MM.dd_'at'_HH.mm.ss.SSS") + "_" + fileName;
    }

    /**
     * There are some contexts where log4j is silent. This tries to get around those limitations.
     *
     * @param level the level
     * @param message the message
     */
    public static void log(final Level level, final String message) {
        logger.log(level, message);
    }

    /**
     * Quit.
     */
    public void quit() {
        if (uiDriver != null) {
            try {
                uiDriver.quit();
            } catch (Exception e) {
                logger.error("Unable to quit uiDriver due to " + e.getLocalizedMessage(), e);
                throw new TestAutomationException("Unable to quit uiDriver", e);
            }
        } else {
            logger.warn("Called quit() on debugDriver containing null uiDriver");
        }
    }
}
