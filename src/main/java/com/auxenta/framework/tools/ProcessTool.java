/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;

import com.auxenta.framework.tools.os.WindowsProcessTool;


/**
 * The Class ProcessTool.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd, 2014
 */

public class ProcessTool {

    /** The tools. */
    private static HashMap<Platform, IProcessTool> tools = new HashMap<Platform, IProcessTool>();

    /** The logger. */
    private static Logger logger = Logger.getLogger(ProcessTool.class);

    /** The current platform. */
    private static Platform currentPlatform = Platform.getCurrent();

    /** The current tool. */
    private IProcessTool currentTool;

	/**
	 * Instantiates a new process tool.
	 */
	public ProcessTool() {
		tools.put(Platform.VISTA, (IProcessTool) new WindowsProcessTool());
        tools.put(Platform.WINDOWS, (IProcessTool) new WindowsProcessTool());
        currentTool = tools.get(currentPlatform);
    }


    /**
     * Kill leftover processes.
     */
    public  void killLeftoverProcesses() {
        killProcess("iexplorer.exe");
        killProcess("git.exe");
        killProcess("ssh.exe");
    }

    /**
     * Kill process.
     *
     * @param processName the process name
     */
    public void killProcess(final String processName) {
        if (currentTool != null) {
            currentTool.killProcess(processName);
        } else {
            logger.warn("There was no ProcessTool for " + currentPlatform);
        }
    }

    /**
     * Run command.
     *
     * @param command the command
     */
    public  void runCommand(final String command) {
        currentTool.runCommand(command);
    }

    /**
     * List processes.
     *
     * @return the list
     */
    public  List<String> listProcesses() {
        if (currentTool != null) {
            return currentTool.listProcesses();
        } else {
            return getNoToolList();
        }
    }

    /**
     * Filter processes.
     *
     * @param processName the process name
     * @return the list
     */
    public  List<String> filterProcesses(final String processName) {
        if (currentTool != null) {
            return currentTool.filterProcesses(processName);
        } else {
            logger.warn("There was no ProcessTool for " + currentPlatform);
            return getNoToolList();
        }
    }

    /**
     * Gets the no tool list.
     *
     * @return the no tool list
     */
    private static List<String> getNoToolList() {
        String warning = "There was no ProcessTool for " + currentPlatform;
        logger.warn(warning);
        ArrayList<String> noToolList = new ArrayList<String>();
        noToolList.add(warning);
        return noToolList;
    }

    /**
     * Count processes.
     *
     * @return the hash map
     */
    public  HashMap<String, Integer> countProcesses() {
        HashMap<String, Integer> counts = new HashMap<String, Integer>();
        List<String> processList = listProcesses();
        for (String process : processList) {
            if (counts.containsKey(process)) {
                Integer count = counts.get(process);
                counts.put(process, new Integer(count + 1));
            } else {
                counts.put(process, new Integer(1));
            }
        }
        return counts;
    }

    /**
     * Log process counts.
     *
     * @param processCountThreshold the process count threshold
     */
    public  void logProcessCounts(final int processCountThreshold) {
        HashMap<String, Integer> counts = countProcesses();
        for (String process : counts.keySet()) {
            if (counts.get(process) > processCountThreshold) {
                logger.warn("Threshold " + processCountThreshold + " exceeded for process " + process + ":" + counts
                        .get(process));
            } else {
                logger.debug("process count for " + process + ":" + counts.get(process));
            }
        }
    }

    /**
     * Force kill process.
     *
     * @param processName the process name
     */
    public  void forceKillProcess(final String processName) {
        if (currentTool != null) {
            currentTool.forceKillProcess(processName);
        } else {
            logger.warn("There was no ProcessTool for " + currentPlatform);
        }
    }
}
