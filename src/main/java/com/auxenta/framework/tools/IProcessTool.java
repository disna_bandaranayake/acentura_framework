/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

/**
 * @author Niroshen Landerz
 * @date Sept 4th 2014
 */

import java.util.List;

/**
 * The Interface IProcessTool.
 */
public interface IProcessTool {

	/** The Constant ERROR_PREFIX. */
	static final String ERROR_PREFIX = "ERROR: ";

	/**
	 * List processes.
	 *
	 * @return the list
	 */
	List<String> listProcesses();

	/**
	 * Filter processes.
	 *
	 * @param processName
	 *            the process name
	 * @return the list
	 */
	List<String> filterProcesses(String processName);

	/**
	 * Kill process.
	 *
	 * @param processName
	 *            the process name
	 */
	void killProcess(String processName);

	/**
	 * Force kill process.
	 *
	 * @param processName
	 *            the process name
	 */
	void forceKillProcess(String processName);

	/**
	 * Run command.
	 *
	 * @param command
	 *            the command
	 * @return the list
	 */
	List<String> runCommand(String command);

}
