/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools.os;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.remote.SessionNotFoundException;

import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.FrameworkException;
import com.auxenta.framework.tools.IProcessTool;

/**
 * The Class WindowsProcessTool.
 *
 * @author Niroshen Landerz
 * @date Aug 28th 2014
 */

public class WindowsProcessTool implements IProcessTool {

	/** The Constant CMD_KILL_WINDOWS_PREFIX. */
	private static final String CMD_KILL_WINDOWS_PREFIX = "taskkill /IM ";

	/** The Constant CMD_FORCE_KILL_WINDOWS_PREFIX. */
	private static final String CMD_FORCE_KILL_WINDOWS_PREFIX = "taskkill /F /T /IM ";

	/** The Constant CMD_KILL_WINDOWS_SUFFIX. */
	private static final String CMD_KILL_WINDOWS_SUFFIX = "* /f /t ";

	/** The Constant CMD_LIST_WINDOWS_PREFIX. */
	private static final String CMD_LIST_WINDOWS_PREFIX = "tasklist ";

	/** The Constant CMD_LIST_WINDOWS_NAME_PREFIX. */
	private static final String CMD_LIST_WINDOWS_NAME_PREFIX = " /fi \"Imagename eq ";

	/** The Constant CMD_LIST_WINDOWS_NAME_SUFFIX. */
	private static final String CMD_LIST_WINDOWS_NAME_SUFFIX = "\"";

	/** The logger. */
	private Logger logger = Logger.getLogger(this.getClass());

	/** The list headers. */
	private static ArrayList<String> listHeaders;

	/**
	 * Instantiates a new windows process tool.
	 */
	public WindowsProcessTool() {
		listHeaders = new ArrayList<String>();
		listHeaders.add("");
		listHeaders.add("Image Name                     PID Session Name        Session#    Mem Usage");
		listHeaders.add("========================= ======== ================ =========== ============");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.IProcessTool#filterProcesses(java.lang.String)
	 */
	@Override
	public List<String> filterProcesses(final String processName) {
		String listCommand = CMD_LIST_WINDOWS_PREFIX + CMD_LIST_WINDOWS_NAME_PREFIX + processName
				+ CMD_LIST_WINDOWS_NAME_SUFFIX;
		logger.info("Filtering list of processes with OS command: " + listCommand);
		List<String> output = runCommand(listCommand);
		output.remove(listCommand);
		return output;
	}

	/**
	 * Filter processes direct.
	 *
	 * @param processName
	 *            the process name
	 * @return the list
	 */
	public List<String> filterProcessesDirect(final String processName) {
		String listCommand = CMD_LIST_WINDOWS_PREFIX + CMD_LIST_WINDOWS_NAME_PREFIX + processName
				+ CMD_LIST_WINDOWS_NAME_SUFFIX;
		logger.info("Filtering list of processes with OS command: " + listCommand);
		List<String> output = runCommand(listCommand);
		output.remove(listCommand);
		return output;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.auxenta.test.tools.IProcessTool#listProcesses()
	 */
	@Override
	public List<String> listProcesses() {
		String listCommand = CMD_LIST_WINDOWS_PREFIX;
		logger.info("Listing processes with OS command: " + listCommand);
		List<String> output = runCommand(listCommand);
		for (String header : listHeaders) {
			output.remove(header);
		}
		ArrayList<String> processNames = new ArrayList<String>();
		for (String log : output) {
			processNames.add(log.substring(0, log.indexOf(" ")));
		}
		return processNames;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.auxenta.test.tools.IProcessTool#killProcess(java.lang.String)
	 */
	@Override
	public void killProcess(final String processName) {
		logger.info("Killing browser processes with OS command...");
		runCommand(CMD_KILL_WINDOWS_PREFIX + " " + processName + CMD_KILL_WINDOWS_SUFFIX);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.IProcessTool#forceKillProcess(java.lang.String)
	 */
	@Override
	public void forceKillProcess(final String processName) {
		logger.info("Forced killing browser processes with OS command...");
		runCommand(CMD_FORCE_KILL_WINDOWS_PREFIX + " " + processName);
	}

	/**
	 * Force kill process direct.
	 *
	 * @param processName
	 *            the process name
	 */
	public void forceKillProcessDirect(final String processName) {
		logger.info("Forced killing browser processes with OS command...");
		runCommand(CMD_FORCE_KILL_WINDOWS_PREFIX + " " + processName);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.auxenta.test.tools.IProcessTool#runCommand(java.lang.String)
	 */
	public List<String> runCommand(final String command) {
		ArrayList<String> output = new ArrayList<String>();
		BufferedReader stdInput = null;
		BufferedReader stdError = null;

		try {

			logger.debug("Running command: " + command);
			// using the Runtime exec method:
			Process commandProcess = Runtime.getRuntime().exec(command);

			stdInput = new BufferedReader(new InputStreamReader(commandProcess.getInputStream()));

			stdError = new BufferedReader(new InputStreamReader(commandProcess.getErrorStream()));

			// read the output from the command
			logger.debug("Here is the standard output of the command:\n");
			String cmdStdOut = "no output";
			while ((cmdStdOut = stdInput.readLine()) != null) {
				logger.debug(cmdStdOut);
				output.add(cmdStdOut);
			}

			// read any errors from the attempted command
			logger.info("Here is the standard error of the command (if any):\n");
			String cmdStdErr = "no error messages";
			while ((cmdStdErr = stdError.readLine()) != null) {
				logger.warn(cmdStdErr);
				output.add(ERROR_PREFIX + cmdStdErr);
			}

		} catch (IOException e) {
			logger.warn("Exception While trying to execute:\n " + command + " " + e.getLocalizedMessage());
		} finally {
			if (stdInput != null) {
				try {
					stdInput.close();
				} catch (IOException e) {
					logger.error("Exception trying to close Standard Input from command:\n "
				+ command + "\n ", e);
					throw new FrameworkException("IOException", e);
				}
			}
			if (stdError != null) {
				try {
					stdError.close();
				} catch (IOException e) {
					logger.warn("Exception trying to close Standard Error from command:\n "
				+ command + "\n ", e);
					throw new FrameworkException("IOException", e);
				}
			}
		}
		return output;
	}

	public void FirefoxAndChromeKiller(IConfig configObject, String browser){
		try {
			if (configObject.getValue("browser").equalsIgnoreCase("chrome")){
				while (filterProcesses("chrome.exe").size() > 1) {
					killProcess("chrome.exe");
				}
				
				while (filterProcesses("chromedriver.exe").size() > 1) {
					forceKillProcess("chromedriver.exe");
				}
			}
			
			if (configObject.getValue("browser").equalsIgnoreCase("internetexplorer")){
				while (filterProcesses("iexplore.exe").size() > 1) {
					killProcess("iexplore.exe");
				}
				
				while (filterProcesses("IEDriverServer.exe").size() > 1) {
					forceKillProcess("IEDriverServer.exe");
				}
			}
		} catch (SessionNotFoundException sn){
			logger.info("Session already been closed.");
			logger.info(sn.getMessage());
		}
		catch (Exception e) {
			logger.warn("While listing processes got exception ", e);
			throw new TestAutomationException("While listing processes got exception ", e);
		}
	}
}
