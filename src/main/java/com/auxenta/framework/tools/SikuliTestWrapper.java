/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.sikuli.api.DesktopScreenRegion;
import org.sikuli.api.ImageTarget;
import org.sikuli.api.Relative;
import org.sikuli.api.ScreenRegion;
import org.sikuli.api.Target;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopMouse;

/**
 * This wrapper class provides the basic actions, which can be performed with
 * Sikuli-API jar. Please make sure to add below POM dependency to your project.
 *
 * <dependency> <groupId>org.sikuli</groupId>
 * <artifactId>sikuli-api</artifactId> <version>1.1.0</version> </dependency>
 *
 * NOTE: Exceptions are caught and returned as a boolean output, if you don't
 * like it, feel free to remove the exception handling and let the exception
 * propagate and be handled by the caller.
 *
 * It is designed this way for simplicity, so the user don't have to explicitly
 * deal with exceptions in their test scripts.
 *
 *
 * @author Niroshen Landerz
 *
 */
public class SikuliTestWrapper implements SikuliElement {

	/** The logger. */
	private static Logger logger = Logger.getLogger(SikuliTestWrapper.class);

	/** The mouse. */
	private static Mouse mouse = new DesktopMouse();

	/** The Constant defaultTimeout. */
	private static final int defaultTimeout = 5000;

	/** The Constant defaultImageResolution. */
	private static final ImageResolutionLevel defaultImageResolution = ImageResolutionLevel.LOW;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElement(java.lang.String)
	 */
	@Override
	public boolean clickOnElement(final String imagePath) {
		return clickOnElement(imagePath, defaultImageResolution, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElement(java.lang.String,
	 * com.auxenta.test.tools.ImageResolutionLevel)
	 */
	@Override
	public boolean clickOnElement(final String imagePath, final ImageResolutionLevel sensitivity) {
		return clickOnElement(imagePath, sensitivity, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElement(java.lang.String,
	 * int)
	 */
	@Override
	public boolean clickOnElement(final String imagePath, final int timeoutInMilliSeconds) {
		return clickOnElement(imagePath, defaultImageResolution, timeoutInMilliSeconds);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElement(java.lang.String,
	 * com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public boolean clickOnElement(final String imagePath, final ImageResolutionLevel sensitivity,
			final int timeoutInMilliseconds) {
		boolean result;
		try {
			mouse.click(getScreenRegion(imagePath, sensitivity, timeoutInMilliseconds).getCenter());
			result = true;
		} catch (Exception ignored) {
			logger.error("Exception(ignoreble exception)", ignored);
			result = false;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementCoordinates(java.lang
	 * .String, int, int)
	 */
	@Override
	public boolean clickOnElementCoordinates(final String imagePath, final int xAxis, final int yAxis) {
		return clickOnElementCoordinates(imagePath, xAxis, yAxis, defaultImageResolution, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementCoordinates(java.lang
	 * .String, int, int, int)
	 */
	@Override
	public boolean clickOnElementCoordinates(final String imagePath, final int xAxis, final int yAxis,
			final int timeoutInMilliseconds) {
		return clickOnElementCoordinates(imagePath, xAxis, yAxis, defaultImageResolution,
				timeoutInMilliseconds);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementCoordinates(java.lang
	 * .String, int, int, com.auxenta.test.tools.ImageResolutionLevel)
	 */
	@Override
	public boolean clickOnElementCoordinates(final String imagePath, final int xAxis, final int yAxis,
			final ImageResolutionLevel sensitivity) {
		return clickOnElementCoordinates(imagePath, xAxis, yAxis, defaultImageResolution, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementCoordinates(java.lang
	 * .String, int, int, com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public boolean clickOnElementCoordinates(final String imagePath, final int xAxis, final int yAxis,
			final ImageResolutionLevel sensitivity, final int timeoutInMilliseconds) {
		try {
			mouse.click(getScreenRegion(imagePath, sensitivity, timeoutInMilliseconds)
					.getRelativeScreenLocation(xAxis, yAxis));
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the given x and y screen coordinates. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerRightCorner(java
	 * .lang.String)
	 */
	@Override
	public boolean clickOnElementLowerRightCorner(final String imagePath) {
		return clickOnElementLowerRightCorner(imagePath, defaultImageResolution, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerRightCorner(java
	 * .lang.String, com.auxenta.test.tools.ImageResolutionLevel)
	 */
	@Override
	public boolean clickOnElementLowerRightCorner(final String imagePath,
			final ImageResolutionLevel sensitivity) {
		return clickOnElementLowerRightCorner(imagePath, sensitivity, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerRightCorner(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnElementLowerRightCorner(final String imagePath, final int timeoutInMilliseconds) {
		return clickOnElementLowerRightCorner(imagePath, defaultImageResolution, timeoutInMilliseconds);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerRightCorner(java
	 * .lang.String, com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public boolean clickOnElementLowerRightCorner(final String imagePath,
			final ImageResolutionLevel sensitivity, final int timeoutInMilliseconds) {
		try {
			mouse.click(getScreenRegion(imagePath, sensitivity, timeoutInMilliseconds).getLowerRightCorner());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the lower right corner of specified screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerLeftCorner(java
	 * .lang.String)
	 */
	@Override
	public boolean clickOnElementLowerLeftCorner(final String imagePath) {
		return clickOnElementLowerLeftCorner(imagePath, defaultImageResolution, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerLeftCorner(java
	 * .lang.String, com.auxenta.test.tools.ImageResolutionLevel)
	 */
	@Override
	public boolean clickOnElementLowerLeftCorner(final String imagePath,
			final ImageResolutionLevel sensitivity) {
		return clickOnElementLowerLeftCorner(imagePath, sensitivity, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerLeftCorner(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnElementLowerLeftCorner(final String imagePath, final int timeoutInMilliseconds) {
		return clickOnElementLowerLeftCorner(imagePath, defaultImageResolution, timeoutInMilliseconds);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementLowerLeftCorner(java
	 * .lang.String, com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public boolean clickOnElementLowerLeftCorner(final String imagePath,
			final ImageResolutionLevel sensitivity, final int timeoutInMilliseconds) {
		try {
			mouse.click(getScreenRegion(imagePath, sensitivity, timeoutInMilliseconds).getLowerLeftCorner());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the lower left corner of specified screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementUpperLeftCorner(java
	 * .lang.String)
	 */
	@Override
	public boolean clickOnElementUpperLeftCorner(final String imagePath) {
		return clickOnElementUpperLeftCorner(imagePath, defaultImageResolution, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementUpperLeftCorner(java
	 * .lang.String, com.auxenta.test.tools.ImageResolutionLevel)
	 */
	@Override
	public boolean clickOnElementUpperLeftCorner(final String imagePath,
			final ImageResolutionLevel sensitivity) {
		return clickOnElementUpperLeftCorner(imagePath, sensitivity, defaultTimeout);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementUpperLeftCorner(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnElementUpperLeftCorner(final String imagePath, final int timeoutInMilliseconds) {
		return clickOnElementUpperLeftCorner(imagePath, defaultImageResolution, timeoutInMilliseconds);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnElementUpperLeftCorner(java
	 * .lang.String, com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public boolean clickOnElementUpperLeftCorner(final String imagePath,
			final ImageResolutionLevel sensitivity, final int timeoutInMilliseconds) {
		try {
			mouse.click(getScreenRegion(imagePath, sensitivity, timeoutInMilliseconds).getUpperLeftCorner());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the upper left corner of specified screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnRelativeToRightElement(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnRelativeToRightElement(final String imagePath, final int relativeToRightValue) {
		return clickOnRelativeToRightElement(imagePath, relativeToRightValue, 5000);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnRelativeToRightElement(java
	 * .lang.String, int, int)
	 */
	@Override
	public boolean clickOnRelativeToRightElement(final String imagePath, final int relativeToRightValue,
			final int timeoutInMilliSeconds) {
		try {
			mouse.click(Relative.to(getScreenRegion(imagePath, defaultImageResolution, defaultTimeout))
					.right(relativeToRightValue).getScreenRegion().getCenter());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the relative to right screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnRelativeToLeftElement(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnRelativeToLeftElement(final String imagePath, final int relativeToLeftValue) {
		try {
			mouse.click(Relative.to(getScreenRegion(imagePath, defaultImageResolution, defaultTimeout))
					.left(relativeToLeftValue).getScreenRegion().getCenter());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the relative to left screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnRelativeToAboveElement(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnRelativeToAboveElement(final String imagePath, final int relativeToAboveValue) {
		try {
			mouse.click(Relative.to(getScreenRegion(imagePath, defaultImageResolution, defaultTimeout))
					.above(relativeToAboveValue).getScreenRegion().getCenter());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the relative to above screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#clickOnRelativeToBelowElement(java
	 * .lang.String, int)
	 */
	@Override
	public boolean clickOnRelativeToBelowElement(final String imagePath, final int relativeToBelowValue) {
		try {
			mouse.click(Relative.to(getScreenRegion(imagePath, defaultImageResolution, defaultTimeout))
					.below(relativeToBelowValue).getScreenRegion().getCenter());
			return true;
		} catch (Exception err) {
			logger.error("Failed to click on the relative to below screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#doubleClickOnElement(java.lang.String
	 * )
	 */
	@Override
	public boolean doubleClickOnElement(final String imagePath) {
		try {
			mouse.doubleClick(getScreenRegion(imagePath, defaultImageResolution, defaultTimeout)
					.getCenter());
			return true;
		} catch (Exception err) {
			logger.error("Failed to double click on specified screen region. :", err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#isElementPresent(java.lang.String,
	 * int)
	 */
	@Override
	public boolean isElementPresent(final String imagePath, final int timeoutInMilliseconds) {
		boolean result;
		try {
			result = getScreenRegion(imagePath, defaultImageResolution, timeoutInMilliseconds) != null ? true
					: false;
		} catch (Exception ignored) {
			result = false;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#isElementPresent(java.lang.String,
	 * com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public boolean isElementPresent(final String imagePath, final ImageResolutionLevel sensitivity,
			final int timeoutInMilliseconds) {
		boolean result;
		try {
			ScreenRegion desktopScreenRegion = new DesktopScreenRegion();
			Target target = new ImageTarget(new File(imagePath));
			target.setMinScore(sensitivity.getValue());
			desktopScreenRegion.wait(target, timeoutInMilliseconds);
			result = desktopScreenRegion.find(target) != null ? true : false;
		} catch (Exception ignored) {
			result = false;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#rightClickOnElement(java.lang.String
	 * )
	 */
	@Override
	public boolean rightClickOnElement(final String imagePath) {
		try {
			mouse.rightClick(getScreenRegion(imagePath, defaultImageResolution, defaultTimeout).getCenter());
			return true;
		} catch (Exception err) {
			logger.error("Failed to right click on specified screen region. :" + err);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#waitForElementToAbsent(java.lang
	 * .String, int)
	 */
	@Override
	public boolean waitForElementToAbsent(final String imagePath, final int timeoutInMilliseconds) {
		boolean result;
		try {
			ScreenRegion desktopScreenRegion = new DesktopScreenRegion();
			Target target = new ImageTarget(new File(imagePath));
			desktopScreenRegion.wait(target, timeoutInMilliseconds);
			result = desktopScreenRegion.find(target) != null ? false : true;
		} catch (Exception ignored) {
			result = true;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#waitForElementToPresent(java.lang
	 * .String, int)
	 */
	@Override
	public boolean waitForElementToPresent(final String imagePath, final int timeoutInMilliseconds) {
		boolean result;
		try {
			ScreenRegion desktopScreenRegion = new DesktopScreenRegion();
			Target target = new ImageTarget(new File(imagePath));
			desktopScreenRegion.wait(target, timeoutInMilliseconds);
			result = desktopScreenRegion.find(target) != null ? true : false;
		} catch (Exception ignored) {
			result = false;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.auxenta.test.tools.SikuliElement#getCountOfSimilarImagesOnScreen(
	 * java.lang.String, com.auxenta.test.tools.ImageResolutionLevel, int)
	 */
	@Override
	public int getCountOfSimilarImagesOnScreen(final String imagePath,
			final ImageResolutionLevel imageSensitivity, final int timeoutInMilliseconds) {
		int simalarImagesCount = 0;
		try {
			ScreenRegion desktopScreenRegion = new DesktopScreenRegion();
			Target target = new ImageTarget(new File(imagePath));
			target.setMinScore(imageSensitivity.getValue());
			desktopScreenRegion.wait(target, timeoutInMilliseconds);

			List<ScreenRegion> similarImages = desktopScreenRegion.findAll(target);
			simalarImagesCount = similarImages.size();
		} catch (Exception ignored) {
			simalarImagesCount = 0;
		}
		return simalarImagesCount;
	}

	/**
	 * Gets the screen region.
	 *
	 * @param imagePath
	 *            the image path
	 * @param imageSensitivity
	 *            the image sensitivity
	 * @param timeoutInMilliseconds
	 *            the timeout in milliseconds
	 * @return the screen region
	 */
	private ScreenRegion getScreenRegion(final String imagePath, final ImageResolutionLevel imageSensitivity,
			final int timeoutInMilliseconds) {
		ScreenRegion desktopScreenRegion = new DesktopScreenRegion();
		Target target = new ImageTarget(new File(imagePath));
		desktopScreenRegion.wait(target, defaultTimeout);
		return desktopScreenRegion.find(target);
	}
}