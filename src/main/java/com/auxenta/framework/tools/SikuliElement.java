/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

/**
 * The Interface SikuliElement.
 */
public interface SikuliElement {

    /**
     * Click on element.
     *
     * @param imagePath the image path
     * @return true, if successful
     */
    boolean clickOnElement(String imagePath);

    /**
     * Click on element.
     *
     * @param imagePath the image path
     * @param imageSensitivity the image sensitivity
     * @return true, if successful
     */
    boolean clickOnElement(String imagePath, ImageResolutionLevel imageSensitivity);

    /**
     * Click on element.
     *
     * @param imagePath the image path
     * @param timeoutInMilliSeconds the timeout in milli seconds
     * @return true, if successful
     */
    boolean clickOnElement(String imagePath, int timeoutInMilliSeconds);

    /**
     * Click on element.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElement(
            final String imagePath,
            final ImageResolutionLevel sensitivity,
            final int timeoutInMilliseconds);

    /**
     * Click on element coordinates.
     *
     * @param imagePath the image path
     * @param xAxis the x axis
     * @param yAxis the y axis
     * @return true, if successful
     */
    boolean clickOnElementCoordinates(String imagePath, int xAxis, int yAxis);

    /**
     * Click on element coordinates.
     *
     * @param imagePath the image path
     * @param xAxis the x axis
     * @param yAxis the y axis
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementCoordinates(
            final String imagePath,
            final int xAxis,
            final int yAxis,
            final int timeoutInMilliseconds);

    /**
     * Click on element coordinates.
     *
     * @param imagePath the image path
     * @param xAxis the x axis
     * @param yAxis the y axis
     * @param sensitivity the sensitivity
     * @return true, if successful
     */
    boolean clickOnElementCoordinates(
            final String imagePath,
            final int xAxis,
            final int yAxis,
            final ImageResolutionLevel sensitivity);

    /**
     * Click on element coordinates.
     *
     * @param imagePath the image path
     * @param xAxis the x axis
     * @param yAxis the y axis
     * @param sensitivity the sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementCoordinates(
            final String imagePath,
            final int xAxis,
            final int yAxis,
            final ImageResolutionLevel sensitivity,
            final int timeoutInMilliseconds);

    /**
     * Click on element lower right corner.
     *
     * @param imagePath the image path
     * @return true, if successful
     */
    boolean clickOnElementLowerRightCorner(final String imagePath);

    /**
     * Click on element lower right corner.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @return true, if successful
     */
    boolean clickOnElementLowerRightCorner(
            final String imagePath,
            final ImageResolutionLevel sensitivity);

    /**
     * Click on element lower right corner.
     *
     * @param imagePath the image path
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementLowerRightCorner(final String imagePath, final int timeoutInMilliseconds);

    /**
     * Click on element lower right corner.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementLowerRightCorner(
            final String imagePath,
            final ImageResolutionLevel sensitivity,
            final int timeoutInMilliseconds);

    /**
     * Click on element lower left corner.
     *
     * @param imagePath the image path
     * @return true, if successful
     */
    boolean clickOnElementLowerLeftCorner(final String imagePath);

    /**
     * Click on element lower left corner.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @return true, if successful
     */
    boolean clickOnElementLowerLeftCorner(
            final String imagePath,
            final ImageResolutionLevel sensitivity);

    /**
     * Click on element lower left corner.
     *
     * @param imagePath the image path
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementLowerLeftCorner(final String imagePath, final int timeoutInMilliseconds);

    /**
     * Click on element lower left corner.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementLowerLeftCorner(
            final String imagePath,
            final ImageResolutionLevel sensitivity,
            final int timeoutInMilliseconds);
 
    /**
     * Click on element upper left corner.
     *
     * @param imagePath the image path
     * @return true, if successful
     */
    boolean clickOnElementUpperLeftCorner(final String imagePath);

    /**
     * Click on element upper left corner.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @return true, if successful
     */
    boolean clickOnElementUpperLeftCorner(
            final String imagePath,
            final ImageResolutionLevel sensitivity);

    /**
     * Click on element upper left corner.
     *
     * @param imagePath the image path
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementUpperLeftCorner(final String imagePath, final int timeoutInMilliseconds);

    /**
     * Click on element upper left corner.
     *
     * @param imagePath the image path
     * @param sensitivity the sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean clickOnElementUpperLeftCorner(
            final String imagePath,
            final ImageResolutionLevel sensitivity,
            final int timeoutInMilliseconds);

    /**
     * Click on relative to right element.
     *
     * @param imagePath the image path
     * @param relativeToRightValue the relative to right value
     * @return true, if successful
     */
    boolean clickOnRelativeToRightElement(final String imagePath, final int relativeToRightValue);

    /**
     * Click on relative to right element.
     *
     * @param imagePath the image path
     * @param relativeToRightValue the relative to right value
     * @param timeoutInMilliSeconds the timeout in milli seconds
     * @return true, if successful
     */
    boolean clickOnRelativeToRightElement(
            final String imagePath,
            final int relativeToRightValue,
            final int timeoutInMilliSeconds);

    /**
     * Click on relative to left element.
     *
     * @param imagePath the image path
     * @param relativeToLeftValue the relative to left value
     * @return true, if successful
     */
    boolean clickOnRelativeToLeftElement(final String imagePath, final int relativeToLeftValue);

    /**
     * Click on relative to above element.
     *
     * @param imagePath the image path
     * @param relativeToAboveValue the relative to above value
     * @return true, if successful
     */
    boolean clickOnRelativeToAboveElement(final String imagePath, final int relativeToAboveValue);

    /**
     * Click on relative to below element.
     *
     * @param imagePath the image path
     * @param relativeToBelowValue the relative to below value
     * @return true, if successful
     */
    boolean clickOnRelativeToBelowElement(final String imagePath, final int relativeToBelowValue);

    /**
     * Double click on element.
     *
     * @param imagePath the image path
     * @return true, if successful
     */
    boolean doubleClickOnElement(final String imagePath);

    /**
     * Checks if is element present.
     *
     * @param imagePath the image path
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if is element present
     */
    boolean isElementPresent(final String imagePath, final int timeoutInMilliseconds);

    /**
     * Checks if is element present.
     *
     * @param imagePath the image path
     * @param imageSensitivity the image sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if is element present
     */
    boolean isElementPresent(
            final String imagePath,
            final ImageResolutionLevel imageSensitivity,
            final int timeoutInMilliseconds);

    /**
     * Right click on element.
     *
     * @param imagePath the image path
     * @return true, if successful
     */
    boolean rightClickOnElement(final String imagePath);

    /**
     * Wait for element to absent.
     *
     * @param imagePath the image path
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean waitForElementToAbsent(final String imagePath, final int timeoutInMilliseconds);

    /**
     * Wait for element to present.
     *
     * @param imagePath the image path
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return true, if successful
     */
    boolean waitForElementToPresent(final String imagePath, final int timeoutInMilliseconds);

    /**
     * Gets the count of similar images on screen.
     *
     * @param imagePath the image path
     * @param imageSensitivity the image sensitivity
     * @param timeoutInMilliseconds the timeout in milliseconds
     * @return the count of similar images on screen
     */
    int getCountOfSimilarImagesOnScreen(
            final String imagePath,
            final ImageResolutionLevel imageSensitivity,
            final int timeoutInMilliseconds);

}
