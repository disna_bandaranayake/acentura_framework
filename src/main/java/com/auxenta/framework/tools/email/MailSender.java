package com.auxenta.framework.tools.email;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.auxenta.framework.BasicTestObject;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;

/**
 * 
 * @author Niroshen Landerz
 *
 * Usage : - Call in the following order, make sure at least one recipient enmail address is set
 * GmailSender sender = new GmailSender();
 * sender.setpProtocol // Optional;
 * sender.setSender("myEmailNameWithout@gmail.com", "mypassword");
 * sender.setTo("a@b.com, s@w.com"); // Optional
 * sender.setCc("a@b.com, s@w.com"); // Optional
 * sender.setBcc("a@b.com, s@w.com"); // Optional
 * sender.setSubject("The subject");	// Optional
 * sender.setBody("The body");			// Optional
 * sender.addAttachment("TestFile.txt"); // optional
 * sender.send();
 */

public class MailSender
{
	/** The config. */
	protected static IConfig config = DefaultConfig.getConfig();
	
	private static Logger logger = Logger.getLogger(BasicTestObject.class);
    private static String protocol;

    private static String username;
    private static String password;
    private static String proxyHost;
    private static String proxyPort;
    private static String proxyUser;
    private static String proxyPassword;
    private static String sslTrust;
    
    private void setSslTrust(String sslTrust){
    	this.sslTrust = sslTrust;
    }
    
    public void setTo(String to) {
		this.to = to;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	private String to = ""; 
    private String cc = "";
    private String bcc = "";
    
    
    public static void setProtocol(String protocol) {
		MailSender.protocol = protocol;
	}

    private Session session;
    private Message message;
    private Multipart multipart;

    public MailSender() throws MessagingException
    {
        this.multipart = new MimeMultipart();
        MailSender.username = config.getValue("email.user");
        MailSender.password = config.getValue("email.password");
        MailSender.protocol = config.getValue("email.protocol");
        if (config.getValue("proxy.enable").equalsIgnoreCase("true")){
        	MailSender.proxyHost = config.getValue("proxy.host");
        	MailSender.proxyPort = config.getValue("proxy.port");
        	MailSender.proxyUser = config.getValue("proxy.user");
        	MailSender.proxyPassword = config.getValue("proxy.password");
        	this.session = getSession();
            this.message = new MimeMessage(session);
        	}
        	else {
        		throw new MessagingException();
        	}
    }
    
  
    public void setSubject(String subject) throws MessagingException
    {
        message.setSubject(subject);
    }

    public void setBody(String body) throws MessagingException
    {
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(body);
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);
    }

    public void addAttachment(String filePath, String fileName) throws MessagingException
    {
        BodyPart messageBodyPart = getFileBodyPart(filePath, fileName);
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);
    }

    public void addReciepentsToCcBcc() throws AddressException, MessagingException {
    	if (to.isEmpty() && cc.isEmpty() && bcc.isEmpty()){
    		throw new MessagingException();
    	}
    	setMessageRecipients(to, Message.RecipientType.TO);
    	setMessageRecipients(cc, Message.RecipientType.CC);
    	setMessageRecipients(bcc, Message.RecipientType.BCC);
    }
    
    public void send() throws MessagingException
    {
    	addReciepentsToCcBcc();
        Transport transport = session.getTransport(protocol);
        if (!username.isEmpty() && !password.isEmpty())
        {
        transport.connect(username, password);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        } 
        else {
        	throw new MessagingException();
        }
        	
    }

    private BodyPart getFileBodyPart(String filePath, String fileName) throws MessagingException
    {
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource dataSource = new FileDataSource(filePath + fileName);
        messageBodyPart.setDataHandler(new DataHandler(dataSource));
        
        messageBodyPart.setFileName(fileName);

        return messageBodyPart;
    }
    
    private Session getSession()
    {
        Properties properties = getMailServerProperties(false);
        
        Session session = Session.getDefaultInstance(properties);

        return session;
    }

    private Session getSessionProxy()
    {
        Properties properties = getMailServerProperties(true);
        
        Session session = Session.getDefaultInstance(properties,
        		new Authenticator(){
        	protected PasswordAuthentication getPasswordAuthentication() {
        	return new PasswordAuthentication(proxyUser, proxyPassword);
        	}});

        return session;
    }
    
    private Properties getMailServerProperties(boolean proxyEnabled)
    {
    	logger.info("Executing the getMailServerProperties");
        Properties properties = System.getProperties();
        
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "outlook.dialog.dialoggsm.com");
        properties.put("mail.smtp.user", username);
        properties.put("mail.smtp.password", password);
        properties.put("mail.smtp.port", "25");
        properties.put("mail.smtp.auth", "false");
        if (proxyEnabled){
        	logger.info("Now Using proxy");
        properties.put("http.useProxy","true");
        properties.put("http.proxyPort",proxyPort);
        properties.put("http.proxyHost",proxyHost);
        properties.put("http.proxyUser", proxyUser);
        properties.put("http.proxyPassword", proxyPassword);
        if (!sslTrust.isEmpty() && sslTrust != null){
        	properties.put("mail.smtp.ssl.trust", "*");
        	}

        }

        return properties;
    }
    
    private void setMessageRecipients(String recipient, Message.RecipientType recipientType)
			throws AddressException, MessagingException {
		InternetAddress[] addressArray = buildInternetAddressArray(recipient);
		
		if ((addressArray != null) && (addressArray.length > 0)) {
			message.setRecipients(recipientType, addressArray);
		}
	}
	
	/**
	 * The address can be a comma-separated sequence of email addresses.
	 * 
	 * @see mail.internet.InternetAddress.parse() for details.
	 *
	 */
	private InternetAddress[] buildInternetAddressArray(String address) throws AddressException {
		// could test for a null or blank String but I'm letting parse just
		// throw an exception
		InternetAddress[] internetAddressArray = null;
		try {
			internetAddressArray = InternetAddress.parse(address);
		} catch (AddressException ae) {
			// do logging here
			throw ae;
		}
		return internetAddressArray;
	}
	/*  public void setSender(String username, String password) throws MessagingException
    {
    	if (!username.isEmpty() && !password.isEmpty()){
        MailSender.username = username;
        MailSender.password = password;
        this.session = getSession();
        this.message = new MimeMessage(session);
    	}
    	else {
    		throw new MessagingException();
    	}
    }*/

   /* public void setSender(String username, String password, String proxyHost, String proxyPort, String proxyUser, String proxyPassword) throws MessagingException
    {
    	if (!username.isEmpty() && !password.isEmpty()){
        MailSender.username = username;
        MailSender.password = password;
        MailSender.proxyHost = proxyHost;
        MailSender.proxyPort = proxyPort;
        MailSender.proxyUser = proxyUser;
        MailSender.proxyPassword = proxyPassword;
        this.session = getSessionProxy();
        this.message = new MimeMessage(session);
    	}
    	else {
    		throw new MessagingException();
    	}
    }*/

}