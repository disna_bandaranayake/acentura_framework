/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.util.List;
import java.util.Random;

/**
 * A class provides all kinds of useful methods for eslenium framework.
 *
 * @author Niroshen Landerz
 * @Date - Sept 1st 2014
 */
public class DataGenerator {

	/**
	 * Gets the random num.
	 *
	 * @return the random num
	 */
	public static String getRandomNum() {
		Random generator = new Random();
		int num = generator.nextInt(10);
		String randomNum = "";
		for (int i = 0; i < num; i++) {
			randomNum += generator.nextInt(10);
		}
		return randomNum;
	}

	/**
	 * Returning a string of randomNum with given num of digits.
	 *
	 * @param num the num
	 * @return the random num
	 */
	public static String getRandomNum(final int num) {
		String randomNum = "";
		Random generator = new Random();
		for (int i = 0; i < num; i++) {
			randomNum += generator.nextInt(10);
		}
		return randomNum;
	}

	static {
		StringBuilder tmp = new StringBuilder();
		for (char ch = '0'; ch <= '9'; ++ch)
			tmp.append(ch);
		for (char ch = 'a'; ch <= 'z'; ++ch)
			tmp.append(ch);
	}

	/**
	 * Generate random string.
	 *
	 * @param length the length
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String generateRandomString(final int length) throws Exception {

		StringBuffer buffer = new StringBuffer();

		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

		int charactersLength = characters.length();

		for (int i = 0; i < length; i++) {
			double index = Math.random() * charactersLength;
			buffer.append(characters.charAt((int) index));
		}
		return buffer.toString();
	}

	/**
	 * Generate random charcter only string.
	 *
	 * @param length the length
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String generateRandomCharcterOnlyString(final int length) throws Exception {

		StringBuffer buffer = new StringBuffer();

		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		int charactersLength = characters.length();

		for (int i = 0; i < length; i++) {
			double index = Math.random() * charactersLength;
			buffer.append(characters.charAt((int) index));
		}
		return buffer.toString();
	}
	
	/**
	 * Get A random value from a list of Strings(drop down menu).
	 *
	 * @param dropDownListAll the list of dropDownListAll
	 * 
	 */
	public String getRandomValueFromList(List<String> dropDownListAll){
		Random rand = new Random();
	    return dropDownListAll.get(rand.nextInt(dropDownListAll.size()));
	}
}