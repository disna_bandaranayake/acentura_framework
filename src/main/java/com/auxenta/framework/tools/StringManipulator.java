/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.util.Map;
import org.springframework.util.StringUtils;


/**
 * A class provides all kinds of useful methods for eslenium framework.
 *
 * @author Niroshen Landerz
 * @Date - Sept 1st 2014
 */
public class StringManipulator extends StringUtils{

	public static double parseDoubleSafely(String str) {
	    double result = 0;
	    try {
	        result = Double.parseDouble(str);
	    } catch (NullPointerException npe) {
	    } catch (NumberFormatException nfe) {
	    }
	    return result;
	}
	
	  /**
	   * Serializes to the format "name=value;name=value".
	   * 
	   * @return String with the above format.
	   */
	public static String  convertMapToSeperatedValueString(Map<String, String> option, char seperationCharacter){
		StringBuilder sb = new StringBuilder();
	    boolean first = true;
	    for (String key : option.keySet()) {
	      if (first) {
	        first = false;
	      } else {
	        sb.append(seperationCharacter);
	      }
	      sb.append(key).append('=').append(option.get(key));
	    }
	    return sb.toString();
	  }
	
	
	}