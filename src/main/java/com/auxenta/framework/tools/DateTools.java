/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * The Class DateTools.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public class DateTools {

	/** The logger. */
	private static Logger logger = Logger.getLogger(DateTools.class);

	/** The Constant MMddHHmm. */
	public static final SimpleDateFormat MMddHHmm = new SimpleDateFormat("MMddHHmm");

	/** The Constant MMddHHmmss. */
	public static final SimpleDateFormat MMddHHmmss = new SimpleDateFormat("MMddHHmmss");

	/** The Constant yyMMddHHmmss. */
	public static final SimpleDateFormat yyMMddHHmmss = new SimpleDateFormat("yyMMddHHmmss");

	/** The Constant yyyyMMddHHmmss. */
	public static final SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyMMddHHmmss");

	/** The Constant HUMAN_DATE_FORMAT. */
	public static final SimpleDateFormat HUMAN_DATE_FORMAT = new SimpleDateFormat("hh:mm a z, MMM dd, yyyy");

	/** The Constant MILLISECONDS_PER_MINUTE. */
	public static final int MILLISECONDS_PER_MINUTE = 60000;

	/**
	 * Seconds since.
	 *
	 * @param startDateTimestamp
	 *            the start date timestamp
	 * @param currentDateTimestamp
	 *            the current date timestamp
	 * @return the long
	 */
	public static long secondsSince(final Date startDateTimestamp, final Date currentDateTimestamp) {
		logger.info("CURRENT TIME:[" + currentDateTimestamp.getTime() + "] START TIME:["
				+ startDateTimestamp.getTime() + "]");
		return (currentDateTimestamp.getTime() - startDateTimestamp.getTime()) / 1000;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @param specifiedFormatString
	 *            the specified format string
	 * @return the time stamp
	 */
	public static String getTimeStamp(final String specifiedFormatString) {
		final SimpleDateFormat specifiedFormat = new SimpleDateFormat(specifiedFormatString);
		return specifiedFormat.format(new Date());
	}

	/**
	 * Gets the time stamp with offset minutes.
	 *
	 * @param specifiedFormatString
	 *            the specified format string
	 * @param timeOffsetMinutes
	 *            the time offset minutes
	 * @return the time stamp with offset minutes
	 */
	public static String getTimeStampWithOffsetMinutes(final String specifiedFormatString,
			final int timeOffsetMinutes) {
		final SimpleDateFormat specifiedFormat = new SimpleDateFormat(specifiedFormatString);
		return specifiedFormat.format(new Date(System.currentTimeMillis() + timeOffsetMinutes * 60000));
	}

	/**
	 * Gets the time stamp with seconds.
	 *
	 * @return the time stamp with seconds
	 */
	public static String getTimeStampWithSeconds() {
		return yyyyMMddHHmmss.format(new Date());
	}

	/**
	 * Gets the formatted time for given time in mills.
	 *
	 * @param time
	 *            the time
	 * @param format
	 *            the format
	 * @return the formatted time for given time in mills
	 */
	public static String getFormattedTimeForGivenTimeInMills(final String time, final String format) {
		final SimpleDateFormat specifiedFormat = new SimpleDateFormat(format);

		return specifiedFormat.format(new Date(Long.parseLong(time)));
	}

	/**
	 * Gets the system time.
	 *
	 * @return the system time
	 */
	public static String getSystemTime() {
		return String.valueOf(System.currentTimeMillis());
	}

	/**
	 * Gets the off set day in given format.
	 *
	 * @param timeStampInMillis
	 *            the time stamp in millis
	 * @param days
	 *            the days
	 * @param format
	 *            the format
	 * @return the off set day in given format
	 */
	public static String getOffSetDayInGivenFormat(final String timeStampInMillis, final int days,
			final String format) {
		Long timeInMillis = Long.parseLong(timeStampInMillis);
		timeInMillis = timeInMillis + days * 3600000 * 24;
		final SimpleDateFormat specifiedFormat = new SimpleDateFormat(format);
		return specifiedFormat.format(new Date(timeInMillis));
	}

	/**
	 * Checks if is time stamp within range.
	 *
	 * @param timeDisplayed
	 *            the time displayed
	 * @param systemTime
	 *            the system time
	 * @param seconds
	 *            the seconds
	 * @return the boolean
	 * @throws ParseException
	 *             the parse exception
	 */
	public static Boolean isTimeStampWithinRange(final String timeDisplayed, final String systemTime,
			final int seconds) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		long stampTime = format.parse(timeDisplayed).getTime();
		long sysTime = format.parse(systemTime).getTime();
		if (Math.abs(stampTime - sysTime) < seconds * 1000) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is time stamp within range.
	 *
	 * @param timeDisplayed
	 *            the time displayed
	 * @param systemTime
	 *            the system time
	 * @param timeformat
	 *            the timeformat
	 * @param seconds
	 *            the seconds
	 * @return the boolean
	 * @throws ParseException
	 *             the parse exception
	 */
	public static Boolean isTimeStampWithinRange(final String timeDisplayed, final String systemTime,
			final String timeformat, int seconds) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(timeformat);
		long stampTime = format.parse(timeDisplayed).getTime();
		long sysTime = format.parse(systemTime).getTime();
		if (Math.abs(stampTime - sysTime) < seconds * 1000) {
			return true;
		}
		return false;
	}

	/**
	 * Gets the time in millis for given time with format.
	 *
	 * @param time
	 *            the time
	 * @param format
	 *            the format
	 * @return the time in millis for given time with format
	 * @throws ParseException
	 *             the parse exception
	 */
	public static String getTimeInMillisForGivenTimeWithFormat(final String time, final String format)
			throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date date = dateFormat.parse(time);
		return String.valueOf(date.getTime());
	}

	/**
	 * Gets the time converted from given format to new format.
	 *
	 * @param time
	 *            the time
	 * @param inputFormat
	 *            the input format
	 * @param outputFormat
	 *            the output format
	 * @return the time converted from given format to new format
	 * @throws ParseException
	 *             the parse exception
	 */
	public static String getTimeConvertedFromGivenFormatToNewFormat(final String time,
			final String inputFormat, String outputFormat) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(inputFormat);
		Date date = dateFormat.parse(time);
		SimpleDateFormat oFormat = new SimpleDateFormat(outputFormat);
		return oFormat.format(date);
	}
}
