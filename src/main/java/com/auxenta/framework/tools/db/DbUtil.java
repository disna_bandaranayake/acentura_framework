package com.auxenta.framework.tools.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.dbutils.ResultSetHandler;


public class DbUtil {
	private static Connection myConnection = null;

	static {
		try {
			Class.forName("org.h2.Driver");
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DbUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// Create a ResultSetHandler implementation to convert the
	// first row into an Object[].
	ResultSetHandler<Object[]> h = new ResultSetHandler<Object[]>() {
	    public Object[] handle(ResultSet rs) throws SQLException {
	        if (!rs.next()) {
	            return null;
	        }
	    
	        ResultSetMetaData meta = rs.getMetaData();
	        int cols = meta.getColumnCount();
	        Object[] result = new Object[cols];

	        for (int i = 0; i < cols; i++) {
	            result[i] = rs.getObject(i + 1);
	        }

	        return result;
	    }
	};

	
	public static void setupConn(String theDbPath) throws SQLException {
		if (null == myConnection || myConnection.isClosed()) {
			myConnection = DriverManager.getConnection("jdbc:h2:" + theDbPath);
		}
	}

	public static Statement getStatement() throws SQLException {
		if (null == myConnection || myConnection.isClosed()) {
			SQLException ex = new SQLException("No valid database connection!");
			Logger.getLogger(DbUtil.class.getName()).log(Level.SEVERE, null, ex);
			throw ex;
		}

		return myConnection.createStatement();
	}

	public static void closeConn() throws SQLException {
		myConnection.close();
	}

	public static void setupDB(String theDbPath) throws SQLException {

		setupConn(theDbPath);
		runScript("init.sql");
	}

	public static void runScript(String thePath) throws SQLException {

		Statement stat = getStatement();
		stat.execute("runscript from '" + thePath + "'");
		stat.close();
	}

}
