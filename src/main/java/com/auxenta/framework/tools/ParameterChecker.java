/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import com.auxenta.framework.exceptions.InvalidURLException;

/**
 * Helper methods for validating parameters.
 *
 * @author Niroshen Landerz
 * @date Aug 30th 2014
 */

public class ParameterChecker {

	/**
	 * Check url.
	 *
	 * @param url
	 *            the url
	 * @return the string
	 */
	public static String checkURL(String url) {
		if (url == null || "".equals(url)) {
			throw new InvalidURLException(" Url can't be an empty string !. URL = '" + url + "'");
		}
		if (url.startsWith("file")) {
			return url;
		}
		if (!url.startsWith("http")) {
			url = "http://" + url;
		}
		return url;
	}

	/**
	 * Checks if is null or empty.
	 *
	 * @param value
	 *            the value
	 * @return true, if is null or empty
	 */
	public static boolean isNullOrEmpty(final Object value) {
		if (value == null || "".equals(value.toString()))
			return true;
		return false;
	}
}
