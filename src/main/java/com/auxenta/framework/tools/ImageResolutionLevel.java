/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

/**
 * Enum for holding all the image resolution levels that can be used with
 * SikuliTestWrapper methods.
 *
 * @author Niroshen Landerz
 */
public enum ImageResolutionLevel {

	/** The high. */
	HIGH(1),

	/** The very high medium. */
	VERY_HIGH_MEDIUM(0.94),

	/** The high medium. */
	HIGH_MEDIUM(0.8),

	/** The medium. */
	MEDIUM(0.5),

	/** The low medium. */
	LOW_MEDIUM(0.2),

	/** The low. */
	LOW(0.1),

	/** The very low. */
	VERY_LOW(0.01);

	/** The value. */
	private final double value;

	/**
	 * Instantiates a new image resolution level.
	 *
	 * @param value
	 *            the value
	 */
	private ImageResolutionLevel(double value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

}
