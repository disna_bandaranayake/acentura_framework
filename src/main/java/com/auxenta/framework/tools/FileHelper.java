/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.apache.log4j.Logger;

import com.auxenta.framework.BasicTestObject;

/**
 * The Class FileHelper.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public class FileHelper {

	/**
	 * Copy a file from a location to another location .
	 *
	 * @author Niroshen Landerz
	 * @param sourceFile
	 *            the source file
	 * @param destFile
	 *            the dest file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	
	/** The logger. */
	private static Logger logger = Logger.getLogger(BasicTestObject.class);
	
	public static void copyFile(final File sourceFile, final File destFile) throws IOException {
		if (!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;
		if (sourceFile != null){
			
		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		} finally {
			if (source != null) {
				source.close();
			}
			if (destination != null) {
				destination.close();
			}
		}
		}
	}
	
	public static void deleteFile(File file) throws IOException{
		if (file.exists()){
			logger.info("Trying to delete the file");
			if (!file.delete()){
				logger.info("----- FAILED TO DELETE THE FILE ----------------");
			};
		}
	}

}
