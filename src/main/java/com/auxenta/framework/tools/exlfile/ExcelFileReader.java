/*
 *================================================================
 * Copyright  (c)     : 2015 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools.exlfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFCreationHelper;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;

import com.auxenta.framework.exception.TestAutomationException;

/**
 * The Class ExcelFileReader.
 */
public class ExcelFileReader {

	/** The path. */
	public String path;

	/** The fis. */
	public FileInputStream fis = null;

	/** The file out. */
	public FileOutputStream fileOut = null;

	/** The workbook. */
	private static HSSFWorkbook workbook = null;

	/** The sheet. */
	private static HSSFSheet sheet = null;

	/** The row. */
	private static HSSFRow row = null;

	/** The cell. */
	private static HSSFCell cell = null;

	/** The sheet name. */
	private static String activeSheetName;
	
	/** sheet names */
	private static List<String> sheetNameList = new ArrayList<String>();

	/**
	 * Instantiates a new excel file reader.
	 * 
	 * @param path
	 *            : Fully qualified name where the excel file is, including the
	 *            file name
	 * @param givenSheetName
	 *            : - Method name should have it's own sheet to pull the data,
	 *            and param should be the name of the method
	 * @throws Exception 
	 */
	public ExcelFileReader(String path, String givenSheetName) throws Exception {
		activeSheetName = givenSheetName;
		this.path = path;
		try {
			fis = new FileInputStream(new File(path));
			workbook = new HSSFWorkbook(fis);

			int sheetNumbers = workbook.getNumberOfSheets();
			for (int i = 0; i < sheetNumbers; i++) {
				if (givenSheetName.equals(workbook.getSheetName(i))) {
					workbook.setActiveSheet(i);
					sheet = workbook.getSheetAt(i);
					return;
				}
			}
			throw new Exception("File " + path + " does not contains the sheet" + givenSheetName);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}
	
	public ExcelFileReader(String path) throws Exception {	
		this.path = path;
		try {
			fis = new FileInputStream(new File(path));
			workbook = new HSSFWorkbook(fis);
			return;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}
	
	/**
	 * Return list of sheet names in work book
	 * @return
	 * @throws Exception
	 */
	public List<String> getSheetNameList() throws Exception {
		int numberOfSheets = workbook.getNumberOfSheets();
		if (numberOfSheets != 0){
			sheetNameList.clear();
			for(int i=0;i<numberOfSheets;i++){
				String sheetName = workbook.getSheetName(i);
				sheetNameList.add(sheetName);			
			}
		}
		return sheetNameList;
	}

	/**Active sheet would be changed to the given sheet, and set the static variable to active. 
	 * 
	 * @param sheetName
	 */
	public static void changeActiveSheet(String sheetName){
		int sheetIndex = workbook.getSheetIndex(sheetName);
		workbook.setActiveSheet(sheetIndex);
		sheet = workbook.getSheetAt(sheetIndex);
		activeSheetName = workbook.getSheetName(sheetIndex);
		return;
	}
	
	/**
	 * Gets the row count active sheet.
	 * 
	 * @return the row count active sheet
	 */

	public int getRowCountActiveSheet() {
		int number = sheet.getLastRowNum() + 1;
		return number;
	}

	/**Will find all the cells on the row 1
	 * 
	 * @param sheetName
	 * @return Array of string with the header row labels.
	 */
	public String [] getHeaderRowValues(String sheetName){
		changeActiveSheet(sheetName);
		int columnCount = getColumnCount(sheetName);
		String header[] = new String[columnCount];
		for (int i=0; i< columnCount; i++){
			header[i] = getCellData(i, 1);
		}
		return header;
	}
	
	/**
	 * Will find the row number of the first finding of the exact mathcing
	 * @param sheetName
	 * @param cellContent
	 * @return
	 */
	public static int findRow(String sheetName, String cellContent) {
		int sheetIndex = workbook.getSheetIndex(sheetName);
		workbook.setActiveSheet(sheetIndex);
		sheet = workbook.getSheetAt(sheetIndex);
	    for (Row row : sheet) {
	        for (Cell cell : row) {
	            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
	                if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
	                    return row.getRowNum();  
	                }
	            }
	        }
	    }               
	    return 0;
	}
	
	/**
	 * Gets the excel active sheet first two column data.
	 * 
	 * @return the excel active sheet first two column data
	 * @throws Exception
	 *             the exception
	 */
	public HashMap<String, String> getExcelActiveSheetFirstTwoColumnData() throws Exception {
		HashMap<String, String> recordsMap = new HashMap<String, String>();
		int lastRow = sheet.getLastRowNum() + 2;
		try {
			// Finding whether shee has any data...
			if (lastRow < 1) {
				throw new Exception("Give sheet " + sheet.getSheetName() + "Does not have any records");
			}
			for (int i = 1; i < lastRow; i++) {
				recordsMap.put(getCellData(0, i), getCellData(1, i));
			}

			return recordsMap;
		} catch (Exception e) {

			e.printStackTrace();
			throw new Exception("date sheet seems to be empty or some other error");
		}
	}

	// returns the row count in a sheet
	/**
	 * Gets the row count.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @return the row count
	 */
	public int getRowCount(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if (index == -1)
			return 0;
		else {
			sheet = workbook.getSheetAt(index);
			int number = sheet.getLastRowNum() + 1;
			return number;
		}

	}

	/**
	 * Gets the cell data.
	 * 
	 * @param colNum
	 *            the col num
	 * @param rowNum
	 *            the row num
	 * @return the cell data
	 */
	private String getCellData(int colNum, int rowNum) {

		try {
			if (rowNum <= 0)
				return "";

			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(colNum);
			if (cell == null)
				return "";
			cell.setCellType(Cell.CELL_TYPE_STRING);
			return cell.getStringCellValue().trim();
		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colNum + " does not exist  in xls";
		}

	}

	// returns the data from a cell
	/**
	 * Gets the cell data.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @param colName
	 *            the col name
	 * @param rowNum
	 *            the row num
	 * @return the cell data
	 */
	public String getCellData(String sheetName, String colName, int rowNum) {
		if (colName == null){
			throw new TestAutomationException("Column Name is Null, hence cannot compare the cell");
		}
		try {
			if (rowNum <= 0)
				return "";

			int index = workbook.getSheetIndex(sheetName);
			int col_Num = -1;
			if (index == -1)
				return "";

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {

					row.getCell(i).getStringCellValue();
				} catch (NullPointerException e){
					continue;
				}
				if (row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
					col_Num = i;
				break;
			}
			if (col_Num == -1)
				return "";

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(col_Num);

			if (cell == null)
				return "";
			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC
					|| cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					// format in form of M/D/YY
					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.MONTH) + 1 + "/"
							+ cellText;
				}

				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());

		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colName + " does not exist in xls";
		}
	}

	// returns the data from a cell
	/**
	 * Gets the cell data.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @param colNum
	 *            the col num
	 * @param rowNum
	 *            the row num
	 * @return the cell data
	 */
	public String getCellData(String sheetName, int colNum, int rowNum) {
		try {
			if (rowNum < 0)
				return "";

			int index = workbook.getSheetIndex(sheetName);

			if (index == -1)
				return "";

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(rowNum);
			if (row == null)
				return "";
			cell = row.getCell(colNum);
			if (cell == null)
				return "";

			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC
					|| cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					// format in form of M/D/YY
					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.MONTH) + 1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
							+ cellText;
				}

				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());
		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colNum + " does not exist  in xls";
		}
	}

	// returns the date from a cell of the active sheet
	/**
	 * Gets the celldata from active sheet.
	 * 
	 * @param colName
	 *            the col name
	 * @param rowNum
	 *            the row num
	 * @return the celldata from active sheet
	 */
	public String getCelldataFromActiveSheet(String colName, int rowNum) {
		try {
			if (rowNum <= 0)
				return "";

			int index = workbook.getSheetIndex(activeSheetName);
			int col_Num = -1;
			if (index == -1)
				return "";
			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				if (row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
					col_Num = i;
			}
			if (col_Num == -1)
				return "";

			row = sheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(col_Num);

			if (cell == null)
				return "";
			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC
					|| cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					// format in form of M/D/YY
					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.MONTH) + 1 + "/"
							+ cellText;
				}

				return cellText;
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());

		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + colName + " does not exist in xls";
		}
	}

	// returns true if data is set successfully else false
	/**
	 * Sets the cell data.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @param colName
	 *            the col name
	 * @param rowNum
	 *            the row num
	 * @param data
	 *            the data
	 * @return true, if successful
	 */
	public boolean setCellData(String sheetName, String colName, int rowNum, String data) {
		try {
			if (rowNum <= 0)
				return false;

			int index = workbook.getSheetIndex(sheetName);
			int colNum = -1;
			if (index == -1)
				return false;

			sheet = workbook.getSheetAt(index);

			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {

					row.getCell(i).getStringCellValue();
				} catch (NullPointerException e){
					continue;
				}
				if (row.getCell(i).getStringCellValue().trim().equals(colName)){
					colNum = i;
					break;
				}
			}
			if (colNum == -1)
				return false;

			sheet.autoSizeColumn(colNum);
			row = sheet.getRow(rowNum - 1);
			if (row == null){
				row = sheet.createRow(rowNum - 1);
			}
			cell = row.getCell(colNum);
			if (cell == null){
				cell = row.createCell(colNum);
			}

			// cell style
			cell.setCellValue(data);
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// returns true if data is set successfully else false
	/**
	 * Sets the cell data.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @param colName
	 *            the col name
	 * @param rowNum
	 *            the row num
	 * @param data
	 *            the data
	 * @param url
	 *            the url
	 * @return true, if successful
	 */
	public boolean setCellData(String sheetName, String colName, int rowNum, String data, String url) {
		try {
			//fis = new FileInputStream(path);
			//workbook = new HSSFWorkbook();

			if (rowNum <= 0)
				return false;

			int index = workbook.getSheetIndex(sheetName);
			int colNum = -1;
			if (index == -1)
				return false;

			sheet = workbook.getSheetAt(index);
			row = sheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				if (row.getCell(i).getStringCellValue().trim().equalsIgnoreCase(colName))
					colNum = i;
			}

			if (colNum == -1)
				return false;
			sheet.autoSizeColumn(colNum);
			row = sheet.getRow(rowNum - 1);
			if (row == null)
				row = sheet.createRow(rowNum - 1);

			cell = row.getCell(colNum);
			if (cell == null)
				cell = row.createCell(colNum);

			cell.setCellValue(data);
			HSSFCreationHelper createHelper = workbook.getCreationHelper();

			// cell style for hyperlinks
			// by default hypelrinks are blue and underlined
			CellStyle hlink_style = workbook.createCellStyle();
			HSSFFont hlink_font = workbook.createFont();
			hlink_font.setUnderline(HSSFFont.U_SINGLE);
			hlink_font.setColor(IndexedColors.BLUE.getIndex());
			hlink_style.setFont(hlink_font);

			HSSFHyperlink link = createHelper.createHyperlink(HSSFHyperlink.LINK_FILE);
			link.setAddress(url);
			cell.setHyperlink(link);
			cell.setCellStyle(hlink_style);

			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);

			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// returns true if sheet is created successfully else false
	/**
	 * Adds the sheet.
	 * 
	 * @param sheetname
	 *            the sheetname
	 * @return true, if successful
	 */
	public boolean addSheet(String sheetname) {

		FileOutputStream fileOut;
		try {
			workbook.createSheet(sheetname);
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// returns true if sheet is removed successfully else false if sheet does
	// not exist
	/**
	 * Removes the sheet.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @return true, if successful
	 */
	public boolean removeSheet(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if (index == -1)
			return false;

		FileOutputStream fileOut;
		try {
			workbook.removeSheetAt(index);
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// returns true if column is created successfully
	/**
	 * Adds the column.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @param colName
	 *            the col name
	 * @return true, if successful
	 */
	@SuppressWarnings("deprecation")
	public boolean addColumn(String sheetName, String colName) {
		try {
			//fis = new FileInputStream(path);
			//workbook = new HSSFWorkbook();
			int index = workbook.getSheetIndex(sheetName);
			if (index == -1)
				return false;

			HSSFCellStyle style = workbook.createCellStyle();
			style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
			style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			sheet = workbook.getSheetAt(index);

			row = sheet.getRow(0);
			if (row == null)
				row = sheet.createRow(0);

			if (row.getLastCellNum() == -1)
				cell = row.createCell(0);
			else
				cell = row.createCell(row.getLastCellNum());

			cell.setCellValue(colName);
			cell.setCellStyle(style);

			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;

	}

	// removes a column and all the contents
	/**
	 * Removes the column.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @param colNum
	 *            the col num
	 * @return true, if successful
	 */
	public boolean removeColumn(String sheetName, int colNum) {
		try {
			if (!isSheetExist(sheetName))
				return false;
			//fis = new FileInputStream(path);
			//workbook = new HSSFWorkbook();
			sheet = workbook.getSheet(sheetName);
			HSSFCellStyle style = workbook.createCellStyle();
			style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
			style.setFillPattern(HSSFCellStyle.NO_FILL);

			for (int i = 0; i < getRowCount(sheetName); i++) {
				row = sheet.getRow(i);
				if (row != null) {
					cell = row.getCell(colNum);
					if (cell != null) {
						cell.setCellStyle(style);
						row.removeCell(cell);
					}
				}
			}
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	// find whether sheets exists
	/**
	 * Checks if is sheet exist.
	 * 
	 * @param sheetName
	 *            the sheet name
	 * @return true, if is sheet exist
	 */
	public boolean isSheetExist(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if (index == -1) {
			index = workbook.getSheetIndex(sheetName.toUpperCase());
			if (index == -1)
				return false;
			else
				return true;
		} else
			return true;
	}

	/**Return the row number of the given cell, in a sheet under a column
	 * 
	 * @param sheetName
	 * @param colName
	 * @param cellValue
	 * @return
	 */
	public int getCellRowNum(String sheetName, String colName, String cellValue) {
		for (int i = 2; i <= getRowCount(sheetName); i++) {
			if (getCellData(sheetName, colName, i).equalsIgnoreCase(cellValue)) {
				try {
				workbook.close();
				} catch (IOException e) {
					
				}
				return i;
			}
		}
		return -1;
	}
	
	/**
	 *  returns number of columns in a sheet
	 * @param sheetName
	 * @return
	 */
	public int getColumnCount(String sheetName) {
		// check if sheet exists
		if (!isSheetExist(sheetName))
			return -1;
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(0);
		if (row == null)
			return -1;
		return row.getLastCellNum();
	}
	
	/**
	 * Gets the first granton number.
	 * 
	 * @return the first granton number
	 * @throws Exception
	 *             the exception
	 */
	public String readAndRemoveFirstRow() throws Exception {
		String value = readAndRemoveRow(1, 0);
		return value;
	}

	/**
	 * Read and remove granton row.
	 * 
	 * @param rowNum
	 *            the row num
	 * @param columnNum
	 *            the column num
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public String readAndRemoveRow(int rowNum, int columnNum) throws Exception {
		String returnValue;
		try {
			Row row = sheet.getRow(rowNum);
			Cell cell = row.getCell(columnNum);
			cell.setCellType(Cell.CELL_TYPE_STRING);
			returnValue = cell.getStringCellValue().trim();
			sheet.removeRow(row);

			int lastRowNum = sheet.getLastRowNum();
			if (rowNum >= 0 && rowNum < lastRowNum) {
				sheet.shiftRows(rowNum + 1, lastRowNum, -1);
			}
			if (rowNum == lastRowNum) {
				HSSFRow removingRow = sheet.getRow(rowNum);
				if (removingRow != null) {
					sheet.removeRow(removingRow);
				}
			}

			
			Row lastRow = sheet.getRow(lastRowNum++);
			Cell lastCell = lastRow.createCell(0);
			lastCell.setCellValue(returnValue);

			for (int i = 0; i < lastRowNum; i++) {
				Row rowUsed = sheet.getRow(i);
				Cell cellUsed = rowUsed.getCell(1);
				if(cellUsed == null) {
					cellUsed = rowUsed.createCell(1);
					cellUsed.setCellValue(returnValue);
					break;
				}
			}

			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public boolean setCellDataUsingColumnHeaderRowNumber(String sheetName, int columnNo, int rowNum, String value){
		try {
	//		workbook.close();
			try {
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			fis = new FileInputStream(new File(path));
			HSSFWorkbook tempWorkBook = new HSSFWorkbook(fis);
			
			if (rowNum <= 0)
				return false;

			int index = tempWorkBook.getSheetIndex(sheetName);
			if (index == -1){
				return false;
			}
			sheet = tempWorkBook.getSheetAt(index);
			
			HSSFRow newrow = sheet.getRow(rowNum);
			HSSFCell newcell = newrow.getCell(columnNo);

			newcell.setCellValue(value);
			fileOut = new FileOutputStream(path);
			tempWorkBook.write(fileOut);
			fileOut.close();
			tempWorkBook.close();
//			workbook = new HSSFWorkbook(fis);

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}