/*
 *================================================================
 * Copyright  (c)    : 2015 Dialog Axiata PLC, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.tools;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import com.auxenta.framework.config.DynamicDataManager;

/**
 * The Class NICGeneratortwo.
 */
public class NICGenerator {
	
	/**
	 * First two digits.
	 *
	 * @param age the age
	 * @return the string
	 */
	public String firstTwoDigits(int age){
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int birthYear = year - age;
		String bYear = Integer.toString(birthYear);
		String LastDigits = bYear.substring(2, 4);
		return LastDigits;
	}

	/**
	 * Three digits male.
	 *
	 * @return the string
	 */
	public String threeDigitsMale() {

		Calendar calendar = Calendar.getInstance();
		int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
		int days = dayOfYear;
		String formatLastFourDig = String.format("%03d", days);
		return formatLastFourDig;

	}

	/**
	 * Three digitsfemale.
	 *
	 * @return the int
	 */
	public int threeDigitsfemale() {

		Calendar calendar = Calendar.getInstance();
		int dayOfYear = calendar.get(Calendar.DAY_OF_YEAR);
		int nicDig = 500;
		int threeDigit = dayOfYear + nicDig;
		return threeDigit;

	}

	/**
	 * Generate false id.
	 *
	 * @return the string
	 */
	private String generateFalseID() {
		int min = 366;
		int max = 500;

		Random randomDays = new Random();

		List<Integer> arrayDays = new ArrayList<>();

		int threeDigit = randomDays.nextInt(max - min) + min;
		while (arrayDays.contains(threeDigit))
			threeDigit = randomDays.nextInt(max - min) + min;
		arrayDays.add(threeDigit);

		String threeDigFalse = String.format("%03d", arrayDays.get(0));
		return threeDigFalse;
	}

	/**
	 * Valid female nic.
	 *
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws URISyntaxException
	 *             the URI syntax exception
	 */
	public String validFemaleNIC() throws IOException, URISyntaxException {
		String firstDigit = DynamicDataManager.getProperty("nicyear");

		String lastFourDigit = DynamicDataManager.getProperty("niclastfourdigitsfemale");
		int cnvrtLastFourDigit = Integer.parseInt(lastFourDigit);
		int incByOne = cnvrtLastFourDigit + 1;
		String formatLastFourDig = String.format("%04d", incByOne);
		DynamicDataManager.setProperty("niclastfourdigitsfemale", formatLastFourDig);

		int threeDig = threeDigitsfemale();
		String cnvrtThreeDig = Integer.toString(threeDig);

		String vldFemaleNic = firstDigit + cnvrtThreeDig + formatLastFourDig + "V";
		return vldFemaleNic;
	}

	/**
	 * Valid male nic.
	 *
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws URISyntaxException
	 *             the URI syntax exception
	 */
	public String validMaleNIC() throws IOException, URISyntaxException {
		String firstDigit = DynamicDataManager.getProperty("nicyear");

		String lastFourDigit = DynamicDataManager.getProperty("niclastfourdigitsmale");
		int cnvrtLastFourDigit = Integer.parseInt(lastFourDigit);
		int incByOne = cnvrtLastFourDigit + 1;
		String formatLastFourDig = String.format("%04d", incByOne);
		DynamicDataManager.setProperty("niclastfourdigitsmale", formatLastFourDig);

		String vldMaleNic = firstDigit + threeDigitsMale() + formatLastFourDig + "V";
		return vldMaleNic;
	}
	
	/**
	 * Valid male nic with given age.
	 *
	 * @param age the age
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws URISyntaxException the URI syntax exception
	 */
	public String validMaleNICWithGivenAge(int age) throws IOException, URISyntaxException {
		String firstDigit = firstTwoDigits(age);

		String lastFourDigit = DynamicDataManager.getProperty("niclastfourdigitsmale");
		int cnvrtLastFourDigit = Integer.parseInt(lastFourDigit);
		int incByOne = cnvrtLastFourDigit + 1;
		String formatLastFourDig = String.format("%04d", incByOne);
		DynamicDataManager.setProperty("niclastfourdigitsmale", formatLastFourDig);

		String vldMaleNic = firstDigit + threeDigitsMale() + formatLastFourDig + "V";
		return vldMaleNic;
	}

	/**
	 * Valid juvenile male nic.
	 *
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws URISyntaxException
	 *             the URI syntax exception
	 */
	public String validJuvenileMaleNic() throws IOException, URISyntaxException {
		String firstDigit = DynamicDataManager.getProperty("juvenilenicyear");

		String lastFourDigit = DynamicDataManager.getProperty("niclastfourdigitsmale");
		int cnvrtLastFourDigit = Integer.parseInt(lastFourDigit);
		int incByOne = cnvrtLastFourDigit + 1;
		String formatLastFourDig = String.format("%04d", incByOne);
		DynamicDataManager.setProperty("niclastfourdigitsmale", formatLastFourDig);

		String vldMaleNic = firstDigit + threeDigitsMale() + formatLastFourDig + "V";
		return vldMaleNic;
	}

	/**
	 * Invalid nic.
	 *
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws URISyntaxException
	 *             the URI syntax exception
	 */
	public String invalidNic() throws IOException, URISyntaxException {
		String firstDigit = DynamicDataManager.getProperty("nicyear");

		String lastFourDigit = DynamicDataManager.getProperty("niclastfourdigitsfemale");
		int cnvrtLastFourDigit = Integer.parseInt(lastFourDigit);
		int incByOne = cnvrtLastFourDigit + 1;
		String formatLastFourDig = String.format("%04d", incByOne);
		DynamicDataManager.setProperty("niclastfourdigitsfemale", formatLastFourDig);

		String invldFemaleNic = firstDigit + generateFalseID() + formatLastFourDig + "V";
		return invldFemaleNic;
	}

	/**
	 * Gets the days.
	 *
	 * @param days
	 *            the days
	 * @return the days
	 */
	private int getDays(int days) {
		if (days > 500) {
			return (days - 500);
		} else {
			return days;
		}
	}

	/**
	 * Gets the date and months.
	 *
	 * @param days
	 *            the days
	 * @return the date and months
	 */
	private int[] getdateAndMonths(int days) {
		int sMonth = 0;
		int sDays = 0;
		int daysFromNic = getDays(days);
		int month[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		for (int i = 0; i < month.length; i++) {
			if (daysFromNic < month[i]) {
				sMonth = i + 1;
				sDays = daysFromNic;
				break;
			} else {
				daysFromNic = daysFromNic - month[i];
			}
		}
		return new int[] { sDays, sMonth };
	}

	/**
	 * Gets the gender using nic.
	 *
	 * @param nic
	 *            the nic
	 * @return the gender using nic
	 */
	public String getGenderUsingNic(String nic) {
		String M = "Male", F = "Female";
		int d = Integer.parseInt(nic.substring(4, 7));
		if (d > 500) {
			return F;
		} else {
			return M;
		}
	}

	/**
	 * Generate dob using nic.
	 *
	 * @param nic
	 *            the nic
	 * @return the string
	 */
	public String generateDobUsingNIC(String nic) {
		int year = 1900 + Integer.parseInt(nic.substring(0, 2));
		int days = Integer.parseInt(nic.substring(2, 5));
		int[] result = getdateAndMonths(days);
		String month = digitsInMonth(Integer.toString(result[1]));
		String dob = Integer.toString(year) + "-" + month + "-" + Integer.toString(result[0]);
		return dob;
	}
	
	/**
	 * Generate dob using nic and age.
	 *
	 * @param nic the nic
	 * @param age the age
	 * @return the string
	 */
	public String generateDobUsingNICAndAge(String nic, int age) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int birthYear = year - age;
		String bYear = Integer.toString(birthYear);
		int days = Integer.parseInt(nic.substring(2, 5));
		int[] result = getdateAndMonths(days);
		String month = digitsInMonth(Integer.toString(result[1]));
		String dob = month + "/" + Integer.toString(result[0]) + "/" + bYear;
		return dob;
	}

	/**
	 * Digits in month.
	 *
	 * @param month the month
	 * @return the string
	 */
	private String digitsInMonth(String month) {
		if (Integer.parseInt(month) < 10) {
			return month = "0" + month;
		} else {
			return month;
		}
	}
}
