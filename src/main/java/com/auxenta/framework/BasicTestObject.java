/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.FirefoxProfiles;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.core.FWObject;
import com.auxenta.framework.core.IPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.drivers.DefaultUIDriver;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.InvalidURLException;
import com.auxenta.framework.exceptions.StartPageClassException;
import com.auxenta.framework.exceptions.StartPageURLException;
import com.auxenta.framework.listeners.CapturePageOnFailureListener;
import com.auxenta.framework.tools.DateTools;
import com.auxenta.framework.tools.ParameterChecker;
import com.auxenta.framework.tools.ProcessTool;
import com.auxenta.framework.tools.email.MailSender;
import com.auxenta.framework.tools.os.WindowsProcessTool;
import com.auxenta.framework.util.Browser;
import com.beust.jcommander.ParameterException;

/**
 * This is the base class for all tests in auxtest framework. It takes care of
 * uidriver for test. It also provides function to start a page from config
 * setting.
 *
 * @author Niroshen Landerz
 * @date Aug 25th 2014
 */

@Listeners(value = { CapturePageOnFailureListener.class })
public class BasicTestObject extends FWObject {

	/**
	 * The Enum TestApplication.
	 */
	protected enum TestApplication {

		/** The Dialog. */
		Dialog
	}

	/** The working dir. */
	protected String workingDir;

	/** The download dir. */
	protected String downloadDir;

	/** The env. */
	protected static String env;

	/** The base_url. */
	protected String base_url;
	
	/** The Constant DEFAULT. */
	private static final String DEFAULT = "DEFAULT";

	/** The logger. */
	private static Logger logger = Logger.getLogger(BasicTestObject.class);

	/** The desired capabilities. */
	private DesiredCapabilities desiredCapabilities = null;

	/** The default_screen_shot_link. */
	private String default_screen_shot_link;

	/** The test method name. */
	public static String testMethodName;

	/**
	 * Instantiates a new basic test object.
	 */
	public BasicTestObject() {
		super();
	}

	/**
	 * Instantiates a new basic test object.
	 *
	 * @param driver
	 *            the driver
	 */
	public BasicTestObject(final IUIDriver driver) {
		super(driver);
	}

	/**
	 * Launch browser of your choice and go to {@code url}. Browser type and url
	 * provided in config.cfg will be ignored
	 *
	 * @param browser
	 *            the browser
	 * @param url
	 *            the url
	 */
	public void launchBrowser(final Browser browser, final String url) {
		uiDriver = new DefaultUIDriver(browser);
		uiDriver.maximizeWindow();
		uiDriver.get(ParameterChecker.checkURL(url));
	}

	/**
	 * Launch Firefox browser with specific Firefox profile and go to
	 * {@code startpageurl} which provided in {@code config/config.cfg} file.
	 *
	 * @param profile
	 *            - you can use predefined profile from {@link FirefoxProfiles}
	 *            class
	 */
	public void launchFirefoxBrowserWithProfile(final FirefoxProfile profile) {
		uiDriver = new DefaultUIDriver(profile);
		uiDriver.get(ParameterChecker.checkURL(config.getValue(ConfigKeys.KEY_START_PAGE_URL.getKey())));
	}

	/**
	 * Launch Firefox browser with specific Firefox profile and go to
	 * {@code url}.
	 *
	 * @param profile
	 *            - you can use predefined profile from {@link FirefoxProfiles}
	 *            class
	 * @param url
	 *            the url
	 */
	public void launchFirefoxBrowserWithProfile(final FirefoxProfile profile, final String url) {
		uiDriver = new DefaultUIDriver(profile);
		uiDriver.get(ParameterChecker.checkURL(url));
		uiDriver.maximizeWindow();
	}

	/**
	 * Launch browser with {@link DesiredCapabilities} and go to
	 * {@code startpageurl} which provided in {@code config/config.cfg} file.
	 *
	 * @param cap
	 *            the cap
	 */
	public void launchBrowserWithDesiredCapabilities(final DesiredCapabilities cap) {
		uiDriver = new DefaultUIDriver(cap);
		uiDriver.get(ParameterChecker.checkURL(config.getValue(ConfigKeys.KEY_START_PAGE_URL.getKey())));
	}

	/**
	 * Sets the desired capabilities.
	 *
	 * @param capabilities
	 *            the new desired capabilities
	 */
	public void setDesiredCapabilities(final DesiredCapabilities capabilities) {
		this.desiredCapabilities = capabilities;
	}

	/**
	 * Gets the start page.
	 *
	 * @return the start page
	 */
	public IPageObject getStartPage() {
		logger.trace("Calling getStartPage()");
		String pageclass = config.getValue(ConfigKeys.KEY_START_PAGE_CLASS.getKey());
		String pageurl = config.getValue(ConfigKeys.KEY_START_PAGE_URL.getKey());
		pageurl = validateParam(pageclass, pageurl);
		// print page URL to instruction log file
		logInstruction("Go to URL: " + pageurl);
		if (desiredCapabilities == null) {
			uiDriver = new DefaultUIDriver();
			uiDriver.get(pageurl);
		} else {
			this.launchBrowserWithDesiredCapabilities(desiredCapabilities);
		}

		// the following is needed if a website has an invalid ssl certificate
		sslCheck(pageurl);
		return initializePageObject(pageclass, pageurl);
	}

	/**
	 * {@code startpageurl} and {@code startpageclass} <br>
	 * must be specified in {@code config/config.cfg} file.<br>
	 * <br>
	 * Verifies url and page class is retreieved from config.cfg.<br>
	 * Checks to see if url/pageobject != null.<br>
	 *
	 * @param pageclass
	 *            the pageclass
	 * @param pageurl
	 *            the pageurl
	 * @return exceptions
	 */
	private String validateParam(final String pageclass, final String pageurl) {
		if (pageclass == null && "".equals(pageclass)) {
			throw new ParameterException("Failed to get value for 'startpageclass' from config.cfg");
		}
		if (pageurl == null || pageurl.equals("")) {
			throw new ParameterException("Failed to get value for 'startpageurl' from config.cfg");
		}
		try {
			return ParameterChecker.checkURL(pageurl);
		} catch (InvalidURLException e) {
			throw new StartPageURLException("Invalid url format for url:" + pageurl, e);
		}
	}

	/**
	 * Initialize page object.
	 *
	 * @param pageclass
	 *            the pageclass
	 * @param pageurl
	 *            the pageurl
	 * @return page object
	 */
	private IPageObject initializePageObject(final String pageclass, final String pageurl) {
		try {
			@SuppressWarnings("unchecked")
			Class<IPageObject> clazz = (Class<IPageObject>) Class.forName(pageclass);
			IPageObject pageObject;
			try {
				pageObject = clazz.getDeclaredConstructor(IUIDriver.class).newInstance(uiDriver);
				pageObject.setUrl(pageurl);
				return pageObject;
			} catch (InstantiationException e) {
				logger.error("InstantiationException ", e);
				throw new TestAutomationException("InstantiationException ", e);
			} catch (IllegalAccessException e) {
				logger.error("IllegalAccessException ", e);
				throw new TestAutomationException("IllegalAccessException ", e);
			} catch (IllegalArgumentException e) {
				logger.error("IllegalArgumentException ", e);
				throw new TestAutomationException("IllegalArgumentException ", e);
			} catch (InvocationTargetException e) {
				logger.error("InvocationTargetException ", e);
				throw new TestAutomationException("InvocationTargetException ", e);
			} catch (NoSuchMethodException e) {
				logger.error("NoSuchMethodException ", e);
				throw new TestAutomationException("NoSuchMethodException ", e);
			} catch (SecurityException e) {
				logger.error("SecurityException ", e);
				throw new TestAutomationException("SecurityException ", e);
			}

		} catch (ClassNotFoundException e) {
			StartPageClassException ex = new StartPageClassException(
					"Page object Class Not Found! 'startpageclass' = '" + pageclass
							+ "'. Make sure this class exists and not misspelled in config,cfg file.");
			logger.error("Error", ex);
			throw ex;
		}

	}

	/**
	 * Checks if page displayed shows warning<br>
	 * about invalid SSL certificate<br>
	 * .
	 *
	 * @param pageurl
	 *            the pageurl
	 */
	private void sslCheck(final String pageurl) {
		if (uiDriver.getWebDriver() instanceof InternetExplorerDriver && pageurl.startsWith("https://")) {
			String source = uiDriver.getPageSource();
			// following tests if page displayed is IE page warning about
			// invalid SSL certificate
			if (source.contains("Continue to this website (not recommended).")) {
				uiDriver.navigate().to("javascript:document.getElementById('overridelink').click()");
			}
		}
	}

	/**
	 * Prints the verified elements.
	 */
	public static void printVerifiedElements() {
		logger.debug("Totally verified: " + verificationCount + " elements");
		List<String> eles = verificationElements;
		for (String ele : eles) {
			logger.debug(ele);
		}
		logger.debug("Totally verified: " + verificationCount + " elements");
		for (String ele : eles) {
			logger.debug(ele);
		}
	}

	/**
	 * Before each test, use TestNG and Java Reflection to store and log the
	 * name of the current test.
	 *
	 * This is also wher
	 *
	 * @param browser
	 *            the browser
	 * @param os
	 *            the os
	 * @param browserVersion
	 *            the browser version
	 * @param context
	 *            the context
	 */
	@BeforeTest(alwaysRun = true)
	@Parameters({ "browser", "os", "browser_version","environment" , "url"})
	public void beforeTest(@Optional(DEFAULT) String browser,
			@Optional(DEFAULT) String os,
			@Optional(DEFAULT) String browserVersion, 
			@Optional ("null") String environment,
			@Optional ("null") String url, 
			ITestContext context) 
	{
		logger.info("======BEGIN Test workflow============");
		logger.info("BEGIN Test: " + context.getName());
		context.setAttribute(CapturePageOnFailureListener.SCREENSHOT_ATTRIBUTE, default_screen_shot_link);
		try {
			String driverURL = config.getValue(ConfigKeys.KEY_DRIVER_URL.getKey());
			if (driverURL != null && driverURL.contains("http")) {
				context.setAttribute(CapturePageOnFailureListener.DRIVER_URL_ATTRIBUTE, driverURL);
			}
		} catch (Exception e) {
			logger.warn("Unable to set driverUrl for listeners: ", e);
			throw new TestAutomationException("Unable to set driverUrl for listeners", e);
		}

		sameOrDefault(os, ConfigKeys.KEY_OS, config);
		sameOrDefault(browser, ConfigKeys.KEY_BROWSER, config);
		sameOrDefault(browserVersion, ConfigKeys.KEY_BROWSER_VERSION, config);
		String browserName = config.getValue(ConfigKeys.KEY_BROWSER.getKey());

		Browser browserType = Browser.valueOf(browserName.toUpperCase());
		multiplyTimeouts(browserType.getTimeout());
	}
	
	@BeforeClass (alwaysRun = true)
	public void beforeClass(){
		logger.info("Tests of the class " + super.getClass().getName()+ " execution started" );
	}
	
	@AfterClass(alwaysRun=true)
	public void afterClass() {
		
	}
	
	/**
	 * After test.
	 *
	 * @param context
	 *            the context
	 */
	@AfterTest(alwaysRun = true)
	public void afterTest(final ITestContext context) {
		logger.info("END Test: " + context.getName());
		logger.info("======END Test workflow============");
	}
	
    /**
     * Before each test method, use TestNG and Java Reflection to store and log
     * the name of the current test method.
     *
     * @param method
     *            the method
     */
	@BeforeMethod(alwaysRun = true)
	@Parameters({ "environment", "url" })
	public void beforeMethod(@Optional("null") String environment, @Optional("null") String url, ITestContext context,
			final Method method) {
		logger.info("-------------BEGIN Test Method-------------------");
		logger.info("BEGIN Test Method: " + method.getName());
		testMethodName = method.getName();

		String retryTest = config.getValue("quitdriveronfailure");
		String calledByFailed = config.getValue("testFailed");
		if (retryTest.equals("true") && calledByFailed.equals("true"))
		{
			config.setValue("testFailed", "");
		}
		if (uiDriver != null) {
			try {
				uiDriver.close();
				uiDriver.quit();
				new WindowsProcessTool().FirefoxAndChromeKiller(config, config.getValue("browser"));
			} catch (Exception e) {
				logger.info(e.getMessage());
			}
		}
		initializeTestEnvironment(TestApplication.Dialog, environment, url, context);
		return;
		}

	/**
	 * After method.
	 *
	 * @param testMethod
	 *            the test method
	 */
	@AfterMethod(alwaysRun = true)
	public void afterMethod(final Method testMethod) {
		logger.info("END Test Method: " + testMethod.getName());
		logger.info("-------------END Test Method-------------------");
	}

	/** Before Suite. Suite is an XML, and this method suppose to execute once before a suite begins it's execution
	 * @param context
	 * @throws IOException
	 */
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite(ITestContext context) throws IOException{
		logger.info("Starting the test Suite: " + context.getCurrentXmlTest().getSuite().getName() );
		logger.info("-------------TEST SUITE STARTED-------------------");
		if (config.getValue("configure.clean.results.file").equalsIgnoreCase("true")){
			String resultsHtmlFile = config.getValue("projects.base.dir") + config.getValue("results.dir") + "Test_Result.html";
			logger.info(resultsHtmlFile);
			File file = new File(resultsHtmlFile);
			logger.info("-------------Deleting the file-------------------");
//			FileHelper.deleteFile(file);
			new WindowsProcessTool().FirefoxAndChromeKiller(config, config.getValue("browser"));
		}
	}
	
	/**
	 * After suite.
	 *
	 * @param processCountWarn
	 *            the process count warn
	 * @throws MessagingException
	 */
	@SuppressWarnings("static-access")
	@AfterSuite(alwaysRun = true)
	@Parameters({ "processCountWarn" })
	public void afterSuite(@Optional("5") int processCountWarn) throws MessagingException {
		DateTools date = new DateTools();
		new WindowsProcessTool().FirefoxAndChromeKiller(config, config.getValue("browser"));
		if (config.getValue("configure.email").equalsIgnoreCase("true")){
			
		String resultsHtmlFilePath = config.getValue("projects.base.dir") + config.getValue("results.dir");
		String fileName = "Test_Result.html";
		
		MailSender gmailSender = new MailSender();
		
		gmailSender.setTo(config.getValue("email.sendTo"));
		gmailSender.setCc(config.getValue("email.sendCC"));
		gmailSender.setBcc(config.getValue("email.sendBCC"));
		gmailSender.setSubject(config.getValue("email.Subject") + " " + date.getTimeStamp("yyyy.MM.dd_'at'_HH.mm") );
		gmailSender.setBody(config.getValue("email.Body") + " " + config.getValue("startpageurl"));
		gmailSender.addAttachment(resultsHtmlFilePath, fileName);
		gmailSender.send();
		}
	}
	
	/**
	 * Same or default.
	 *
	 * @param param
	 *            the param
	 * @param key
	 *            the key
	 * @param config
	 *            the config
	 */
	public void sameOrDefault(final String param, final ConfigKeys key, final IConfig config) {
		if (!param.equals(DEFAULT)) {
			config.setValue(key.getKey(), param);
		}

	}

	/**
	 * Sets the default screenshot link.
	 *
	 * @param pathPrefix
	 *            the new default screenshot link
	 */
	public void setDefaultScreenshotLink(final String pathPrefix) {
		this.default_screen_shot_link = pathPrefix;
	}

	/**
	 * Gets the default screenshot link.
	 *
	 * @return the default screenshot link
	 */
	public String getDefaultScreenshotLink() {
		return this.default_screen_shot_link;
	}

	// Internal Methods
	/**
	 * Initialize test environment.
	 * 
	 * @param environment
	 *            the environment
	 * @return the default config
	 */
	private DefaultConfig initializeTestEnvironment(String environment) {
		// Set the environment. If it's not in the XML file, set it to the the
		// value in the
		// config.cfg file
		if (environment == null) {
			env = config.getValue(ConfigKeys.KEY_ENVIRONMENT.getKey());
		} else
			env = environment;

		if (env.equals(""))
			Assert.fail("No value is set for environment in either the XML file or config.cfg file");

		String configFile = "/config/config.cfg";
		return new DefaultConfig(configFile);
	}

	/**
	 * Initialize test environment.
	 * 
	 * @param application
	 *            the application
	 * @param environment
	 *            the environment
	 * @param url
	 *            the url
	 * @param context
	 *            the context
	 */
	protected void initializeTestEnvironment(final TestApplication application, final String environment,
			String url, final ITestContext context) {

		workingDir = System.getProperty("user.dir");
		downloadDir = context.getOutputDirectory();
		config = initializeTestEnvironment(environment);

		if (url == null) {
			if (TestApplication.Dialog.equals(application))
				url = config.getValue("startpageurl");
		}
		if (uiDriver != null){
			uiDriver.quit();
		}
		
		if (config.getValue("browser").equalsIgnoreCase("chrome"))
		{
			new ProcessTool().killProcess("chromedriver.exe");
		}
		
		if (config.getValue("browser").equalsIgnoreCase("internetexplorer"))
		{
			new ProcessTool().killProcess("IEDriverServer.exe");
		}
		launchBrowser(url, context);
	}
	
	/**
	 * Multiply timeouts.
	 *
	 * @param timeoutMultiplier
	 *            the timeout multiplier
	 */
	private void multiplyTimeouts(final int timeoutMultiplier) {
		for (ConfigKeys cfgKey : ConfigKeys.values()) {
			if (cfgKey.toString().contains("TIMEOUT")) {
				String timeout = config.getValue(cfgKey.getKey());
				if (timeout != null && !timeout.isEmpty()) {
					config.setValue(cfgKey.getKey(), "" + Long.parseLong(timeout) * timeoutMultiplier);
				}
			}
		}

	}

	/**
	 * Launch browser.
	 *
	 * @param url            the url
	 * @param context the context
	 */
	private void launchBrowser(String url, ITestContext context) {
		if (config.getValue("browser").equalsIgnoreCase("firefox")) {
	
			FirefoxProfile profile = FirefoxProfiles.getNoPromptOnDownloadProfile(context
					.getOutputDirectory());
			profile.setPreference("dom.max_script_run_time", "120");
			launchFirefoxBrowser(url, profile);
		} else {
			uiDriver = new DefaultUIDriver();
			uiDriver.maximizeWindow();
			uiDriver.get(url);
			base_url = uiDriver.getCurrentUrl();
		}
	}

	/**
	 * Launch firefox browser.
	 * 
	 * @param url
	 *            the url
	 * @param profile
	 *            the profile
	 */
	private void launchFirefoxBrowser(String url, FirefoxProfile profile) {
		launchFirefoxBrowserWithProfile(profile, url);
		base_url = uiDriver.getCurrentUrl();
		// loginPage = new LoginPage(uiDriver);
	}
}
