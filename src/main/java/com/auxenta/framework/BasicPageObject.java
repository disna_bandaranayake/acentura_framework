/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.auxenta.framework.core.IPageObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.InvalidURLException;
import com.auxenta.framework.locators.ElementLocatorByDriver;
import com.auxenta.framework.locators.IElementLocator;
import com.auxenta.framework.ui.support.AdfClientSynchedWithServerChecker;
import com.auxenta.framework.util.ByUtil;
import com.auxenta.framework.util.UIType;
import com.google.common.base.Function;

/**
 * The Class BasicPageObject.
 * 
 * @author Niroshen Landerz
 * @date Aug 24th 2014
 * 
 *       This is the base class for all pages in auxtest framework. It
 *       implements pageobject interface and provides functions for initializing
 *       start page.
 */

public class BasicPageObject extends BasicObject implements IPageObject {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(BasicPageObject.class);

	/** The Constant WEB_DRIVER_WAIT_TIME. */
	private static final long WEB_DRIVER_WAIT_TIME = 30;

	/** The Constant TIME_OUT_IN_SECOND. */
	private static final int TIME_OUT_IN_SECOND = 30;

	/** The Constant TREAD_SLEEP_TIME. */
	private static final long TREAD_SLEEP_TIME = 500;

	/** The page url. */
	private String pageUrl = "";

	/**
	 * Instantiates a new basic page object.
	 * 
	 * @param driver
	 *            the driver
	 */
	protected BasicPageObject(final IUIDriver driver) {
		super(driver);

		if (config_pageload_timeout > 0) {
			uiDriver.sleep(config_pageload_timeout);
		}
	}

	/**
	 * Instantiates a new basic page object.
	 * 
	 * @param driver
	 *            the driver
	 * @param url
	 *            the url
	 */
	protected BasicPageObject(final IUIDriver driver, final String url) {
		super(driver);
		this.pageUrl = url;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.BasicObject#getLocator(com.auxenta
	 * .test.auxtest.framework.util.UIType, java.lang.String)
	 */
	public IElementLocator getLocator(final UIType uiType, final String value) {
		return new ElementLocatorByDriver(uiDriver, ByUtil.getBy(uiType, value));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.core.IPageObject#setUrl(java.lang.
	 * String)
	 */
	public void setUrl(final String url) {
		pageUrl = url;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.auxenta.test.auxtest.framework.core.IPageObject#getUrl()
	 */
	public String getUrl() {
		return pageUrl;
	}

	/**
	 * Checks if is displayed.
	 * 
	 * @return true, if is displayed
	 */
	public boolean isDisplayed() {
		return this.pageUrl.equals(this.uiDriver.getCurrentUrl());
	}

	/**
	 * Waits for the page to be displayed.
	 * 
	 * @param timeOutInSeconds
	 *            the max seconds to wait
	 */
	public void waitToBeDisplayed(final long timeOutInSeconds) {
		final String expectedUrl = this.pageUrl;
		if (!StringUtils.isBlank(expectedUrl)) {
			WebDriverWait wait = new WebDriverWait(uiDriver, timeOutInSeconds);
			wait.until(new Function<WebDriver, Boolean>() {

				@Override
				public Boolean apply(final WebDriver input) {
					return expectedUrl.equals(input.getCurrentUrl());
				}
			});
		}
	}

	/**
	 * Waits for the page to be displayed.
	 * 
	 * @param pauseForMillisecs
	 *            the minimum wait time in milliseconds
	 * @param timeOutInSeconds
	 *            the max seconds to wait
	 */
	public void waitToBeDisplayed(final long pauseForMillisecs, final long timeOutInSeconds) {
		this.uiDriver.sleep(pauseForMillisecs);
		this.waitToBeDisplayed(timeOutInSeconds);
	}

	/**
	 * Waits for the page to be displayed.
	 */
	public void waitToBeDisplayed() {
		this.waitToBeDisplayed(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.auxenta.test.auxtest.framework.core.IPageObject#start()
	 */
	public void start() {
		if (uiDriver != null && !"".equals(pageUrl)) {
			uiDriver.get(pageUrl);
		} else {
			throw new InvalidURLException("Page URL NUll");
		}
	}

	/**
	 * Wait for ajax.
	 * 
	 * @author RasithaE Use this method with care. Not tested, but should work
	 *         for most places. Comments added by Niroshen
	 */
	public void waitForAjax() {
		int timeoutInSeconds = TIME_OUT_IN_SECOND;

		logger.info("Checking active ajax calls by calling jquery.active");
		try {
			if (uiDriver instanceof JavascriptExecutor) {
				JavascriptExecutor jsDriver = (JavascriptExecutor) uiDriver;

				for (int i = 0; i < timeoutInSeconds; i++) {
					Object numberOfAjaxConnections = jsDriver.executeScript("return $.active");
					// return should be a number
					if (numberOfAjaxConnections instanceof Long) {
						Long n = (Long) numberOfAjaxConnections;
						logger.info("Number of active jquery ajax calls: " + n);
						if (n.longValue() == 0L) {
							break;
						}
					}
					Thread.sleep(TREAD_SLEEP_TIME);
				}
			} else {
				logger.info("Web driver: " + uiDriver + " cannot execute javascript");
			}
		} catch (InterruptedException e) {
			logger.error("InterruptedException ", e);
			throw new TestAutomationException("InterruptedException ", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.core.IPageObject#waitTillPageLoadComplete
	 * ()
	 */
	@Override
	public void waitTillPageLoadComplete() {

		AdfClientSynchedWithServerChecker adfClientSynchedWithServerChecker = new AdfClientSynchedWithServerChecker();
		WebDriverWait wait = new WebDriverWait(uiDriver, WEB_DRIVER_WAIT_TIME);
		try {
			wait.until(adfClientSynchedWithServerChecker);
		} catch (TimeoutException exception) {
			logger.info("Page time out exception");
		}

	}

	public void waitTillPageLoadCompleteOld() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(final WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals(
						"complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(uiDriver, WEB_DRIVER_WAIT_TIME);
		wait.until(pageLoadCondition);

	}
}
