/*
 *================================================================
 * Copyright  (c)     : 2015 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.ui.support;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * The Class ClientSynchedWithServer.
 */
public class AdfClientSynchedWithServerChecker implements ExpectedCondition<Boolean> {


	// return false if AdfPage object and functions do not exist
	// if they do exist return true if page is fully loaded and ready or reason
	// why this is not completed yet
	/** The js. */
	String js = "return typeof AdfPage !== 'undefined' && " + "typeof AdfPage.PAGE !== 'undefined' && "
			+ "typeof AdfPage.PAGE.isSynchronizedWithServer === 'function' && "
			+ "(AdfPage.PAGE.isSynchronizedWithServer() || "
			+ "(typeof AdfPage.PAGE.whyIsNotSynchronizedWithServer === 'function' && "
			+ "AdfPage.PAGE.whyIsNotSynchronizedWithServer()))";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.common.base.Function#apply(java.lang.Object)
	 */
	@Override
	public Boolean apply(WebDriver driver) {
		JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
		Boolean pageResult = jsDriver.executeScript("return document.readyState").equals("complete");
		
		if (pageResult) {
			Object result = jsDriver.executeScript(js);
			//logger.error("ADF page ready " + result);
			return Boolean.TRUE.equals(result);
		} else {
			//logger.error("ADF page not loaded yet");
			return false;
		}

	}
}
