/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import org.apache.log4j.Logger;
import org.testng.Reporter;

/**
 * An abstract class for all objects in auxtest framework.
 *
 * @author Niroshen Landerz
 * @date Aug 29th 2014
 */

public abstract class FWObject extends FWNoUIDriverObject implements IUIObject {

    // Logger object for human-readable steps to reproduce automatic actions.
    // this must be a static variable so all child instances write to the same logger
    /** The logger. */
    private static Logger LOGGER = Logger.getLogger(FWObject.class);

    /** The ui driver. */
    protected IUIDriver uiDriver;

    /**
     * Instantiates a new FW object.
     */
    public FWObject() {
        super();
        LOGGER.trace("FWObject.FWObject(0)");
    }

    /**
     * Instantiates a new FW object.
     *
     * @param driver the driver
     */
    public FWObject(final IUIDriver driver) {
        this();
        LOGGER.trace("FWObject.FWObject(1)");
        uiDriver = driver;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIObject#setUIDriver(com.auxenta.test.auxtest.
     * framework.core.IUIDriver)
     */
    @Override
    public void setUIDriver(final IUIDriver uiDriverVal) {
        this.uiDriver = uiDriverVal;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIObject#getUIDriver()
     */
    @Override
    public IUIDriver getUIDriver() {
        return uiDriver;
    }

    /**
     * Log an instruction that a person should follow to reproduce one step of a test. Passed string
     * is logged to the HTML reports
     *
     *
     *
     * @param instruction
     *            - instruction that a person should follow to reproduce one step of a test
     */
    public void logInstruction(final String instruction) {
        Reporter.log("&bull; " + instruction + "<br/>");
        LOGGER.info(instruction);
    }
}
