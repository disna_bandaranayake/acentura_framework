/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import java.util.ArrayList;
import java.util.logging.Level;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;

import com.auxenta.framework.drivers.IUIAlert;
import com.auxenta.framework.drivers.IUIFrameLocator;
import com.auxenta.framework.drivers.IUIWindowLocator;
import com.auxenta.framework.util.SearchScope;
import com.auxenta.framework.util.UIType;

/**
 * <li><b>Present</b> - Element in DOM
 *
 * <li><b>Absent</b> - Element is NOT in DOM
 *
 * <li><b>Displayed</b> - Element is in DOM and is visible
 *
 * <li><b>Hidden</b> - Element is in DOM and is NOT visible
 *
 * <li><b>Wait</b> - Waits for something to happen and THROWS TimeOutException on failure
 *
 * <li><b>is/has</b> - Returns boolean, true or false, does not throw exception.
 *
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */
public interface IUIDriver
        extends WebDriver, IUIObjectNoUIDriver, JavascriptExecutor, HasCapabilities, Navigation {

	
    /**
     * Delete all the cookies for the current domain.
     *
     * @see Options#deleteAllCookies()
     */
    void deleteAllCookies();

    /**
     * Execute script.
     *
     * @param script the script
     * @return the string
     */
    String executeScript(String script);

    /**
     * Load URL.
     *
     * @param arg0 the arg0
     */
    void get(String arg0);

    /**
     * Gets the all links on page.
     *
     * @return the all links on page
     */
    ArrayList<String> getAllLinksOnPage();

    /**
     * JavaScript call {@code return [element].innerHTML; }
     *
     * <p>
     * Returns the innerHTML of the provided UIElement as returned by the javascript method
     * [element].innerHTML, if and only if the browser's javascript engine supports innerHTML.
     * </p>
     *
     * @param element
     *            as the UIElement
     * @return the innerHTML of the UIElement
     */
    String getInnerHtmlOf(IUIElement element);

    /**
     * JavaScript call {@code return [element].outerHTML; }
     *
     * <p>
     * Returns the outerHTML of the provided UIElement as returned by the javascript method
     * [element].outerHTML, if and only if the browser's javascript engine supports outerHTML.
     * </p>
     *
     * @param element
     *            as the UIElement
     * @return the outerHTML of the UIElement
     */
    String getOuterHtmlOf(IUIElement element);

    /**
     * JavaScript call {@code return document.getElementById(id).getAtribute(attribute)}
     *
     * @param id the id
     * @param attribute the attribute
     * @return the attribute from disabled field by id
     */
    String getAttributeFromDisabledFieldByID(String id, String attribute);

    /**
     * Uses JavaScript call {@code return document.getElementById(id).value}
     *
     * @param id the id
     * @return the value from disabled field by id
     */
    String getValueFromDisabledFieldByID(String id);

    /**
     * Checks if is available.
     *
     * @return true, if is available
     */
    boolean isAvailable();

    /**
     * Checks if is window title contains.
     *
     * @param substring            - substring to look for in page title
     * @return {@code true} if page title contains {@code substring}, {@code false} otherwise
     */
    boolean isWindowTitleContains(String substring);

    /**
     * Gets the window size.
     *
     * @return the window size
     */
    Dimension getWindowSize();

    /**
     * Gets the client screen size.
     *
     * @return the client screen size
     */
    Dimension getClientScreenSize();

    /**
     * Maximize current window to full screen.
     */
    void maximizeWindow();

     /**
      * JavaScript call to scroll to the bottom of the page.
      */
    void scrollToPageBottom();

     /**
      * Wait for text on page.
      *
      * @param expectedArray the expected array
      * @return array of strings that are present on page
      */
    String[] waitForTextOnPage(String[] expectedArray);

    /**
     * Wait for text to be absent in DOM.
     *
     * @param expected the expected
     */
    void waitToBeAbsent(IUIElement expected);

    /**
     * Wait for text to be absent in DOM.
     *
     * @param expected the expected
     * @param timeout the timeout
     */
    void waitToBeAbsent(IUIElement expected, long timeout);

    /**
     * Wait for {@code expected} element to be present in the DOM.
     *
     * @param expected the expected
     */
    void waitToBePresent(IUIElement expected);

    /**
     * Wait for {@code expected} element to be present in the DOM.
     *
     * @param expected the expected
     * @param timeout the timeout
     */
    void waitToBePresent(IUIElement expected, long timeout);

    /**
     * Wait for {@code stalling} element to be appear for {@code timeToAppear} and if it appears,
     * wait {@code timeToDisappear} to be hidden from the page.
     *
     * @param stallingElement the stalling element
     * @param timeToAppear the time to appear
     * @param timeToBeHidden the time to be hidden
     */
    void waitForStallingElementToBeHidden(
            IUIElement stallingElement,
            long timeToAppear,
            long timeToBeHidden);

    /**
     * Gets the UI alert.
     *
     * @return the UI alert
     */
    IUIAlert getUIAlert();

    /**
     * Gets the web driver.
     *
     * @return the web driver
     */
    WebDriver getWebDriver();

    /**
     * Is the specified text visible anywhere on the page regardless of which element contains it?.
     *
     * @param text            to find
     * @return true if an element on the page contains this text
     */
    boolean isTextVisible(String text);

    /**
     * Gets the UI object type.
     *
     * @return the UI object type
     */
    // @Deprecated
    String getUIObjectType();

    /**
     * Test if browser has closed by trying to count the windows. This will throw a
     * SessionNotFoundException if the browser is closed;
     * hasQuit() is the opposite of isAvailable().
     * neither will ever throw an exception.
     * 
     * @return true if the browser has closed.
     */
    boolean hasQuit();

    /**
     * Wait for {@code expected} element to be displayed on the page.
     *
     * @param expected the expected
     */
    void waitToBeDisplayed(IUIElement expected);

    /**
     * Wait for {@code expected} element to be displayed on the page.
     *
     * @param expected the expected
     * @param timeout the timeout
     */
    void waitToBeDisplayed(IUIElement expected, long timeout);

    /**
     * Wait for {@code expected} element to be hidden.
     *
     * @param expected the expected
     */
    void waitToBeHidden(IUIElement expected);

    /**
     * Wait for {@code expected} element to be hidden.
     *
     * @param expected the expected
     * @param timeout the timeout
     */
    void waitToBeHidden(IUIElement expected, long timeout);

    /**
     * Gets the UI window locator.
     *
     * @return the UI window locator
     */
    IUIWindowLocator getUIWindowLocator();

    /**
     * Which text is on page.
     *
     * @param expectedArray the expected array
     * @return the string[]
     */
    String[] whichTextIsOnPage(String[] expectedArray);

    /**
     * Gets the UI frame locator.
     *
     * @return the UI frame locator
     */
    IUIFrameLocator getUIFrameLocator();

    /**
     * Wait until {@code <title>} matches {@code title}.
     *
     * @param title the title
     * @param scope the scope
     */
    void waitForTitleToMatch(String title, SearchScope scope);

    /**
     * Check if {@code text} is present in {@code <title>}.
     *
     * @param text the text
     * @return true if text is substring of title, false otherwise
     */
    boolean isTextPresentInTitle(String text);

    /**
     * Get xpath for any element on the page.
     *
     * @param element the element
     * @return the x path
     */
    String getXPath(WebElement element);

    /**
     * Wait for title to change.
     *
     * @param previousTitle the previous title
     */
    void waitForTitleToChange(String previousTitle);

    /**
     * Wait for title to change.
     *
     * @param previousTitle the previous title
     * @param timeout the timeout
     */
    void waitForTitleToChange(String previousTitle, long timeout);

    /**
     * Position the browser using javascript. Useful for local testing or ensuring that the size is
     * large enough to display the page
     *
     * @param x
     *            left side of browser from left side of screen
     * @param y
     *            top of browser from top of screen
     * @param height
     *            height of browser
     * @param width
     *            Width of browser
     */
    void moveBrowserToXYHeightWidth(int x, int y, int height, int width);

    /**
     * Determine if a capability is listed as available by the current webdriver.
     *
     * @param capability the capability
     * @return true if the String appears in the list of capabilities
     */
    boolean hasCapability(String capability);

    /**
     * Find ui element.
     *
     * @param type the type
     * @param value the value
     * @return the IUI element
     */
    IUIElement findUIElement(UIType type, String value);

    /**
     * Find ui element.
     *
     * @param type the type
     * @param value the value
     * @param description the description
     * @return the IUI element
     */
    IUIElement findUIElement(UIType type, String value, String description);

    /**
     * Gets the console log.
     *
     * @param levelFilter the level filter
     * @return the console log
     */
    LogEntries getConsoleLog(Level levelFilter);

    /**
     * Send a CharSequence (such as from the {@link Keys} enum) to the browser. This is useful for
     * sending Keys.TAB to switch between elements. see
     * {@link org.openqa.selenium.interactions.Actions#sendKeys(java.lang.CharSequence...)}
     *
     * @param keysToSend the keys to send
     */
    void sendKeysToBrowser(CharSequence... keysToSend);

    /**
     * Wait for {@code text} to display.
     *
     * @param text the text
     */
    void waitForTextToDisplay(String text);

    /**
     * Wait for {@code text} to display.
     *
     * @param text the text
     * @param timeout the timeout
     */
    void waitForTextToDisplay(String text, long timeout);

    /**
     * Wait for {@code text} to be hidden.
     *
     * @param text the text
     */
    void waitForTextToHide(String text);

    /**
     * Wait for {@code text} to be hidden.
     *
     * @param text the text
     * @param timeout the timeout
     */
    void waitForTextToHide(String text, long timeout);

    /**
     * Wait for text on page.
     *
     * @param expectedArray the expected array
     * @param timeout the timeout
     * @return array of strings that are present on page
     */
    String[] waitForTextOnPage(String[] expectedArray, long timeout);

    /**
     * Wait until {@code <title>} matches {@code title}.
     *
     * @param title the title
     * @param scope the scope
     * @param timeout the timeout
     */
    void waitForTitleToMatch(String title, SearchScope scope, long timeout);

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebDriver#quit()
     */
    void quit();
    
    /**
     * Driver will wait till the ajax components are been completed on loading
     */
    void waitForAjax();
    
    /**
     * Driver will wait till the Adf Page load completed using a Script exposed in ADF framework. Can only be used on adf pages 
     */
    void waitTillAdfPageLoadComplete();
    
    /**
     * Driver will wait till page load complete using a simple dom script
     */
    void waitTillPageLoadComplete();
    
    IUIElement FindByTextJQuery(IUIDriver driver, String Tagname, String Text);
    
     
    
}
