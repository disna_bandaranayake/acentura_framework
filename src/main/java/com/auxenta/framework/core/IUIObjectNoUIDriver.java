package com.auxenta.framework.core;

import java.util.concurrent.TimeUnit;

/**
 * An interface for uiobject that has no UIDriver
 * 
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 * 
 */

public interface IUIObjectNoUIDriver {

    /**
     * Causes the currently executing thread to sleep
     *
     * @param ms
     *            - the length of time to sleep in milliseconds
     */
    void sleep(long ms);

    /**
     * Causes the currently executing thread to sleep
     *
     * @param ms
     *            - the length of time to sleep in milliseconds
     * @param reason
     *            - Why do we need this wait
     */
    void sleep(long ms, String reason);

    /**
     * Causes the currently executing thread to sleep
     *
     * @param time
     * @param unit
     * @param reason
     *            - Why do we need this wait
     */
    void sleep(long time, TimeUnit unit, String reason);

    /**
     * Causes the currently executing thread to sleep
     *
     * @param time
     * @param unit
     */
    void sleep(long time, TimeUnit unit);
}
