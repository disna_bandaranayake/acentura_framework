/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import java.util.List;

import org.openqa.selenium.By;

import com.auxenta.framework.exceptions.TimeOutException;

/**
 * @author Niroshen Landerz
 * @date Aug 25th 2014
 *
 * {@link IUIElement} group
 * 
 */
public interface IUIElements extends IUIEleBase {

    IUIElements replaceValues(String... values);

    /**
     * How many {@link IUIElement} objects do we have in {@link IUIElements}
     *
     * @return
     */
    int length();

    /**
     * Gets the UI elements list.
     *
     * @return the UI elements list
     */
    List<IUIElement> getUIElementsList();

    /**
     * Under current {@link IUIElements}, get {@link IUIElement} by index
     * 
     * @param index
     * @return {@link IUIElement} or null if index is out of bound
     */
    IUIElement getUIElementByIndex(int index);

    /**
     * Under current {@link IUIElements}, get {@link IUIElement} based on its innerText
     *
     * @param text
     * @return {@link IUIElement} or null if element is not in the list
     */
    IUIElement getUIElementByText(String text);

    /**
     * Under current {@link IUIElements}, get {@link IUIElement} by index, and than get its child
     * using {@link By}
     *
     * @param index
     * @param childBy
     * @return {@link IUIElement} or null if index was out of bound
     */
    IUIElement getChildUIElementByIndex(int index, By childBy);

    /**
     * Under current {@link IUIElements}, get {@link IUIElement} by index and then get List of its
     * child {@link IUIElement}s using {@link By}
     *
     * @param index
     * @param childBy
     * @return Elements under specific index found by {@code childBy} or null
     */
    List<IUIElement> getChildUIElementsByIndex(int index, By childBy);

    /**
     * Under current {@link IUIElements}, get {@link IUIElement} by text and then get its child using
     * {@link By}
     *
     * @param text
     * @param childBy
     * @return
     */
    IUIElement getChildUIElementByText(String text, By childBy);

    /**
     * Under current {@link IUIElements}, get {@link IUIElement} by text and then get List of its
     * child {@link IUIElement}s using {@link By}
     *
     * @param text
     * @param childBy
     * @return
     */
    List<IUIElement> getChildUIElementsByText(String text, By childBy);

    /**
     * Get all children under current {@link IUIElements} using {@link By}
     *
     * @param childBy
     * @return
     */
    List<IUIElement> getAllChildUIElements(By childBy);

    /**
     * Wait for all elements in {@link IUIElements} list to be gone from DOM
     *
     */
    void waitToBeAbsent();

    /**
     * Wait for all in {@link #elements} list to be gone from DOM
     *
     * @param timeout
     */
    void waitToBeAbsent(long timeout);

    /**
     * Wait for all {@link IUIElements} to be hidden
     *
     */
    void waitToBeHidden();

    /**
     * Wait for all {@link IUIElements} to be hidden
     *
     * @param timeout
     */
    void waitToBeHidden(long timeout);

    /**
     * Wait for all {@link IUIElements} to be displayed
     *
     */
    void waitToBeDisplayed();

    /**
     * Wait for all {@link IUIElements} to be displayed
     *
     * @param timeout
     */
    void waitToBeDisplayed(long timeout);

    /**
     * Wait for all {@link IUIElements} to be in DOM
     *
     * @param timeout
     * @throws TimeOutException
     *             if {@link #elements} list is null after timeout
     */
    void waitToBePresent(long timeout);

    /**
     * Wait for all {@link IUIElements} to be in DOM
     *
     * @throws TimeOutException
     *             if {@link #elements} list is null after timeout
     */
    void waitToBePresent();

    /**
     * Is list of {@link IUIElements} initialized ?
     *
     * @return false if {@link #elements} == null, true otherwise
     */
    boolean areInitialized();

    /**
     * Locate all {@link IUIElements}
     */
    void initializeAll();

    /**
     * Locate all elements
     *
     * @return false if list of {@link IUIElements} is null, true otherwise
     */
    boolean doFindAll();

    /**
     * Locate all elements
     *
     * @return false if list of {@link IUIElements} is null, true otherwise
     */
    boolean areAllPresent();

    /**
     * Wait for {@code numberOfElements} to appear in {@link IUIElements}
     *
     * @param numberOfElements
     *            - minimum number of elements to be present in {@link IUIElements}
     * @param timeout
     */
    void waitForNumberOfElementsToBePresent(int numberOfElements, long timeout);

    /**
     * Wait for {@code numberOfElements} to appear in {@link IUIElements}
     *
     * @param numberOfElements
     *            - minimum number of elements to be present in {@link IUIElements}
     */
    void waitForNumberOfElementsToBePresent(int numberOfElements);

    /**
     * Are all {@link IUIElements} displayed
     *
     * @return true if all elements are visible, false otherwise
     */
    boolean areAllDisplayed();

    /**
     * Return a visible element in the elements group.
     *
     * @return
     */
    IUIElement getOneDisplayedElement();

    /**
     * Return a visible element in the elements group in certain time.
     *
     * @return
     */
    IUIElement getOneDisplayedElement(long timeout);
}
