/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.ConfigFileException;
import com.auxenta.framework.util.Timeout;

/**
 * An abstract class for all objects in auxtest framework.
 * 
 * @author Niroshen Landerz
 * @date Aug 28th 2014
 */

public abstract class FWNoUIDriverObject implements IUIObjectNoUIDriver {

	/** The logger. */
	private static Logger logger = Logger.getLogger(FWNoUIDriverObject.class);

	/** The config. */
	protected static IConfig config = DefaultConfig.getConfig();

	/** The default str. */
	protected static String defaultStr = "";

	/** The todo. */
	protected static String TODO = "To be realized ....";

	/** The verification count. */
	protected static long verificationCount;

	/** The verification elements. */
	protected static List<String> verificationElements = new ArrayList<String>();

	// Default timeout for framework to verify an element
	/** The config_verify_timeout. */
	protected static long config_verify_timeout = 30000;
	// Default interval time for framework to verify an element
	/** The config_verify_interval. */
	protected static long config_verify_interval = 1000;
	// Default timeout for framework to cache an element (cache function is
	// removed)
	/** The config_element_timeout. */
	protected static long config_element_timeout = 30000;
	// Default timeout for framework to cache elements (cache function is
	// removed)
	/** The config_elements_timeout. */
	protected static long config_elements_timeout = 30000;
	// Default timeout for all waiting function in framework
	/** The config_wait_timeout. */
	protected static long config_wait_timeout = 30000;
	// Default timeout for a page to be loaded
	/** The config_pageload_timeout. */
	protected static long config_pageload_timeout = 30000;

	/**
	 * Instantiates a new FW no ui driver object.
	 */
	public FWNoUIDriverObject() {
		initConfig();
		setupConfigValue();
	}

	/**
	 * Inits the config.
	 */
	private void initConfig() {
		try {
			logger.trace("FWNoUIDriverObject.initConfig");
			if (config == null) {
				config = DefaultConfig.getConfig();
			}
		} catch (ConfigFileException ex) {
			logger.debug(ex);
			throw new TestAutomationException("Configuration falier ", ex);
		}
	}

	/**
	 * Setup config value.
	 */
	private void setupConfigValue() {
		logger.trace("FWNoUIDriverObject.setupConfigValue start");
		config_verify_timeout = config.getLongValue(ConfigKeys.KEY_VERIFY_TIMEOUT.getKey(),
				Timeout.VERIFY_TIMEOUT.getValue());
		config_verify_interval = config.getLongValue(ConfigKeys.KEY_VERIFY_INTERVAL.getKey(),
				Timeout.VERIFY_INTERVAL.getValue());
		config_element_timeout = config.getLongValue(ConfigKeys.KEY_ELEMENT_TIMEOUT.getKey(),
				Timeout.ELEMENT_TIMEOUT.getValue());
		config_elements_timeout = config.getLongValue(ConfigKeys.KEY_ELEMENTS_TIMEOUT.getKey(),
				Timeout.ELEMENTS_TIMEOUT.getValue());
		config_wait_timeout = config.getLongValue(ConfigKeys.KEY_WAIT_TIMEOUT.getKey(),
				Timeout.WAIT_TIMEOUT.getValue());
		config_pageload_timeout = config.getLongValue(ConfigKeys.KEY_PAGELOAD_TIMEOUT.getKey(),
				Timeout.PAGELOAD_TIMEOUT.getValue());
		logger.trace("FWNoUIDriverObject.setupConfigValue end");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.core.IUIObjectNoUIDriver#sleep(long)
	 */
	@Override
	public void sleep(final long ms) {
		sleep(ms, TimeUnit.MILLISECONDS, "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.core.IUIObjectNoUIDriver#sleep(long,
	 * java.lang.String)
	 */
	@Override
	public void sleep(final long ms, final String reason) {
		sleep(ms, TimeUnit.MILLISECONDS, reason);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.core.IUIObjectNoUIDriver#sleep(long,
	 * java.util.concurrent.TimeUnit)
	 */
	@Override
	public void sleep(final long time, final TimeUnit unit) {
		sleep(time, unit, "");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.core.IUIObjectNoUIDriver#sleep(long,
	 * java.util.concurrent.TimeUnit, java.lang.String)
	 */
	@Override
	public void sleep(final long time, final TimeUnit unit, final String reason) {
		if (time > 0) {
			try {
				logger.debug("Sleeping for " + time + " " + unit.toString() + ". " + reason);
				unit.sleep(time);
			} catch (InterruptedException e) {
				logger.error("Caught InterruptedException", e);
				throw new TestAutomationException("InterruptedException ", e);
			}
		}
	}

}
