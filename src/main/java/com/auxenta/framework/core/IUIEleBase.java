/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import com.auxenta.framework.locators.IElementLocator;

/**
 * An interface for basic uielement.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public interface IUIEleBase extends IUIObject {

    /**
     * Gets the locator.
     *
     * @return the locator
     */
    IElementLocator getLocator();
}
