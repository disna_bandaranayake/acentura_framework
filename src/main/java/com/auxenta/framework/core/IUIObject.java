/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;


/**
 * An interface for uiobject that has UIDriver.
 *
 * @author Niroshen Landerz
 * @date Sept 6th 2014
 */

public interface IUIObject {

    /**
     * Sets the UI driver.
     *
     * @param uiDriver the new UI driver
     */
    void setUIDriver(IUIDriver uiDriver);

    /**
     * Gets the UI driver.
     *
     * @return the UI driver
     */
    IUIDriver getUIDriver();
}
