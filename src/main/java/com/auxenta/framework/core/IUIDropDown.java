/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

/**
 * An interface for uidropdown.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public interface IUIDropDown extends IUIElement {

    /**
     * Choose option.
     *
     * @param option the option
     */
    void chooseOption(final String option);
}
