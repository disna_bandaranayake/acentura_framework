/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;

import com.auxenta.framework.drivers.IErrorDetector;
import com.auxenta.framework.elements.UIActions;
import com.auxenta.framework.exceptions.TimeOutException;
import com.auxenta.framework.util.SearchScope;

/**
 * The Interface IUIElement.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

/**
 * <li><b>Present</b> - Element in DOM
 *
 * <li><b>Absent</b> - Element is NOT in DOM
 *
 * <li><b>Displayed</b> - Element is in DOM and is visible
 *
 * <li><b>Hidden</b> - Element is in DOM and is NOT visible
 *
 * <li><b>Wait</b> - Waits for something to happen and THROWS TimeOutException on failure
 *
 * <li><b>is/has</b> - Returns boolean, true or false, does not throw exception
 */
public interface IUIElement extends WebElement, IUIEleBase, Locatable {

    /**
     * Wait for element to be hidden.
     *
     * @param waitTime the wait time
     */
    void waitToBeHidden(long waitTime);

    /**
     * Wait for element to be displayed.
     *
     * @param waitTime the wait time
     */
    void waitToBeDisplayed(long waitTime);

    /**
     * Checks if is displayed after waiting.
     *
     * @return true, if is displayed after waiting
     */
    boolean isDisplayedAfterWaiting();

    /**
     * Wait for an element to be displayed, but do not fail. This method name conforms to the is vs.
     * wait naming convention.
     *
     * @param waitTime the wait time
     * @return true, if is displayed after waiting
     */
    boolean isDisplayedAfterWaiting(long waitTime);

     /**
      * Clears the input and sends the text to it.
      *
      * @param keysToSend            the keys to send to the element
      */
    public void clearAndSendKeys(final CharSequence... keysToSend);

    /**
     * sendKeysToTextInput with rigorous error handling.
     *
     * @param textToEnter the text to enter
     */

    public void clearAndSendKeysRobustly(final String textToEnter);

    /**
     * Click element and timeout.
     *
     * @param timeout
     *            - milliseconds to wait
     */
    void click();
    
    /**
     * Click element and timeout.
     *
     * @param timeout
     *            - milliseconds to wait
     */
    void clickAndWait(long timeout);

    /**
     * Clicks the element and wait for a window to change title.
     */
    void clickThenWaitForPageTitleChange();

    /**
     * Clicks the element and wait for a window to change title.
     *
     * @param timeout            - how much time to wait for a new page load
     */
    void clickThenWaitForPageTitleChange(long timeout);

    /**
     * Clicks the element and wait for a window to change title.
     *
     * @param errorDetector the error detector
     */
    void clickThenWaitForPageTitleChange(final IErrorDetector errorDetector);

    /**
     * Clicks the element and wait for a window to change title.
     *
     * @param errorDetector the error detector
     * @param timeout the timeout
     */
    void clickThenWaitForPageTitleChange(final IErrorDetector errorDetector, final long timeout);

     /**
      * Clicks the closing element and wait for the number of open windows to decrease, if the number
      * does not decrease throws {@link TimeOutException}.
      */
    void clickThenWaitForAWindowToClose();

    /**
     * Clicks the closing element and wait for the number of open windows to decrease, if the number
     * does not decrease throws {@link TimeOutException}.
     *
     * @param timeout the timeout
     */
    void clickThenWaitForAWindowToClose(final long timeout);

    /**
     * Will check the number of active windows and continue once one has opened. Use this to click a
     * button you expect to open a window but there is a delay, and you dont want to switch to a new
     * window until you know one has opened
     */
    void clickThenWaitForAWindowToOpen();

    /**
     * Will check the number of active windows and continue once one has opened. Use this to click a
     * button you expect to open a window but there is a delay, and you dont want to switch to a new
     * window until you know one has opened
     *
     * @param timeout the timeout
     */
    void clickThenWaitForWindowToOpen(long timeout);

    /**
     * Keep on clicking element every {@code interval} until {@code expected} element is present or
     * {@code timeout} is reached.
     *
     * @param expected
     *           Expected UIElement
     * @param timeout
     *            duration to wait in ms
     * @param intervals
     *            duration to pause between clicks in ms.
     *
     */
    void clickUntilElementDisplayed(IUIElement expected, long timeout, long intervals);

    /**
     * Click until element displayed.
     *
     * @param expected the expected
     * @param timeout the timeout
     * @param intervals the intervals
     * @param errorDetector the error detector
     */
    void clickUntilElementDisplayed(
    		final IUIElement expected,
    		final long timeout,
            final long intervals,
            final IErrorDetector errorDetector);

    /**
     * Keep on clicking element every {@code interval} seconds until {@code expected} element is not
     * visible or {@code timeout} is reached.
     *
     * @param expected            Element that you expect to appear
     * @param timeout            duration to wait in ms
     * @param intervals            duration to pause between clicks in ms.
     */
    void clickUntilElementHidden(final IUIElement expected, final long timeout, final long intervals);

    /**
     * Click on element until it is null (not in the DOM) , or timeout is reached.
     *
     * @param timeout the timeout
     */
    void clickUntilAbsent(long timeout);

    /**
     * Check to see if text is contained in the current element's text node.
     *
     * @param text the text
     * @return true if text node contains matching text
     */
    boolean containsText(String text);

    /**
     * Tries to initialize {@link WebElement} on current page.
     *
     * @return true if element was initialized, false otherwise
     */
    boolean doFind();

    /**
     * Initialize {@code UIElement}.
     */
    void initialize();

    /**
     * Checks if is initialized.
     *
     * @return true, if is initialized
     */
    boolean isInitialized();

    /**
     * Finds child {@link IUIElement}.
     *
     * @param by the by
     * @return Desired {@link IUIElement} or {@code null} if {@link IUIElement} was not found
     */
    IUIElement findUIElement(By by);

    /**
     * Finds child {@link IUIElement}.
     *
     * @param by the by
     * @param timeout the timeout
     * @return Desired {@link IUIElement} or {@code null} if {@link IUIElement} was not found
     */
    IUIElement findUIElement(By by, long timeout);

    /**
     * Finds list of children {@link IUIElement}s.
     *
     * @param by the by
     * @return List of desired {@link IUIElement} or {@code null} if {@link IUIElement} was not found
     */
    List<IUIElement> findUIElements(By by);

    /**
     * Finds list of children {@link IUIElement}s.
     *
     * @param by the by
     * @param timeout the timeout
     * @return List of desired {@link IUIElement} or {@code null} if {@link IUIElement} was not found
     */
    List<IUIElement> findUIElements(By by, long timeout);

    /**
     * Gets the height.
     *
     * @return the height
     */
    int getHeight();

    /**
     * Gets the selected options.
     *
     * @return the selected options
     */
    List<String> getSelectedOptions();

    /**
     * Gets the value.
     *
     * @return the value
     */
    String getValue();

    /**
     * Gets the width.
     *
     * @return the width
     */
    int getWidth();

     /**
      * Replace value for dynamically defined element.
      *
      * @param values the values
      * @return the IUI element
      */
    IUIElement replaceValues(String... values);

    /**
     * Scroll element into view using JavaScript.
     */
    void scrollIntoView();


    /**
     * Sets the checkbox.
     *
     * @param isChecked the new checkbox
     */
    void setCheckbox(boolean isChecked);

    /**
     * Send control + {@code optKey} to the element.
     *
     * @param optKey the opt key
     */
    void sendCtrlPlus(String optKey);

    /**
     * Send Enter key to the element.
     */
    void sendEnter();

    /**
     * Send escape key to the element.
     */
    void sendEscape();


    /**
     * Send any key from keyboard including special keys.
     *
     * @param key the key
     */
    void sendKeys(Keys key);

    /**
     * Send any key as string.
     *
     * @param CharSequence any alfanumeric
     */
    void sendKeys(final CharSequence... keysToSend);
    
    /**
     * Verify text box contains {@code expected}.
     *
     * @param expected the expected
     */
    void assertInputContains(String expected);

    /**
     * Asserts that element exists on the page, but is not visible.
     *
     * @param timeout the timeout
     */
    void assertNotDisplayed(long timeout);

    /**
     * Wait for element to be present in DOM.
     */
    void waitToBePresent();

    /**
     * Wait for element to be present in DOM.
     *
     * @param timeout the timeout
     */
    void waitToBePresent(long timeout);

    /**
     * Wait for children elements to be displayed.
     */
    void waitForChildrenToDisplay();

    /**
     * Wait for elements children to be displayed.
     *
     * @param timeout the timeout
     */
    void waitForChildrenToDisplay(long timeout);

    /**
     * Wait for elements children to be displayed.
     *
     * @param errorDetector the error detector
     * @param timeout the timeout
     */
    void waitForChildrenToDisplay(IErrorDetector errorDetector, long timeout);

    /**
     * Wait for element to be absent in DOM.
     */
    void waitToBeAbsent();

    /**
     * Wait for element to be absent in DOM.
     *
     * @param timeout the timeout
     */
    void waitToBeAbsent(long timeout);

    /**
     * Wait for element to be displayed.
     */
    void waitToBeDisplayed();

    /**
     * Wait for element to be hidden.
     */
    void waitToBeHidden();

    /**
     * Wait for element to be enabled.
     *
     * @see WebElement#isEnabled()
     */
    void waitToBeEnabled();

    /**
     * Wait for element to be enabled.
     *
     * @param timeout the timeout
     * @see WebElement#isEnabled()
     */
    void waitToBeEnabled(long timeout);

    /**
     * Wait for element to be NOT enabled.
     *
     * @see WebElement#isEnabled()
     */
    void waitToBeDisabled();

    /**
     * Wait for element to be NOT enabled.
     *
     * @param timeout the timeout
     * @see WebElement#isEnabled()
     */
    void waitToBeDisabled(long timeout);

    /**
     * Wait for attribute to contain some string.
     *
     * @param attribute the attribute
     * @param partialValue the partial value
     */
    void waitForAttributeToContain(String attribute, String partialValue);

    /**
     * Wait for attribute to contain some string.
     *
     * @param attribute the attribute
     * @param partialValue the partial value
     * @param timeout the timeout
     */
    void waitForAttributeToContain(String attribute, String partialValue, long timeout);

    /**
     * Find ui element.
     *
     * @param by the by
     * @param description the description
     * @return the IUI element
     */
    IUIElement findUIElement(By by, String description);

    /**
     * Find ui element.
     *
     * @param by the by
     * @param description the description
     * @param timeout the timeout
     * @return the IUI element
     */
    IUIElement findUIElement(By by, String description, long timeout);

    /**
     * Find ui elements.
     *
     * @param by the by
     * @param description the description
     * @return the list
     */
    List<IUIElement> findUIElements(By by, String description);

    /**
     * Checks if is absent.
     *
     * @return true, if is absent
     */
    boolean isAbsent();

    /**
     * Checks if is present.
     *
     * @return true, if is present
     */
    boolean isPresent();

    /**
     * Gets the web element.
     *
     * @return the web element
     */
    WebElement getWebElement();

    /**
     * some elements cannot be clicked using regular click method.
     */
    void clickByJavascript();

    /**
     * Click on element without waiting for element to be displayed. This function will not throw
     * timeOutException if element is not displayed
     */
    void clickNoWait();

    /**
     * Gets the UI actions.
     *
     * @return the UI actions
     */
    UIActions getUIActions();

    /**
     * Keep on clicking element every {@code interval} until any of {@code expected} element is
     * displayed or {@code timeout} is reached.
     *
     * @param expected
     *            Expected UIElement
     * @param timeout
     *            duration to wait in ms
     * @param intervals
     *            duration to pause between clicks in ms.
     *
     */
    void clickUntilElementsDisplayed(List<IUIElement> expected, long timeout, long intervals);

    /**
     * Click until elements displayed.
     *
     * @param expected the expected
     * @param timeout the timeout
     * @param intervals the intervals
     * @param errorDetector the error detector
     */
    void clickUntilElementsDisplayed(
            List<IUIElement> expected,
            long timeout,
            long intervals,
            IErrorDetector errorDetector);

    /**
     * wait until the element has expected text.
     *
     * @param text the text
     * @param scope the scope
     */
    void waitForMatchText(String text, SearchScope scope);

    /**
     * Select all options for multiple select node.
     */
    void selectAllOptions();

    /**
     * Select additional option for multiple select node.
     *
     * @param text the text
     */
    void selectByVisibleText(String text);

    /**
     * Remove option from multiple select node.
     *
     * @param text the text
     */
    void deselectByVisibleText(String text);

    /**
     * Deselect all options for multiple select node.
     */
    void deselectAllOptions();

    /**
     * Sets the description.
     *
     * @param desc the new description
     */
    void setDescription(String desc);

    /**
     * Gets the description.
     *
     * @return the description
     */
    String getDescription();

    /**
     * Find ui elements.
     *
     * @param by the by
     * @param description the description
     * @param timeout the timeout
     * @return the list
     */
    List<IUIElement> findUIElements(By by, String description, long timeout);

    /**
     * Use this in order to handle file selection dialog <br>
     * This method will send keys to file input box <br>
     * For example {@code <input type="file" id="uploader" />}.
     *
     * @param keysToSend            - file path
     */
    void sendKeysToFileInput(CharSequence... keysToSend);

    /**
     * Wait to be hidden.
     *
     * @param errorDetector the error detector
     * @param timeout the timeout
     */
    void waitToBeHidden(IErrorDetector errorDetector, long timeout);

    /**
     * Check if div has text that may have been updated by JavaScript.
     *
     * @return True if text is present, and this element is displayed
     */
    boolean hasText();

    /**
     * Wait for element to be present in DOM.
     *
     * @param errorDetector the error detector
     * @param timeout the timeout
     */
    void waitToBePresent(IErrorDetector errorDetector, long timeout);

    /**
     * wait for element to be clickable in Dom.
     */
    void waitToBeClick();
/**
 * Confirms whether the attribute looking for in an element is available. 
 * @param attribute
 * @return
 */
    boolean isAttribtuePresent(String attribute);
        
}
