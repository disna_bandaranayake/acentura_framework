/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.core;

import com.auxenta.framework.util.UIType;

/**
 * An interface for pageobject.
 *
 * @author Niroshen Landerz
 * @date Sept 2nd 2014
 */

public interface IPageObject {

	/**
	 * Creates the element.
	 *
	 * @param uiType
	 *            the ui type
	 * @param value
	 *            the value
	 * @return the IUI element
	 */
	public IUIElement createElement(UIType uiType, String value);

	/**
	 * Sets the url.
	 *
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url);

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl();

	/**
	 * Go to url specified in config file.
	 */
	public void start();

	/**
	 * a method that could be used if you want to wait till the document loading
	 * is reported as completed.
	 */
	public void waitTillPageLoadComplete();
}
