/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.auxenta.framework.core.FWObject;
import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIEleBase;
import com.auxenta.framework.locators.IElementLocator;

/**
 * An abstract class for base element in auxtest framework.
 *
 * @author Niroshen Landerz
 * @date 28th August 2014
 */

public class FWEleBase extends FWObject implements IUIEleBase {

    /** The logger. */
    private static Logger logger = Logger.getLogger(FWEleBase.class);

    /** The locator. */
    protected IElementLocator locator;

    /** The replace values. */
    protected List<String> replaceValues = new ArrayList<String>();

    /** The description. */
    protected String description = null;

    /** The original description. */
    protected String originalDescription = null;

    /**
     * Instantiates a new FW ele base.
     *
     * @param driver the driver
     */
    protected FWEleBase(final IUIDriver driver) {
        super(driver);
        logger.trace("FWEleBase.FWEleBase(1)");
    }

    /**
     * Instantiates a new FW ele base.
     *
     * @param driver the driver
     * @param locatorVal the locator
     */
    protected FWEleBase(final IUIDriver driver, final IElementLocator locatorVal) {
        super(driver);
        logger.trace("FWEleBase.FWEleBase(2)");
        this.locator = locatorVal;
    }

    /**
     * Instantiates a new FW ele base.
     *
     * @param driver the driver
     * @param locatorVal the locator
     * @param descriptionVal the description
     */
    protected FWEleBase(final IUIDriver driver, final IElementLocator locatorVal, final String descriptionVal) {
        super(driver);
        this.locator = locatorVal;
        this.description = descriptionVal;
        this.originalDescription = descriptionVal;
    }

    /**
     * Sets the description.
     *
     * @param descriptionVal the new description
     */
    public void setDescription(final String descriptionVal) {
        this.description = descriptionVal;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIEleBase#getLocator()
     */
    @Override
    public IElementLocator getLocator() {
        return locator;
    }

    /**
     * Use the description in calls to toString();.
     *
     * @return the string
     */
    @Override
    public String toString() {
        if (description == null) {
            return super.toString();
        } else {
            return description;
        }
    }

}
