/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.MoveToOffsetAction;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.remote.CapabilityType;

import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.drivers.InputDevices;
import com.auxenta.framework.exceptions.FrameworkException;

/**
 * The Class DefaultUIActions.
 *
 * @author Niroshen Landerz
 * @date Aug 30th 2014
 */

public class DefaultUIActions implements UIActions {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(DefaultUIActions.class);

	/** The ui driver. */
	private IUIDriver uiDriver;

	/** The builder. */
	private Actions builder;

	/** The element. */
	private IUIElement element;

	/**
	 * Instantiates a new default ui actions.
	 *
	 * @param newUIDriver the new ui driver
	 * @param uiElement the ui element
	 */
	public DefaultUIActions(final IUIDriver newUIDriver, final IUIElement uiElement) {
		this.uiDriver = newUIDriver;
		this.element = uiElement;
	}

	/**
	 * Gets the actions.
	 *
	 * @return the actions
	 */
	private Actions getActions() {
		if (builder == null && uiDriver != null) {
			builder = new Actions(uiDriver.getWebDriver());
		}
		return builder;
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#doubleClick()
	 */
	@Override
	public void doubleClick() {
		logger.debug("Double click on " + element.toString());
		element.waitToBePresent();
		this.getActions().doubleClick(element).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#rightClick()
	 */
	@Override
	public void rightClick() {
		logger.debug("Right click on " + element.toString());
		element.waitToBePresent();
		this.getActions().contextClick(element).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#sendKeys(java.lang.CharSequence[])
	 */
	@Override
	public void sendKeys(final CharSequence... keysToSend) {
		logger.debug("Send keys '" + Arrays.toString(keysToSend) + "' to element " + element);
		element.waitToBePresent();
		this.getActions().sendKeys(element, keysToSend).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#click()
	 */
	@Override
	public void click() {
		logger.debug("Click on element " + element.toString());
		element.waitToBePresent();
		this.getActions().moveToElement(element).click().build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#
	 * dragAndDrop(com.auxenta.test.auxtest.framework.core.IUIElement)
	 */
	@Override
	public void dragAndDrop(final IUIElement target) {
		logger.debug("Drag element " + element.toString() + " and drop it to " + target.toString());
		element.waitToBePresent();
		target.waitToBePresent();
		this.getActions().dragAndDrop(element, target).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#dragAndDrop(int, int)
	 */
	@Override
	public void dragAndDrop(final int xOffset, final int yOffset) {
		logger.debug("Drag and drop element " + element.toString() + " to offset with X = '" + xOffset
				+ "', Y = '" + yOffset + "'");
		element.waitToBePresent();
		this.getActions().dragAndDropBy(element, xOffset, yOffset).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#clickOnLocation()
	 */
	@Override
	public void clickOnLocation() {
		WebDriver driver = uiDriver.getWebDriver();
		if (driver instanceof HasInputDevices) {
			Locatable location = (Locatable) element;
			logger.debug("Clicking on location of " + element.toString());
			InputDevices device = new InputDevices(uiDriver);
			ClickAction clicker = new ClickAction(device.getMouse(), location);
			clicker.perform();
		} else {
			throw new FrameworkException("This method is unsupported for driver of type: " + driver);
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#mouseMoveHere()
	 */
	@Override
	public void mouseMoveHere() {
		logger.debug("Move mouse to the middle of " + element.toString());
		element.waitToBePresent();
		this.getActions().moveToElement(element).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#hoverOver()
	 */
	@Override
	public void hoverOver() {
		WebDriver driver = uiDriver.getWebDriver();
		if (driver instanceof HasInputDevices) {
			if (uiDriver.hasCapability(CapabilityType.HAS_NATIVE_EVENTS)) {
				HasInputDevices devices = (HasInputDevices) driver;
				Locatable location = (Locatable) element;
				logger.debug("Hover Over " + element.toString());
				MoveToOffsetAction hover = new MoveToOffsetAction(devices.getMouse(), location, 0, 0);
				hover.perform();
			} else {
				throw new FrameworkException("Cannot hover without native events on " + element.toString());
			}
		} else {
			throw new FrameworkException("This method is unsupported for driver of type: " + driver);
		}
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#hoverOverAndClickOn()
	 */
	@Override
	public void hoverOverAndClickOn() {
		logger.debug("Move to and click on " + element.toString());
		element.waitToBePresent();
		this.getActions().moveToElement(element).click().build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#clickAndHold()
	 */
	@Override
	public void clickAndHold() {
		logger.debug("Click and hold left mouse btn on " + element.toString());
		element.waitToBePresent();
		this.getActions().clickAndHold(element).build().perform();
	}

	/* (non-Javadoc)
	 * @see com.auxenta.test.auxtest.framework.elements.UIActions#release()
	 */
	@Override
	public void release() {
		logger.debug("Release left mouse btn on " + element.toString());
		element.waitToBePresent();
		this.getActions().release(element).build().perform();
	}

}
