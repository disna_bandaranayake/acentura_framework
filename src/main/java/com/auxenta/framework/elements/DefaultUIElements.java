/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.core.IUIElements;
import com.auxenta.framework.exceptions.TimeOutException;
import com.auxenta.framework.locators.ElementLocatorBySelf;
import com.auxenta.framework.locators.IElementLocator;

/**
 * A generic uielements based on uielement group
 *
 * <li><b>Present</b> - Elements are DOM
 *
 * <li><b>Absent</b> - Elements are NOT in DOM
 *
 * <li><b>Displayed</b> - Elements are in DOM and are visible
 *
 * <li><b>Hidden</b> - Elements are in DOM and are NOT visible
 *
 * <li><b>Wait</b> - Waits for something to happen and THROWS TimeOutException on failure
 *
 * <li><b>are/is/has</b> - Returns boolean, true or false, does not throw exception.
 *
 * @author Niroshen Landerz
 * @date Sept 4th 2014
 */

public class DefaultUIElements extends FWEleBase implements IUIElements {

    /** The logger. */
    private static Logger logger = Logger.getLogger(DefaultUIElements.class);

    /** The elements. */
    private List<WebElement> elements;

    /**
     * Instantiates a new default ui elements.
     *
     * @param driver the driver
     * @param locator the locator
     */
    public DefaultUIElements(final IUIDriver driver, final IElementLocator locator) {
        super(driver, locator);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#length()
     */
    @Override
    public int length() {
        logger.debug("Getting length for " + this.toString());
        int length = -1;
        waitToBePresent();
        if (elements != null) {
            length = elements.size();
        }
        logger.debug("Got length: " + length + " for " + this.toString());
        return length;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getUIElementByIndex(int)
     */
    @Override
    public IUIElement getUIElementByIndex(final int index) {
        logger.debug("Getting element by index:" + index);
        IUIElement element = null;
        waitToBePresent();
        if (elements != null && elements.size() > 0 && elements.size() > index) {
            WebElement desiredElement = elements.get(index);
            element = new DefaultUIElement(
                    uiDriver,
                    new ElementLocatorBySelf(desiredElement),
                    uiDriver.getXPath(desiredElement));
        }
        logger.debug((element == null ? "Element not found" : "Found element:") + this.toString());
        return element;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getUIElementByText(java.lang.String)
     */
    @Override
    public IUIElement getUIElementByText(final String text) {
        logger.debug("Getting element by text:" + text);
        IUIElement element = null;
        waitToBePresent();
        if (elements != null && elements.size() > 0) {
            WebElement found = null;
            for (WebElement ele : elements) {
                String eleText = ele.getText();
                if (eleText.contains(text)) {
                    found = ele;
                    break;
                }
            }
            element = (found == null ? null : new DefaultUIElement(
                    uiDriver,
                    new ElementLocatorBySelf(found),
                    text));
        }
        logger.debug((element == null ? "Element not found" : "Found element:") + this.toString());
        return element;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getChildUIElementByIndex(int, org.openqa.selenium.By)
     */
    @Override
    public IUIElement getChildUIElementByIndex(final int index, final By childBy) {
        IUIElement element = getUIElementByIndex(index);
        return element == null ? null : element.findUIElement(childBy);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getChildUIElementsByIndex(int, org.openqa.selenium.By)
     */
    @Override
    public List<IUIElement> getChildUIElementsByIndex(final int index, final By childBy) {
        IUIElement element = getUIElementByIndex(index);
        return element == null ? null : element.findUIElements(childBy);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#
     * getChildUIElementByText(java.lang.String, org.openqa.selenium.By)
     */
    @Override
    public IUIElement getChildUIElementByText(final String text, final By childBy) {
        IUIElement element = getUIElementByText(text);
        return element == null ? null : element.findUIElement(childBy);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#
     * getChildUIElementsByText(java.lang.String, org.openqa.selenium.By)
     */
    @Override
    public List<IUIElement> getChildUIElementsByText(final String text, final By childBy) {
        IUIElement element = getUIElementByText(text);
        return element == null ? null : element.findUIElements(childBy);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getAllChildUIElements(org.openqa.selenium.By)
     */
    @Override
    public List<IUIElement> getAllChildUIElements(final By childBy) {
        List<IUIElement> children = new ArrayList<IUIElement>();
        List<IUIElement> uiElements = getUIElementsList();
        for (IUIElement uiEle : uiElements) {
            IUIElement ele = uiEle.findUIElement(childBy);
            if (ele != null) {
                children.add(ele);
            }
        }
        return children.size() == 0 ? null : children;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getUIElementsList()
     */
    @Override
    public List<IUIElement> getUIElementsList() {
        waitToBePresent();
        if (elements == null) {
            return null;
        } else {
            List<IUIElement> elementList = new ArrayList<IUIElement>();
            for (WebElement ele : elements) {
                String description = null;
                String innerText = null;
                // trying to find good description for an element
                try {
                    innerText = ele.getText();
                    if (innerText == null | innerText.equals("")) {
                        description = "";
                    } else {
                        description = "Inner text = '" + innerText + "'";
                    }
                } catch (Exception e) {
                    // we don't really care about description, its fine to set it to null
                	logger.error("Exception. but continuing ", e);
                }

                elementList.add(new DefaultUIElement(
                        uiDriver,
                        new ElementLocatorBySelf(ele),
                        description));
            }
            return elementList;
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#areAllDisplayed()
     */
    @Override
    public boolean areAllDisplayed() {
        if (!this.areInitialized()) {
            initializeAll();
        }
        boolean result = true;
        if (this.areInitialized()) {
            for (WebElement element : elements) {
                try {
                    if (!element.isDisplayed()) {
                        logger.debug("Element " + this.toString() + " is not displayed");
                        result = false;
                        break;
                    }
                } catch (StaleElementReferenceException ex) {
                    elements = null;
                    result = false;
                    logger.debug(ex.toString());
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#doFindAll()
     */
    @Override
    public boolean doFindAll() {
        this.initializeAll();
        return this.areInitialized();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#replaceValues(java.lang.String[])
     */
    @Override
    public IUIElements replaceValues(final String... values) {
        locator.replaceValues(values);
        waitToBePresent();
        return this;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitForNumberOfElementsToBePresent(int)
     */
    @Override
    public void waitForNumberOfElementsToBePresent(final int numberOfElements) {
        waitForNumberOfElementsToBePresent(numberOfElements, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitForNumberOfElementsToBePresent(int, long)
     */
    @Override
    public void waitForNumberOfElementsToBePresent(final int numberOfElements, final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < stopTime) {
            this.doFindAll();
            if (elements != null && elements.size() >= numberOfElements) {
                return;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        this.doFindAll();
        if (elements != null && elements.size() >= numberOfElements) {
            return;
        }
        throw new TimeOutException(
                "Failed while waiting for " + numberOfElements + " elements to be present under " + this
                        .toString());
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#areAllPresent()
     */
    @Override
    public boolean areAllPresent() {
        return doFindAll();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#initializeAll()
     */
    @Override
    public void initializeAll() {
        elements = locator.locateElements();
        if (elements == null) {
            logger.debug("Failed to find UI Elements" + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#areInitialized()
     */
    @Override
    public boolean areInitialized() {
        return (elements == null) ? false : true;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBePresent()
     */
    @Override
    public void waitToBePresent() {
        this.waitToBePresent(config_verify_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBePresent(long)
     */
    @Override
    public void waitToBePresent(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        while (!this.areAllPresent() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        this.areAllPresent();
        if (elements == null) {
            throw new TimeOutException("Failed to find elements under " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBeDisplayed(long)
     */
    @Override
    public void waitToBeDisplayed(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        while (!areAllDisplayed() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        areAllDisplayed();
        if (!this.areAllDisplayed()) {
            throw new TimeOutException("Not all of the elements are displayed under " + this
                    .toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBeDisplayed()
     */
    @Override
    public void waitToBeDisplayed() {
        this.waitToBeDisplayed(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBeHidden(long)
     */
    @Override
    public void waitToBeHidden(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        while (areAllPresent() && areAllDisplayed() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        if (this.areAllDisplayed()) {
            throw new TimeOutException("Not all elements are hidden under " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBeHidden()
     */
    @Override
    public void waitToBeHidden() {
        this.waitToBeHidden(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBeAbsent(long)
     */
    @Override
    public void waitToBeAbsent(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        while (areAllPresent() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        areAllPresent();
        if (!(elements == null)) {
            throw new TimeOutException("Not all elements are absent under " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#waitToBeAbsent()
     */
    @Override
    public void waitToBeAbsent() {
        waitToBeAbsent(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.elements.FWEleBase#toString()
     */
    @Override
    public String toString() {
        String desc = getDescription();
        String elementLocator;

        if (!locator.toString().equals("")) {
            elementLocator = locator.toString();
        } else {
            elementLocator = "";
        }
        if (desc != null && elementLocator != null) {
            return "[Description = {" + desc + "}, Locator = {" + elementLocator + "}]";
        } else
            if (elementLocator != null) {
                return "[Locator = " + elementLocator + "]";
            } else
                if (desc != null) {
                    return "[Description = " + desc + "]";
                } else {
                    return "";
                }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getOneDisplayedElement()
     */
    @Override
    public IUIElement getOneDisplayedElement() {
        return getOneDisplayedElement(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElements#getOneDisplayedElement(long)
     */
    @Override
    public IUIElement getOneDisplayedElement(final long timeout) {
        WebElement ele = null;
        long start = System.currentTimeMillis();
        while (ele == null) {
            for (WebElement element : elements) {
                if (element.isDisplayed()) {
                    ele = element;
                    break;
                }
            }
            if (ele != null || System.currentTimeMillis() - start > timeout) {
                break;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        for (WebElement element : elements) {
            if (element.isDisplayed()) {
                ele = element;
                break;
            }
        }
        return ele == null ? null : new DefaultUIElement(
                uiDriver,
                new ElementLocatorBySelf(ele),
                description);
    }
}
