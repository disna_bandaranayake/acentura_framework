/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.drivers.DummyErrorDetector;
import com.auxenta.framework.drivers.IErrorDetector;
import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.TimeOutException;
import com.auxenta.framework.locators.ElementLocatorBySelf;
import com.auxenta.framework.locators.IElementLocator;
import com.auxenta.framework.util.ByUtil;
import com.auxenta.framework.util.SearchScope;
import com.auxenta.framework.util.UIType;

/**
 * A generic uielement based on WebElement, which implements all functions for UIElement. <br>
 *
 * <li><b>Present</b> - Element in DOM
 *
 * <li><b>Absent</b> - Element is NOT in DOM
 *
 * <li><b>Displayed</b> - Element is in DOM and is visible
 *
 * <li><b>Hidden</b> - Element is in DOM and is NOT visible
 *
 * <li><b>Wait</b> - Waits for something to happen and THROWS TimeOutException on failure
 *
 * <li><b>is/has</b> - Returns boolean, true or false, does not throw exception
 *
 *
 * @author Niroshen Landerz
 * @date Sept 4th 2014
 */
public class DefaultUIElement extends FWEleBase implements IUIElement {

    /** The logger. */
    private static Logger logger = Logger.getLogger(DefaultUIElement.class);

    /** The Constant WEB_DRIVER_WAIT_TIME. */
	private static final long WEB_DRIVER_WAIT_TIME = 30;
    /** The element. */
    private WebElement element;

    /** The Constant DUMMY_ERROR_DETECTOR. */
    private static final IErrorDetector DUMMY_ERROR_DETECTOR = new DummyErrorDetector();

    /**
     * Instantiates a new default ui element.
     *
     * @param driver the driver
     * @param locator the locator
     */
    public DefaultUIElement(final IUIDriver driver, final IElementLocator locator) {
        this(driver, locator, null);
    }

    /**
     * Instantiates a new default ui element.
     *
     * @param uiDriver the ui driver
     * @param locator the locator
     * @param description the description
     */
    public DefaultUIElement(final IUIDriver uiDriver, final IElementLocator locator, final String description) {
        super(uiDriver, locator, description);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#assertInputContains(java.lang.String)
     */
    @Override
    public void assertInputContains(final String expected) {
        initialize();
        logger.debug("Validate that  '" + expected + "' is already in the " + this.toString() + " box");
        String actual = element.getText();
        Assert.assertTrue(actual.contains(expected));
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#assertNotDisplayed(long)
     */
    @Override
    public void assertNotDisplayed(final long timeout) {
        this.initialize();
        Assert.assertNotNull(element);
        Assert.assertFalse(this.isDisplayed());
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#clear()
     */
    @Override
    public void clear() {
        waitToBeDisplayed();
        logger.debug("Clear text from " + this.toString());
        element.clear();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clearAndSendKeys(java.lang.CharSequence[])
     */
    @Override
    public void clearAndSendKeys(final CharSequence... keysToSend) {
        waitToBePresent();
        try {
            this.clear();
        } catch (WebDriverException wde) {
            logger.error("WebDriverException", wde);
            throw new TestAutomationException("WebDriverException", wde);
        }
        if (this.isDisplayed()) {
            this.sendKeys(keysToSend);
        } else {
            logger.warn("Failed to send keys to " + this.toString());
            DebugUIElement.debugObject("invisible text box input", this);
        }

    }

    /**
     * This method accomplishes the same goal as clearAndSendKeys but with extensive fault
     * tollerence, checking and retrying. Use this when the textbox has trouble clearing. Useful for
     * when chromedriver does not allow sendKeys() to work:
     * <link>https://code.google.com/p/chromedriver/issues/detail?id=35</link>
     *
     * @param textToEnter the text to enter
     */
    public void clearAndSendKeysRobustly(String textToEnter) {

        try {
            waitToBeDisplayed();

            // Clearing an empty text box can throw exceptions,
            // but some pages display default values with javascript
            // that are not easy to detect.
            // trigger javascript, and ensure the box is not empty
            // before clearing
            try {
                logger.debug("clearing text");
                if (this.isDisplayed()) {
                    String previousValue = this.getAttribute("value");
                    if (previousValue != null && !previousValue.isEmpty()) {
                        this.sendKeys("clearing");
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.sendKeys(Keys.BACK_SPACE);
                        this.clear();
                    }
                }

                logger.debug("cleared");
            } catch (RuntimeException rte) {
                logger.error("Exception clearing text box:", rte);
                throw new TestAutomationException("Exception in clearing text box", rte);
            }
            logger.debug("Entering " + this + "=" + textToEnter);
            this.sendKeys(textToEnter);
        } catch (WebDriverException wde) {
        	logger.error("WebDriverException:", wde);
            if (wde.getLocalizedMessage().contains("Element is no longer valid")) {
                try {
                    DebugUIElement.debugObject(this);
                    this.waitToBeDisplayed();
                    this.clear();
                    logger.debug("Entering " + this + "=" + textToEnter);
                    this.sendKeys(textToEnter);
                } catch (Exception e) {
                	logger.error("Exception trying to send keys", e);
                    Assert.fail("Exception trying to send keys to " + this + " again: " + e
                            .getLocalizedMessage());
                }
            }
        } catch (Exception e) {
        	logger.error("Exception trying to send keys", e);
            Assert.fail("Exception trying to send keys to " + this + ": " + e.getLocalizedMessage());
        }
        textToEnter = textToEnter.replaceAll("\n", "");
        if (this.isAbsent()) {
            Assert.fail("could not find textBox " + this);
        }
        String actual = this.getAttribute("value");
        actual = actual.replaceAll("\n", "");
        waitToBeDisplayed();
        Assert.assertEquals(actual.trim(), textToEnter.trim());
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#click()
     */
    public void click() {
        try {
            waitToBeDisplayed();
            waitToBeClick();
        } catch (TimeOutException e) {
            String errorMessage = "Failed to click on element " + this.toString()
            		+ " because this element is not displayed. If desired behavior is to click on element "
            		+ "which is not displayed, and continue without exception "
            		+ "please use clickNoWait() method ! Previous Exception: " + e.getMessage();
            logger.error(errorMessage, e);
            throw new TimeOutException(errorMessage);
        }
        if (isEnabled()) {
            try {
                element.click();
            } catch (WebDriverException ex) {
                if (ex.getMessage().contains("Element is not clickable at point")) {
                    // Exception likely caused by div hiding element, try again after scrolling
                    logger.debug("Browser can't click element, scrolling and clicking again");
                    scrollIntoView();
                    element.click();
                }
            }
            logger.debug("Clicked on element: '" + this.toString() + "'");
        } else {
            logger.warn("Element(" + this.toString() + ") is not clickable!");
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickAndWait(long)
     */
    @Override
    public void clickAndWait(final long timeout) {

        this.click();
        sleep(timeout, "Waiting after performed click");

    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickByJavascript()
     */
    @Override
    public void clickByJavascript() {
        try {
            waitToBeDisplayed();
        } catch (TimeOutException e) {
            String errorMessage = "Failed to click on element " + this.toString()
            		+ " because this element is not displayed. If desired behavior is to click on element which is not displayed, and continue without exception please use clickNoWait() method ! Previous Exception: " + e
                    .getMessage();
            logger.error(errorMessage);
            throw new TimeOutException(errorMessage);
        }
        super.uiDriver.executeScript("arguments[0].click();", element);
        logger.debug("Clicked on element: '" + this.toString() + "'");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickNoWait()
     */
    @Override
    public void clickNoWait() {
        if (isDisplayed() && isEnabled()) {
            try {
                element.click();
            } catch (WebDriverException ex) {
            	 logger.error("WebDriverException", ex);
                if (ex.getMessage().contains("Element is not clickable at point")) {
                    // Exception likely caused by div hiding element, try again after scrolling
                    logger.debug("Browser can't click element, scrolling and clicking again");
                    scrollIntoView();
                    element.click();
                }
            }
            logger.debug("Clicked on element: '" + this.toString() + "'");
        } else {
            logger.warn("Element(" + this.toString() + ") is not clickable!");
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickThenWaitForAWindowToClose()
     */
    @Override
    public void clickThenWaitForAWindowToClose() {
        clickThenWaitForAWindowToClose(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickThenWaitForAWindowToClose(long)
     */
    @Override
    public void clickThenWaitForAWindowToClose(final long timeout) {
        waitToBeDisplayed();
        int startingNumWindowHandles = super.getUIDriver().getWindowHandles().size();
        int currentNumWindowHandles = startingNumWindowHandles;
        this.click();
        logger.debug("Closing window, Number of original window handles = " + startingNumWindowHandles);
        long stopTime = System.currentTimeMillis() + timeout;
        do {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
            super.getUIDriver().getUIAlert().acceptAlertIfPresent(1000L);
            currentNumWindowHandles = super.getUIDriver().getWindowHandles().size();
            if (currentNumWindowHandles < startingNumWindowHandles) {
                return;
            }
        } while (stopTime > System.currentTimeMillis());

        throw new TimeOutException(
                "Timed out waiting " + timeout + " seconds for a window to close after clicking ='" + this
                        .toString() + "'");

    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickThenWaitForAWindowToOpen()
     */
    @Override
    public void clickThenWaitForAWindowToOpen() {
        clickThenWaitForWindowToOpen(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickThenWaitForPageTitleChange()
     */
    @Override
    public void clickThenWaitForPageTitleChange() {
        clickThenWaitForPageTitleChange(DUMMY_ERROR_DETECTOR);

    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * clickThenWaitForPageTitleChange(com.auxenta.test.auxtest.framework.drivers.IErrorDetector)
     */
    @Override
    public void clickThenWaitForPageTitleChange(final IErrorDetector errorDetector) {
        clickThenWaitForPageTitleChange(errorDetector, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * clickThenWaitForPageTitleChange(com.auxenta.test.auxtest.framework.drivers.IErrorDetector, long)
     */
    @Override
    public void clickThenWaitForPageTitleChange(final IErrorDetector errorDetector, final long timeout) {
        final String previousTitle = super.getUIDriver().getTitle();
        waitToBeDisplayed();
        this.click();
        ExpectedCondition<Boolean> nextPage = new ExpectedCondition<Boolean>() {

            public Boolean apply(final WebDriver d) {
                String newTitle = d.getTitle();
                boolean titleChanged = !newTitle.equals(previousTitle);
                return Boolean.valueOf(titleChanged);
            }
        };
        WebDriverWait wait = new WebDriverWait(super.getUIDriver(), TimeUnit.SECONDS.convert(
                timeout,
                TimeUnit.MILLISECONDS));
        try {
            wait.until(nextPage);
            logger.debug("Clicking on " + this.toString() + " changed the page title from '"
            + previousTitle + "' to '" + super.getUIDriver().getTitle() + "'");
        } catch (RuntimeException re) {
            errorDetector.assertNoError();
            throw new TimeOutException(
                    "Page title did not change from " + previousTitle + " after "
            + timeout + " " + TimeUnit.MILLISECONDS.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickThenWaitForPageTitleChange(long)
     */
    @Override
    public void clickThenWaitForPageTitleChange(final long timeout) {
        clickThenWaitForPageTitleChange(DUMMY_ERROR_DETECTOR, timeout);

    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickThenWaitForWindowToOpen(long)
     */
    @Override
    public void clickThenWaitForWindowToOpen(final long timeout) {
        waitToBeDisplayed();
        int startingNumWindowHandles = super.getUIDriver().getWindowHandles().size();
        int currentNumWindowHandles = startingNumWindowHandles;
        logger.debug("Opening new window, Number of original window handles = " + startingNumWindowHandles);
        this.click();
        long stopTime = System.currentTimeMillis() + timeout;
        do {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
            currentNumWindowHandles = super.getUIDriver().getWindowHandles().size();
            if (currentNumWindowHandles > startingNumWindowHandles) {
                return;
            }
        } while (stopTime > System.currentTimeMillis());

        throw new TimeOutException(
                "Timed out waiting " + timeout + " seconds for a window to open after clicking ='" + this
                        .toString() + "'");

    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickUntilAbsent(long)
     */
    @Override
    public void clickUntilAbsent(final long timeout) {
        waitToBePresent();
        long timedout = System.currentTimeMillis() + timeout;
        logger.debug("Waiting to disappear from DOM " + this.toString());
        do {
            this.click();
            if (timeout < config_verify_interval) {
                sleep(timeout, "Waiting for element to disappear from DOM " + this.toString());
            } else {
                sleep(config_verify_interval, "Waiting for element to disappear from DOM " + this
                        .toString());
            }
            doFind();
        } while (element != null && timedout > System.currentTimeMillis());
        if (element != null) {
            String message = this.toString() + " is still present after clicking for " + timeout + " milliseconds";
            logger.error(message);
            throw new TimeOutException(message);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * clickUntilElementDisplayed(com.auxenta.test.auxtest.framework.core.IUIElement, long, long)
     */
    @Override
    public void clickUntilElementDisplayed(final IUIElement expected, final long timeout, final long intervals) {
        clickUntilElementDisplayed(expected, timeout, intervals, new DummyErrorDetector());
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * clickUntilElementDisplayed(com.auxenta.test.auxtest.framework.core.IUIElement, long, long, com.auxenta.test.auxtest.framework.drivers.IErrorDetector)
     */
    @Override
    public void clickUntilElementDisplayed(
    		final IUIElement expected,
    		final long timeout,
    		final long intervals,
    		final IErrorDetector errorDetector) {

        logger.debug("Clicking on " + expected + " every " + intervals
        		+ " milliseconds until visible or timeout after " + timeout);
        long timedout = System.currentTimeMillis() + timeout;
        do {
            this.click();
            sleep(intervals, "Waiting for element to be displayed " + expected);
            errorDetector.assertNoError();
            expected.initialize();
        } while (!expected.isDisplayed() && timedout > System.currentTimeMillis());
        if (!expected.isDisplayed()) {
            String message = expected + " is is not visible after clicking for " + timeout + " milliseconds";
            logger.error(message);
            DebugUIElement.debugObject(expected);
            throw new TimeOutException(message);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * clickUntilElementHidden(com.auxenta.test.auxtest.framework.core.IUIElement, long, long)
     */
    @Override
    public void clickUntilElementHidden(final IUIElement expected, final long timeout, final long intervals) {
        waitToBeDisplayed();
        logger.debug("Clicking on " + expected + " every " + intervals
        		+ " milliseconds until its hidden or timeout after " + timeout);
        long timedout = System.currentTimeMillis() + timeout;
        do {
            this.click();
            sleep(intervals, "Waiting for element to be hidden " + expected);
            expected.initialize();
        } while (expected.isDisplayed() && timedout > System.currentTimeMillis());
        if (expected.isDisplayed()) {
            String message = expected + " is still present after clicking "
        + this.toString() + " for " + timeout + " milliseconds";
            logger.error(message);
            throw new TimeOutException(message);
        }

    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#clickUntilElementsDisplayed(java.util.List, long, long)
     */
    public void clickUntilElementsDisplayed(final List<IUIElement> expected, final long timeout, final long intervals) {
        clickUntilElementsDisplayed(expected, timeout, intervals, new DummyErrorDetector());
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * clickUntilElementsDisplayed(java.util.List, long, long, com.auxenta.test.auxtest.framework.drivers.IErrorDetector)
     */
    public void clickUntilElementsDisplayed(
    		final List<IUIElement> expected,
    		final long timeout,
    		final long intervals,
    		final IErrorDetector errorDetector) {

        boolean isAnyElemDisplayed = false;
        logger.debug("Clicking on " + expected + " every " + intervals
        		+ " milliseconds until visible or timeout after " + timeout);
        long timedout = System.currentTimeMillis() + timeout;
        do {
            this.click();
            sleep(intervals);
            errorDetector.assertNoError();
            for (IUIElement elem : expected) {
                elem.initialize();
                if (elem.isDisplayed()) {
                    isAnyElemDisplayed = true;
                    break;
                }
            }

        } while (!isAnyElemDisplayed && timedout > System.currentTimeMillis());
        if (!isAnyElemDisplayed) {
            String message = "none of the expected elements are not visible after clicking for "
        + timeout + " milliseconds. expected elements: " + expected;
            logger.error(message);
            for (IUIElement elem : expected) {
                DebugUIElement.debugObject(elem);
            }
            throw new TimeOutException(message);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#containsText(java.lang.String)
     */
    @Override
    public boolean containsText(final String text) {
        boolean result = element.getText().contains(text);
        logger.debug(((result) ? "Element contains text " : "Element does not contain text ") + text + " , " + this
                .toString());
        return result;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#deselectAllOptions()
     */
    @Override
    public void deselectAllOptions() {
        waitToBeDisplayed();
        Select select = new Select(element);
        logger.debug("Deselecting all options for " + this.toString());
        select.deselectAll();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#deselectByVisibleText(java.lang.String)
     */
    @Override
    public void deselectByVisibleText(final String text) {
        waitToBeDisplayed();
        Select select = new Select(element);
        logger.debug("Deselecting option with text " + text + " for " + this.toString());
        select.deselectByVisibleText(text);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#doFind()
     */
    @Override
    public boolean doFind() {
        this.initialize();
        return this.isInitialized();
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#findElement(org.openqa.selenium.By)
     */
    @Override
    public WebElement findElement(final By by) {
        WebElement ele = null;
        initialize();
        try {
            if (element != null) {
                ele = element.findElement(by);
            }
        } catch (NoSuchElementException ex) {
            throw new TestAutomationException("NoSuchElementException", ex);
        }
        logger.debug(ele == null
                                ? "Failed to find element " + ByUtil.getByStr(by)
                                : "Found element " + ByUtil.getByStr(by));
        return ele;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#findElements(org.openqa.selenium.By)
     */
    @Override
    public List<WebElement> findElements(final By by) {
        logger.debug("Looking for elements by: " + ByUtil.getByStr(by));
        List<WebElement> list = null;
        initialize();
        if (element != null) {
            list = element.findElements(by);
        }
        if (list == null) {
            logger.debug("Found 0 elements by " + ByUtil.getByStr(by));
        } else {
            logger.debug("Found " + list.size() + " elements by: " + ByUtil.getByStr(by));
            for (WebElement item : list) {
                logger.trace(item.toString());
            }
        }
        return list;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#findUIElement(org.openqa.selenium.By)
     */
    @Override
    public IUIElement findUIElement(final By by) {
        return findUIElement(by, null, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#findUIElement(org.openqa.selenium.By, long)
     */
    @Override
    public IUIElement findUIElement(final By by, final long timeout) {
        return findUIElement(by, null, timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#findUIElement(org.openqa.selenium.By, java.lang.String)
     */
    @Override
    public IUIElement findUIElement(final By by, final String description) {
        return findUIElement(by, description, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * findUIElement(org.openqa.selenium.By, java.lang.String, long)
     */
    @Override
    public IUIElement findUIElement(final By by, final String description, final long timeout) {
        WebElement ele = null;
        long stopTime = System.currentTimeMillis() + timeout;
        logger.trace("Try to find " + description + " using " + by + " for " + timeout + " milliseconds");
        while (System.currentTimeMillis() < stopTime) {
            ele = findElement(by);
            if (ele != null) {
                break;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
//        ele = findElement(by);

        return ele == null ? null : new DefaultUIElement(
                uiDriver,
                new ElementLocatorBySelf(ele),
                description);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#findUIElements(org.openqa.selenium.By)
     */
    @Override
    public List<IUIElement> findUIElements(final By by) {
        return findUIElements(by, null, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#findUIElements(org.openqa.selenium.By, long)
     */
    @Override
    public List<IUIElement> findUIElements(final By by, final long timeout) {
        return findUIElements(by, null, timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#findUIElements(org.openqa.selenium.By, java.lang.String)
     */
    @Override
    public List<IUIElement> findUIElements(final By by, final String description) {
        return findUIElements(by, description, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * findUIElements(org.openqa.selenium.By, java.lang.String, long)
     */
    @Override
    public List<IUIElement> findUIElements(final By by, final String description, final long timeout) {
        List<WebElement> elements = null;
        long stopTime = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < stopTime) {
            elements = findElements(by);
            if (elements != null && elements.size() > 0) {
                break;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        elements = findElements(by);
        if (elements == null || elements.size() == 0) {
            return null;
        }
        List<IUIElement> elementList = new ArrayList<IUIElement>();
        for (WebElement ele : elements) {
            elementList.add(new DefaultUIElement(
                    uiDriver,
                    new ElementLocatorBySelf(ele),
                    description));
        }
        return elementList;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#getAttribute(java.lang.String)
     */
    @Override
    public String getAttribute(final String name) {
        String attribute = defaultStr;
        waitToBePresent();
        attribute = element.getAttribute(name);
        logger.debug("Attribute Name = '" + name + "' Value = " + attribute + ", for " + this
                .toString());
        return attribute;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.internal.Locatable#getCoordinates()
     */
    @Override
    public Coordinates getCoordinates() {
        return ((Locatable) element).getCoordinates();
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#getCssValue(java.lang.String)
     */
    @Override
    public String getCssValue(final String propertyName) {
        logger.debug("Getting value for property: " + propertyName);
        String cssValue = defaultStr;
        waitToBePresent();
        cssValue = element.getCssValue(propertyName);
        logger.debug("Got value of element " + this.toString() + " for property: "
        + propertyName + " with css value: " + cssValue);
        return cssValue;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.elements.FWEleBase#getDescription()
     */
    @Override
    public String getDescription() {
        return description;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#getHeight()
     */
    @Override
    public int getHeight() {
        int result = -1;
        waitToBePresent();
        result = element.getSize().getHeight();
        logger.debug("Element height  = " + result + "px for element " + this.toString());
        return result;

    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#getLocation()
     */
    @Override
    public Point getLocation() {
        Point p = null;
        waitToBePresent();
        p = element.getLocation();
        logger.debug("X = " + p.getX() + "Y = " + p.getY() + " for element: " + this.toString());
        return p;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#getSelectedOptions()
     */
    @Override
    public List<String> getSelectedOptions() {
        List<String> options = new ArrayList<String>();
        waitToBeDisplayed();
        Select select = new Select(element);
        logger.debug("Getting list of selected items under " + this.toString());
        for (WebElement ele : select.getAllSelectedOptions()) {
            logger.debug("Element is selected " + ele.toString());
            options.add(ele.getText());
        }
        return options;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#getSize()
     */
    @Override
    public Dimension getSize() {
        Dimension size = null;
        waitToBePresent();
        size = element.getSize();
        logger.debug("Height = " + size.getHeight() + ", Width = " + size.getWidth() + " for element: " + this
                .toString());
        return size;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#getTagName()
     */
    @Override
    public String getTagName() {
        String tagName = defaultStr;
        waitToBePresent();
        tagName = element.getTagName();
        logger.debug("Tag Name = " + tagName + ", for " + this.toString());
        return tagName;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#getText()
     */
    @Override
    public String getText() {
        String text = defaultStr;
        waitToBePresent();
        try {
            text = element.getText();
            logger.debug("Inner Text = " + text + ", for " + this.toString());
        } catch (StaleElementReferenceException ex) {
            logger.error(ex);
        }

        return text;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#getUIActions()
     */
    @Override
    public UIActions getUIActions() {
        this.initialize();
        return new DefaultUIActions(uiDriver, this);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#getValue()
     */
    @Override
    public String getValue() {
        String value = defaultStr;
        waitToBePresent();
        value = this.getAttribute("value");
        logger.debug("Attribute = value, Value = " + value + ", for " + this.toString());
        return value;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#getWebElement()
     */
    @Override
    public WebElement getWebElement() {
        initialize();
        return element;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#getWidth()
     */
    @Override
    public int getWidth() {
        int result = -1;
        waitToBePresent();
        result = element.getSize().getWidth();
        logger.debug("Element width  = " + result + "px for element " + this.toString());
        return result;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#hasText()
     */
    @Override
    public boolean hasText() {
        return element != null && element.isDisplayed() && !element.getText().isEmpty();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#initialize()
     */
    @Override
    public void initialize() {
        element = locator.locateElement();
        if (element == null) {
            logger.info("Failed to find UI Element " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#isAbsent()
     */
    @Override
    public boolean isAbsent() {
        return !doFind();
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#isDisplayed()
     */
    @Override
    public boolean isDisplayed() {
        boolean isDisplayed = false;
        boolean isJScriptDisplayed = false;
        initialize();
        if (element != null) {
            try {
                isDisplayed = element.isDisplayed();
                
//                isJScriptDisplayed = super.uiDriver.executeScript("return $('element').is(:visible);", element);
//                var javascriptCapableDriver = (IJavascriptExecutor)Driver;
//                bool jQueryBelivesElementIsVisible = javascriptCapableDriver.ExecuteScript("return $('#myElement').is(:visible);");
                logger.debug((isDisplayed ? "Element is displayed " : "Element is hidden ") + this
                        .toString());
            } catch (StaleElementReferenceException ex) {
                element = null;
                isDisplayed = false;
                logger.error("StaleElementReferenceException for isDisplayed()  " + this.toString(), ex);
            }
        } else {
            logger.debug(("Element is null ") + this.toString());
        }
        return isDisplayed;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#isDisplayedAfterWaiting()
     */
    @Override
    public boolean isDisplayedAfterWaiting() {
        return isDisplayedAfterWaiting(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#isDisplayedAfterWaiting(long)
     */
    @Override
    public boolean isDisplayedAfterWaiting(final long timeout) {
        try {
            waitToBeDisplayed(timeout);
            logger.debug("Element is displayed " + this.toString());
            return true;
        } catch (TimeOutException toe) {
            logger.error("Element is NOT displayed " + this.toString(), toe);
            return false;
        }
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#isEnabled()
     */
    @Override
    public boolean isEnabled() {
        boolean enabled = false;
        doFind();
        if (element != null) {
            try {
                enabled = element.isEnabled();
                logger.debug((enabled ? "Element is enabled " : "Element is disabled ") + this
                        .toString());
            } catch (StaleElementReferenceException ex) {
                element = null;
                enabled = false;
                logger.error("StaleElementReferenceException", ex);
            }
        }

        return enabled;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#isInitialized()
     */
    @Override
    public boolean isInitialized() {
        return (element == null) ? false : true;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#isPresent()
     */
    @Override
    public boolean isPresent() {
        return doFind();
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#isSelected()
     */
    @Override
    public boolean isSelected() {
        boolean selected = false;
        doFind();
        if (element != null) {
            try {
                selected = element.isSelected();
            } catch (StaleElementReferenceException ex) {
                element = null;
                selected = false;
                logger.warn(ex);
            }
        }
        logger.debug((selected ? "Element is selected " : "Element is not selected ") + this
                .toString());
        return selected;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#replaceValues(java.lang.String[])
     */
    @Override
    public IUIElement replaceValues(final String... values) {
        locator.replaceValues(values);
        if (description != null) {
            description = originalDescription;
            for (int i = 0; i < values.length; i++) {
                String value = values[i];
                description = description.replace("{" + (i + 1) + "}", value);
            }
        }
        return this;
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#scrollIntoView()
     */
    @Override
    public void scrollIntoView() {
        // make sure element is present to avoid javascript error
    	if ((!isDisplayed()) && (isPresent())){
            logger.debug("Scrolling to the element " + this.toString());
            super.uiDriver.executeScript("arguments[0].scrollIntoView(true);", element);
    	}
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#selectAllOptions()
     */
    @Override
    public void selectAllOptions() {
        waitToBeDisplayed();
        Select select = new Select(element);
        for (WebElement ele : select.getOptions()) {
            logger.debug("Selecting option with text '" + ele.getText() + "' for " + this
                    .toString());
            select.selectByVisibleText(ele.getText());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#selectByVisibleText(java.lang.String)
     */
    @Override
    public void selectByVisibleText(final String text) {
        waitToBeDisplayed();
        Select select = new Select(element);
        if (select.getAllSelectedOptions().size() <= 0) {
            logger.debug("No options for selecting by text: '" + text + "', element: " + this
                    .toString());
        }
        logger.debug("Selecting option with text '" + text + "' for " + this.toString());
        select.selectByVisibleText(text);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#sendCtrlPlus(java.lang.String)
     */
    @Override
    public void sendCtrlPlus(final String optKey) {
        waitToBeDisplayed();
        this.sendKeys(Keys.CONTROL + optKey);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#sendEnter()
     */
    @Override
    public void sendEnter() {
        waitToBeDisplayed();
        this.sendKeys(Keys.ENTER);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#sendEscape()
     */
    @Override
    public void sendEscape() {
        waitToBeDisplayed();
        this.sendKeys(Keys.ESCAPE);
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#sendKeys(java.lang.CharSequence[])
     */
    
    public void sendKeys(final CharSequence... keysToSend) {
        waitToBeDisplayed();
        try {
            logger.debug("Sending keys: " + Arrays.toString(keysToSend) + " to " + this.toString());
            element.sendKeys(keysToSend);
        } catch (StaleElementReferenceException ex) {
            element = null;
            logger.error("StaleElementReferenceException", ex);
            throw new TestAutomationException("StaleElementReferenceException", ex);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#sendKeys(org.openqa.selenium.Keys)
     */
    
    public void sendKeys(final Keys key) {
        waitToBeDisplayed();
        logger.debug("Sending key: " + key + " to " + this.toString());
        element.sendKeys(key);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#sendKeysToFileInput(java.lang.CharSequence[])
     */
    @Override
    public void sendKeysToFileInput(final CharSequence... keysToSend) {
        waitToBePresent();
        try {
            logger.debug("Sending keys: " + Arrays.toString(keysToSend) + " to " + this.toString());
            element.sendKeys(keysToSend);
        } catch (StaleElementReferenceException ex) {
            element = null;
            logger.error("StaleElementReferenceException", ex);
            throw new TestAutomationException("StaleElementReferenceException", ex);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#setCheckbox(boolean)
     */
    @Override
    public void setCheckbox(final boolean isChecked) {
        waitToBeDisplayed();
        if (!isEnabled()) {
            logger.info("Checkbox " + this.toString() + " is not enabled");
        } else {
            if (isSelected() != isChecked) {
                this.click();
                logger.info("Set checkbox " + this.toString() + " to " + isChecked);
                if (isSelected() != isChecked) {
                    // sometimes IE fails to click the checkbox above, try again using javascript
                    super.uiDriver.executeScript("arguments[0].click();", element);
                    logger.info("2nd attempt to set checkbox " + this.toString() + " to " + isChecked);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.elements.FWEleBase#setDescription(java.lang.String)
     */
    @Override
    public void setDescription(final String desc) {
        description = desc;
    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.WebElement#submit()
     */
    @Override
    public void submit() {
        waitToBeDisplayed();
        logger.debug("Submitting element: " + this.toString());
        element.submit();
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.elements.FWEleBase#toString()
     */
    @Override
    public String toString() {
        String desc = getDescription();
        String elementLocator;
        if (locator.toString().equals("") && element != null) {
            elementLocator = "WebElement = " + element.toString();
        } else {
            if (!locator.toString().equals("")) {
                elementLocator = "Locator = " + locator.toString();
            } else {
                elementLocator = "";
            }
        }
        if (desc != null && elementLocator != null) {
            return "[Description = {" + desc + "}, " + elementLocator + "]";
        } else
            if (elementLocator != null) {
                return "[" + elementLocator + "]";
            } else {
                if (desc != null) {
                    return "[Description = {" + desc + "}]";
                } else {
                    return "";
                }
            }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * waitForAttributeToContain(java.lang.String, java.lang.String)
     */
    @Override
    public void waitForAttributeToContain(final String attribute, final String partialValue) {
        waitForAttributeToContain(attribute, partialValue, config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * waitForAttributeToContain(java.lang.String, java.lang.String, long)
     */
    @Override
    public void waitForAttributeToContain(final String attribute, final String partialValue, final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < stopTime) {
            String attr = getAttribute(attribute);
            if (attr != null && attr.contains(partialValue)) {
                logger.debug(this.toString() + " has attribute '" + attribute 
                		+ "' that contains value '" + partialValue + "'");
                return;
            } else {
            	sleep(config_verify_interval);
            }
        }
        throw new TimeOutException(
        		"Element " + this.toString() + " does not contains partial value '" 
        + partialValue + "' for attribute '" + attribute + "'. value = ");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitForChildrenToDisplay()
     */
    @Override
    public void waitForChildrenToDisplay() {
        waitForChildrenToDisplay(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * waitForChildrenToDisplay(com.auxenta.test.auxtest.framework.drivers.IErrorDetector, long)
     */
    @Override
    public void waitForChildrenToDisplay(final IErrorDetector errorDetector, final long timeout) {
        logger.debug("Wait children to appear for at most " + timeout + " milliseconds, under " + this
                .toString());
        long stopTime = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < stopTime) {
            errorDetector.assertNoError();
            List<WebElement> elements = findElements(ByUtil.getBy(UIType.Xpath, "./*"));
            if (elements != null && elements.size() > 0) {
                for (WebElement element : elements) {
                    if (element != null && element.isDisplayed()) {
                        return;
                    }
                    if (timeout < config_verify_interval) {
                        sleep(timeout);
                    } else {
                        sleep(config_verify_interval);
                    }
                }
            }
        }
        errorDetector.assertNoError();
        List<WebElement> elements = findElements(ByUtil.getBy(UIType.Xpath, "./*"));
        if (elements != null && elements.size() > 0) {
            for (WebElement element : elements) {
                if (element != null && element.isDisplayed()) {
                    return;
                }
            }
        }
        throw new TimeOutException(
                "Failed while waiting for elements '" + this.toString() + "' children to be displayed",
                timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitForChildrenToDisplay(long)
     */
    @Override
    public void waitForChildrenToDisplay(final long timeout) {
        waitForChildrenToDisplay(DUMMY_ERROR_DETECTOR, timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * waitForMatchText(java.lang.String, com.auxenta.test.auxtest.framework.util.SearchScope)
     */
    @Override
    public void waitForMatchText(final String text, final SearchScope scope) {
        long timeout = System.currentTimeMillis() + config_wait_timeout;
        String currentText = "";
        while (System.currentTimeMillis() < timeout) {
            currentText = getText();
            if (currentText != null) {
                switch (scope) {
                    case CONTAINS:
                        if (currentText.contains(text)) {
                            return;
                        }
                    case EQUALS:
                        if (currentText.equals(text)) {
                            return;
                        }
                }
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        currentText = getText();
        if (currentText != null)
            switch (scope) {
                case CONTAINS:
                    if (currentText.contains(text)) {
                        return;
                    }
                case EQUALS:
                    if (currentText.equals(text)) {
                        return;
                    }
            }
        throw new TimeOutException(
                "The element did not display text '" + text 
                + "' as expected. instead it displayed '" + currentText + "'.");
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeAbsent()
     */
    @Override
    public void waitToBeAbsent() {
        waitToBeAbsent(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeAbsent(long)
     */
    @Override
    public void waitToBeAbsent(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        logger.debug("Waiting for element to be absent in DOM " + this.toString());
        while (doFind() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        doFind();
        if (element == null) {
            return;
        } else {
            throw new TimeOutException("Element still present on the page " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeClick()
     */
    @Override
    public void waitToBeClick() {
    	WebDriverWait wait = new WebDriverWait(uiDriver, WEB_DRIVER_WAIT_TIME);
    	wait.until(ExpectedConditions.elementToBeClickable(this));
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeDisabled()
     */
    @Override
    public void waitToBeDisabled() {
        waitToBeDisabled(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeDisabled(long)
     */
    @Override
    public void waitToBeDisabled(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        logger.debug("Waiting for element to be disabled " + this.toString());
        while (doFind() && isEnabled() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        if (element == null || !isEnabled()) {
            return;
        } else {
            throw new TimeOutException("Failed to disable " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeDisplayed()
     */
    @Override
    public void waitToBeDisplayed() {
        waitToBeDisplayed(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeDisplayed(long)
     */
    @Override
    public void waitToBeDisplayed(final long timeout) {
        long startTime = System.currentTimeMillis();
        long stopTime = startTime + timeout;
        boolean isDisplayed = false;
        logger.debug("Waiting " + timeout + "ms for element to be displayed " + this.toString());
        while (System.currentTimeMillis() < stopTime) {
        	isDisplayed = isDisplayed();
            if (isDisplayed) {
                break;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
            if (uiDriver.getWebDriver() instanceof InternetExplorerDriver) {
                // this method was failing sometimes in IE, the following line fixed the problem
//                scrollIntoView();
            }
        }
//        isDisplayed = isDisplayed();
        long waitTime = System.currentTimeMillis() - startTime;
        if (element != null && isDisplayed) {
            logger.debug("After " + waitTime + "ms, element is displayed wait time " + this.toString());
            return;
        } else {
            String errorMessage = "After " + waitTime + "ms, failed to display element " + this
                    .toString();
            logger.error(errorMessage);
            throw new TimeOutException(errorMessage);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeEnabled()
     */
    @Override
    public void waitToBeEnabled() {
        waitToBeEnabled(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeEnabled(long)
     */
    @Override
    public void waitToBeEnabled(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        waitToBePresent(timeout);
        logger.debug("Waiting for element to be enabled " + this.toString());
        while ((!isEnabled()) && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        if (element != null && isEnabled()) {
            return;
        } else {
            throw new TimeOutException("Failed to enable " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeHidden()
     */
    @Override
    public void waitToBeHidden() {
        waitToBeHidden(config_wait_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * waitToBeHidden(com.auxenta.test.auxtest.framework.drivers.IErrorDetector, long)
     */
    @Override
    public void waitToBeHidden(final IErrorDetector errorDetector, final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        boolean isDisplayed = true;
        logger.debug("Waiting for element to be hidden " + this.toString());
        while (System.currentTimeMillis() < stopTime) {
            errorDetector.assertNoError();
            isDisplayed = isDisplayed();
            if (!isDisplayed) {
                break;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        isDisplayed = isDisplayed();
        if (!isDisplayed) {
            return;
        } else {
            throw new TimeOutException("Failed to hide element " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBeHidden(long)
     */
    @Override
    public void waitToBeHidden(final long timeout) {
        long stopTime = System.currentTimeMillis() + timeout;
        boolean isDisplayed = true;
        logger.debug("Waiting for element to be hidden " + this.toString());
        while (System.currentTimeMillis() < stopTime) {
        	isDisplayed = isDisplayed();
            if (!isDisplayed) {
                break;
            }
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        isDisplayed = isDisplayed();
        if (!isDisplayed) {
            return;
        } else {
            throw new TimeOutException("Failed to hide element " + this.toString());
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBePresent()
     */
    @Override
    public void waitToBePresent() {
        waitToBePresent(config_verify_timeout);
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#
     * waitToBePresent(com.auxenta.test.auxtest.framework.drivers.IErrorDetector, long)
     */
    @Override
    public void waitToBePresent(final IErrorDetector errorDetector, final long timeout) {
        long startTime = System.currentTimeMillis();
        long stopTime = startTime + timeout;
        logger.debug("Waiting " + timeout + "ms for element to be present " + this.toString());
        while (!doFind() && System.currentTimeMillis() < stopTime) {
            errorDetector.assertNoError();
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        if (doFind()) {
            return;
        } else {
            String errorMessage = "After " + (System.currentTimeMillis() - startTime) 
            		+ "ms, failed while element initialization " + this
                    .toString();
            logger.error(errorMessage);
            throw new TimeOutException(errorMessage);
        }
    }

    /* (non-Javadoc)
     * @see com.auxenta.test.auxtest.framework.core.IUIElement#waitToBePresent(long)
     */
    @Override
    public void waitToBePresent(final long timeout) {
        long startTime = System.currentTimeMillis();
        long stopTime = startTime + timeout;
	        logger.debug("Waiting " + timeout + "ms for element to be present " + this.toString());
        while (!doFind() && System.currentTimeMillis() < stopTime) {
            if (timeout < config_verify_interval) {
                sleep(timeout);
            } else {
                sleep(config_verify_interval);
            }
        }
        if (doFind()) {
            return;
        } else {
            String errorMessage = "After " + (System.currentTimeMillis() - startTime) 
            		+ "ms, failed while element initialization " + this
                    .toString();
            logger.error(errorMessage);
            throw new TimeOutException(errorMessage);
        }
    }
  
    /**
     * Confirms whether element has the attribute it is looking for
     * @param attribute
     * @return
     */
    @Override
    public boolean isAttribtuePresent(String attribute) {
    	waitToBePresent();
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null){
                result = true;
            }
        } catch (Exception e) {        	
        }

        return result;
    }
    /**
     * This method has not been implemented yet
     * @param target
     * @return
     */
    @Deprecated
    public <X> X getScreenshotAs(OutputType<X> target){
		return null;
    	
    }

	public Rectangle getRect() {
		return ((DefaultUIElement) element).getRect();
	}
    
    
}
