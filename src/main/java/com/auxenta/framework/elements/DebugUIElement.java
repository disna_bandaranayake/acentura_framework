/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.auxenta.framework.core.IUIDriver;
import com.auxenta.framework.core.IUIElement;
import com.auxenta.framework.exception.TestAutomationException;

/**
 * This class supplies methods that log debugging info about web elements .
 *
 * @author Niroshen Landerz
 * @date Aug 31st 2014
 */
public class DebugUIElement {

    /** The logger. */
    private static Logger logger = Logger.getLogger(DebugUIElement.class);

    /** The ui driver. */
    private static IUIDriver uiDriver;

    /** The ui element. */
    private IUIElement uiElement;

    /**
     * Instantiates a new debug ui element.
     *
     * @param driverVal the driver
     */
    public DebugUIElement(final IUIDriver driverVal) {
        DebugUIElement.uiDriver =  driverVal;
    }

    /**
     * Instantiates a new debug ui element.
     *
     * @param eVal the e
     */
    public DebugUIElement(final IUIElement eVal) {
        uiDriver = eVal.getUIDriver();
        uiElement = eVal;
    }

    /**
     * Debug.
     */
    public void debug() {
        debugObject(uiElement);
    }

    /**
     * Debug visibility.
     *
     * @return the string
     */
    public String debugVisibility() {
       return getDebugVisibility(uiElement);
    }

    /**
     * Debug draw red outline.
     */
    public void debugDrawRedOutline() {
        uiDriver.executeScript("arguments[0].style.border = '1px solid red';", uiElement.getWebElement());
    }

    /**
     * Debug draw red outline around element.
     *
     * @param element the element
     */
    public static void debugDrawRedOutlineAroundElement(final IUIElement element) {
        uiDriver = element.getUIDriver();
        uiDriver.executeScript("arguments[0].style.border = '1px solid red';", element);
    }


    /**
     * List children.
     */
    public void listChildren() {
        listChildren(uiElement.getWebElement());
    }

    /**
     * List children.
     *
     * @param parent the parent
     */
    public static void listChildren(final WebElement parent) {
        ArrayList<WebElement> elements = (ArrayList<WebElement>) parent.findElements(By
                .xpath("child::node()"));
        for (WebElement element : elements) {
            logger.debug("child: " + element.getTagName() + " = " + element.getText());
        }
    }



    /**
     * Useful when you get an object, but maybe not the one you were looking for or it does not
     * behave the way you expect.
     *
     * @param name the name
     * @param uiElement the ui element
     */
    public static void debugObject(final String name, final IUIElement uiElement) {
        try {
            WebElement webElement = uiElement.getWebElement();
            logger.debug(name + ":tag=" + webElement.getTagName());
            logger.debug(name + ":CSSid=" + webElement.getCssValue("id"));
            logger.debug(name + ":id=" + webElement.getAttribute("id"));
            logger.debug(name + ":title=" + webElement.getAttribute("title"));
            logger.debug(name + ":name=" + webElement.getAttribute("name"));
            logger.debug(name + ":src=" + webElement.getAttribute("src"));
            logger.debug(name + ":CSSclass=" + webElement.getCssValue("class"));
            logger.debug(name + ":class=" + webElement.getAttribute("class"));
            logger.debug(name + ":text=" + webElement.getText());
            logger.debug(name + ":enabled=" + webElement.isEnabled());
            logger.debug(name + ": " + getDebugVisibility(uiElement));
            logger.debug(name + ":selected=" + webElement.isSelected());
            logger.debug(name + ":CSSvalue=" + webElement.getCssValue("value"));
            logger.debug(name + ":value=" + webElement.getAttribute("value"));
            logger.debug(name + "/.." + webElement.findElement(By.xpath("..")).getTagName());
            List<WebElement> children = webElement.findElements(By.xpath("*"));
            logger.debug(name + "children=" + children.size());
        } catch (Exception e) {
            logger.error("While printing debugObject messages, got error: ", e);
            throw new TestAutomationException("Error while printing debugObject messages", e);
        }
    }

    /**
     * Get debug string describing element's visibility in detail.
     *
     * @param uiElement the ui element
     * @return debug message listing attributes relevant to visibility.
     */
    public static String getDebugVisibility(final IUIElement uiElement) {
        try {
            WebElement element = uiElement.getWebElement();
            String display = element.getAttribute("display");
            String visible = element.getAttribute("visibility");
            boolean enabled = element.isEnabled();
            String width = element.getAttribute("width");
            String height = element.getAttribute("height");
            return "isDisplayed=" + element.isDisplayed() + ", display=" + display
            		+ ", visible=" + visible + ", enabled=" + enabled + ", width=" + width
            		+ ", height=" + height;
        } catch (Exception e) {
        	logger.error("Exception: ", e);
        	 throw new TestAutomationException("Exception", e);
        }
    }

    /**
     * Useful when you get an object, but maybe not the one you were looking for or it does not
     * behave the way you expect.
     *
     * @param uiElement the ui element
     */
    public static void debugObject(final IUIElement uiElement) {
        debugObject(uiElement.getDescription(), uiElement);
    }
}
