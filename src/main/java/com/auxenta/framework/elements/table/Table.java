/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements.table;

import org.openqa.selenium.By;

import com.auxenta.framework.BasicPieceObject;
import com.auxenta.framework.core.IUIElement;

/**
 * This class represents HTML Table {@code <table>} <br>.
 *
 * @author Niroshen Landerz
 * @date Aug 29th 2014
 */
public class Table extends BasicPieceObject {

    /** The table root locator. */
    private IUIElement tableRootLocator;

    /**
     * Create new {@link BasicPieceObject} which represents HTML Table.
     *
     * @param tableRoot
     *            - root of the table, i.e {@code <table>} node
     */
    public Table(final IUIElement tableRoot) {
        super(tableRoot);
        this.tableRootLocator = tableRoot;
    }

    /**
     * Body.
     *
     * @return {@link TBody} object which represents table body
     */
    public TBody body() {
        return new TBody(getTBodyElement());
    }

    /**
     * Head.
     *
     * @return {@link THead} object which represents table head
     */
    public THead head() {
        return new THead(getTHeadElement());
    }

    /**
     * Foot.
     *
     * @return {@link TFoot} object which represents table foot
     */
    public TFoot foot() {
        return new TFoot(getTFootElement());
    }

    /**
     * Gets the t head element.
     *
     * @return the t head element
     */
    public IUIElement getTHeadElement() {
        return tableRootLocator.findUIElement(By.xpath("child::thead"));
    }

    /**
     * Gets the t body element.
     *
     * @return the t body element
     */
    public IUIElement getTBodyElement() {
        return tableRootLocator.findUIElement(By.xpath("child::tbody"));
    }

    /**
     * Gets the t foot element.
     *
     * @return the t foot element
     */
    public IUIElement getTFootElement() {
        return tableRootLocator.findUIElement(By.xpath("child::tfoot"));
    }
}
