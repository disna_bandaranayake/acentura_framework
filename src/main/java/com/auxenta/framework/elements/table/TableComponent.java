/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.StaleElementReferenceException;

import com.auxenta.framework.core.IUIElement;

/**
 * This class designed to be extended by {@link TBody} , {@link TFoot}, and
 * {@link THead}. It provides common features to those classes only. Use
 * {@link Table} class in order to mimic {@code <table>} object and get access
 * to all table components from {@link Table} class
 *
 * @author Niroshen Landerz
 * @date Sept 1st 2014
 */

public class TableComponent {

	/** The table component element. */
	private IUIElement tableComponentElement;

	/**
	 * Instantiates a new table component.
	 *
	 * @param tableComponentElement
	 *            the table component element
	 */
	protected TableComponent(final IUIElement tableComponentElementVal) {
		this.tableComponentElement = tableComponentElementVal;
	}

	/**
	 * Gets the all rows.
	 *
	 * @return the all rows
	 */
	public List<IUIElement> getAllRows() {
		List<IUIElement> rows = tableComponentElement.findUIElements(By.xpath("child::tr"));
		return rows;
	}

	/**
	 * Get cell from specified row by index. Use enum to define cell index. Cell
	 * index starts with zero. Zero for first column in current component, one
	 * for second column and so on
	 *
	 * @param rowFromTableComponent
	 *            - {@code <tr>} element
	 * @param cellIndex
	 *            - column index, starts with 0
	 * @return UIElement representing cell in this table, or null if user passed
	 *         invalid index
	 */
	public IUIElement getCellFromRow(final IUIElement rowFromTableComponent, final int cellIndex) {
		List<IUIElement> cells = this.getCellsFromRow(rowFromTableComponent);
		if (cells.size() > cellIndex) {
			//cells.get(cellIndex).scrollIntoView();
			return cells.get(cellIndex);
		}
		return null;
	}

	/**
	 * Gets the cells from column.
	 *
	 * @param columnIndex
	 *            - column index, starts with 0
	 * @return List of cells from all rows in specified column
	 */
	public List<IUIElement> getCellsFromColumn(final int columnIndex) {
		int col = columnIndex +1;
		return tableComponentElement.findUIElements(By.xpath("tr/td[" + col +"]|tr/th[" + col +"]"));
		
	}

	/**
	 * Gets the cells from row.
	 *
	 * @param rowFromTableComponent
	 *            the row from table component
	 * @return List of cells in a given row. Note that it returns only child td
	 *         or th elements
	 */
	public List<IUIElement> getCellsFromRow(final IUIElement rowFromTableComponent) {
	//	rowFromTableComponent.findUIElement(By.xpath("td[1]|th[1]")).scrollIntoView();
		return rowFromTableComponent.findUIElements(By.xpath("child::td|child::th"));
	}

	/**
	 * Gets the cells text from row.
	 *
	 * @param rowFromTableComponent
	 *            the row from table component
	 * @return List of cells in a given row. Note that it returns only child td
	 *         or th elements
	 */
	public List<String> getCellsTextFromRow(final IUIElement rowFromTableComponent) {
		List<String> returnList = new ArrayList<String>() ;
		rowFromTableComponent.findUIElement(By.xpath("td[1]|th[1]")).scrollIntoView();
		List<IUIElement> cellList = rowFromTableComponent.findUIElements(By.xpath("td|th"));
		for (IUIElement element : cellList){
			if ("".equals(element.getText())){
				element.scrollIntoView();
			}
			returnList.add(element.getText());
		}
		
		return returnList;
	}
	
	/**
	 * Gets the number of rows.
	 *
	 * @return the number of rows
	 */
	public int getNumberOfRows() {
		
		return getAllRows().size();
	}
	
	/**
	 * Gets the number of columns in the table
	 * @return number of columns in the table
	 */
	public int getNumberOfColumns(){
		return getCellsFromRow(getRow(0)).size();
	}

	/**
	 * Get row from table by index.
	 *
	 * @param rowIndex
	 *            - index of a row ( index starts from 0 )
	 * @return {@code <tr>} node element
	 */
	public IUIElement getRow(final int rowIndex) {
		List<IUIElement> rows = this.getAllRows();
		if (rowIndex <= rows.size()) {
			return rows.get(rowIndex);
		} else {
			return null;
		}
	}

	/**
	 * Get the row in the grid containing text. If the text is a link, use
	 * getRowContainingText(text, true)
	 *
	 * @param text
	 *            The text to look for in the row
	 * @return UIElement representing the found row
	 */
	public IUIElement getRowContainingText(final String text) {
		try {
			IUIElement row = tableComponentElement.findUIElement(By.xpath("tr[contains(., '" + text + "')]"));
			return row;
			
		} catch (StaleElementReferenceException e) {
			throw new InvalidElementStateException();
		}
	}

	/**
	 * Get {@code <tr>} (table row) node element from table component that
	 * contains {@code text} in provided column.
	 *
	 * @param text
	 *            - Unique string to identify row
	 * @param columnIndex
	 *            - index of a column ( index starts from 0 )
	 * @return {@code <tr>} node element
	 */
	public IUIElement getRowContainingTextInColumn(final String text, int columnIndex) {
		columnIndex++;
		String xpath = ".//tr[*[self::td|self::th][" + columnIndex + "][contains(text(),\"" + text + "\")]]";
		return tableComponentElement.findUIElement(By.xpath(xpath));
	}

	public int getRowIndexUsingTextInColumn(final String text, int columnIndex) {
		String cellText;
		List<IUIElement> elements = getCellsFromColumn(columnIndex);
		int i = -1;
		for (Iterator<IUIElement> iterator = elements.iterator(); iterator.hasNext();) {
			IUIElement cell = iterator.next();
			cellText = cell.getText();
			if (cellText.equals(""))
			{
			cell.scrollIntoView();
			cellText = cell.getText();
			}
			if (cellText.trim().equalsIgnoreCase(text.trim())){
				i++;
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public int getColumnIndexUsingTextInRow(final String text, int rowIndex) {
		List<IUIElement> elements = getCellsFromRow(getRow(rowIndex));
		int i = -1;
		for (Iterator<IUIElement> iterator = elements.iterator(); iterator.hasNext();) {
			IUIElement cell = iterator.next();
			String str = cell.getText().trim();
					if(str == ""){
						cell.scrollIntoView();
						str = cell.getText().trim();
					}
			if (str.equalsIgnoreCase(text.trim())){
				i++;
				return i;
			}
			i++;
		}
		return -1;
	}
	
	/**
	 * Get column index.
	 *
	 * @param text
	 *            - Unique string to identify row and column
	 * @return - index of a column ( index starts from 0 )
	 */
	
	public int getColumnIndex(final String text) {
		IUIElement rowWithProvidedText = getRowContainingText(text);
		rowWithProvidedText.findUIElement(By.xpath("td[1]|th[1]")).scrollIntoView();

		String rowText = rowWithProvidedText.getText();
		String lines[] = rowText.split("\\r?\\n");
		if (lines.length == rowWithProvidedText.findUIElements(By.xpath("td|th")).size()) {
			int retValue = Arrays.asList(lines).indexOf(text);
			if (retValue > -1) {
				return retValue;
			}
		}

		List<IUIElement> cellList = rowWithProvidedText.findUIElements(By.xpath("td|th"));
		int i = -1;
		for (IUIElement element : cellList) {
			element.scrollIntoView();

			String temp = element.getText();
			if ("".equals(temp)) {
				element.scrollIntoView();
				temp = element.getText();
			}
			i++;
			if (text.equalsIgnoreCase(temp)) {
				return i;
			}
		}
		int colIndex = -1;
		IUIElement link = null;
		List<IUIElement> cols = getCellsFromRow(rowWithProvidedText);
		for (int j = 0; j < cols.size(); j++) {
			IUIElement cell = cols.get(j);
			cell.scrollIntoView();
			String cellText = cell.getText().trim();
			if (cellText.equals(text)) {
				colIndex = j;
				return colIndex;
			}

			if (cellText.length() == 0) {

				try {
					link = cell.findUIElement(By.tagName("a"));
				} catch (Exception e) {
					continue;
				}

				if ((link != null && link.getAttribute("title").equals(text)) || cell.getText().trim().equals(text)) {
					colIndex = i;
					return colIndex;
				}
			}

		}
		return colIndex;
	}

	/**
	 * Get row index where the first instance of the given test is displayed.
	 *
	 * @param text
	 *            = Exact text as it appears on the cell,
	 * @return integer value of the row number.
	 */
	public int getRowIndex(final String text) {
		int rowIndex = -2;
		String tempRowText;
		List<IUIElement> rows = getAllRows();
		IUIElement row = getRowContainingText(text);
		String rowText = row.getText().trim();
		for (int i = 0; i < rows.size(); i++) {
			tempRowText = rows.get(i).getText();
			if (tempRowText.equals("")){
				rows.get(i).scrollIntoView();
				tempRowText = rows.get(i).getText();
			}
			if (tempRowText.trim().equalsIgnoreCase(rowText)) {
				rowIndex = i;
				break;
			}
		}
		return rowIndex;

		/*
		 * IUIElement rowWithProvidedText = getRowContainingText(text);
		 * List<IUIElement> cells = getCellsFromRow(rowWithProvidedText); for
		 * (int i = 0; i < cells.size(); i++) { if
		 * (cells.get(i).getText().equals(text)) { rowIndex = i; break; } }
		 * return rowIndex;
		 */
	}

	/**
	 * Will return the cell element for the caller.
	 *
	 * @param rowIndex
	 *            = First row in a table is 0, and numbered accordingly
	 * @param columnIndex
	 *            = First column in a table is 0, and numbered accordingly
	 * @return the cell element.
	 */
	public IUIElement getCellFromRowIndexColumnIndex(final int rowIndex, final int columnIndex) {
		IUIElement row = getRow(rowIndex);
		int i = columnIndex + 1;
		IUIElement temp = row.findUIElement(By.xpath("td["+ i +"] | th[" + i +"]"));
		temp.isDisplayed();
		//temp.scrollIntoView();
		return temp;
	}

	/**
	 * Will return the cell element .
	 *
	 * @param text
	 *            - Exact text that is appearing inside the Cell... may not work
	 *            if cell text is different to what is fully inside the cell.
	 * @return Element cell
	 */
	public IUIElement getCellFromText(final String text) {
		int x = -1;
		int y = -1;
		x = getRowIndex(text);
		y = getColumnIndex(text);
		return getCellFromRowIndexColumnIndex(x, y);
	}
}
