/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements.table;

import com.auxenta.framework.core.IUIElement;

/**
 * This class represents {@code <tbody>} object.
 *
 * @author Niroshen Landerz
 * @date Sept 4th 2014
 */

public class TBody extends TableComponent {

    /**
     * Instantiates a new t body.
     *
     * @param tBodyElement the t body element
     */
    protected TBody(final IUIElement tBodyElement) {
        super(tBodyElement);
    }
}
