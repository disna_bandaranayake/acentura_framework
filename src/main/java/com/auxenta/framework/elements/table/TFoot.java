/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements.table;

import com.auxenta.framework.core.IUIElement;

/**
 * This class represents {@code <tfoot>} object.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */

public class TFoot extends TableComponent {

    /**
     * Instantiates a new t foot.
     *
     * @param tFootElement the t foot element
     */
    protected TFoot(final IUIElement tFootElement) {
        super(tFootElement);
    }
}
