/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.elements.table;

import com.auxenta.framework.core.IUIElement;

/**
 * This class represents {@code <thead>} object.
 *
 * @author Niroshen Landerz
 * @date Sept 3rd 2014
 */

public class THead extends TableComponent {


    /**
     * Instantiates a new t head.
     *
     * @param tHeadElement the t head element
     */
    protected THead(final IUIElement tHeadElement) {
        super(tHeadElement);
    }
}
