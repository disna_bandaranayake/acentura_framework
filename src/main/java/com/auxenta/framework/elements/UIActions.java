/**
 * 
 */
package com.auxenta.framework.elements;

import com.auxenta.framework.core.IUIElement;

/**
 * Actions performed by {@link Mouse} and {@link Keyboard} such as drag and drop, hover over,
 * rightClick and so on
 *
 * @author Niroshen Landerz
 * @date Aug 29th 2014
 */
public interface UIActions {

    /**
     * Click on element by location
     */
    void clickOnLocation();

    /**
     * Hover over an element
     *
     */
    void hoverOver();

    /**
     * Hover element and click on it
     *
     */
    void hoverOverAndClickOn();

    /**
     * Send a CharSequence to the element.
     *
     * @param keysToSend
     */
    void sendKeys(CharSequence... keysToSend);

    /**
     * Use actions to move the mouse to the element
     *
     * @param elem
     */
    void mouseMoveHere();

    /**
     * Simulate mouse double click
     *
     *
     * @param elem
     */
    void doubleClick();

    /**
     * Move mouse to current element and click
     */
    void click();

    /**
     * Drop current element into {@code target} element
     *
     * @param target
     *            - droppable uielement
     */
    void dragAndDrop(IUIElement target);

    /**
     * see
     * {@link org.openqa.selenium.interactions.Actions#contextClick(org.openqa.selenium.WebElement)}
     */
    void rightClick();

    /**
     * see {@link Actions#dragAndDropBy(org.openqa.selenium.WebElement, int, int)}
     *
     * @param xOffset xOffset
     * @param yOffset yOffset
     */
    void dragAndDrop(int xOffset, int yOffset);

    /**
     * see {@link Actions#clickAndHold(org.openqa.selenium.WebElement)}
     */
    void clickAndHold();

    /**
     * see {@link Actions#release(org.openqa.selenium.WebElement)}
     */
    void release();

}
