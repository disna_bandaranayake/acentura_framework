/*
 *================================================================
 * Copyright  (c)     : 2015 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.Assert;

/**
 * The Class AssertResult.
 */
public class AssertResult {

	/** The status. */
	private String status;

	/** The title. */
	private String title;

	/** The failer message. */
	private String failierMessage;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the failer message.
	 *
	 * @return the failer message
	 */
	public String getFailierMessage() {
		return failierMessage;
	}

	/**
	 * Sets the failer message.
	 *
	 * @param failierMessage the new failer message
	 */
	public void setFailierMessage(String failierMessage) {
		this.failierMessage = failierMessage;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
