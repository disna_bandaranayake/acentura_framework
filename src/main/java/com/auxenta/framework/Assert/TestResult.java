/*
 *================================================================
 * Copyright  (c)     : 2015 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.Assert;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class TestResult.
 */
public class TestResult {

	/** The assertions. */
	private List<AssertResult> assertions = new ArrayList<>();

	/** The status. */
	private String status;
	
	/** The test case name. */
	private String testCaseName;
	
	/** The proccess id. */
	private Long proccessId;
	
	/** The execution finish. */
	private Boolean executionFinish;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the assertions.
	 *
	 * @return the assertions
	 */
	public List<AssertResult> getAssertions() {
		return assertions;
	}

	/**
	 * Sets the assertion.
	 *
	 * @param assertion the new assertion
	 */
	public void setAssertion(AssertResult assertion) {
		this.assertions.add(assertion);
	}

	/**
	 * Gets the proccess id.
	 *
	 * @return the proccess id
	 */
	public Long getProccessId() {
		return proccessId;
	}

	/**
	 * Sets the proccess id.
	 *
	 * @param proccessId the new proccess id
	 */
	public void setProccessId(Long proccessId) {
		this.proccessId = proccessId;
	}

	/**
	 * Gets the test case name.
	 *
	 * @return the test case name
	 */
	public String getTestCaseName() {
		return testCaseName;
	}

	/**
	 * Sets the test case name.
	 *
	 * @param testCaseName the new test case name
	 */
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	/**
	 * Gets the execution finish.
	 *
	 * @return the execution finish
	 */
	public Boolean getExecutionFinish() {
		return executionFinish;
	}

	/**
	 * Sets the execution finish.
	 *
	 * @param executionFinish the new execution finish
	 */
	public void setExecutionFinish(Boolean executionFinish) {
		this.executionFinish = executionFinish;
	}

}
