/*
 *================================================================
 * Copyright  (c)     : 2015 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.Assert;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.testng.Assert;

import com.auxenta.framework.logger.ResultLogger;

/**
 * The Class AuxAssert.
 */
public class AuxAssert {

	/** The map. */
	private static Map<Long, TestResult> map = new HashMap<>();

	private static ResultLogger resultLogger = new ResultLogger();
	
	//public static TestResult result = null;
	/**
	 * Inits the.
	 * 
	 * @param name
	 *            the name
	 */
	public static void init(String name) {
		long threadId = Thread.currentThread().getId();
		TestResult oldTestResult = map.get(threadId);
		if (oldTestResult != null) {
			logResult(oldTestResult);
		}
		TestResult result = new TestResult();
		result.setExecutionFinish(false);
		result.setProccessId(threadId);
		result.setTestCaseName(name);
		result.setStatus("PASSED");
		map.put(threadId, result);
	}

	/**
	 * Assert equals.
	 * 
	 * @param actual
	 *            the actual
	 * @param expected
	 *            the expected
	 * @param title
	 *            the title
	 */
	public static void assertEquals(Object actual, Object expected, String title) {
		long threadId = Thread.currentThread().getId();
		TestResult result = map.get(threadId);
		if (result == null) {
			Assert.assertEquals(actual, expected, title);
		} else {
			AssertResult assertResult = new AssertResult();
			assertResult.setTitle(title);
			if ((expected == null) && (actual == null)) {
				assertResult.setStatus("PASSED");
			} else if ((expected != null) && (expected.equals(actual))) {
				assertResult.setStatus("PASSED");
			} else {
				assertResult.setStatus("FAILED");
				result.setStatus("FAILED");
				assertResult.setFailierMessage("Expected [" + expected + "] but found [" + actual + "]");
			}
			// result.setStatus("PASSED"); NRL 14-6-2017 modified the line to fix the issue of Assert true failing and results setting to Passed
			result.setAssertion(assertResult);
			map.put(threadId, result);
		}

	}

	/**
	 * Assert true.
	 * 
	 * @param condition
	 *            the condition
	 * @param title
	 *            the title
	 */
	public static void assertTrue(boolean condition, String title) {

		long threadId = Thread.currentThread().getId();
		TestResult result = map.get(threadId);
		if (result == null) {
			Assert.assertTrue(condition, title);
		} else {
			AssertResult assertResult = new AssertResult();
			assertResult.setTitle(title);
			if (condition) {
				assertResult.setStatus("PASSED");
			} else {
				assertResult.setStatus("FAILED");
				result.setStatus("FAILED");
				assertResult.setFailierMessage("Assert condition false");
			}

			result.setAssertion(assertResult);
			map.put(threadId, result);
		}
	}

	/**
	 * Finished.
	 */
	public static void finished() {
		long threadId = Thread.currentThread().getId();
		TestResult result = map.get(threadId);
		
		if (result != null) {
			result.setExecutionFinish(true);
			String resultMessage = logResult(result);
			map.put(threadId, null);
			if ("FAILED".equals(result.getStatus())) {
				Assert.fail(resultMessage);
			}
		}
	}
	
	public static void failierRetry() {
		long threadId = Thread.currentThread().getId();
		TestResult result = map.get(threadId);
		if (result != null) {
			result.setStatus("FAILED");
			result.setExecutionFinish(false);
			map.put(threadId, null);
		}
	}

	/**
	 * Clear all asser results.
	 */
	public static void clearAllAsserResults() {
		Set<Long> keySet = map.keySet();
		for (Iterator<Long> iterator = keySet.iterator(); iterator.hasNext();) {
			Long treadId = (Long) iterator.next();
			TestResult result = map.get(treadId);
			if (result != null) {
				result.setStatus("FAILED");
				logResult(map.get(treadId));
			}
			map.put(treadId, null);
		}
	}

	public static String getResultStatusOfCurrentThread(){
		long threadId = Thread.currentThread().getId();
		TestResult result = map.get(threadId);
		return result.getStatus();
		
	}
	
	public static void setResultStatusOfCurrentThread(String status){
		long threadId = Thread.currentThread().getId();
		TestResult result = map.get(threadId);
		result.setStatus(status);
		map.put(threadId, result);
		
	}
	
	/**
	 * Log result.
	 * 
	 * @param result
	 *            the result
	 * @return the string
	 */
	
	private static String logResult(TestResult result) {
		String resultMessage = result.getStatus() + " : " + result.getTestCaseName();
		int i = 1;
		for (Iterator<AssertResult> iterator = result.getAssertions().iterator(); iterator.hasNext();) {
			AssertResult assertion = iterator.next();
			resultMessage = resultMessage + "\n " + assertion.getStatus() + " Assert " + i + " "
					+ assertion.getTitle();
			if (assertion.getFailierMessage() != null) {
				resultMessage = resultMessage + " Reason : " + assertion.getFailierMessage();
			}
			i++;
		}
		
		if (result.getExecutionFinish()) {
			resultMessage = resultMessage + "\n " + result.getTestCaseName() + " Successfully executed till end " + "\n" ;
		} else {
			resultMessage = resultMessage + "\n " + result.getTestCaseName() + " Terminated abrubtly due to errors " + "\n" ;
		}

		resultLogger.logResult(resultMessage);
		return resultMessage;
	}
}
