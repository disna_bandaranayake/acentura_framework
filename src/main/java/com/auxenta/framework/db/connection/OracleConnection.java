/*
 *================================================================
 * Copyright  (c)    : 2015 Dialog Axiata PLC, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.db.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;

/**
 * The Class OracleConnection.
 */
public class OracleConnection {

	/** The connection. */
	private static Connection connection;
	
	private static Connection connection2;

	/**
	 * Open db connection.
	 *
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	private static void openDBConnection() throws SQLException, ClassNotFoundException {

		IConfig config = DefaultConfig.getConfig();

		String driverName = "oracle.jdbc.driver.OracleDriver";
		try {

			Class.forName(driverName);

		} catch (ClassNotFoundException e) {
			System.err.println("Where is your Oracle JDBC Driver? "  + e.getMessage());
			throw e;
		}

		String url = config.getValue(ConfigKeys.KEY_DBURL.getKey().toString());
		String username = config.getValue(ConfigKeys.KEY_DBUSERNAME.getKey().toString());
		String password = config.getValue(ConfigKeys.KEY_DBUSERPASSWORD.getKey().toString());

		try {
			connection = DriverManager.getConnection("jdbc:oracle:thin:@" + url, username, password);
		} catch (SQLException e) {
			System.err.println("SQL Connection error : " + e.getMessage());
			throw e;
		}

	}
	
	/**
	 * Open two db connection.
	 *
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	private static void openDBConnection2() throws SQLException, ClassNotFoundException {

		IConfig config = DefaultConfig.getConfig();

		String driverName = "oracle.jdbc.driver.OracleDriver";
		try {

			Class.forName(driverName);

		} catch (ClassNotFoundException e) {
			System.err.println("Where is your Oracle JDBC Driver? "  + e.getMessage());
			throw e;
		}

		String url2 = config.getValue(ConfigKeys.KEY_DBURL2.getKey().toString());
		String username2 = config.getValue(ConfigKeys.KEY_DBUSERNAME2.getKey().toString());
		String password2 = config.getValue(ConfigKeys.KEY_DBUSERPASSWORD2.getKey().toString());


		try {
			connection2 = DriverManager.getConnection("jdbc:oracle:thin:@" + url2, username2, password2);
		} catch (SQLException e) {
			System.err.println("SQL Connection error : " + e.getMessage());
			throw e;
		}

	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		if (connection != null && !connection.isClosed()) {
			return connection;
		}
		openDBConnection();
		return connection;

	}
	
	/**
	 * Gets the second connection.
	 *
	 * @return the connection
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static Connection getConnection2() throws SQLException, ClassNotFoundException {
		if (connection2 != null && !connection2.isClosed()) {
			return connection2;
		}
		openDBConnection2();
		return connection2;

	}

}
