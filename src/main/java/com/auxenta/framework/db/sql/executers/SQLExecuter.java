/*
 *================================================================
 * Copyright  (c)    : 2015 Dialog Axiata PLC, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.db.sql.executers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import com.auxenta.framework.config.ConfigKeys;
import com.auxenta.framework.config.DefaultConfig;
import com.auxenta.framework.config.IConfig;
import com.auxenta.framework.db.connection.OracleConnection;
import com.auxenta.framework.db.sql.QueryResult;

/**
 * The Class SQLExecuter.
 */
public class SQLExecuter {

	/** The date format. */
	private static String dateFormat = "yyyy-MM-dd h:mm:ss";
	/** The sdf. */
	private static SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);



	/**
	 * Gets the query results.
	 *
	 * @param Query
	 *            the query
	 * @param newDateFormat
	 *            the new date format
	 * @return the query results
	 * @throws Exception
	 *             the exception
	 */
	public static QueryResult getQueryResults(String Query, String newDateFormat) throws Exception {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(newDateFormat);
		return executeQuery(Query, dateFormatter);
	}
	
	/**
	 * Gets the query results from second connection
	 *
	 * @param Query
	 *            the query
	 * @param newDateFormat
	 *            the new date format
	 * @return the query results
	 * @throws Exception
	 *             the exception
	 * @author Disna_Auxenta            
	 */
	public static QueryResult getQueryResults(String Query, String newDateFormat, int dbInstance) throws Exception {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(newDateFormat);
		return executeQuery(Query, dateFormatter,dbInstance);
	}

	/**
	 * gets the query results.
	 * 
	 * @param Query
	 *            the query
	 * @return the query result
	 * @throws Exception
	 *             the exception
	 */
	public static QueryResult getQueryResults(String Query) throws Exception {
		return executeQuery(Query, sdf);

	}
	
	/**
	 * gets the query results from second connection
	 * 
	 * @param Query
	 *            the query
	 * @return the query result
	 * @throws Exception
	 *             the exception
	 * @author Disna_Auxenta
	 */
	public static QueryResult getQueryResults(String Query, int dbInstance) throws Exception {
		return executeQuery(Query, sdf, dbInstance);

	}

	/**
	 * Execute query.
	 *
	 * @param Query
	 *            the query
	 * @param dateFormatter
	 *            the date formatter
	 * @return the query result
	 * @throws Exception
	 *             the exception
	 */
	
	

	private static QueryResult executeQuery(String Query, DateFormat dateFormatter) throws Exception {
		QueryResult queryResult = new QueryResult();
		try {
			Connection connection = OracleConnection.getConnection();
			Statement statement = null;
			try {
				statement = connection.createStatement();
				ResultSet resultSet = null;
				try {
					resultSet = statement.executeQuery(Query);

					while (resultSet.next()) {

						ResultSetMetaData rsmd = resultSet.getMetaData();
						int columnCount = rsmd.getColumnCount();
						Map<String, String> row = new HashMap<>();
						// The column count starts from 1
						for (int i = 1; i < columnCount + 1; i++) {
							String name = rsmd.getColumnName(i);
							// Object value;
							Object value = resultSet.getObject(name);

							if (value == null) {
								value = "";
							}

							if (value instanceof Date) {
								
								java.util.Date date = resultSet.getTimestamp(name);
										
								value = dateFormatter.format(date);
							}

							row.put(name, value.toString().trim().replace("  ", " "));

						}
						queryResult.enqueRow(row);
					}

				} finally {
					resultSet.close();
				}

			} finally {
				statement.close();
			}
		} catch (Exception e) {
			System.err.println("SQL Connection error : " + e.getMessage());
			throw e;
		}

		return queryResult;
	}
	
	/**
	 * Execute query from second connection
	 *
	 * @param Query
	 *            the query
	 * @param dateFormatter
	 *            the date formatter
	 * @return the query result
	 * @throws Exception
	 *             the exception
	 * @author Disna_Auxenta
	 */
	private static QueryResult executeQuery(String Query, DateFormat dateFormatter, int dbInstance) throws Exception {
		QueryResult queryResult = new QueryResult();
		try {
			Connection connection = OracleConnection.getConnection2();
			Statement statement = null;
			try {
				statement = connection.createStatement();
				ResultSet resultSet = null;
				try {
					resultSet = statement.executeQuery(Query);

					while (resultSet.next()) {

						ResultSetMetaData rsmd = resultSet.getMetaData();
						int columnCount = rsmd.getColumnCount();
						Map<String, String> row = new HashMap<>();
						// The column count starts from 1
						for (int i = 1; i < columnCount + 1; i++) {
							String name = rsmd.getColumnName(i);
							// Object value;
							Object value = resultSet.getObject(name);

							if (value == null) {
								value = "";
							}

							if (value instanceof Date) {
								
								java.util.Date date = resultSet.getTimestamp(name);
										
								value = dateFormatter.format(date);
							}

							row.put(name, value.toString().trim().replace("  ", " "));

						}
						queryResult.enqueRow(row);
					}

				} finally {
					resultSet.close();
				}

			} finally {
				statement.close();
			}
		} catch (Exception e) {
			System.err.println("SQL Connection error : " + e.getMessage());
			throw e;
		}

		return queryResult;
	}

	/**
	 * Execute update query.
	 *
	 * @param Query the query
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer executeUpdateQuery(String Query) throws Exception {
		Integer result = 0;
		try {
			Connection connection = OracleConnection.getConnection();
			Statement statement = null;
			try {
				statement = connection.createStatement();
				result = statement.executeUpdate(Query);

			} finally {
				statement.close();
			}
		} catch (Exception e) {
			System.err.println("SQL Connection error : " + e.getMessage());
			throw e;
		}

		return result;
	}
	
	/**
	 * Execute update query.
	 *
	 * @param Query the query for second connection
	 * @return the integer
	 * @throws Exception the exception
	 * @author Disna_Auxenta
	 */
	public static Integer executeUpdateQuery(String Query,int dbInstance) throws Exception {
		Integer result = 0;
		try {
			Connection connection = OracleConnection.getConnection2();
			Statement statement = null;
			try {
				statement = connection.createStatement();
				result = statement.executeUpdate(Query);

			} finally {
				statement.close();
			}
		} catch (Exception e) {
			System.err.println("SQL Connection error : " + e.getMessage());
			throw e;
		}

		return result;
	}

}
