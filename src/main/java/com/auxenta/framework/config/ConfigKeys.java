/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.config;

/**
 * An interface for holding a reserved config keys.
 *
 * @author Niroshen Landerz
 * @date Sept 1st 2014
 */

public enum ConfigKeys {
    
    /** The key driver url. */
    KEY_DRIVER_URL("driverurl"),
    
    /** The key browser. */
    KEY_BROWSER("browser"),
    
    /** The key environment. */
    KEY_ENVIRONMENT("environment"),
    
    /** The key start page url. */
    KEY_START_PAGE_URL("startpageurl"),
    
    /** The key template path. */
    KEY_TEMPLATE_PATH("templatepath"),
    
    /** The key data workbook path. */
    KEY_DATA_WORKBOOK_PATH("dataworkbookpath"),
    
    /** The key screenshot path. */
    KEY_SCREENSHOT_PATH("screenshotdirectory"),
    
    /** The key start page class. */
    KEY_START_PAGE_CLASS("startpageclass"),
    
    /** The key chrome driver. */
    KEY_CHROME_DRIVER("webdriver.chrome.driver"),
    
    /** The key chrome driver path. */
    KEY_CHROME_DRIVER_PATH("chrome.driver.path"),
    
    /** The key ie driver. */
    KEY_IE_DRIVER("webdriver.ie.driver"),
    
    /** The key ie driver path. */
    KEY_IE_DRIVER_PATH("ie.driver.path"),

    /** The key wait timeout. */
    KEY_WAIT_TIMEOUT("waittimeout"),
    
    /** The key verify timeout. */
    KEY_VERIFY_TIMEOUT("verifytimeout"),
    
    /** The key verify interval. */
    KEY_VERIFY_INTERVAL("verifyinterval"),
    
    /** The key element timeout. */
    KEY_ELEMENT_TIMEOUT("elementtimeout"),
    
    /** The key elements timeout. */
    KEY_ELEMENTS_TIMEOUT("elementstimeout"),
    
    /** The key pageload timeout. */
    KEY_PAGELOAD_TIMEOUT("pageloadtimeout"),

    /** The key max failed test retry. */
    KEY_MAX_FAILED_TEST_RETRY("maxfailedtestretry"),
    
    /** The key browser version. */
    KEY_BROWSER_VERSION("browser_version"),
    
    /** The key force driver download. */
    KEY_FORCE_DRIVER_DOWNLOAD("force_download"),
    
    /** The key skip driver download. */
    KEY_SKIP_DRIVER_DOWNLOAD("skip_download"),
    
    /** Oracle DB Connection User Name  */
    KEY_DBUSERNAME("dbUser"),
    
    /** Oracle DB Connection User Password  */
    KEY_DBUSERPASSWORD("dbPassword"),
    
    /** Oracle DB Connection URL */
    KEY_DBURL("dbURL"),
        
    /** Default User  */
    KEY_DEFAULT_APPLICATION_USER("defaultUser"),
    
    /** Default Password  */
    KEY_DEFAULT_APPLICATION_PASSWORD("defaultPassword"),
    
    /** Oriantation of the device, Vertical or Horizontal */
    KEY_ORIENTATION("oriantation"),
    
    /** Operating System of the Device */
    KEY_OS("operatingSystem"),
    
    /** Page that will get you back to the home */
    KEY_HOME_PAGE_URL("homePageUrl"),
    
    /** The server url. */
    SERVER_URL("SERVER_URL"),
    
    /** The key data workbook path. */
    PROJECT_BASE_DIR("projects.base.dir"),
    
    /** Oracle DB Connection User Name  */
    KEY_DBUSERNAME2("dbUser1"),
    
    /** Oracle DB Connection User Password  */
    KEY_DBUSERPASSWORD2("dbPassword1"),
    
    /** Oracle DB Connection URL */
    KEY_DBURL2("dbURL1");
    
    

    
    /** The key. */
    String key;
    

    /**
     * Instantiates a new config keys.
     *
     * @param key the key
     */
    private ConfigKeys(String key) {
        this.key = key;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }
    

    

}