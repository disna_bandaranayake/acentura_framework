/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.config;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.auxenta.framework.exceptions.FrameworkException;

/**
 * This class holds predefined {@link FirefoxProfile}s.
 *
 * @author Niroshen Landerz
 * @date Aug 22nd 2014
 */

public class FirefoxProfiles {

	/** The logger. */
	private static Logger logger = Logger.getLogger(FirefoxProfiles.class);

	/**
	 * Gets the no prompt on download profile.
	 *
	 * @return the no prompt on download profile
	 */
	public static FirefoxProfile getNoPromptOnDownloadProfile() {
		FirefoxProfile prof = new FirefoxProfile();
		prof.setAssumeUntrustedCertificateIssuer(true);
		prof.setPreference("Browser.link.open_newwindow.restriction", 1);
		prof.setPreference("privacy.popups.policy", 0);
		prof.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/octstream,application/pdf,text/csv,text/doc,application/vnd.ms-excel,application/msword");
		prof.setPreference("browser.helperApps.alwaysAsk.force", false);
		prof.setPreference("browser.download.manager.showWhenStarting", false);
		return prof;
	}

	/**
	 * Gets the no prompt on download profile.
	 *
	 * @param downloadDir
	 *            value for {@code browser.download.dir} preference
	 * @return the no prompt on download profile
	 */
	public static FirefoxProfile getNoPromptOnDownloadProfile(final String downloadDir) {
		FirefoxProfile prof = new FirefoxProfile();
		prof.setAssumeUntrustedCertificateIssuer(true);
		prof.setPreference("Browser.link.open_newwindow.restriction", 1);
		prof.setPreference("privacy.popups.policy", 0);
		prof.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"application/octstream,application/pdf,text/csv,text/doc,application/vnd.ms-excel,application/msword");
		prof.setPreference("browser.helperApps.alwaysAsk.force", false);
		prof.setPreference("browser.download.manager.showWhenStarting", false);
		prof.setPreference("browser.download.dir", downloadDir);
		prof.setPreference("browser.download.folderList", 2);
		prof.setPreference("browser.download.useDownloadDir", true);
		return prof;
	}

	/**
	 * In order to start firefox with firebug, you need to download
	 * firebug-x.x.x.xpi from <a
	 * href="http://getfirebug.com/releases/firebug">http
	 * ://getfirebug.com/releases/firebug</a><br>
	 * Make sure you download firebug version that compatible with your firefox
	 * version
	 *
	 * @param fireBugPath
	 *            - path to firebug-version.xpi
	 * @param firebugVersion
	 *            - version
	 * @return {@link FirefoxProfiles} with firebug plugin enabled
	 */
	public static FirefoxProfile getFirebugProfile(final String fireBugPath, final String firebugVersion) {
		File file = new File(fireBugPath);
		FirefoxProfile fireBug = new FirefoxProfile();
		try {
			fireBug.addExtension(file);
		} catch (IOException e) {
			logger.error("Error", e);
			throw new FrameworkException("Unable to find " + fireBugPath, e);
		}
		fireBug.setPreference("extensions.firebug.currentVersion", firebugVersion);
		return fireBug;
	}
}
