/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.config;

/**
 * An interface for config, which provides functions for accessing config values.
 *
 * @author Niroshen Landerz
 * @date Sept 1st 2014
 */

public interface IConfig {

    /**
     * Get value from config.cfg file
     *
     * @param key the key
     * @return the value
     */
    public String getValue(String key);

    /**
     * Gets the long value.
     *
     * @param key the key
     * @param defaultValue the default value
     * @return the long value
     */
    public Long getLongValue(String key, long defaultValue);

    /**
     * Set value in config.cfg
     *
     * @param key the key
     * @param value the value
     */
    void setValue(String key, String value);

}
