/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.ConfigFileException;
import com.auxenta.framework.util.Default;
import com.auxenta.framework.util.Timeout;

/**
 * A class for reading config file from resource.
 *
 * @author Niroshen Landerz
 * @date Aug 31 2014
 */
public class ConfigReader {

	/** The logger. */
	private static Logger logger = Logger.getLogger(ConfigReader.class);

	/** The config file. */
	private static Properties configFile;

	/** The path. */
	private String path = Default.CONFIG_PATH.getValue();

	/** The var depth. */
	private int varDepth = Default.CONFIG_VAR_DEPTH;

	/** The Constant FIND_VARS_PATTERN. */
	private static final Pattern FIND_VARS_PATTERN = Pattern.compile("\\%([a-zA-Z0-9_]+)\\%");

	/**
	 * Instantiates a new config reader.
	 */
	ConfigReader() {
	}

	/**
	 * Instantiates a new config reader.
	 *
	 * @param path
	 *            the path
	 */
	public ConfigReader(final String path) {
		this.path = path;
	}

	/**
	 * Read config.
	 *
	 * @return the properties
	 */
	public Properties readConfig() {
		InputStream istream = getClass().getResourceAsStream(path);
		if (null == istream) {
			try {
				istream = new FileInputStream(path);
			} catch (FileNotFoundException e) {
				logger.error("FileNotFoundException", e);
				throw new TestAutomationException(path, e);
			}
		}
		configFile = new java.util.Properties(getDefaultProperties());
		try {
			configFile.load(istream);
			istream.close();
		} catch (Exception ex) {
			logger.error("Error", ex);
			throw new ConfigFileException(path);
		}
		resolveCommandLineArgs();
		resolveVariables();
		return configFile;
	}

	/**
	 * Gets the default properties.
	 *
	 * @return the default properties
	 */
	private Properties getDefaultProperties() {
		Properties def = new Properties();
		def.setProperty(ConfigKeys.KEY_DRIVER_URL.getKey(), String.valueOf(Default.DRIVER_URL.getValue()));
		def.setProperty(ConfigKeys.KEY_OS.getKey(), String.valueOf(Default.OS));
		def.setProperty(ConfigKeys.KEY_BROWSER.getKey(), String.valueOf(Default.DRIVER_BROWSER));
		def.setProperty(ConfigKeys.KEY_START_PAGE_URL.getKey(), String.valueOf(Default.START_PAGE.getValue()));
		// Timeout
		def.setProperty(ConfigKeys.KEY_WAIT_TIMEOUT.getKey(), String.valueOf(Timeout.WAIT_TIMEOUT.getValue()));
		def.setProperty(ConfigKeys.KEY_VERIFY_TIMEOUT.getKey(),
				String.valueOf(Timeout.VERIFY_TIMEOUT.getValue()));
		def.setProperty(ConfigKeys.KEY_VERIFY_INTERVAL.getKey(),
				String.valueOf(Timeout.VERIFY_INTERVAL.getValue()));
		def.setProperty(ConfigKeys.KEY_ELEMENT_TIMEOUT.getKey(),
				String.valueOf(Timeout.ELEMENT_TIMEOUT.getValue()));
		def.setProperty(ConfigKeys.KEY_ELEMENTS_TIMEOUT.getKey(),
				String.valueOf(Timeout.ELEMENTS_TIMEOUT.getValue()));
		def.setProperty(ConfigKeys.KEY_PAGELOAD_TIMEOUT.getKey(),
				String.valueOf(Timeout.PAGELOAD_TIMEOUT.getValue()));
		return def;
	}

	/**
	 * replace any settings with command line arguments.
	 */
	private void resolveCommandLineArgs() {
		Iterator<Object> it = configFile.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next().toString();
			String value = "";
			try {
				value = System.getProperty(key);
				value = (value != null) ? value : "";
			} catch (Exception e) {
				value = "";
			}
			if (!value.equals("")) {
				configFile.setProperty(key, value);
			}
		}
	}

	/**
	 * Recursively substitute variables.
	 */
	private void resolveVariables() {
		boolean hasVariables = false;
		int depth = 0;
		do {
			hasVariables = false;
			for (Entry<Object, Object> entry : configFile.entrySet()) {
				String value = entry.getValue().toString();
				if (value.contains("%")) {
					hasVariables = true;
					String newValue = resolve(FIND_VARS_PATTERN, value);
					configFile.put(entry.getKey(), newValue);
				}
			}
		} while (hasVariables && depth++ < varDepth);
	}

	/**
	 * Resolve.
	 *
	 * @param pattern
	 *            the pattern
	 * @param text
	 *            the text
	 * @return the string
	 */
	private String resolve(final Pattern pattern, final String text) {
		Matcher m = pattern.matcher(text);
		Map<String, String> replacements = new HashMap<String, String>();
		while (m.find()) {
			int gc = m.groupCount();
			if (gc == 1) {
				String g0 = m.group(0);
				String g1 = m.group(1);
				String value = null;
				Object o = configFile.getProperty(g1);
				if (null != o) {
					value = o.toString();
				}
				replacements.put(g0, value);
			}
		}
		String newText = text;
		for (Entry<String, String> en : replacements.entrySet()) {
			String k = en.getKey();
			String replacement = replacements.get(k);
			if (replacement != null)
				newText = newText.replace(k, replacement);
		}
		return newText;
	}
}
