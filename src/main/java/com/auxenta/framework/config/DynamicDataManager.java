/*
 *================================================================
 * Copyright  (c)    : 2015 Dialog Axiata PLC, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * The Class DynamicDataManager.
 */
public class DynamicDataManager {

	/** The Constant CONFIG_RUNTIME_DATA_PROPERTIES. */
	private static final String CONFIG_RUNTIME_DATA_PROPERTIES = "config/runtimeData.properties";
	
	/** The config prop. */
	private static Properties configProp = null;

	/**
	 * Cash properties.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void cashProperties() throws IOException {

		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream(CONFIG_RUNTIME_DATA_PROPERTIES);

		try {
			configProp = new Properties();
			configProp.load(in);
		} finally {
			in.close();
		}
	}

	/**
	 * Gets the property.
	 *
	 * @param key the key
	 * @return the property
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String getProperty(String key) throws IOException {
		if (configProp == null) {
			new DynamicDataManager().cashProperties();
		}
		return configProp.getProperty(key);
	}

	/**
	 * Sets the property.
	 *
	 * @param key the key
	 * @param value the value
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws URISyntaxException the URI syntax exception
	 */
	public static void setProperty(String key, String value) throws IOException, URISyntaxException {
		configProp.setProperty(key, value);

		try {
			
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			URL url = classLoader.getResource(CONFIG_RUNTIME_DATA_PROPERTIES);
			File file = new File(url.toURI().getPath());
			OutputStream output =  new FileOutputStream(file);
			configProp.store(output, null);
		} finally {
			
			new DynamicDataManager().cashProperties();
		}
	}

}
