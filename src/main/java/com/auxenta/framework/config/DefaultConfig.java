/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.config;

import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.auxenta.framework.exception.TestAutomationException;
import com.auxenta.framework.exceptions.ConfigFileException;

/**
 * A generic Config realized by Properties.
 *
 * @author Niroshen Landerz
 * @date Aug 28th 2014
 */

public class DefaultConfig implements IConfig {

	/** The logger. */
	private static Logger logger = Logger.getLogger(DefaultConfig.class);

	/** The config file. */
	private Properties configFile;

	/** The config. */
	private static IConfig config = null;

	/**
	 * Instantiates a new default config.
	 */
	public DefaultConfig() {
		ConfigReader reader = new ConfigReader();
		try {
			configFile = reader.readConfig();
		} catch (ConfigFileException ex) {
			logger.error("ConfigFileException", ex);
			throw new TestAutomationException("ConfigFileException ", ex);

		}
	}

	/**
	 * Instantiates a new default config.
	 *
	 * @param path
	 *            to config file
	 */
	public DefaultConfig(final String path) {
		ConfigReader reader = new ConfigReader(path);
		try {
			configFile = reader.readConfig();
		} catch (ConfigFileException ex) {
			logger.error(ex);
			throw new TestAutomationException("ConfigFileException ", ex);
		}

	}

	/**
	 * Instantiates a new default config.
	 *
	 * @param properties
	 *            the properties
	 */
	public DefaultConfig(final Properties properties) {
		configFile = properties;
	}

	/**
	 * Gets the config.
	 *
	 * @return the config
	 */
	public static IConfig getConfig() {
		if (config == null) {
			config = new DefaultConfig();
		}
		return config;
	}

	/**
	 * Gets the config.
	 *
	 * @param path
	 *            the path
	 * @return the config
	 */
	public static IConfig getConfig(final String path) {
		if (config == null) {
			config = new DefaultConfig(path);
		}
		return config;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.config.IConfig#getValue(java.lang.
	 * String)
	 */
	@Override
	public String getValue(String key) {
		String value = configFile.getProperty(key);
		return (value == null) ? "" : value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.config.IConfig#setValue(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public void setValue(String key, String value) {
		configFile.setProperty(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.test.auxtest.framework.config.IConfig#getLongValue(java.lang
	 * .String, long)
	 */
	@Override
	public Long getLongValue(String key, long defaultValue) {
		String longStr = getValue(key).trim();
		logger.trace("Getting config value for " + key);
		if (longStr == null || "".equals(longStr)) {
			logger.trace(" config key " + key + " was not set. Returning default value=" + defaultValue);
			return defaultValue;
		} else {
			logger.trace(" config key " + key + " found");
			long foundValue = Long.parseLong(longStr);
			logger.trace("Config " + key + "=" + foundValue);
			return foundValue;
		}
	}

	/**
	 * Prints the all.
	 */
	public void printAll() {
		for (Entry<Object, Object> entry : configFile.entrySet()) {
			logger.debug(entry.getKey() + "=" + entry.getValue());
		}
	}

}
