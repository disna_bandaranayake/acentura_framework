/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.auxenta.framework.exception;

/**
 * The Class LocalCoinsException.
 */
public class TestAutomationException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8362298626912267189L;

	/** The exception msg. */
	private String exceptionMsg;

	/**
	 * Instantiates a new local coins exception.
	 * 
	 * @param exceptionMsgVal
	 *            the exception msg val
	 * @param e
	 *            the e
	 */
	public TestAutomationException(final String exceptionMsgVal, final Throwable e) {
		super(exceptionMsgVal, e);
		this.exceptionMsg = exceptionMsgVal;
	}

	/**
	 * Instantiates a new local coins exception.
	 * 
	 * @param exceptionMsgVal
	 *            the exception msg val
	 */
	public TestAutomationException(final String exceptionMsgVal) {
		this.exceptionMsg = exceptionMsgVal;
	}

	/**
	 * Gets the exception msg.
	 *
	 * @return the exception msg
	 */
	public String getExceptionMsg() {
		return this.exceptionMsg;
	}

	/**
	 * Sets the exception msg.
	 *
	 * @param exceptionMsgVal the new exception msg
	 */
	public void setExceptionMsg(final String exceptionMsgVal) {
		this.exceptionMsg = exceptionMsgVal;
	}
}
